<?php

class Meta extends MetaCore
{
    public $page;
    public $configurable = 1;
    public $title;
    public $longtitle;
    public $description;
    public $longdescription;
    public $keywords;
    public $url_rewrite;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'meta',
        'primary' => 'id_meta',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            'page' =>            array('type' => self::TYPE_STRING, 'validate' => 'isFileName', 'required' => true, 'size' => 64),
            'configurable' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),

            /* Lang fields */
            'title' =>            array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
            'longtitle' =>            array('type' => self::TYPE_HTML, 'lang' => true, 'size' => 255),
            'description' =>    array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'longdescription' =>    array('type' => self::TYPE_HTML, 'lang' => true, 'size' => 3999999999999),
           'keywords' =>        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'url_rewrite' =>    array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isLinkRewrite', 'size' => 255),
        ),
    );
}