<?php

class State extends StateCore
{
    public static function getIsoById($id_state)
    {
        if ((int)$id_state == 0) return;        
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT `iso_code`
			FROM `'._DB_PREFIX_.'state`
			WHERE `id_state` = '.(int)$id_state);
    }
    
    public static function getIsoByName($name = null)
    {
        if ($name == null) return;        
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT `iso_code`
			FROM `'._DB_PREFIX_.'state`
			WHERE `name` = \'' .$name. '\'');
    }
}