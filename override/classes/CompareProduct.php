<?php 

class CompareProduct extends CompareProductCore
{
    public static function cleanCompareProducts($period = 'week')
    {
        if ($period === 'week') {
            $interval = '1 WEEK';
        } elseif ($period === 'month') {
            $interval = '1 MONTH';
        } elseif ($period === 'year') {
            $interval = '1 YEAR';
        } else {
            return;
        }

        if ($interval != null) {
            Db::getInstance()->execute('
			DELETE cp, c FROM `'._DB_PREFIX_.'compare_product` cp, `'._DB_PREFIX_.'compare` c
			WHERE cp.date_upd < DATE_SUB(NOW(), INTERVAL '.$interval.') AND c.`id_compare`=cp.`id_compare`');
        }
    }
    
    public static function removeAllCompareProducts($id_compare)
    {
        Db::getInstance()->execute('
		DELETE cp, c FROM `'._DB_PREFIX_.'compare_product` cp, `'._DB_PREFIX_.'compare` c
		WHERE c.`id_compare`=cp.`id_compare` AND c.`id_compare` = '.(int)$id_compare);
    }
}