<?php

abstract class ObjectModel extends ObjectModelCore
{
   public function toggleFeatured()
    {
        // Object must have a variable called 'active'
        if (!array_key_exists('featured', $this)) {
            throw new PrestaShopException('property "featured" is missing in object '.get_class($this));
        }

        // Update only active field
        $this->setFieldsToUpdate(array('featured' => true));

        // Update active status on object
        $this->featured = !(int)$this->featured;

        // Change status to active/inactive
        return $this->update(false);
     }
}