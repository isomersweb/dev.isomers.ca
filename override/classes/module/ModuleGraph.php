<?php

abstract class ModuleGraph extends ModuleGraphCore
{

    protected function csvExport($datas)
    {
        $context = Context::getContext();

        $this->setEmployee($context->employee->id);
        $this->setLang($context->language->id);

        $layers = isset($datas['layers']) ?  $datas['layers'] : 1;
        if (isset($datas['option'])) {
            $this->setOption($datas['option'], $layers);
        }
        $this->getData($layers);

        // @todo use native CSV PHP functions ?
        // Generate first line (column titles)
        if (is_array($this->_titles['main'])) {
            for ($i = 0, $total_main = count($this->_titles['main']); $i <= $total_main; $i++) {
                if ($i > 0) {
                    $this->_csv .= ';';
                }
                if (isset($this->_titles['main'][$i])) {
                    $this->_csv .= $this->_titles['main'][$i];
                }
            }
        } else { // If there is only one column title, there is in fast two column (the first without title)
            $this->_csv .= ';'.$this->_titles['main'];
        }
        $this->_csv .= "\n";
        if (count($this->_legend)) {
            $total = 0;
            if ($datas['type'] == 'pie') {
                foreach ($this->_legend as $key => $legend) {
                    for ($i = 0, $total_main = (is_array($this->_titles['main']) ? count($this->_values) : 1); $i < $total_main; ++$i) {
                        $total += (is_array($this->_values[$i])  ? $this->_values[$i][$key] : $this->_values[$key]);
                    }
                }
            }
            if (Tools::getValue('exportType') == 5) {
                $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->_query);
                foreach ($res as $val) {
                    foreach ($val as $val1) {
                        $this->_csv .= $val1;
                        $this->_csv .= ';';
                    }
                }
            }
            else {
                foreach ($this->_legend as $key => $legend) {
                    echo $legend;
                    $this->_csv .= $legend.';';
                    for ($i = 0, $total_main = (is_array($this->_titles['main']) ? count($this->_values) : 1); $i < $total_main; ++$i) {
                        if (!isset($this->_values[$i]) || !is_array($this->_values[$i])) {
                            if (isset($this->_values[$key])) {
                                // We don't want strings to be divided. Example: product name
                                if (is_numeric($this->_values[$key])) {
                                    $this->_csv .= $this->_values[$key] / (($datas['type'] == 'pie') ? $total : 1);
                                } else {
                                    $this->_csv .= $this->_values[$key];
                                }
                            } else {
                                $this->_csv .= '0';
                            }
                        } else {
                            // We don't want strings to be divided. Example: product name
                            if (is_numeric($this->_values[$i][$key])) {
                                $this->_csv .= $this->_values[$i][$key] / (($datas['type'] == 'pie') ? $total : 1);
                            } else {
                                $this->_csv .= $this->_values[$i][$key];
                            }
                        }
                        $this->_csv .= ';';
                    }
                    $this->_csv .= "\n";
                }
            }
        } 
        $this->_displayCsv();
    }


}
