<?php

class PricesDropController extends PricesDropControllerCore {

    public function initContent()
    {
        parent::initContent();

        $this->productSort();
        $nbProducts = Product::getPricesDrop($this->context->language->id, null, null, true);
        $this->pagination($nbProducts);

        $products = Product::getPricesDrop($this->context->language->id, (int)$this->p - 1, (int)$this->n, false, $this->orderBy, $this->orderWay);
        $this->addColorsToProductList($products);

        $current_date = date('Y-m-d H:i:s');
        $sql = 'SELECT crl.name, cr.description 
					FROM `'._DB_PREFIX_.'cart_rule` cr
					LEFT JOIN `'._DB_PREFIX_.'cart_rule_lang` crl ON (crl.`id_cart_rule` = cr.`id_cart_rule`)
					WHERE cr.date_from < \''.$current_date . '\'
                    AND cr.date_to > \''.$current_date .'\'
                    AND crl.id_lang='.Context::getContext()->language->id .'
                    AND id_customer =0';
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        
        $this->context->smarty->assign(array(
            'vouchers' => $result
        ));
        
        $this->context->smarty->assign(array(
            'products' => $products,
            'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            'nbProducts' => $nbProducts,
            'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
            'comparator_max_item' => Configuration::get('PS_COMPARATOR_MAX_ITEM')
        ));

        $this->setTemplate(_PS_THEME_DIR_.'prices-drop.tpl');
    }    
}
