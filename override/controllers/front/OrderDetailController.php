<?php
class OrderDetailController extends OrderDetailControllerCore
{
    /**
     * OrderDetailController::getBelvgDiscountsTplDir()
     * 
     * @return string
     */ 
    public function initContent()
    {
        if (!($id_order = (int)Tools::getValue('id_order')) || !Validate::isUnsignedId($id_order)) {
            $this->errors[] = Tools::displayError('Order ID required');
        } else {
            $order = new Order($id_order);            
            if ($refunds = OrderSlip::getOrdersSlip((int)$order->id_customer, $id_order)) {
                $refundsDetails = array();
                foreach ($refunds as $refund) {
                    $OrdersSlipDetail = OrderSlip::getOrdersSlipDetail2($refund['id_order_slip']);
                    foreach ($OrdersSlipDetail as $row) {
                        $refundsDetails[] = $row;
                    }
                }
//                d($refundsDetails);
            }         
            $query = 'SELECT o.id_order, SUM(pp.amount) AS paypal_amount, SUM(m.amount) AS moneris_amount
			FROM '._DB_PREFIX_.'orders o
			LEFT JOIN '._DB_PREFIX_.'paypal_usa_transaction pp ON o.id_order = pp.id_order 
			LEFT JOIN '._DB_PREFIX_.'moneris_transaction m ON o.id_order = m.id_order 
            WHERE o.id_order=\''.$id_order . '\' AND (pp.type = \'refund\' OR m.type = \'refund\')';            
            $refunds_payments = Db::getInstance()->getRow($query);

            $tpl_vars['refunds'] = $refunds;
            $tpl_vars['refunds_details'] = $refundsDetails;
            $tpl_vars['refunds_payments'] = $refunds_payments;
            $smarty = $this->context->smarty;
            $smarty->assign($tpl_vars);
        }
        return parent::initContent();    
    }        
}
