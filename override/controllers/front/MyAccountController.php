<?php

class MyAccountController extends MyAccountControllerCore 
{
    public $auth = true;
    public $php_self = 'my-account';
    public $authRedirection = 'my-account';
    public $ssl = true;

    protected $customer;

    public function setMedia()
    {
        parent::setMedia();
        //$this->addCSS(_THEME_CSS_DIR_.'my-account.css');
        $this->addJS(array(
            _THEME_JS_DIR_.'history.js',
            _THEME_JS_DIR_.'tools.js' // retro compat themes 1.5
        ));
        $this->addJqueryPlugin(array('scrollTo', 'footable', 'footable-sort'));
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        $this->customer = $this->context->customer;
                
        $has_address = $this->context->customer->getAddresses($this->context->language->id);
        $this->context->smarty->assign(array(
            'has_customer_an_address' => empty($has_address),
            'voucherAllowed' => (int)CartRule::isFeatureActive(),
            'returnAllowed' => (int)Configuration::get('PS_ORDER_RETURN')
        ));
        $this->context->smarty->assign('HOOK_CUSTOMER_ACCOUNT', Hook::exec('displayCustomerAccount'));
        
        /****************** USER INFO **********************/
        if ($this->customer->birthday) {
            $birthday = explode('-', $this->customer->birthday);
        } else {
            $birthday = array('-', '-', '-');
        }

        /* Generate years, months and days */
        $this->context->smarty->assign(array(
                'years' => Tools::dateYears(),
                'sl_year' => $birthday[0],
                'months' => Tools::dateMonths(),
                'sl_month' => $birthday[1],
                'days' => Tools::dateDays(),
                'sl_day' => $birthday[2],
                'errors' => $this->errors,
                'birthday' => $this->customer->birthday,
                'firstname' => $this->customer->firstname,
                'lastname' => $this->customer->lastname,
                'email' => $this->customer->email,
                'emailpass' => Tools::encrypt($this->customer->email) == $this->customer->passwd ? true: false,
                'id_gender' => $this->customer->id_gender,
                'genders' => Gender::getGenders(),
            ));

        // Call a hook to display more information
        $this->context->smarty->assign(array(
            'HOOK_CUSTOMER_IDENTITY_FORM' => Hook::exec('displayCustomerIdentityForm'),
        ));

        $newsletter = Configuration::get('PS_CUSTOMER_NWSL') || (Module::isInstalled('blocknewsletter') && Module::getInstanceByName('blocknewsletter')->active);
        $this->context->smarty->assign('newsletter', $newsletter);
        $this->context->smarty->assign('optin', (bool)Configuration::get('PS_CUSTOMER_OPTIN'));

        $this->context->smarty->assign('field_required', $this->context->customer->validateFieldsRequiredDatabase());        
        
        /****************** ADDRESSES **********************/        
        $total = 0;
        $multiple_addresses_formated = array();
        $ordered_fields = array();
        $addresses = $this->context->customer->getAddresses($this->context->language->id);
        // @todo getAddresses() should send back objects
        foreach ($addresses as $detail) {
            $address = new Address($detail['id_address']);
            $multiple_addresses_formated[$total] = AddressFormat::getFormattedLayoutData($address);
            unset($address);
            ++$total;

            // Retro theme < 1.4.2
            $ordered_fields = AddressFormat::getOrderedAddressFields($detail['id_country'], false, true);
        }

        // Retro theme 1.4.2
        if ($key = array_search('Country:name', $ordered_fields)) {
            $ordered_fields[$key] = 'country';
        }

        $addresses_style = array(
            'company' => 'address_company',
            'vat_number' => 'address_company',
            'firstname' => 'address_name',
            'lastname' => 'address_name',
            'address1' => 'address_address1',
            'address2' => 'address_address2',
            'city' => 'address_city',
            'country' => 'address_country',
            'phone' => 'address_phone',
            'phone_mobile' => 'address_phone_mobile',
            'alias' => 'address_title',
        );

        $this->context->smarty->assign(array(
            'addresses_style' => $addresses_style,
            'multipleAddresses' => $multiple_addresses_formated,
            'ordered_fields' => $ordered_fields,
            'addresses' => $addresses, // retro compat themes 1.5ibility Theme < 1.4.1
        ));     
        
        /****************** HISTORY **********************/ 
        
        if ($orders = Order::getCustomerOrders($this->context->customer->id)) {
            foreach ($orders as &$order) {
                $myOrder = new Order((int)$order['id_order']);
                if (Validate::isLoadedObject($myOrder)) {
                    $order['virtual'] = $myOrder->isVirtual(false);
                }
            }
        }
        $sponsorDelete = 0;
        
        if ($id_sponsorship = RewardsSponsorshipModel::isSponsorised((int)$this->context->customer->id, true)) {
			$sponsorship = new RewardsSponsorshipModel((int)$id_sponsorship);
			$sponsor = new Customer((int)$sponsorship->id_sponsor);
            //p($sponsorship);
            
            if (Tools::isSubmit('deleteSponsor')) {
                //$id_sponsorship = Tools::getValue('id_sponsorship');
                $query = 'DELETE FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE `id_sponsorship` = '. (int)$id_sponsorship;
                if (Db::getInstance()->Execute($query)) {                
                    $module = new allinone_rewards();
                    $template = 'sponsorship-cancel';
                    $vars = array(
        				'{email}' => $sponsor->email,
        				'{lastname}' => $sponsor->lastname,
        				'{firstname}' => $sponsor->firstname,
        				'{friend_lastname}' => $sponsorship->lastname,
        				'{friend_firstname}' => $sponsorship->firstname,
        				'{friend_email}' => $sponsorship->email,
                    );
                    $module->sendMail((int)$this->context->language->id, $template,  $module->getL('Sponsor removing'), $vars, $sponsor->email, $sponsor->firstname.' '.$sponsor->lastname);                    $sponsorDelete++;
                }
            }
            else {
                $this->context->smarty->assign(array(
                    'sponsor' => $sponsor,
                    'id_sponsorship' => $id_sponsorship
                ));                
            }
		}
        
        $this->context->smarty->assign(array(
            'orders' => $orders,
            'invoiceAllowed' => (int)Configuration::get('PS_INVOICE'),
            'reorderingAllowed' => !(bool)Configuration::get('PS_DISALLOW_HISTORY_REORDERING'),
            'slowValidation' => Tools::isSubmit('slowvalidation'),
            'sponsorDelete' => $sponsorDelete
        ));   
              
        $this->setTemplate(_PS_THEME_DIR_.'my-account.tpl');
    }

}

 