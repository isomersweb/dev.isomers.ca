<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class IdentityController extends IdentityControllerCore
{
    public function initContent()
    {
        $sponsored_friend = Db::getInstance()->getValue('SELECT id_customer FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE id_customer = '. $this->customer->id);

        $this->context->smarty->assign('sponsored_friend', $sponsored_friend);
        parent::initContent();
    }
    
    public function postProcess()
    {
        $origin_newsletter = (bool)$this->customer->newsletter;

        if (Tools::isSubmit('submitIdentity')) {
            $email = trim(Tools::getValue('email'));

            if (Tools::getValue('months') != '' && Tools::getValue('days') != '' && Tools::getValue('years') != '') {
                $this->customer->birthday = (int)Tools::getValue('years').'-'.(int)Tools::getValue('months').'-'.(int)Tools::getValue('days');
            } elseif (Tools::getValue('months') == '' && Tools::getValue('days') == '' && Tools::getValue('years') == '') {
                $this->customer->birthday = null;
            } else {
                $this->errors[] = Tools::displayError('Invalid date of birth.');
            }

            if (Tools::getIsset('old_passwd')) {
                $old_passwd = trim(Tools::getValue('old_passwd'));
            }
            
            //echo Tools::encrypt($old_passwd) ."---".$this->context->cookie->passwd;die;
            if (!Validate::isEmail($email)) {
                $this->errors[] = Tools::displayError('This email address is not valid');
            } 
            elseif ($this->customer->email != $email && Customer::customerExists($email, true)) {
                $this->errors[] = Tools::displayError('An account using this email address has already been registered.');
            }
            if (!Tools::getIsset('old_passwd') || (Tools::encrypt($old_passwd) != $this->context->cookie->passwd)) {
                $this->errors['old_passwd'] = Tools::displayError('The password you entered is incorrect.');
            }                 
            if (Tools::getValue('passwd') != Tools::getValue('confirmation')) {
                $this->errors['passwd'] = Tools::displayError('The password and confirmation do not match.');
            } 
            //else {
                //$prev_id_default_group = $this->customer->id_default_group;

                // Merge all errors of this file and of the Object Model
                $this->errors = array_merge($this->errors, $this->customer->validateController());
            //}
            if (!count($this->errors)) {
                //$this->customer->id_default_group = (int)$prev_id_default_group;
                
                //echo $this->customer->id."qwerty".Tools::getValue('id_default_group');die;
                if (Tools::getIsset('id_default_group')) {
                    $old_id_default_group = $this->customer->id_default_group;
                    $id_default_group = $this->customer->id_default_group = Tools::getValue('id_default_group');
                    $this->customer->updateGroup(array($id_default_group));
               	    if (version_compare(_PS_VERSION_, '1.6', '>='))
            			$template = '16-';
                    
                    if ($id_default_group == 3 && $old_id_default_group != $id_default_group) {
                    //if ($old_id_default_group == 3) {                        
                        $query = 'SELECT firstname, lastname, email FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE `id_customer` !=0 AND `id_sponsor` = '.(int)$this->customer->id;
                        $result = Db::getInstance()->ExecuteS($query);
                        if (count($result) > 0) {                
                           foreach ($result as $val) {
                                $vars = array(
                    				'{email}' => $this->customer->email,
                    				'{lastname}' => $this->customer->lastname,
                    				'{firstname}' => $this->customer->firstname,
                                );
                                Mail::Send($this->context->language->id, 'friends-cancel', Mail::l('Rewards Program : Account Change'), $vars, $val['email'], $val['firstname'].' '.$val['lastname']);
                                //$this->sendMail((int)$this->context->language->id, $template, 'Rewards Program : Account Change', $vars, $val['email'], $val['firstname'].' '.$val['lastname']);
                            }
                            Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'rewards_sponsorship` WHERE `id_sponsor` = '.(int)$this->customer->id); 
                        }
                    }
                    
                    Db::getInstance()->Execute('
            			DELETE FROM `'._DB_PREFIX_.'rewards_template_customer`
            			WHERE `id_customer` = '.(int)$this->customer->id); 
                    if ($id_default_group == 5) {
                        Db::getInstance()->Execute('
                			INSERT INTO `'._DB_PREFIX_.'rewards_template_customer` (`id_template`, `id_customer`)
                			VALUE (2,'.(int)$this->customer->id.')');
                        Db::getInstance()->Execute('
                			INSERT INTO `'._DB_PREFIX_.'rewards_template_customer` (`id_template`, `id_customer`)
                			VALUE (3,'.(int)$this->customer->id.')');
                        Db::getInstance()->Execute('
                			INSERT INTO `'._DB_PREFIX_.'rewards_template_customer` (`id_template`, `id_customer`)
                			VALUE (5,'.(int)$this->customer->id.')');                    
                    }
                    elseif ($id_default_group == 7) {
                        /*Db::getInstance()->Execute('
                			INSERT INTO `'._DB_PREFIX_.'rewards_template_customer` (`id_template`, `id_customer`)
                			VALUE (2,'.(int)$this->customer->id.')');*/
                        Db::getInstance()->Execute('
                			INSERT INTO `'._DB_PREFIX_.'rewards_template_customer` (`id_template`, `id_customer`)
                			VALUE (4,'.(int)$this->customer->id.')');
                        Db::getInstance()->Execute('
                			INSERT INTO `'._DB_PREFIX_.'rewards_template_customer` (`id_template`, `id_customer`)
                			VALUE (6,'.(int)$this->customer->id.')');
                        
                    }
                    if ($old_id_default_group != $id_default_group) {
                        Db::getInstance()->Execute('
                			INSERT INTO `'._DB_PREFIX_.'customer_group_log` (`id_customer`, `id_group`, `date_upd`)
                			VALUE ('.(int)$this->customer->id.','.$id_default_group.',\''.date('Y-m-d H:i:s').'\')');
                        
                       $vars = array(
            				'{email}' => $this->customer->email,
            				'{lastname}' => $this->customer->lastname,
            				'{firstname}' => $this->customer->firstname,
                        );
                        if ($id_default_group == 5) {
                            Mail::Send($this->context->language->id, 'sales-team-welcome', Mail::l('The sales-team group was changed'), $vars, $this->customer->email, $this->customer->firstname.' '.$this->customer->lastname);
                        }
                        else if ($id_default_group == 7) {
                            Mail::Send($this->context->language->id, 'refer-friend-welcome', Mail::l('The refer-friend group was changed'), $vars, $this->customer->email, $this->customer->firstname.' '.$this->customer->lastname);
                        }
                        else {
                            Mail::Send($this->context->language->id, 'house-account-welcome', Mail::l('The house-account group was changed'), $vars, $this->customer->email, $this->customer->firstname.' '.$this->customer->lastname);      
                        }
                        //$this->sendMail((int)$this->context->language->id, $template,  'The Customer group was changed', $vars, $vars['email'], $vars['firstname'].' '.$vars['lastname']); 
                   }
                   
               }

                $this->customer->firstname = Tools::ucwords($this->customer->firstname);

                if (Configuration::get('PS_B2B_ENABLE')) {
                    $this->customer->website = Tools::getValue('website'); // force update of website, even if box is empty, this allows user to remove the website
                    $this->customer->company = Tools::getValue('company');
                }
                if (!Tools::getIsset('newsletter')) {
                    $this->customer->newsletter = 0;
                    if ($module_newsletter = Module::getInstanceByName('blocknewsletter')) {
                        $_POST['action'] = '1'; 
                    //d($module_newsletter);
                        $module_newsletter->newsletterRegistration();                       
                    }
                } elseif (!$origin_newsletter && Tools::getIsset('newsletter')) {
                    if ($module_newsletter = Module::getInstanceByName('blocknewsletter')) {
                        /** @var Blocknewsletter $module_newsletter */
                        if ($module_newsletter->active) {
                            $module_newsletter->confirmSubscription($this->customer->email);
                        }
                    }
                }

                if (!Tools::getIsset('optin')) {
                    $this->customer->optin = 0;
                }
                if (Tools::getValue('passwd')) {
                    $this->context->cookie->passwd = $this->customer->passwd;
                }
                if ($this->customer->update()) {
                    $this->context->cookie->customer_lastname = $this->customer->lastname;
                    $this->context->cookie->customer_firstname = $this->customer->firstname;
                    $this->context->smarty->assign('confirmation', 1);
                } else {
                    $this->errors[] = Tools::displayError('The information cannot be updated.');
                }
            }
        } else {
            $_POST = array_map('stripslashes', $this->customer->getFields());
        }

        return $this->customer;
    }
    
   	public function sendMail($id_lang, $template, $subject, $data, $mail, $name, $attachment=null) {
   	    if (version_compare(_PS_VERSION_, '1.6', '>='))
			$template = '16-'.$template;
		$iso = Language::getIsoById((int)$id_lang);
		if (file_exists(_PS_ROOT_DIR_.'/mails/'.$iso.'/'.$template.'.txt') && file_exists(_PS_ROOT_DIR_.'/mails/'.$iso.'/'.$template.'.html')) {
			return Mail::Send((int)$id_lang, $template,  Mail::l($subject), $data, $mail, $name, Configuration::get('PS_SHOP_EMAIL'), Configuration::get('PS_SHOP_NAME'), NULL, NULL, _PS_ROOT_DIR_.'/mails/', $attachment);
        }
		else if (file_exists(_PS_ROOT_DIR_.'/mails/en/'.$template.'.txt') && file_exists(_PS_ROOT_DIR_.'/mails/en/'.$template.'.html')) {
			$id_lang = Language::getIdByIso('en');
			if ($id_lang)
				return Mail::Send((int)$id_lang, $template, Mail::l($subject), $data, $mail, $name, Configuration::get('PS_SHOP_EMAIL'), Configuration::get('PS_SHOP_NAME'), NULL, NULL, _PS_ROOT_DIR_.'/mails/', $attachment);
		}
		return false;
	}
    
    
}
