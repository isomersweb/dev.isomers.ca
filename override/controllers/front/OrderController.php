<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class OrderController extends OrderControllerCore
{
    const STEP_CONFIRM = 4;
    

    public function initContent()
    {
        ParentOrderController::initContent();
//        parent::initContent();
        
        if (Tools::isSubmit('ajax') && Tools::getValue('method') == 'updateExtraCarrier') {
            // Change virtualy the currents delivery options
            $delivery_option = $this->context->cart->getDeliveryOption();
            $delivery_option[(int)Tools::getValue('id_address')] = Tools::getValue('id_delivery_option');
            $this->context->cart->setDeliveryOption($delivery_option);
            $this->context->cart->save();
            $summary = $this->context->cart->getSummaryDetails();
            $return = array(
                'content' => Hook::exec(
                    'displayCarrierList',
                    array(
                        'address' => new Address((int)Tools::getValue('id_address'))
                    )
                ),
                'summary' => $summary
            );
            $this->ajaxDie(Tools::jsonEncode($return));
        }
        if (Tools::isSubmit('ajax') && Tools::getValue('method') == 'updateOrderCarrier') {
            // Change virtualy the currents delivery options
            $delivery_option = $this->context->cart->getDeliveryOption();
            $delivery_option[(int)Tools::getValue('id_address')] = Tools::getValue('id_delivery_option');
            $this->context->cart->setDeliveryOption($delivery_option);
            $this->context->cart->save();
            $summary = $this->context->cart->getSummaryDetails();          
            $this->ajaxDie(Tools::jsonEncode($summary));
        }

        if ($this->nbProducts) {
            $this->context->smarty->assign('virtual_cart', $this->context->cart->isVirtualCart());
        }

        if (!Tools::getValue('multi-shipping')) {
            $this->context->cart->setNoMultishipping();
        }

        // Check for alternative payment api
        $is_advanced_payment_api = (bool)Configuration::get('PS_ADVANCED_PAYMENT_API');

        // 4 steps to the order
        
        if ($id_employee = Tools::getValue('id_employee')) {
            $this->context->cookie->id_employee = $id_employee;
//            $_SESSION['id_employee'] = $id_employee;
        }
        if ($order_source = Tools::getValue('source')) {
            $this->context->cookie->source = $order_source;
//            $_SESSION['source'] = $order_source;
        }
        
        switch ((int)$this->step) {

            case OrderController::STEP_SUMMARY_EMPTY_CART:
                $this->context->smarty->assign('empty', 1);
                $this->setTemplate(_PS_THEME_DIR_.'shopping-cart.tpl');
            break;

            case OrderController::STEP_ADDRESSES:
                $this->_assignAddress();
                $this->processAddressFormat();
                if (Tools::getValue('multi-shipping') == 1) {
                    $this->_assignSummaryInformations();
                    $this->context->smarty->assign('product_list', $this->context->cart->getProducts());
                    $this->setTemplate(_PS_THEME_DIR_.'order-address-multishipping.tpl');
                } else {
                    $this->setTemplate(_PS_THEME_DIR_.'order-address.tpl');
                }
            break;

            case OrderController::STEP_DELIVERY:
                /*
                if (isset($this->context->cookie->paypal_express_checkout_token) && !empty($this->context->cookie->paypal_express_checkout_token)) {
                    Tools::redirect('index.php?controller=order&step=3');
                }
                */
                if (Tools::isSubmit('processAddress')) {
                    $this->processAddress();
                }
                if (!isset($this->context->cookie->paypal_express_checkout_token) && empty($this->context->cookie->paypal_express_checkout_token)) {
                    $this->autoStep();
                }
                
                $this->_assignCarrier();
                $this->_assignSummaryInformations();
                $this->setTemplate(_PS_THEME_DIR_.'order-carrier.tpl');
            break;

            case OrderController::STEP_PAYMENT:

                // Check that the conditions (so active) were accepted by the customer
                $this->context->smarty->assign('orderStep', 'payment');
                /*
                $cgv = Tools::getValue('cgv') || $this->context->cookie->check_cgv;
                
                if ($is_advanced_payment_api === false && Configuration::get('PS_CONDITIONS')
                    && (!Validate::isBool($cgv) || $cgv == false)) {
                    Tools::redirect('index.php?controller=order&step=2');
                }*/

                if ($is_advanced_payment_api === false) {
                    Context::getContext()->cookie->check_cgv = true;
                }
                // Check the delivery option is set
                if ($this->context->cart->isVirtualCart() === false) {                                           
                    if (!Tools::getValue('delivery_option') && !Tools::getValue('id_carrier') && !$this->context->cart->delivery_option && !$this->context->cart->id_carrier) {
                        Tools::redirect('index.php?controller=order&step=2');
                    } elseif (!Tools::getValue('id_carrier') && !$this->context->cart->id_carrier) {
                        $deliveries_options = Tools::getValue('delivery_option');
                        if (!$deliveries_options) {
                            $deliveries_options = $this->context->cart->delivery_option;
                        }

                        foreach ($deliveries_options as $delivery_option) {
                            if (empty($delivery_option)) {
                                Tools::redirect('index.php?controller=order&step=2');
                            }
                        }
                    }                    
                }

                if (!isset($this->context->cookie->paypal_express_checkout_token) && empty($this->context->cookie->paypal_express_checkout_token)) {
                    $this->autoStep();
                }

                // Bypass payment step if total is 0
                if (($id_order = $this->_checkFreeOrder()) && $id_order) {
                    if ($this->context->customer->is_guest) {
                        $order = new Order((int)$id_order);
                        $email = $this->context->customer->email;
                        $this->context->customer->mylogout(); // If guest we clear the cookie for security reason
                        Tools::redirect('index.php?controller=guest-tracking&id_order='.urlencode($order->reference).'&email='.urlencode($email));
                    } else {
                        Tools::redirect('index.php?controller=history');
                    }
                }
                $this->_assignPayment();
                $this->_assignWrappingAndTOS();
                
                if ($is_advanced_payment_api === true) {
                    $this->_assignAddress();
                }

                // assign some informations to display cart
                $this->_assignSummaryInformations();
                $this->setTemplate(_PS_THEME_DIR_.'order-payment.tpl');
            break;
            
            case OrderController::STEP_CONFIRM:

                if (isset($this->context->cookie->paypal_express_checkout_token) && !empty($this->context->cookie->paypal_express_checkout_token)) {
                    Tools::redirect('index.php?controller=order&step=3');
                }

                $cgv = Tools::getValue('cgv') || $this->context->cookie->check_cgv;

                if ($is_advanced_payment_api === false && Configuration::get('PS_CONDITIONS')
                    && (!Validate::isBool($cgv) || $cgv == false)) {
                    Tools::redirect('index.php?controller=order&step=3');
                }
                
                session_start();
                
                if (!empty($this->context->cookie->id_employee) && isset($this->context->cookie->id_employee)) {
                    CartRule::autoAddToCart($this->context);                
                }
                $pm = Tools::getValue('payment_method');
                if (empty($pm) && empty($_SESSION['payment']['payment_method'])) {
                    Tools::redirect('index.php?controller=order&step=3');
                }

                if (!empty($pm)) {
                    $_SESSION['payment']['payment_method'] = $pm;
                    $_SESSION['payment']['cc_num'] = Tools::getValue('cc_num');
                    $_SESSION['payment']['cc_owner'] = Tools::getValue('cc_owner');
                    $_SESSION['payment']['cc_cvd'] = Tools::getValue('cc_cvd');
                    $_SESSION['payment']['cc_exp'] = Tools::getValue('cc_month') . Tools::getValue('cc_year');
                }
                $this->context->smarty->assign(array('body_classes' => array('orderconfirm')));
                
                if (!isset($this->context->cookie->paypal_express_checkout_token) && empty($this->context->cookie->paypal_express_checkout_token)) {
                    $this->autoStep();
                }
                
                $dlv_address = new Address(intval($this->context->cart->id_address_delivery));
                $inv_address = new Address(intval($this->context->cart->id_address_invoice));
                $state = State::getNameById($dlv_address->id_state);

                $this->context->smarty->assign(array(
                    'dlv_address' => $dlv_address,
                    'inv_address' => $inv_address,
                    'state' => $state,
                    'country' => new Country(intval($address->id_country))
                ));
                
                $this->_assignSummaryInformations();
                $this->setTemplate(_PS_THEME_DIR_.'order-confirm.tpl');
            break;
            

            default:
                $this->_assignSummaryInformations();
                $this->setTemplate(_PS_THEME_DIR_.'shopping-cart.tpl');
            break;
        }
//p(Context::getContext()->cookie);
//p($_SESSION);
    }
    

}
