<?php
class AdminCartRulesController extends AdminCartRulesControllerCore
{
    public function displayAjaxSearchCartRuleVouchers()
    {
        $found = false;
        $id_customer = Tools::getValue('customer');
        $sql_base = 'SELECT cr.*, crl.*
						FROM '._DB_PREFIX_.'cart_rule cr
						LEFT JOIN '._DB_PREFIX_.'cart_rule_lang crl ON (cr.id_cart_rule = crl.id_cart_rule AND crl.id_lang = '.(int)$this->context->language->id.')
                        WHERE cr.id_customer = '. $id_customer . ' AND cr.quantity > 0  AND cr.date_to > NOW() ORDER BY cr.date_from DESC';

        if ($vouchers = Db::getInstance()->executeS($sql_base)) {
            $found = true;
        }
        echo Tools::jsonEncode(array('found' => $found, 'vouchers' => $vouchers));
    }

}
