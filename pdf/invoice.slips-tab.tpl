
<table id="slips-tab" width="100%" border="0">
    <thead>
{*    
    	<tr>
    		<th class="product header small" width="20%">{l s='Refund ID' pdf='true'}</th>
    		<th class="product header small" width="80%">{l s='Total (Tax incl.)' pdf='true'}</th>
    	</tr>
*}        
    	<tr>
    		<th class="product header small" width="40%">{l s='Refund Date' pdf='true'}</th>
    		<th class="product header small" width="60%">{l s='Refund Amount' pdf='true'}</th>
    	</tr>
	</thead>
    <tbody>
{*
	   {foreach from=$order_slips item=order_slip}
		<tr>
			<td class="product center small">{$order_slip.id_order_slip}</td>
			<td class="product right small">{displayPrice currency=$order->id_currency price=$order_slip.amount}</td>
    	</tr>
	  {/foreach}
*}      
      {foreach from=$refunds_payments item=refund}
		<tr>
            {if $refund.paypal_amount > 0}
    			<td class="product center small">{dateFormat date=$refund.paypal_date full=0}</td>
    			<td class="product right small">{displayPrice currency=$order->id_currency price=$refund.paypal_amount}</td>
            {else}
    			<td class="product center small">{dateFormat date=$refund.moneris_date full=0}</td>
    			<td class="product right small">{displayPrice currency=$order->id_currency price=$refund.moneris_amount}</td>                
            {/if}
    	</tr>
	  {/foreach}
   </tbody>
</table>

