<?php
$xml = '
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v19="http://fedex.com/ws/ship/v19">
    <soapenv:Header/>
    <soapenv:Body>
        <v19:ProcessShipmentRequest>
            <v19:WebAuthenticationDetail>
                <v19:UserCredential>
                    <v19:Key>8pq4FWobf2kMtCsm</v19:Key>
                    <v19:Password>KfyPUA68YHxYI8YzkgIUoYphT</v19:Password>
                </v19:UserCredential>
            </v19:WebAuthenticationDetail>
            <v19:ClientDetail>
                <v19:AccountNumber>510087186</v19:AccountNumber>
                <v19:MeterNumber>118774824</v19:MeterNumber>
            </v19:ClientDetail>
            <v19:TransactionDetail>
                <v19:CustomerTransactionId>ProcessShip_Basic</v19:CustomerTransactionId>
            </v19:TransactionDetail>
            <v19:Version>
                <v19:ServiceId>ship</v19:ServiceId>
                <v19:Major>19</v19:Major>
                <v19:Intermediate>0</v19:Intermediate>
                <v19:Minor>0</v19:Minor>
            </v19:Version>
            <v19:RequestedShipment>
                <v19:ShipTimestamp>'.date('c').'</v19:ShipTimestamp>
                <v19:DropoffType>REGULAR_PICKUP</v19:DropoffType>
                <v19:ServiceType>INTERNATIONAL_PRIORITY</v19:ServiceType>
                <v19:PackagingType>YOUR_PACKAGING</v19:PackagingType>
                <v19:Shipper>
                    <v19:AccountNumber>510087186</v19:AccountNumber>
                    <v19:Contact>
                        <v19:PersonName>Input Your Information</v19:PersonName>
                        <v19:CompanyName>Input Your Information</v19:CompanyName>
                        <v19:PhoneNumber>1234567890</v19:PhoneNumber>
                        <v19:EMailAddress>Input Your Information</v19:EMailAddress>
                    </v19:Contact>
                    <v19:Address>
                        <v19:StreetLines>105 Tycos Drive</v19:StreetLines>
                        <v19:City>Toronto</v19:City>
                        <v19:StateOrProvinceCode>ON</v19:StateOrProvinceCode>
                        <v19:PostalCode>M6B 1W3</v19:PostalCode>
                        <v19:CountryCode>CA</v19:CountryCode>
                    </v19:Address>
                </v19:Shipper>
                <v19:Recipient>
                    <v19:Contact>
                        <v19:PersonName>Input Your Information</v19:PersonName>
                        <v19:CompanyName>Input Your Information</v19:CompanyName>
                        <v19:PhoneNumber>1234567890</v19:PhoneNumber>
                        <v19:EMailAddress>Input Your Information</v19:EMailAddress>
                    </v19:Contact>
                    <v19:Address>
                        <v19:StreetLines>Input Your Information</v19:StreetLines>
                        <v19:StreetLines>Input Your Information</v19:StreetLines>
                        <v19:City>Coraopolis</v19:City>
                        <v19:StateOrProvinceCode>PA</v19:StateOrProvinceCode>
                        <v19:PostalCode>15108</v19:PostalCode>
                        <v19:CountryCode>US</v19:CountryCode>
                    </v19:Address>
                </v19:Recipient>
                <v19:Origin>
                    <v19:Contact>
                        <v19:PersonName></v19:PersonName>
                        <v19:CompanyName>ISOMERS</v19:CompanyName>
                        <v19:PhoneNumber>1234567890</v19:PhoneNumber>
                        <v19:EMailAddress>Input Your Information</v19:EMailAddress>
                    </v19:Contact>
                    <v19:Address>
                        <v19:StreetLines>105 Tycos Drive</v19:StreetLines>
                        <v19:City>Toronto</v19:City>
                        <v19:StateOrProvinceCode>ON</v19:StateOrProvinceCode>
                        <v19:PostalCode>M6B 1W3</v19:PostalCode>
                        <v19:CountryCode>CA</v19:CountryCode>
                    </v19:Address>
                </v19:Origin>
                <v19:ShippingChargesPayment>
                    <v19:PaymentType>SENDER</v19:PaymentType>
                    <v19:Payor>
                        <v19:ResponsibleParty>
                            <v19:AccountNumber>510087186</v19:AccountNumber>
                            <v19:Contact>
                                <v19:PersonName>Input Your Information</v19:PersonName>
                                <v19:CompanyName>Input Your Information</v19:CompanyName>
                                <v19:PhoneNumber>123456890</v19:PhoneNumber>
                                <v19:EMailAddress>Input Your Information</v19:EMailAddress>
                            </v19:Contact>
                            <v19:Address>
                                <v19:StreetLines>105 Tycos Drive</v19:StreetLines>
                                <v19:City>Toronto</v19:City>
                                <v19:StateOrProvinceCode>ON</v19:StateOrProvinceCode>
                                <v19:PostalCode>M6B 1W3</v19:PostalCode>
                                <v19:CountryCode>CA</v19:CountryCode>
                            </v19:Address>
                        </v19:ResponsibleParty>
                    </v19:Payor>
                </v19:ShippingChargesPayment>
                
                <v19:SpecialServicesRequested>
                    <v19:SpecialServiceTypes>ELECTRONIC_TRADE_DOCUMENTS</v19:SpecialServiceTypes>
                    <v19:EtdDetail>
                        <v19:RequestedDocumentCopies>COMMERCIAL_INVOICE</v19:RequestedDocumentCopies>
                        <!--<v19:DocumentReferences>
                            <v19:LineNumber>1</v19:LineNumber>
                            <v19:CustomerReference>Customer_Reference1_For_DocumentReference</v19:CustomerReference>
                            <v19:DocumentProducer>CUSTOMER</v19:DocumentProducer>
                            <v19:DocumentType>COMMERCIAL_INVOICE</v19:DocumentType>
                            <v19:DocumentId>1090493e180971129</v19:DocumentId>
                            <v19:DocumentIdProducer>CUSTOMER</v19:DocumentIdProducer>
                        </v19:DocumentReferences>
                        <v19:DocumentReferences>
                            <v19:LineNumber>1</v19:LineNumber>
                            <v19:CustomerReference>Customer_Reference1_For_DocumentReference</v19:CustomerReference>
                            <v19:DocumentProducer>CUSTOMER</v19:DocumentProducer>
                            <v19:DocumentType>COMMERCIAL_INVOICE</v19:DocumentType>
                            <v19:DocumentId>1090493e180971129</v19:DocumentId>
                            <v19:DocumentIdProducer>CUSTOMER</v19:DocumentIdProducer>
                        </v19:DocumentReferences>-->
                    </v19:EtdDetail>
                </v19:SpecialServicesRequested>
                <v19:CustomsClearanceDetail>
                    <!--<v19:ImporterOfRecord>
                        <v19:AccountNumber>510087186</v19:AccountNumber>
                        <v19:Contact>
                            <v19:PersonName>Input Your Information</v19:PersonName>
                            <v19:CompanyName>Input Your Information</v19:CompanyName>
                            <v19:PhoneNumber>1234567890</v19:PhoneNumber>
                            <v19:EMailAddress/>
                        </v19:Contact>
                        <v19:Address>
                            <v19:StreetLines>105 Tycos Drive</v19:StreetLines>
                            <v19:City>Toronto</v19:City>
                            <v19:StateOrProvinceCode>ON</v19:StateOrProvinceCode>
                            <v19:PostalCode>M6B 1W3</v19:PostalCode>
                            <v19:CountryCode>CA</v19:CountryCode>
                        </v19:Address>
                    </v19:ImporterOfRecord>-->
                    <v19:DutiesPayment>
                        <v19:PaymentType>SENDER</v19:PaymentType>
                        <v19:Payor>
                            <v19:ResponsibleParty>
                                <v19:AccountNumber>510087186</v19:AccountNumber>
                                <v19:Contact>
                                    <v19:PersonName>Input Your Information</v19:PersonName>
                                    <v19:CompanyName>Input Your Information</v19:CompanyName>
                                    <v19:PhoneNumber>1234567890</v19:PhoneNumber>
                                    <v19:EMailAddress/>
                                </v19:Contact>
                                <v19:Address>
                                    <v19:StreetLines>105 Tycos Drive</v19:StreetLines>
                                    <v19:City>Toronto</v19:City>
                                    <v19:StateOrProvinceCode>ON</v19:StateOrProvinceCode>
                                    <v19:PostalCode>M6B 1W3</v19:PostalCode>
                                    <v19:CountryCode>CA</v19:CountryCode>
                                </v19:Address>
                            </v19:ResponsibleParty>
                        </v19:Payor>
                    </v19:DutiesPayment>
                    <v19:DocumentContent>DOCUMENTS_ONLY</v19:DocumentContent>
                    <v19:CustomsValue>
                        <v19:Currency>USD</v19:Currency>
                        <v19:Amount>100</v19:Amount>
                    </v19:CustomsValue>
                    <v19:CommercialInvoice>
                        <v19:Comments>GodKnows</v19:Comments>
                        <v19:PaymentTerms>string</v19:PaymentTerms>
                        <v19:CustomerReferences>
                            <v19:CustomerReferenceType>CUSTOMER_REFERENCE</v19:CustomerReferenceType>
                            <v19:Value>TC008</v19:Value>
                        </v19:CustomerReferences>
                        <v19:TermsOfSale>FOB</v19:TermsOfSale>
                    </v19:CommercialInvoice>
                    <v19:Commodities>
                        <v19:Name>BOOKS</v19:Name>
                        <v19:NumberOfPieces>1</v19:NumberOfPieces>
                        <v19:Description>Books</v19:Description>
                        <v19:CountryOfManufacture>US</v19:CountryOfManufacture>
                        <v19:Weight>
                            <v19:Units>LB</v19:Units>
                            <v19:Value>10</v19:Value>
                        </v19:Weight>
                        <v19:Quantity>1</v19:Quantity>
                        <v19:QuantityUnits>EA</v19:QuantityUnits>
                        <v19:UnitPrice>
                            <v19:Currency>USD</v19:Currency>
                            <v19:Amount>10.00</v19:Amount>
                        </v19:UnitPrice>
                        <v19:CustomsValue>
                            <v19:Currency>USD</v19:Currency>
                            <v19:Amount>100</v19:Amount>
                        </v19:CustomsValue>
                    </v19:Commodities>
                </v19:CustomsClearanceDetail>
                <v19:LabelSpecification>
                    <v19:LabelFormatType>COMMON2D</v19:LabelFormatType>
                    <v19:ImageType>PDF</v19:ImageType>
                    <v19:LabelStockType>PAPER_8.5X11_TOP_HALF_LABEL</v19:LabelStockType>
                </v19:LabelSpecification>
                
                <v19:ShippingDocumentSpecification>
                    <v19:ShippingDocumentTypes>COMMERCIAL_INVOICE</v19:ShippingDocumentTypes>
                    <v19:CommercialInvoiceDetail>
                        <v19:Format>
                            <v19:ImageType>PDF</v19:ImageType>
                            <v19:StockType>PAPER_LETTER</v19:StockType>
                        </v19:Format>
                    </v19:CommercialInvoiceDetail>
                </v19:ShippingDocumentSpecification>
                <v19:EdtRequestType>ALL</v19:EdtRequestType>
                
                <!--<v19:RateRequestTypes>LIST</v19:RateRequestTypes>-->
                <v19:PackageCount>1</v19:PackageCount>
                <v19:RequestedPackageLineItems>
                    <v19:SequenceNumber>1</v19:SequenceNumber>
                    <v19:InsuredValue>
                        <v19:Currency>USD</v19:Currency>
                        <v19:Amount>100</v19:Amount>
                    </v19:InsuredValue>
                    <v19:Weight>
                        <v19:Units>LB</v19:Units>
                        <v19:Value>30</v19:Value>
                    </v19:Weight>
                    <v19:Dimensions>
                        <v19:Length>20</v19:Length>
                        <v19:Width>15</v19:Width>
                        <v19:Height>20</v19:Height>
                        <v19:Units>IN</v19:Units>
                    </v19:Dimensions>
                    <v19:CustomerReferences>
                        <v19:CustomerReferenceType>CUSTOMER_REFERENCE</v19:CustomerReferenceType>
                        <v19:Value>TC008</v19:Value>
                    </v19:CustomerReferences>
                </v19:RequestedPackageLineItems>
            </v19:RequestedShipment>
        </v19:ProcessShipmentRequest>
    </soapenv:Body>
</soapenv:Envelope>
';

$xml2 = '
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v19="http://fedex.com/ws/ship/v19">
   <SOAP-ENV:Body>
      <v19:ProcessShipmentRequest>
         <v19:WebAuthenticationDetail>
            <v19:UserCredential>
               <v19:Key>8pq4FWobf2kMtCsm</v19:Key>
               <v19:Password>KfyPUA68YHxYI8YzkgIUoYphT</v19:Password>
            </v19:UserCredential>
         </v19:WebAuthenticationDetail>
         <v19:ClientDetail>
            <v19:AccountNumber>510087186</v19:AccountNumber>
            <v19:MeterNumber>118774824</v19:MeterNumber>
         </v19:ClientDetail>
         <v19:TransactionDetail>
            <v19:CustomerTransactionId>*** ProcessShip ***</v19:CustomerTransactionId>
         </v19:TransactionDetail>
         <v19:Version>
            <v19:ServiceId>ship</v19:ServiceId>
            <v19:Major>19</v19:Major>
            <v19:Intermediate>0</v19:Intermediate>
            <v19:Minor>0</v19:Minor>
         </v19:Version>
         <v19:RequestedShipment>
            <v19:ShipTimestamp>2017-01-25T00:00:00-05:00</v19:ShipTimestamp>
            <v19:DropoffType>REGULAR_PICKUP</v19:DropoffType>
            <v19:ServiceType>INTERNATIONAL_ECONOMY</v19:ServiceType>
            <v19:PackagingType>YOUR_PACKAGING</v19:PackagingType>
            <v19:TotalWeight>
               <v19:Units>KG</v19:Units>
               <v19:Value>0.3</v19:Value>
            </v19:TotalWeight>
            <v19:Shipper>
               <v19:Contact>
                  <v19:PersonName />
                  <v19:CompanyName>Isomers Laboratories Inc</v19:CompanyName>
                  <v19:PhoneNumber>1.416.787.2465</v19:PhoneNumber>
               </v19:Contact>
               <v19:Address>
                  <v19:StreetLines>105 Tycos Drive</v19:StreetLines>
                  <v19:City>Toronto</v19:City>
                  <v19:StateOrProvinceCode>ON</v19:StateOrProvinceCode>
                  <v19:PostalCode>M6B 1W3</v19:PostalCode>
                  <v19:CountryCode>CA</v19:CountryCode>
               </v19:Address>
            </v19:Shipper>
            <v19:Recipient>
               <v19:Contact>
                  <v19:PersonName>John Cooper</v19:PersonName>
                  <v19:CompanyName />
                  <v19:PhoneNumber>12345678890</v19:PhoneNumber>
               </v19:Contact>
               <v19:Address>
                  <v19:StreetLines>1 Infinite Loop</v19:StreetLines>
                  <v19:City>Cupertino</v19:City>
                  <v19:StateOrProvinceCode>CA</v19:StateOrProvinceCode>
                  <v19:PostalCode>95014</v19:PostalCode>
                  <v19:CountryCode>US</v19:CountryCode>
                  <v19:Residential>false</v19:Residential>
               </v19:Address>
            </v19:Recipient>
            <v19:Origin>
               <v19:Contact>
                  <v19:PersonName />
                  <v19:CompanyName>Isomers Laboratories Inc</v19:CompanyName>
                  <v19:PhoneNumber>1.416.787.2465</v19:PhoneNumber>
               </v19:Contact>
               <v19:Address>
                  <v19:StreetLines>105 Tycos Drive</v19:StreetLines>
                  <v19:City>Toronto</v19:City>
                  <v19:StateOrProvinceCode>ON</v19:StateOrProvinceCode>
                  <v19:PostalCode>M6B 1W3</v19:PostalCode>
                  <v19:CountryCode>CA</v19:CountryCode>
               </v19:Address>
            </v19:Origin>
            <v19:ShippingChargesPayment>
               <v19:PaymentType>SENDER</v19:PaymentType>
               <v19:Payor>
                  <v19:ResponsibleParty>
                     <v19:AccountNumber>510087186</v19:AccountNumber>
                  </v19:ResponsibleParty>
               </v19:Payor>
            </v19:ShippingChargesPayment>
            <v19:SpecialServicesRequested>
               <v19:SpecialServiceTypes>ELECTRONIC_TRADE_DOCUMENTS</v19:SpecialServiceTypes>
               <v19:EtdDetail>
                  <v19:RequestedDocumentCopies>COMMERCIAL_INVOICE</v19:RequestedDocumentCopies>
               </v19:EtdDetail>
            </v19:SpecialServicesRequested>
            <v19:CustomsClearanceDetail>
               <v19:DutiesPayment>
                  <v19:PaymentType>SENDER</v19:PaymentType>
                  <v19:Payor>
                     <v19:ResponsibleParty>
                        <v19:AccountNumber>510087186</v19:AccountNumber>
                     </v19:ResponsibleParty>
                  </v19:Payor>
               </v19:DutiesPayment>
               <v19:DocumentContent>NON_DOCUMENTS</v19:DocumentContent>
               <v19:CustomsValue>
                  <v19:Currency>CAD</v19:Currency>
                  <v19:Amount>2.5</v19:Amount>
               </v19:CustomsValue>
               <v19:CommercialInvoice>
                  <v19:PaymentTerms />
                  <v19:CustomerReferences>
                     <v19:CustomerReferenceType>CUSTOMER_REFERENCE</v19:CustomerReferenceType>
                     <v19:Value>PPFDGZNCU</v19:Value>
                  </v19:CustomerReferences>
                  <v19:TermsOfSale>FOB</v19:TermsOfSale>
               </v19:CommercialInvoice>
               <v19:Commodities>
                  <v19:Name>RenewaGlow Clay Mask Cleanser 240 ml</v19:Name>
                  <v19:NumberOfPieces>1</v19:NumberOfPieces>
                  <v19:Description>RenewaGlow Clay Mask Cleanser 240 ml
Cosmetic skin care products. NOT FOR RESALE. Samples only.</v19:Description>
                  <v19:CountryOfManufacture>CA</v19:CountryOfManufacture>
                  <v19:HarmonizedCode>3304.99.50</v19:HarmonizedCode>
                  <v19:Weight>
                     <v19:Units>KG</v19:Units>
                     <v19:Value>0.300000</v19:Value>
                  </v19:Weight>
                  <v19:Quantity>1</v19:Quantity>
                  <v19:QuantityUnits>EA</v19:QuantityUnits>
                  <v19:UnitPrice>
                     <v19:Currency>CAD</v19:Currency>
                     <v19:Amount>35.000000</v19:Amount>
                  </v19:UnitPrice>
                  <v19:CustomsValue>
                     <v19:Currency>CAD</v19:Currency>
                     <v19:Amount>2.5</v19:Amount>
                  </v19:CustomsValue>
               </v19:Commodities>
               <v19:ExportDetail>
                  <v19:B13AFilingOption>NOT_REQUIRED</v19:B13AFilingOption>
               </v19:ExportDetail>
            </v19:CustomsClearanceDetail>
            <v19:LabelSpecification>
               <v19:LabelFormatType>COMMON2D</v19:LabelFormatType>
               <v19:ImageType>ZPLII</v19:ImageType>
               <v19:LabelStockType>STOCK_4X6.75_LEADING_DOC_TAB</v19:LabelStockType>
               <v19:LabelPrintingOrientation>TOP_EDGE_OF_TEXT_FIRST</v19:LabelPrintingOrientation>
               <v19:CustomerSpecifiedDetail>
                  <v19:DocTabContent>
                     <v19:DocTabContentType>ZONE001</v19:DocTabContentType>
                     <v19:Zone001>
                        <v19:DocTabZoneSpecifications>
                           <v19:ZoneNumber>1</v19:ZoneNumber>
                           <v19:Header>TO</v19:Header>
                           <v19:LiteralValue>1 Infinite Loop,Cupertino,95014</v19:LiteralValue>
                        </v19:DocTabZoneSpecifications>
                        <v19:DocTabZoneSpecifications>
                           <v19:ZoneNumber>2</v19:ZoneNumber>
                           <v19:Header>REF</v19:Header>
                           <v19:DataField>REQUEST/PACKAGE/CustomerReferences[1]/Value</v19:DataField>
                        </v19:DocTabZoneSpecifications>
                        <v19:DocTabZoneSpecifications>
                           <v19:ZoneNumber>4</v19:ZoneNumber>
                           <v19:Header>Pkg</v19:Header>
                           <v19:LiteralValue>1 of 1</v19:LiteralValue>
                        </v19:DocTabZoneSpecifications>
                        <v19:DocTabZoneSpecifications>
                           <v19:ZoneNumber>5</v19:ZoneNumber>
                           <v19:Header>Date</v19:Header>
                           <v19:DataField>REQUEST/SHIPMENT/ShipTimestamp</v19:DataField>
                           <v19:Justification>RIGHT</v19:Justification>
                        </v19:DocTabZoneSpecifications>
                        <v19:DocTabZoneSpecifications>
                           <v19:ZoneNumber>6</v19:ZoneNumber>
                           <v19:Header>ACTWGT</v19:Header>
                           <v19:DataField>REQUEST/SHIPMENT/TotalWeight/Value</v19:DataField>
                           <v19:Justification>RIGHT</v19:Justification>
                        </v19:DocTabZoneSpecifications>
                        <v19:DocTabZoneSpecifications>
                           <v19:ZoneNumber>7</v19:ZoneNumber>
                           <v19:Header>Ship Cost</v19:Header>
                           <v19:DataField>REPLY/SHIPMENT/RATES/ACTUAL/TotalNetCharge/Amount</v19:DataField>
                           <v19:Justification>RIGHT</v19:Justification>
                        </v19:DocTabZoneSpecifications>
                        <v19:DocTabZoneSpecifications>
                           <v19:ZoneNumber>8</v19:ZoneNumber>
                           <v19:Header>DIMMED</v19:Header>
                           <v19:LiteralValue>2x2x2 CM</v19:LiteralValue>
                           <v19:Justification>RIGHT</v19:Justification>
                        </v19:DocTabZoneSpecifications>
                     </v19:Zone001>
                  </v19:DocTabContent>
               </v19:CustomerSpecifiedDetail>
            </v19:LabelSpecification>
            <v19:ShippingDocumentSpecification>
               <v19:ShippingDocumentTypes>COMMERCIAL_INVOICE</v19:ShippingDocumentTypes>
               <v19:CommercialInvoiceDetail>
                  <v19:Format>
                     <v19:ImageType>PDF</v19:ImageType>
                     <v19:StockType>PAPER_LETTER</v19:StockType>
                  </v19:Format>
               </v19:CommercialInvoiceDetail>
            </v19:ShippingDocumentSpecification>
            <v19:EdtRequestType>ALL</v19:EdtRequestType>
            <v19:PackageCount>1</v19:PackageCount>
            <v19:RequestedPackageLineItems>
               <v19:SequenceNumber>1</v19:SequenceNumber>
               <v19:GroupPackageCount>1</v19:GroupPackageCount>
               <v19:Weight>
                  <v19:Units>KG</v19:Units>
                  <v19:Value>0.3</v19:Value>
               </v19:Weight>
               <v19:Dimensions>
                  <v19:Length>2</v19:Length>
                  <v19:Width>2</v19:Width>
                  <v19:Height>2</v19:Height>
                  <v19:Units>CM</v19:Units>
               </v19:Dimensions>
               <v19:CustomerReferences>
                  <v19:CustomerReferenceType>CUSTOMER_REFERENCE</v19:CustomerReferenceType>
                  <v19:Value>PPFDGZNCU</v19:Value>
               </v19:CustomerReferences>
            </v19:RequestedPackageLineItems>
         </v19:RequestedShipment>
      </v19:ProcessShipmentRequest>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>

';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://wsbeta.fedex.com:443/web-services');
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
$result_xml = curl_exec($ch);

// remove colons and dashes to simplify the xml
$result_xml = str_replace(array(':','-'), '', $result_xml);
$result = @simplexml_load_string($result_xml);

define('SHIP_LABEL2', _PS_ROOT_DIR_. '/orders/labels/test.pdf');

$fp = fopen( SHIP_LABEL2, 'wb');   
fwrite($fp, ($result->SOAPENVBody->ProcessShipmentReply->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
fclose($fp);
/*
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.basename($fp).'"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($fp));
readfile($fp);*/
//echo $result->SOAPENVBody->ProcessShipmentReply->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image;
print '<pre>';
print_r($result);
//d();