<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

class IRFunctions
{
    public function mindecFormat($number)
    {
        $d = 3;
        $n = 2;
        $formatted = false;
        while ((number_format($number, $n) == $number) && ($n >= 0)) {
            --$d;
            --$n;
            $formatted = true;
        }
        if ($formatted) {
            return number_format($number, $d);
        }
        return $number;
    }

    /* customization of PS core function
    *  don't want to display currency sign in module Invoices report.
    *  Instead, currency ISO code is shown in separate table column
    *  when exporting to CSV, don't allow other format except 0000.00
    */
    public static function displayPriceIr($price, $csv = false, $currency = null, Context $context = null)
    {
        if (!is_numeric($price)) {
            return $price;
        }
        if (!$context) {
            $context = Context::getContext();
        }
        if ($currency === null) {
            $currency = $context->currency;
        } elseif (is_int($currency)) {
            $currency = Currency::getCurrencyInstance((int)$currency);
        }

        if (is_array($currency)) {
            $c_format = $currency['format'];
            $c_decimals = (int)$currency['decimals'] * _PS_PRICE_DISPLAY_PRECISION_;
        } elseif (is_object($currency)) {
            $c_format = $currency->format;
            $c_decimals = (int)$currency->decimals * _PS_PRICE_DISPLAY_PRECISION_;
        } else {
            return false;
        }

        $ret = 0;
        if (($is_negative = ($price < 0))) {
            $price *= -1;
        }
        $price = Tools::ps_round($price, $c_decimals);
        if ($csv) {
            $c_format = 0;
        }
        if (($c_format == 2) && ($context->language->is_rtl == 1)) {
            $c_format = 4;
        }

        switch ($c_format) {
            case 0:
            /* 0000.00 */
                $ret = number_format($price, $c_decimals, '.', '');
                break;
            /* X 0,000.00 */
            case 1:
                $ret = number_format($price, $c_decimals, '.', ',');
                break;
            /* 0 000,00 X*/
            case 2:
                $ret = number_format($price, $c_decimals, ',', ' ');
                break;
            /* X 0.000,00 */
            case 3:
                $ret = number_format($price, $c_decimals, ',', '.');
                break;
            /* 0,000.00 X */
            case 4:
                $ret = number_format($price, $c_decimals, '.', ',');
                break;
            /* X 0'000.00  Added for the switzerland currency */
            case 5:
                $ret = number_format($price, $c_decimals, '.', "'");
                break;
        }
        if ($is_negative) {
            $ret = '-'.$ret;
        }

        return $ret;
    }

    public static function getCurrentDateTime($smarty)
    {
        $params = array();
        $curr_dat = new DateTime('NOW');
        $params['date'] = $curr_dat->format('Y-m-d H:i:s');
        $params['full'] = true;
        return Tools::dateFormat($params, $smarty);
    }
}
