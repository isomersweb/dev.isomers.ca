<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

class HTMLTemplateOrdersReport extends HTMLTemplate
{
    private $orders = null;

    public function __construct(OrderList $orders, $smarty)
    {
        $this->smarty = $smarty;
        $this->available_in_your_account = false;
        $this->orders = $orders;

        $this->title = HTMLTemplateOrdersReport::l('Orders report');

        $this->shop = new Shop((int)Context::getContext()->shop->id);
    }

    /**
    * customization of core Prestashop function
    * this is needed to provide compatibility with PS 1.5
    * Generatereport() crashed in 1.5 because member
    * HTMLTemplateOrdersReport::order does not exis
    */

    public function getHeader()
    {
        $id_shop = $this->shop->id;
        $shop_name = Configuration::get('PS_SHOP_NAME', null, null, (int)$id_shop);
        $path_logo = $this->getLogo();

        $width = 0;
        $height = 0;
        if (!empty($path_logo)) {
            list($width, $height) = getimagesize($path_logo);
        }

        $this->smarty->assign(array(
            'logo_path' => $path_logo,
            'img_ps_dir' => 'http://'.Tools::getMediaServer(_PS_IMG_)._PS_IMG_,
            'img_update_time' => Configuration::get('PS_IMG_UPDATE_TIME'),
            'title' => $this->title,
            'date' => $this->date,
            'shop_name' => $shop_name,
            'shop_details' => Configuration::get('PS_SHOP_DETAILS', null, null, (int)$id_shop),
            'width_logo' => $width,
            'height_logo' => $height
        ));

        return $this->smarty->fetch($this->getTemplate('header'));
    }

    public function getContent()
    {
        /*
        * get search fields used to generate the list of orders,
        * to put descriptive strings to the PDF document.
        */
        $search = $this->orders->getSearch();

        $filtered = false;
        if (count($search) > 0) {
            $filtered = true;
        }

        require_once(_PS_MODULE_DIR_.'ordersreport/classes/IRFunctions.php');

        $this->smarty->assign(array(
            'orders' => $this->orders->getList(),
            'amounts' => $this->orders->getAmounts(),
            'report_date' => IRFunctions::getCurrentDateTime(Context::getContext()->smarty),
            'search' => $search,
            'shops' => $this->orders->getShops(),
            'filtered' => $filtered,
            'fields' => $this->orders->getFields(),
            'multistore' => $this->orders->getMultistore(),
            'functions' => $this->orders->getFunctions(),
            'section_orderstatefilter' => $this->orders->aGetOrderStateFilter(),
            'psver17' => (version_compare(_PS_VERSION_, '1.7', '>=') === true)
        ));
//d($this->orders->getAmounts());
        $template = _PS_MODULE_DIR_.'ordersreport/views/templates/admin/report.tpl';
        return $this->smarty->fetch($template);
    }

    /**
    * customization of core Prestashop function - PS 1.5 compatibility
    */
    public function getFooter()
    {
        $shop_address = $this->getShopAddress();

        $this->smarty->assign(
            array(
            'available_in_your_account' => $this->available_in_your_account,
            'shop_address' => $shop_address,
            'shop_fax' => Configuration::get('PS_SHOP_FAX', null, null, (int)$this->shop->id),
            'shop_phone' => Configuration::get('PS_SHOP_PHONE', null, null, (int)$this->shop->id),
            'shop_email' => Configuration::get('PS_SHOP_EMAIL', null, null, (int)$this->shop->id),
            'free_text' => Configuration::get(
                'PS_INVOICE_FREE_TEXT',
                (int)Context::getContext()->language->id,
                null,
                (int)$this->shop->id
            )
            )
        );

        return $this->smarty->fetch($this->getTemplate('footer'));
    }

    /**
    * customization of core Prestashop function - PS 1.5 compatibility
    */
    protected function getLogo()
    {
        $logo = '';
        $id_shop = Context::getContext()->shop->id;
        $ps_logo_order = Configuration::get('PS_LOGO_INVOICE', null, null, (int)$id_shop);

        if ($ps_logo_order != false && file_exists(_PS_IMG_DIR_.$ps_logo_order)) {
            $logo = _PS_IMG_DIR_.$ps_logo_order;
        } else {
            $ps_logo = Configuration::get('PS_LOGO', null, null, (int)$id_shop);
            if ($ps_logo != false && file_exists(_PS_IMG_DIR_.$ps_logo)) {
                $logo = _PS_IMG_DIR_.$ps_logo;
            }
        }
        return $logo;
    }

    public function getFilename()
    {
        return 'orders_report.pdf';
    }

    public function getBulkFilename()
    {
        return 'orders_report.pdf';
    }

    public function getPagination()
    {
        return $this->smarty->fetch($this->getTemplate('pagination'));
    }
}
