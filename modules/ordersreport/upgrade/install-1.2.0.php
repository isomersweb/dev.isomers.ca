<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

function upgrade_module_1_2_0($object)
{
    $object = $object;
    if (!Configuration::updateValue('DNG_ORDER_EXPORT_PHONE', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_EMAIL', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PSUP', '1')) {
        return false;
    }
    return true;
}
