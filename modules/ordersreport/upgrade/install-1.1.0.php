<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

function upgrade_module_1_1_0($object)
{
    $object = $object;
    if (!Configuration::updateValue('DNG_ORDER_EXPORT_PREF', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PNAME', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PEAN', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PUPC', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PQUA', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_PPRI', '1')
        || !Configuration::updateValue('DNG_ORDER_EXPORT_ADDR', '1')) {
        return false;
    }
    return true;
}
