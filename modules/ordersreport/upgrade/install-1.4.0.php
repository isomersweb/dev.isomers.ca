<?php
/**
* 2014-2017 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

function upgrade_module_1_4_0($object)
{
    $object = $object;
    if (!Configuration::updateValue('DNG_ORDREP_ORDER_STATE_FILTER', '')
        || !Configuration::updateValue('DNG_ORDREP_CURRENCY_FILTER', '')
        || !Configuration::updateValue('DNG_ORDREP_SCRIPT_TIMEOUT', '60')
        || !Configuration::deleteByName('DNG_ORDER_DELIVERY_DATE', '0')
        || !Configuration::deleteByName('DNG_ORDER_INV_NUM', '0')
        ) {
        return false;
    }
    return true;
}
