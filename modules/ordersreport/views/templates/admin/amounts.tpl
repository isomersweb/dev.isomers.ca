{*
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*}

{assign var= "currencies" value = count($totals)}

{section name=i loop=$totals}
        
    {assign var="cur" value=$totals[i]['iso_code']}

    {if $currencies gt 1}
      
       <table style="width: 62%">
       <tr><td>
       <div style="background-color: #ddd; text-align: center; font-weight: bold">
             {$cur|escape:'htmlall':'UTF-8'}
       </div>
           </td>
           </tr>
       </table>
    {/if}

    <table style="width: 62%; text-align: left" class="table">
        <tr>
           <td style="width: 24%; text-align: left; font-weight: bold">
             {l s='Paid tax excluded' mod='ordersreport'}
           </td>
         <td style="width: 24%; text-align: left; font-weight: bold"></td>
         <td style="width: 4%; border: none"></td>
         <td style="width: 24%; text-align: left; font-weight: bold">
             {l s='Paid tax included' mod='ordersreport'}
            </td>
         <td style="width: 24%; text-align: left; font-weight: bold"></td>
        </tr>
        <tr>
           <td style="width: 24%; text-align: left; padding-left: 5px">&nbsp;
               {l s='Products' mod='ordersreport'}</td>
           <td style="width: 24%; text-align: right; padding-right: 5px">
               {$functions->displayPriceIR($totals[i]['products'])|escape:'htmlall':'UTF-8'}&nbsp;{$cur|escape:'htmlall':'UTF-8'}</td>
           <td style="width: 4%; border: none"></td>
           <td style="width: 24%; text-align: left; padding-left: 5px">&nbsp;
               {l s='Products' mod='ordersreport'}</td>
           <td style="width: 24%; text-align: right; padding-right: 5px">
               {$functions->displayPriceIR($totals[i]['products_wt'])|escape:'htmlall':'UTF-8'}&nbsp;{$cur|escape:'htmlall':'UTF-8'}</td>
        </tr>
        <tr>
           <td style="width: 24%; text-align: left; padding-left: 5px">&nbsp;
               {l s='Shipping' mod='ordersreport'}</td>
           <td style="width: 24%; text-align: right; padding-right: 5px">
               {$functions->displayPriceIR($totals[i]['shipping'])|escape:'htmlall':'UTF-8'}&nbsp;{$cur|escape:'htmlall':'UTF-8'}</td>
           <td style="width: 4%; border: none"></td>
           <td style="width: 24%; text-align: left; padding-left: 5px">&nbsp;
               {l s='Shipping' mod='ordersreport'}</td>
           <td style="width: 24%; text-align: right; padding-right: 5px">
               {$functions->displayPriceIR($totals[i]['shipping_wt'])|escape:'htmlall':'UTF-8'}&nbsp;{$cur|escape:'htmlall':'UTF-8'}</td>
        </tr>
        {if $show_wrapping}
            <tr>
                <td style="width: 24%; text-align: left; padding-left: 5px">&nbsp;
                    {l s='Wrapping' mod='ordersreport'}</td>
                <td style="width: 24%; text-align: right; padding-right: 5px">
                    {$functions->displayPriceIR($totals[i]['wrapping'])|escape:'htmlall':'UTF-8'}&nbsp;{$cur|escape:'htmlall':'UTF-8'}</td>
                <td style="width: 4%; border: none"></td>
                <td style="width: 24%; text-align: left; padding-left: 5px">&nbsp;
                    {l s='Wrapping' mod='ordersreport'}</td>
                <td style="width: 24%; text-align: right; padding-right: 5px">
                    {$functions->displayPriceIR($totals[i]['wrapping_wt'])|escape:'htmlall':'UTF-8'}&nbsp;{$cur|escape:'htmlall':'UTF-8'}</td>
            </tr>
        {/if}
        {assign var="disc" value=$totals[i]['discount']}
        {assign var="dsign" value=''}
        {if $disc gt 0}
            {$dsign = '-'}
        {/if}
        {assign var="disc_wt" value=$totals[i]['discount_wt']}
        {assign var="dsign_wt" value=''}
        {if $disc_wt gt 0}
            {$dsign_wt = '-'}
        {/if}
        <tr>
           <td style="width: 24%; text-align: left; padding-left: 10px">
               {l s='Discounts' mod='ordersreport'}</td>
           <td style="width: 24%; text-align: right; padding-right: 5px">{$dsign|escape:'htmlall':'UTF-8'}
               {$functions->displayPriceIR($disc)|escape:'htmlall':'UTF-8'}&nbsp;{$cur|escape:'htmlall':'UTF-8'}</td>
           <td style="width: 4%; border: none"></td>
           <td style="width: 24%; text-align: left; padding-left: 10px">
               {l s='Discounts' mod='ordersreport'}</td>
           <td style="width: 24%; text-align: right; padding-right: 5px">
               {$dsign_wt|escape:'htmlall':'UTF-8'}
               {$functions->displayPriceIR($disc_wt)|escape:'htmlall':'UTF-8'}&nbsp;{$cur|escape:'htmlall':'UTF-8'}</td>
        </tr>
        <tr style="font-weight: bold">
            <td style="width: 24%; text-align: left; padding-left: 10px">
                {l s='Total paid' mod='ordersreport'}
            </td>
            <td style="width: 24%; text-align: right; padding-right: 5px">
                {$functions->displayPriceIR($totals[i]['total'])|escape:'htmlall':'UTF-8'}
                {$cur|escape:'htmlall':'UTF-8'}</td>
            <td style="width: 4%; border: none"></td>
            <td style="width: 24%; text-align: left; padding-left: 10px">
                {l s='Total paid' mod='ordersreport'}
            </td>
            <td style="width: 24%; text-align: right; padding-right: 5px">
                {$functions->displayPriceIR($totals[i]['total_wt'])|escape:'htmlall':'UTF-8'}
                {$cur|escape:'htmlall':'UTF-8'}</td>
        </tr>
    </table>
      <br>
    {if $pdf eq false}
        <div><br></div>
    {/if}

{/section}