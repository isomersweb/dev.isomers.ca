{*
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*}

<style type="text/css">
    {literal}
        #orderstate_x:hover {text-decoration: none;}
    {/literal}
</style>

{assign var="filters" value=0}

{if $psver|substr:0:2 gte '16'}
<div id="orderstate" class="panel-collapse collapse">
{/if}
<table style="width: 55%" id="table_orderstatefilter" class="table orders">
    <thead>
    <tr class="nodrag nodrop">
        <th class="text-center" style="width: 5%; font-weight: bold">
            <span class="title_box">{l s='ID' mod='ordersreport'}</span>
        </th>
        <th class="" style="width: 80%; font-weight: bold">
            <span class="title_box">{l s='Order status' mod='ordersreport'}</span>
        </th>
        <th class="text-center" style="width: 10%; font-weight: bold">
            <span class="title_box">{l s='Filter selection' mod='ordersreport'}</span>
        </th>
        <th class="text-center" style="width: 5%; font-weight: bold">
            <span class="title_box">{l s='Count' mod='ordersreport'}</span>
        </th>
    </tr>
    </thead>
    <tbody>
    {section name=i loop=$section_orderstatefilter}
        {assign var="orderstate" value=$section_orderstatefilter[i]['id']}
        {if $orderstate gt 0}
        <tr>
            <td class="pointer text-center">
                {$orderstate|escape:'html':'UTF-8'}
              </td>
            <td class="pointer">
                {$section_orderstatefilter[i]['name']|escape:'html':'UTF-8'}
            </td>
            <td class="pointer text-center">
                {if $section_orderstatefilter[i]['enabled'] eq 1}
                    {$filters = $filters + 1}
                    <input type="checkbox" name="_orderstate_{$orderstate|escape:'htmlall':'UTF-8'}" id="_orderstate_{$orderstate|escape:'htmlall':'UTF-8'}" checked="true" />
                {else}
                    <input type="checkbox" name="_orderstate_{$orderstate|escape:'htmlall':'UTF-8'}" id="_orderstate_{$orderstate|escape:'htmlall':'UTF-8'}" />
                {/if}
            </td>
            <td class="pointer text-center">
                {$section_orderstatefilter[i]['count']|escape:'html':'UTF-8'}
            </td>
        </tr>
        {/if}
    {/section}
    </tbody>
</table>

{if $psver|substr:0:2 gte '16'}
</div>
<span id="orderstate_x">
    {if $filters gt 0}
    <div id="orderstate_i" class="alert alert-info">
        <strong>Filter selection counter!</strong>&nbsp;{l s='You have selected order status prefilters, counting: ' mod='ordersreport'}&nbsp;{$filters|escape:'html':'UTF-8'}
        <a class="pull pull-right" data-toggle="collapse" href="#orderstate">{l s='EXPAND' mod='ordersreport'}&nbsp;<i class="icon icon-chevron-down"></i></a>
    </div>
    {else}
        <a class="pull pull-right" data-toggle="collapse" href="#orderstate">{l s='EXPAND' mod='ordersreport'}&nbsp;<i class="icon icon-chevron-down"></i></a>
    {/if}
</span>

<script type="text/javascript">
$(document).ready(function() {
    $("#orderstate_x").on("click", function(event) {
        $("#orderstate_x").html("");
    });
});
</script>
{/if}
