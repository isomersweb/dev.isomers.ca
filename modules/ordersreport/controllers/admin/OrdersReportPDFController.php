<?php
/**
* 2014-2016 Denago d.o.o.
*
* NOTICE OF LICENSE
*
* Proprietary license by Denago d.o.o.
*
*  @author    Denago d.o.o. <info@denago.eu>
*  @copyright 2014-2016 Denago d.o.o.
*  @license   Proprietary license
*/

class OrdersReportPDFController extends AdminPdfControllerCore
{
    public function generateReport($orders)
    {
        /* hook for all third party pdf modules */
        Hook::exec('actionPDFInvoiceRender', array('order_invoice_list' => $orders));
        $this->generatePDF($orders, 'OrdersReport');
        exit;
    }

    public function printOrders($orders)
    {
        /*
        * Set error reporting to OFF for case when _PS_MODE_DEV_ == true
        * tools\tcpdf\tcpdf.php ERROR on line 22980 Undefined index: cols
        */
        error_reporting(0);
        $this->generatePDF($orders, PDF::TEMPLATE_INVOICE);
        exit;
    }
}
