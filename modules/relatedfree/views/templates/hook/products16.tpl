{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

    <div id="related_products_free" class="block products_block clearfix">
        <h4 class="title_block">{if $related_link==1}<a href="{$link->getCategoryLink($related_category, null, $id_lang)}">{/if}{l s='Related products' mod='relatedfree'}{if $related_link==1}</a>{/if}</h4>
            {if isset($blocks_products) && $blocks_products}
            	{include file="$tpl_dir./product-list.tpl" class="related_products tab-pane" id="related_products" products=$blocks_products}
            {else}
                <ul class="related_products tab-pane">
                	<li class="alert alert-info">{l s='No products at this time.' mod='relatedfree'}</li>
                </ul>
            {/if}
    </div>