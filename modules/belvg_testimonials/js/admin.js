(function($){
	$(".multishop_toolbar").hide();

	$('#id_product').live('keyup', function(){
		var val = $.trim($(this).val());
		if (val.length) {
			$.ajax({
				url: 'ajax_products_list.php',
				data: {q: encodeURIComponent(val), limit: 20},
				success: function(data) {
					$('.ajax-result').html('').hide();
					var products = data.split('\n');
					for (var i = 0; i < products.length - 1; i++) {
						var _product = products[i].split('|');
						$('.ajax-result').append('<a href="javascript:;" rel="'+_product[1]
							+'"><img src="../img/admin/add.gif"> <span>'+_product[0]+'</span></a><br>').show();
					}
				}
			});
		} else {
			$('.ajax-result').html('').hide();
		}
	})
	.attr('autocomplete', 'off')
	.parent()
	.append('<a class="product-clear" href="javascript:;"><img src="../img/admin/disabled.gif" /></a>')
	.append($('<div class="ajax-result">'));

	$('div.ajax-result a').live('click', function(){
		$('#id_product').val($(this).attr('rel')+'. '+$(this).find('span').text()).attr('readonly', 'readonly');
		$('.ajax-result').html('').hide();
		$('.product-clear').show();
	});

	$('.product-clear').click(function(){
		$('#id_product').val('').attr('readonly', false);
		$(this).hide();
	});

	if ($('#id_product').val() != '') {
		$('#id_product').attr('readonly', 'readonly');
	} else {
		$('.product-clear').hide();
	}
	
	$('#belvg_TP_topproduct_form').submit(function(){ 
		if ($('#id_product').attr('readonly') != 'readonly') {
			$('#id_product').focus();
			submited = false;
			return false;
		}
	});
})(jQuery);