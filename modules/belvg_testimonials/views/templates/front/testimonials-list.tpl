<div class="testimonials-wrapper">
    {foreach from=$testimonials item=testimonial name=testimonials}
    <div class="testimonial-item clearfix">
        <div class="col-sm-3">
            <p>{$testimonial.name}, {$testimonial.location}</p>
            <p class="text-grey">{$testimonial.date_add|date_format:"%b, %Y"}</p>
        </div>
        <div class="col-sm-9 ts-content">
            {$testimonial.message}
        </div>
    </div>
    {/foreach}
</div>

{*<ul id="testimonials_list" class="testimonials_list" class="clear">
    {foreach from=$testimonials item=testimonial name=testimonials}
    <li class="block_testimonials {if $smarty.foreach.testimonials.first}first_item{elseif $smarty.foreach.testimonials.last}last_item{/if} {if $smarty.foreach.testimonials.index % 2}alternate_item{else}item{/if} clearfix">
        <div class="testimonials_name"><h4>{$testimonial.name}, {$testimonial.location}</h4></div>
        {if $testimonial.site|replace:'http://':''}<div class="testimonials_site"><a href="{$testimonial.site}">{$testimonial.site}</a></div>{/if}
        <br/>
        <div class="testimonials_message">{$testimonial.message}</div>
        <div class="testimonials_date">{$testimonial.date_upd}</div>
    </li>
    {/foreach}
</ul>
*}