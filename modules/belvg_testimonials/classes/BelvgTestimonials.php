<?php

/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
*         DISCLAIMER   *
* *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* *****************************************************
* @category   Belvg
* @package    BelvgTestimonials
* @author     Dzianis Yurevich (dzianis.yurevich@gmail.com)
* @site       http://module-presta.com
* @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
* @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
*/

require_once _PS_MODULE_DIR_ . 'belvg_testimonials/includer.php';

class BelvgTestimonials extends ObjectModel
{
    public $id;
    public $id_belvg_testimonials;
    public $name;
    public $email;
    public $site;
    public $status = 1;
    public $message;
    public $location;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => "belvg_testimonials",
        'primary' => 'id_belvg_testimonials',
        'multilang' => TRUE,
        'fields' => array(
            'name' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => TRUE),
            'email' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => TRUE),
            'site' => array('type' => self::TYPE_STRING, 'validate' => 'isUrl'),
            'status' => array('type' => self::TYPE_INT),
            'date_add' => array('type' => self::TYPE_DATE),
            'date_upd' => array('type' => self::TYPE_DATE),
            'message' => array(
                'type' => self::TYPE_HTML,
                'lang' => TRUE,
                'validate' => 'isString',
                'size' => 3999999999999,
                'required' => TRUE),
            'location' => array(
                'type' => self::TYPE_STRING,
                'lang' => TRUE,
                'validate' => 'isAddress',
                'required' => TRUE)
            )
        );

    public function __construct($id = NULL, $id_lang = NULL)
    {
        self::$definition['table'] = belvg_testimonials::PREFIX . 'testimonials';
        Shop::addTableAssociation(self::$definition['table'], array('type' => 'shop'));
        parent::__construct($id, $id_lang, NULL);
    }

    public function toggleStatus()
    {
        if ($this->status) {
            $this->status = 0;
        } else {
            $this->status = 1;
        }

        return $this->save();
    }

    public static function getActiveRandomTestimonials($count)
    {
        $testimonials = Db::getInstance()->ExecuteS('
            SELECT `' . self::$definition['primary'] . '`
            FROM `' . _DB_PREFIX_ . belvg_testimonials::PREFIX . 'testimonials' . '` f
            WHERE `status` = 1 ' . (Shop::isFeatureActive() ? 'AND f.`id_belvg_testimonials` IN (
                SELECT sa.id_belvg_testimonials
                FROM `' . _DB_PREFIX_ . 'belvg_testimonials_shop` sa
                WHERE sa.id_shop IN (' . (int)Context::getContext()->shop->id . '))'  : '') . '
            ORDER BY RAND()
            LIMIT ' . (int)$count . '
        ');

        $_testimonials = array();
        foreach ($testimonials as $testimonial) {
            $_testimonials[] = new self($testimonial[self::$definition['primary']], Context::getContext()->language->id);
        }

        return $_testimonials;
    }

    public static function getTestimonials($id_lang, $p, $n, $sortBy = NULL, $searchBy = NULL, $get_total = FALSE, $active = TRUE, Context $context = NULL)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        if ($get_total) {
            $sql = 'SELECT COUNT(f.`id_belvg_testimonials`) AS total
                    FROM `' . _DB_PREFIX_ . 'belvg_testimonials` f
                    LEFT JOIN `' . _DB_PREFIX_ . 'belvg_testimonials_lang` fl ON (f.`id_belvg_testimonials` = fl.`id_belvg_testimonials` AND fl.`id_lang` = ' . (int)$id_lang . ')
                    WHERE 1 ' . (Shop::isFeatureActive() ? 'AND f.`id_belvg_testimonials` IN (
                        SELECT sa.id_belvg_testimonials
                        FROM `' . _DB_PREFIX_ . 'belvg_testimonials_shop` sa
                        WHERE sa.id_shop IN (' . (int)Context::getContext()->shop->id . '))'  : '') .
                    ($searchBy ? ' AND (fl.message LIKE "%' . pSQL($searchBy) . '%")' : '') .
                    ($active ? ' AND f.status = 1' : '');

            return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }

        $sql = 'SELECT f.*, fl.*
        FROM `' . _DB_PREFIX_ . 'belvg_testimonials` f
        LEFT JOIN `' . _DB_PREFIX_ . 'belvg_testimonials_lang` fl ON (f.`id_belvg_testimonials` = fl.`id_belvg_testimonials` AND fl.`id_lang` = ' . (int)$id_lang . ')
        WHERE 1 ' .
        (Shop::isFeatureActive() ? 'AND f.`id_belvg_testimonials` IN (
                        SELECT sa.id_belvg_testimonials
                        FROM `' . _DB_PREFIX_ . 'belvg_testimonials_shop` sa
                        WHERE sa.id_shop IN (' . (int)Context::getContext()->shop->id . '))'  : '') .
        ($active ? ' AND f.status = 1' : '') .
        ($searchBy ? ' AND (fl.message LIKE "%' . pSQL($searchBy) . '%")' : '') . '
        ORDER BY f.`date_add` DESC
        LIMIT ' . (((int)$p - 1) * (int)$n) . ',' . (int)$n;
        //print_r($sql); die;
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
}
