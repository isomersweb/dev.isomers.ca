<script>
	(function($){
		$(document).ready(function(){
			if ($('#block_various_links_footer').length) {
				$('#block_various_links_footer ul li.first_item').after('<li class="item"><a href="{$link->getModuleLink('belvg_testimonials', 'view', [])}">{l s='Testimonials' mod='belvg_testimonials'}</a></li>');
			}
		});
	})(jQuery);
</script>