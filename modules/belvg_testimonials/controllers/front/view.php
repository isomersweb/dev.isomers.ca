<?php

class belvg_testimonialsviewModuleFrontController extends ModuleFrontController {

    public $auth = FALSE;
    public $authRedirection = 'view';
    public $ssl = FALSE;
    public $direction;

    protected $_module = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->context = Context::getContext();
    }

    public function getModule()
    {
        if (is_NULL($this->_module)) {
            $this->_module = new belvg_testimonials;
        }

        return $this->_module;
    }

    protected function l($string, $class = 'belvg_testimonialsviewModuleFrontController', $addslashes = FALSE, $htmlentities = TRUE)
    {
        return $this->getModule()->l($string, $class, $addslashes, $htmlentities);
    }

    public function init()
    {
        parent::init();
        require_once ($this->module->getLocalPath() . 'includer.php');
    }

    public function initContent()
    {
        parent::initContent();

        $this->productSort();
        $this->sortBy = Tools::strtolower(Tools::getValue('sortby'));
        $this->searchBy = Tools::strtolower(trim(Tools::getValue('searchby')));

        $nbTestimonials = BelvgTestimonials::getTestimonials($this->context->language->id, NULL, NULL, $this->sortBy, $this->searchBy, TRUE);

        $this->pagination($nbTestimonials);
        $params = array();
        if (!empty($this->sortBy)) {
            $params['sortby'] = $this->sortBy;
        }

        if (!empty($this->searchBy)) {
            $params['searchby'] = $this->searchBy;
        }

        $this->context->smarty->assign(array(
            'testimonials' => BelvgTestimonials::getTestimonials($this->context->language->id, (int)$this->p, (int)$this->n, $this->sortBy, $this->searchBy),
            'nbTestimonials' => (int)($nbTestimonials),
            'sortBy' => $this->sortBy,
            'searchBy' => $this->searchBy,
            'testimonials_tpl_dir' => _PS_ROOT_DIR_ . '/modules/belvg_testimonials/views/templates/front',
            'requestPage' => Context::getContext()->link->getModuleLink('belvg_testimonials', 'view', $params),
            'request' => Context::getContext()->link->getModuleLink('belvg_testimonials', 'view'),
            'customerName' => ($this->context->customer->logged ? $this->context->customer->firstname . ' ' . $this->context->customer->lastname : FALSE),
            'customerEmail' => ($this->context->customer->logged ? $this->context->customer->email : FALSE),
        ));

        if (isset($this->context->cookie->submit_success) && !empty($this->context->cookie->submit_success)) {
            $this->context->smarty->assign(array(
                'submit_success' => 1,
            ));
            $this->context->cookie->submit_success = NULL;
        } elseif (isset($this->context->cookie->submit_error) && !empty($this->context->cookie->submit_success)) {
            $this->context->smarty->assign(array(
                'submit_error' => 1,
            ));
        }

        $this->setTemplate('view.tpl');
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submit_new_question')) {
            foreach ($_POST as &$post) {
                if (is_string($post)) {
                    $post = trim($post);
                }
            }

            $name = Tools::getValue('name');
            $email = Tools::getValue('email');
            $location = Tools::getValue('location');
            //$_POST['site'] = ltrim($_POST['site'], 'http://');
            //$site = 'http://' . Tools::getValue('site');
            $message = pSQL(Tools::getValue('message'));
            $validate = array();

            if (empty($name) || !Validate::isName($name) || strlen($name) > 128) {
                $validate[] = $this->l('Name is not valid');
            }

            if (!Validate::isEmail($email)) {
                $validate[] = $this->l('Email is not valid');
            }

            /*if (!empty($site) && (!Validate::isUrl($site) || strlen($site) > 128)) {
                $validate[] = $this->l('Site is not valid');
            }*/

            if (empty($location) || !Validate::isAddress($location) || strlen($location) > 128) {
                $validate[] = $this->l('Location is not valid');
            }

            if (empty($message)) {
                $validate[] = $this->l('Testimonials is not valid');
            }

            if (!Validate::isCleanHtml($message)) {
                $validate[] = $this->l('Invalid testimonials');
            }

            if (empty($validate)) {
                $object = new BelvgTestimonials();
                $object->status = 0;
                $object->name = pSQL($name);
                $object->email = pSQL($email);
                //$object->site = pSQL($site);
                $languages = Language::getLanguages();
                foreach ($languages as $language) {
                    $object->message[$language['id_lang']] = nl2br($message);
                    $object->location[$language['id_lang']] = pSQL($location);
                }

                if ($object->add()) {
                    $this->context->cookie->submit_success = 1;
                } else {
                    $this->context->cookie->submit_error = 1;
                }
                $mail = Configuration::get('PS_SHOP_EMAIL');
                $id_lang = Context::getContext()->language->id;
                $subject = $this->l('New testimonial added');
                $name = Configuration::get('PS_SHOP_NAME');
                
                $Name = Configuration::get('PS_SHOP_NAME');
                $email = Configuration::get('PS_SHOP_EMAIL');
                $mail_body = $this->l('New testimonial was added. Please moderate it via backend.'); 
                $subject = $this->l('Testimonials Moderation');
                $header = "From: ". $Name . " <" . $email . ">\r\n"; 

                mail($email, $subject, $mail_body, $header);

                Tools::redirect($this->context->link->getModuleLink('belvg_testimonials', 'view'));
            } else {
                $this->context->smarty->assign(array(
                    'submit_error' => 1,
                    'validate' => $validate,
                ));
            }
        }
    }
}

