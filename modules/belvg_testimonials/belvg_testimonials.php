<?php

/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
*         DISCLAIMER   *
* *************************************** */
/* Do not edit or add to this file if you wish to upgrade Prestashop to newer
* versions in the future.
* ****************************************************
* @package    belvg_testimonials
* @author     Dzianis Yurevich (dzianis.yurevich@gmail.com)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_ . 'eicaptcha/eicaptcha.php';
require_once _PS_MODULE_DIR_ . 'belvg_testimonials/includer.php';

class belvg_testimonials extends Module
{
    const PREFIX = 'belvg_';
    const DISPLAY_COUNT = 'count';
    const DISPLAY_COLUMN = 'column';

    protected $_columns = array('left', 'right');
    protected $_display_count = 5;

    protected $_hooks = array(
        'displayLeftColumn',
        'displayRightColumn',
        'displayHeader');

    protected $_tabs = array(array(
            'class_name' => 'AdminBelvgTestimonials',
            'parent' => 'AdminParentCustomer',
            'name' => 'Testimonials'));

    public function __construct()
    {
        $this->name = 'belvg_testimonials';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'BelVG';
        $this->need_instance = 0;
        $this->module_key = '';

        parent::__construct();

        $this->displayName = $this->l('Belvg Testimonials');
        $this->description = $this->l('Belvg Testimonials');
    }

    public static function getDbPrefix()
    {
        return _DB_PREFIX_ . self::PREFIX;
    }

    public function getDir($file = '')
    {
        return _PS_MODULE_DIR_ . $this->name . DIRECTORY_SEPARATOR . $file;
    }

    public function install()
    {
        $sql = include ($this->getDir('sql/install.php'));
        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }

        $languages = Language::getLanguages();
        foreach ($this->_tabs as $tab) {
            $_tab = new Tab();
            $_tab->class_name = $tab['class_name'];
            $_tab->id_parent = Tab::getIdFromClassName($tab['parent']);
            $_tab->module = $this->name;
            foreach ($languages as $language) {
                $_tab->name[$language['id_lang']] = $this->l($tab['name']);
            }

            $_tab->add();
        }

        $install = parent::install();
        foreach ($this->_hooks as $hook) {
            if (!$this->registerHook($hook)) {
                return FALSE;
            }
        }

        self::setConfig(self::DISPLAY_COUNT, $this->_display_count, 0, 0);
        self::setConfig(self::DISPLAY_COLUMN, current($this->_columns), 0, 0);

        return $install;
    }

    public function uninstall()
    {
        $sql = include ($this->getDir('sql/uninstall.php'));
        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }

        foreach ($this->_tabs as $tab) {
            $idTab = Tab::getIdFromClassName($tab['class_name']);
            if ($idTab) {
                $_tab = new Tab($idTab);
                $_tab->delete();
            }
        }

        self::deleteByName(self::DISPLAY_COUNT);
        self::deleteByName(self::DISPLAY_COLUMN);

        $uninstall = parent::uninstall();
        foreach ($this->_hooks as $hook) {
            if (!$this->unregisterHook($hook)) {
                return FALSE;
            }
        }

        return $uninstall;
    }

    public static function getConfig($name)
    {
        return Configuration::get(self::PREFIX . $name);
    }

    public static function setConfig($name, $value, $id_shop_group = NULL, $id_shop = NULL)
    {
        return Configuration::updateValue(self::PREFIX . $name, $value, FALSE, $id_shop_group, $id_shop);
    }

    public static function deleteByName($name)
    {
        return Configuration::deleteByName(self::PREFIX . $name);
    }

    protected function _displayTestimonials($column)
    {
        if (self::getConfig(self::DISPLAY_COLUMN) == $column) {
            $_tesimonials = BelvgTestimonials::getActiveRandomTestimonials(self::getConfig(self::
                DISPLAY_COUNT));
            if (count($_tesimonials)) {
                $this->context->smarty->assign('belvg_testimonials', $_tesimonials);
                return $this->display(__file__, 'tpl/block.tpl');
            }
        }

        return NULL;
    }

    public function hookDisplayLeftColumn($params)
    {
        return $this->_displayTestimonials('left');
    }

    public function hookDisplayRightColumn($params)
    {
        return $this->_displayTestimonials('right');
    }

    public function hookDisplayHeader($params)
    {
        $this->context->controller->addCss($this->_path . 'css/front.css', 'all');
        return $this->display(__FILE__, 'tpl/header.tpl');
    }

    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit($this->name)) {
            $data = Tools::getValue($this->name);
            if (isset($data[self::DISPLAY_COUNT]) and $count = (int)$data[self::
                DISPLAY_COUNT]) {
                self::setConfig(self::DISPLAY_COUNT, abs($count));
                self::setConfig(self::DISPLAY_COLUMN, $data[self::DISPLAY_COLUMN]);
                $output = $this->displayConfirmation($this->l('Settings updated'));
            } else {
                $output = $this->displayError($this->l('Validation error!'));
            }
        }

        $this->context->smarty->assign('belvg_output', $output);
        $this->context->smarty->assign('belvg_testimonials', $this);
        return $this->display(__file__, 'tpl/admin.tpl');
    }
}
