<?php
/**
* Change the currency Store for GEOIP
*
*/

class CartController extends CartControllerCore
{
    protected function processChangeProductInCart()
    {
        if (!Module::isInstalled('currencybycountry')) {
            return ParentOrderControllerCore::processChangeProductInCart();
        }
        $cbc_module = Module::getInstanceByName('currencybycountry');
        if (!$cbc_module->active) {
            return CartControllerCore::processChangeProductInCart();
        }
        $id_country = (int)Country::getByIso(Tools::strtoupper($cbc_module->getCookieGeoip()));
        if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
            SELECT * FROM `'._DB_PREFIX_.'cbc_product_banned`
            WHERE
                id_country = '.(int)$id_country.' AND
                id_shop = '.(int)$this->context->shop->id.' AND
                id_product='.(int)$this->id_product)) {
            foreach ($result as $row) {
                $this->errors[] = Tools::displayError($cbc_module->countryProductError(), !Tools::getValue('ajax'));
                return;
            }
        }
        if ($this->context->cart && (int)$this->context->cart->id_address_delivery > 0) {
            $delivery = new Address((int)$this->context->cart->id_address_delivery);
            if (Validate::isLoadedObject($delivery) && (int)$delivery->id_country > 0) {
                if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
                    SELECT * FROM `'._DB_PREFIX_.'cbc_product_banned`
                    WHERE
                        id_country = '.(int)$delivery->id_country.' AND
                        id_shop = '.(int)$this->context->shop->id.' AND
                        id_product='.(int)$this->id_product)) {
                    foreach ($result as $row) {
                        $this->errors[] = Tools::displayError($cbc_module->countryProductError(), !Tools::getValue('ajax'));
                        return;
                    }
                }
            }
        }
        return CartControllerCore::processChangeProductInCart();
    }
}
