<?php
/**
* Change the currency Store for GEOIP
*
*/

class OrderOpcController extends OrderOpcControllerCore
{
    protected function _getPaymentMethods()
    {
        if (!Module::isInstalled('currencybycountry')) {
            return OrderOpcControllerCore::_getPaymentMethods();
        }
        $cbc_module = Module::getInstanceByName('currencybycountry');
        if (!$cbc_module->active) {
            return OrderOpcControllerCore::_getPaymentMethods();
        }
        $id_country = (int)Country::getByIso(Tools::strtoupper($cbc_module->getCookieGeoip()));
        if ($this->context->cart && (int)$this->context->cart->id_address_delivery > 0) {
            $delivery = new Address((int)$this->context->cart->id_address_delivery);
            if (Validate::isLoadedObject($delivery) && (int)$delivery->id_country > 0) {
                if ($products = $this->context->cart->getProducts()) {
                    $result = '';
                    $ids_products = array();
                    foreach ($products as &$product) {
                        if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
                            SELECT * FROM `'._DB_PREFIX_.'cbc_product_banned`
                            WHERE
                                id_country = '.(int)$delivery->id_country.' AND
                                id_shop = '.(int)$this->context->shop->id.' AND
                                id_product='.(int)$product['id_product'])) {
                            foreach ($result as $row) {
                                if (!in_array($product['id_product'], $ids_products)) {
                                    $ids_products[] = $product['id_product'];
                                    $result .= '<p class="warning">'.Tools::displayError($product['name'].': '.$cbc_module->countryProductError()).'</p>';
                                }
                            }
                        }
                    }
                    if (count($ids_products) > 0) {
                        return $result;
                    }
                }
            }
        }
        return OrderOpcControllerCore::_getPaymentMethods();
    }
}
