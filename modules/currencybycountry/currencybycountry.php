<?php
/**
* Change the currency Store for GEOIP
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Currencybycountry extends Module
{
    private static $site_url;
    private static $reload_site;
    private $actual;
    private $actual_product;

    public function __construct()
    {
        $this->name = 'currencybycountry';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'pkrava';
        $this->module_key = '532198ff06fd3e6d31f9316c2d8466f7';

        parent::__construct();

        $this->displayName = $this->l('Products purchase restriction');
        $this->description = $this->l('Change the currency Store automatically and locks products by country using IP.');

        self::$site_url = Tools::htmlentitiesutf8((Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].__PS_BASE_URI__);

        if (Module::isInstalled('currencybycountry')) {
            if ((bool)Configuration::get('PS_DETECT_COUNTRY')) {
                Configuration::updateValue('PS_DETECT_COUNTRY', false);
            }
            if ((bool)Configuration::get('PS_GEOLOCATION_ENABLED')) {
                Configuration::updateValue('PS_GEOLOCATION_ENABLED', false);
            }
            if (self::$reload_site == null) {
                self::$reload_site = false;
            }
            $actual_currency = $this->context->cookie->id_currency;
            $this->actual = $this->getCurrencyCountry();
            $this->actual_product = $this->getProductCountry();
            $iso_country = Tools::strtoupper($this->getCookieGeoip());
            $id_country = Country::getByIso($iso_country);
            if (!isset($this->context->cookie->currencybycountry) || (int)$this->context->cookie->currencybycountry != (int)$this->context->shop->id) {
                $this->context->cookie->currencybycountry = (int)$this->context->shop->id;
                if (isset($this->actual['currency'][$this->context->shop->id][$id_country]) && $this->actual['currency'][$this->context->shop->id][$id_country] > 0) {
                    $currency = new Currency((int)$this->actual['currency'][$this->context->shop->id][$id_country]);
                    if ($actual_currency != $currency->id) {
                        self::$reload_site = true;
                    }
                    $this->context->cookie->id_currency = $currency->id;
                    $this->context->cookie->write();
                    if ((int)$this->context->cookie->id_cart) {
                        $cart = new Cart($this->context->cookie->id_cart);
                        if (Validate::isLoadedObject($cart)) {
                            $cart->id_currency = (int)$currency->id;
                            $cart->update();
                        }
                    }
                    if (self::$reload_site && isset($_SERVER['HTTP_USER_AGENT']) && !preg_match('/bot|crawl|slurp|spider/i', $_SERVER['HTTP_USER_AGENT'])) {
                        echo '<form id="setCurrencyCBC" action="" method="post">
                                <input type="hidden" name="id_currency" id="id_currency" value="<?php echo $currency->id; ?>">
                                <input type="hidden" name="SubmitCurrency" value="">
                        </form>
                        <script>document.getElementById("setCurrencyCBC").submit()</script>';
                        exit;
                    }
                }
            }
        }

        /** Backward compatibility */
        require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');
    }
    public function countryProductError()
    {
        return $this->l('This product can not be purchased in your country.');
    }

    public function getCookieGeoip()
    {
        if (!isset($this->context->cookie->cbc_iso_country)) {
            $json = @Tools::jsonDecode(@Tools::file_get_contents('http://ip.kijam.com/geoip/'.Tools::getRemoteAddr()), true);
            if (isset($json['country_code'])) {
                $this->context->cookie->cbc_iso_country = $json['country_code'];
                return $json['country_code'];
            }
            return 'US';
        }
        return $this->context->cookie->cbc_iso_country;
    }

    public function install()
    {
        $db_created = Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'currencybycountry` (
                `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `id_currency` INT(11) NOT NULL,
                `id_shop` INT(11) NOT NULL,
                `id_country` INT(11) NOT NULL,
                `force` INT(1) NOT NULL,
                INDEX(id_currency),
                INDEX(id_shop),
                INDEX(id_country)
                )');

        if ($db_created) {
            $db_created = Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'cbc_product_banned` (
                `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `id_shop` INT(11) NOT NULL,
                `id_country` INT(11) NOT NULL,
                `id_product` INT(1) NOT NULL,
                INDEX(id_shop),
                INDEX(id_country),
                INDEX(id_product)
                )');
        }

        if (!$db_created) {
            $this->_errors[] = $this->l('Failed to create the table in the Database');
        }

        $result = $db_created && parent::install()
            && $this->registerHook('actionProductListOverride')
            && $this->registerHook('displayAdminProductsExtra')
            && $this->registerHook('actionProductSave')
            && (_PS_VERSION_ < '1.5'?$this->registerHook('header'):$this->registerHook('displayHeader'));

        if (!$result && $db_created) {
            Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'currencybycountry`');
            Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'cbc_product_banned`');
        } else {
            Configuration::updateValue('PS_DETECT_COUNTRY_GEO', Configuration::get('PS_DETECT_COUNTRY'));
            Configuration::updateValue('PS_GEOLOCATION_ENABLED_GEO', Configuration::get('PS_GEOLOCATION_ENABLED'));
            if ((bool)Configuration::get('PS_DETECT_COUNTRY')) {
                Configuration::updateValue('PS_DETECT_COUNTRY', false);
            }
            if ((bool)Configuration::get('PS_GEOLOCATION_ENABLED')) {
                Configuration::updateValue('PS_GEOLOCATION_ENABLED', false);
            }
        }

        return $result;
    }

    public function uninstall()
    {
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'currencybycountry`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'cbc_product_banned`');
        Configuration::updateValue('PS_DETECT_COUNTRY', Configuration::get('PS_DETECT_COUNTRY_GEO'));
        Configuration::updateValue('PS_GEOLOCATION_ENABLED', Configuration::get('PS_GEOLOCATION_ENABLED_GEO'));
        return (parent::uninstall());
    }
    public function upgradeOverride()
    {
        @copy(dirname(__FILE__).'/../../override/controllers/front/CartController.php', dirname(__FILE__).'/override/'.time().'-old_cartcontroller.txt');
        @copy(dirname(__FILE__).'/../../override/controllers/front/OrderOpcController.php', dirname(__FILE__).'/override/'.time().'-old_orderopccontroller.txt');
        @copy(dirname(__FILE__).'/../../override/controllers/front/ParentOrderController.php', dirname(__FILE__).'/override/'.time().'-old_parentordercontroller.txt');
        try {
            $this->uninstallOverrides();
        } catch (Exception $e) {
            $this->_errors[] = sprintf(Tools::displayError('Unable to uninstall override: %s'), $e->getMessage());
            return false;
        }
        try {
            $result = $this->installOverrides();
            if (!$result) {
                $this->_errors[] = Tools::displayError('Unable to install override: Prestashop Error');
            } else {
                @unlink(dirname(__FILE__).'/../../cache/class_index.php');
            }
            return $result;
        } catch (Exception $e) {
            $this->_errors[] = sprintf(Tools::displayError('Unable to install override: %s'), $e->getMessage());
            $this->uninstallOverrides();
            return false;
        }
    }
    private function getCurrencyCountry()
    {
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'currencybycountry`');
        $return = array('currency' => array(), 'force' => array());
        if ($result) {
            foreach ($result as $row) {
                $return['currency'][(int)$row['id_shop']][(int)$row['id_country']] = (int)$row['id_currency'];
                $return['force'][(int)$row['id_shop']][(int)$row['id_country']] = (bool)$row['force'];
            }
        }
        return $return;
    }

    private function getProductCountry()
    {
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'cbc_product_banned`');
        $return = array();
        if ($result) {
            foreach ($result as $row) {
                $return[(int)$row['id_product']][(int)$row['id_shop']][(int)$row['id_country']] = true;
            }
        }

        return $return;
    }

    public function hookDisplayHeader($params)
    {
        return $this->hookHeader($params);
    }

    public function hookHeader($params)
    {
        $id_country = (int)Country::getByIso(Tools::strtoupper($this->getCookieGeoip()));

        if ($id_country > 0 && isset($this->actual['currency'][$this->context->shop->id][$id_country])
            && $this->actual['currency'][$this->context->shop->id][$id_country] > 0
            && isset($this->actual['force'][$this->context->shop->id][$id_country])
            && $this->actual['force'][$this->context->shop->id][$id_country]) {
                $this->context->cookie->currencybycountry = 0;
                return '<script>$(document).ready(function() { $("#currencies-block-top").hide(0); }); </script>';
        }

        return '';
    }

    public function hookActionProductListOverride($params)
    {
        $id_country = (int)Country::getByIso(Tools::strtoupper($this->getCookieGeoip()));
        $id_category = Tools::getValue('id_category');
        $id_lang = $this->context->language->id;
        $id_shop = $this->context->shop->id;
        $category_obj = new Category($id_category, $id_lang, $id_shop);
        $n = abs((int)(Tools::getValue('n', ((isset($this->context->cookie->nb_item_per_page) && $this->context->cookie->nb_item_per_page >= 10) ? $this->context->cookie->nb_item_per_page : (int)Configuration::get('PS_PRODUCTS_PER_PAGE')))));
        $p = abs((int)Tools::getValue('p', 1));
        $order_by_values = array(0 => 'name', 1 => 'price', 2 => 'date_add', 3 => 'date_upd', 4 => 'position', 5 => 'manufacturer_name', 6 => 'quantity', 7 => 'ean13');
        $order_way_values = array(0 => 'asc', 1 => 'desc');
        $order_by = Tools::strtolower(Tools::getValue('orderby', $order_by_values[(int)Configuration::get('PS_PRODUCTS_ORDER_BY')])); //here it is assumed that we get a new value for sorting
        $order_way = Tools::strtolower(Tools::getValue('orderway', $order_way_values[(int)Configuration::get('PS_PRODUCTS_ORDER_WAY')]));
        $products = $category_obj->getProducts($id_lang, (int)$p, (int)$n, $order_by, $order_way);

        foreach ($products as &$product) {
            if (isset($this->actual_product[(int)$product['id_product']][(int)$id_shop][(int)$id_country])) {
                $product['available_for_order'] = false;
            }
        }

        $params['catProducts'] = $products;
        $params['nbProducts'] = $category_obj->getProducts(null, null, null, $order_by, $order_way, true);
        $params['hookExecuted'] = true; //please note that if this value is not set as TRUE , then in any case  the standard method Category::getProducts  is called
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
            $id_lang = $this->context->language->id;
            $shops = Shop::getShops();
            $countries = Country::getCountries($id_lang, true);

            $this->smarty->assign('shops', $shops);
            $this->smarty->assign('countries', $countries);
            $this->smarty->assign('actual_product', $this->actual_product[$product->id]);
            return $this->display(__FILE__, 'views/templates/admin/product_extra.tpl');
        }
    }
    public function hookActionProductSave($params)
    {
        if (Tools::getValue('banned_submit')) {
            $id_product = Tools::getValue('id_product');
            $banned = Tools::getValue('banned');
            foreach (Shop::getShops() as $row) {
                Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('DELETE FROM `'._DB_PREFIX_.'cbc_product_banned` WHERE `id_shop` = '.(int)$row['id_shop'].' AND id_product='.(int)$id_product);
            }
            if ($banned) {
                foreach ($banned as $id_shop => $row) {
                    foreach ($row as $id_country => $dummy) {
                        Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('INSERT INTO `'._DB_PREFIX_.'cbc_product_banned`
                                        (`id_shop`, `id_country`, `id_product`)
                                        VALUES
                                        ('.(int)$id_shop.', '.(int)$id_country.', '.(int)$id_product.')');
                    }
                }
            }
        }
    }

    public function getContent()
    {
        if (Tools::getValue('field')) {
            $forces = Tools::getValue('force');
            foreach (Tools::getValue('field') as $id_shop => $row) {
                foreach ($row as $id_country => $id_currency) {
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('
                        DELETE FROM `'._DB_PREFIX_.'currencybycountry`
                        WHERE
                            `id_shop` = '.(int)$id_shop.' AND
                            `id_country` = '.(int)$id_country);
                    $force = isset($forces[$id_shop]) && isset($forces[$id_shop][$id_country]) && (int)$forces[$id_shop][$id_country] > 0?1:0;
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('INSERT INTO `'._DB_PREFIX_.'currencybycountry`
                                (`id_shop`, `id_country`, `id_currency`, `force`)
                                VALUES
                                ('.(int)$id_shop.', '.(int)$id_country.', '.(int)$id_currency.', '.(int)$force.')');
                }
            }
            $this->actual = $this->getCurrencyCountry();
        }

        $id_lang = $this->context->language->id;
        $shops = Shop::getShops();
        $countries = Country::getCountries($id_lang, true);
        $currencies = Currency::getCurrencies(true);
        $actual = $this->actual;

        $str = '<link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.css">
                <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10-dev/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.js"></script>
                <script>
                    $(document).ready(function() {
                        $("#tblSB").dataTable({
                            "iDisplayLength": -1,
                            "aLengthMenu": [[-1], ["'.$this->l('All').'"]]
                            });
                    } );
                </script><h2>'.$this->displayName.'</h2>';

        $str .= '<form action="'.Tools::htmlentitiesutf8($_SERVER['REQUEST_URI']).'" method="post">';
        $str .= '<fieldset class="width">';
        $str .= '<table class="table table-striped table-bordered" id="tblSB">
                <thead><tr><th>'.$this->l('Shop').'</th><th>'.$this->l('Country').'</th><th>'.$this->l('Default Currency').'</th>';

        foreach ($currencies as &$currency) {
            $str .= "<th>{$currency->name}</th>";
        }

        $str .= '<th>'.$this->l('Force Currency').'</th></tr></thead><tbody>';
        if ($shops) {
            foreach ($shops as &$shop) {
                foreach ($countries as &$country) {
                    $str .= "<tr><td>{$shop['name']}</td><td>{$country['name']}</td>";
                    $str .= '<td><input '.(!isset($actual['currency'][$shop['id_shop']][$country['id_country']]) || $actual['currency'][$shop['id_shop']][$country['id_country']] == -1?'checked="checked"':'')." type='radio' name='field[{$shop['id_shop']}][{$country['id_country']}]' value='-1' /></td>";

                    foreach ($currencies as &$currency) {
                        $str .= '<td><input '.(isset($actual['currency'][$shop['id_shop']][$country['id_country']]) && $actual['currency'][$shop['id_shop']][$country['id_country']] == $currency->id?'checked="checked"':'')." type='radio' name='field[{$shop['id_shop']}][{$country['id_country']}]' value='{$currency->id}' /></td>";
                    }
                    $str .= '<td><input '.(isset($actual['force'][$shop['id_shop']][$country['id_country']]) && (int)$actual['force'][$shop['id_shop']][$country['id_country']] > 0?'checked="checked"':'')." type='checkbox' name='force[{$shop['id_shop']}][{$country['id_country']}]' value='1' /></td>";

                    $str .= '</tr>';
                }
            }
        }

        $str .= '</tbody></table>';
        $str .= '<center><input value="'.$this->l('Save').'" type="submit" /></center>';
        $str .= '</fieldset>';
        $str .= '</form>';

        return $str;
    }
}
