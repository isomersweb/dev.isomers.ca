<?php

if (!defined('_CAN_LOAD_FILES_'))
	exit;

//Classes
  //Object Models
  include_once(dirname(__FILE__) . '/FaqCategory.php');
  include_once(dirname(__FILE__) . '/FaqEntry.php');
  //Tools
  include_once(dirname(__FILE__) . '/W4wFaqUpgrader.class.php');

class W4wFaq extends Module
{
	public function __construct()
 	{
 	 	$this->name = 'w4wfaq';
 	 	$this->tab = 'front_office_features';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('FAQ-U');
		$this->description = $this->l('Frequently Asked Questions Module.');

		$this->version = '1.2';
		$this->author = 'Wookstrap';

    $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?')."\\n".$this->l('All your FAQ Entries and Categories will be deleted.');

    if(Module::isInstalled($this->name) && Module::isEnabled($this->name))
      $this->isUpgradeNeeded();
 	}


 	//Installation of the module
 	public function install()
 	{
 		//Show phone number in "question not answered popover"
 		Configuration::updateValue('W4WFAQ_SHOW_PHONE',0);
 		Configuration::updateValue('W4WFAQ_PHONE_NUMBER','');
    //ID of the CMS Page to which hook the FAQ content
    Configuration::updateValue('W4WFAQ_CMS_HOOK_ID',0);
    
    //Should the module include Bootstrap's CSS
    Configuration::updateValue('W4WFAQ_INCLUDE_BOOTSTRAP_CSS',0);
    //Should the module include Bootstrap's JS
    Configuration::updateValue('W4WFAQ_INCLUDE_BOOTSTRAP_JS',0);

    Configuration::updateValue('W4WFAQ_FAQ_MODE','accordion');

    //Used to track need for upgrades. Might go away if we can do this with prestashop version
    Configuration::updateValue('W4WFAQ_DB_VERSION',$this->version);


 		
 	 	if (parent::install() == false 
 	 		//create tables in database
 	 		OR $this->createTables() == false 
 	 		//register css/js hook
 	 		OR $this->registerHook('header') == false 
 	 		//install options controller in back office menu
 	 		OR $this->installTab() == false
 	 	) 
 	 		return false;
		return true;
  	}

  	//removal of the module
  	public function uninstall()
  	{
      if(!parent::uninstall() ||
        //removal of database tables
        !$this->dropTables() ||
        //removal of options controller in back office menu
        !$this->uninstallTab()
      )
        return false;
      //If things went good, unhook and clear config
  		//un-hooking
      $this->unregisterHook('header');
      //removal of configuration entries
      Configuration::deleteByName('W4WFAQ_SHOW_PHONE');
      Configuration::deleteByName('W4WFAQ_PHONE_NUMBER');
      Configuration::deleteByName('W4WFAQ_CMS_HOOK_ID');
      Configuration::deleteByName('W4WFAQ_INCLUDE_BOOTSTRAP_CSS');
      Configuration::deleteByName('W4WFAQ_INCLUDE_BOOTSTRAP_JS');
      Configuration::deleteByName('W4WFAQ_FAQ_MODE');
      Configuration::deleteByName('W4WFAQ_DB_VERSION');
  		return true;
  	}

  	//Installation of the options controller in back office menu (Preferences)
  	public function installTab()
	{
		$tab = new Tab();
		$tab->active = 1;
		//controller class
		$tab->class_name = "AdminW4wFaq";
		$tab->name = array();
		//Menu entry name
		foreach (Language::getLanguages(true) as $lang)
			$tab->name[$lang['id_lang']] = $this->l('Faq Administration');
		$tab->id_parent = (int)Tab::getIdFromClassName('AdminPreferences');
		$tab->module = $this->name;
		return $tab->add();
	}
	
	//removal of the options controller in back office menu (Preferences)
	public function uninstallTab()
	{
		$id_tab = (int)Tab::getIdFromClassName('AdminW4wFaq');
		if ($id_tab)
		{
			$tab = new Tab($id_tab);
			return $tab->delete();
		}
		else
			return true;
	}

	//Create Tables for storing FAQ data
  	public function createTables()
  	{
  		$sql = array();
  		$ok = true;
  		//FAQ Entries Tables
  		$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."w4wfaq_entry`(
			`id_w4wfaq_entry` int(10) unsigned NOT NULL auto_increment,
			`id_w4wfaq_category` int(10) unsigned NOT NULL,
      `fa_icon_entry` varchar(50) NOT NULL default '',
			`position` int(10) unsigned NOT NULL default '0',
			`has_controls` tinyint(1) unsigned NOT NULL DEFAULT '0',
			PRIMARY KEY (`id_w4wfaq_entry`)
			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";
		
  		$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."w4wfaq_entry_lang`(
  			`id_w4wfaq_entry` int(10) unsigned NOT NULL auto_increment,
  			`id_lang` int(10) unsigned NOT NULL,
  			`title` varchar(255) NOT NULL,
  			`content` longtext,
  			PRIMARY KEY (`id_w4wfaq_entry`,`id_lang`)
  			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

  		//FAQ Categories Tables
  		$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."w4wfaq_category`(
			`id_w4wfaq_category` int(10) unsigned NOT NULL auto_increment,
      `fa_icon_category` varchar(50) NOT NULL default '',
			`position` int(10) unsigned NOT NULL default '0',
			PRIMARY KEY (`id_w4wfaq_category`)
			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";
		
  		$sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."w4wfaq_category_lang`(
  			`id_w4wfaq_category` int(10) unsigned NOT NULL auto_increment,
  			`id_lang` int(10) unsigned NOT NULL,
  			`name` varchar(255) NOT NULL,
  			PRIMARY KEY (`id_w4wfaq_category`,`id_lang`)
  			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";

  		foreach($sql as $query)
  		{
  			$ok = $ok && Db::getInstance()->execute($query);
  			if(!$ok)
  			{
  				//if something went wrong, delete tables that might have been created before error.
  				$this->errors = $this->l('SQL ERROR : Table Creating Failed.');
  				$this->dropTables();
  				return false;
  			}
  		}

  		//create default "General" category if none exist (editable in back office)
  		if(FaqCategory::getCount()==0)
  		{
  			$cat = new FaqCategory();
  			$trads = array();

  			$languages = Language::getLanguages(false);
  			foreach ($languages as $language)
  			{
  				$trads[(int)$language['id_lang']] = $this->l('General');
  			}
  			$cat->name = $trads;
  			$cat->save();
  		}

  		return true;
  	}

    public function isUpgradeNeeded()
    {
      $oldconf = Configuration::get('W4WFAQ_DB_VERSION');
      if($oldconf===false || $oldconf != $this->version)
      {
        $upgrades = W4wFaqUpgrader::checkUpgrades();
        if(count($upgrades)>0)
        {
          W4wFaqUpgrader::doUpgrades();
          Configuration::updateValue('W4WFAQ_DB_VERSION',$this->version);
          return true;
        }else{
          Configuration::updateValue('W4WFAQ_DB_VERSION',$this->version);
          return false;
        }
      }else{
        return false;
      }
    }

  	//removal of database tables related to FAQ data
  	public function dropTables()
  	{
  		$ok = true;
  		$sql = array();

  		$sql[] = "DROP TABLE IF EXISTS "._DB_PREFIX_."w4wfaq_entry";	
  		$sql[] = "DROP TABLE IF EXISTS "._DB_PREFIX_."w4wfaq_entry_lang";	
  		$sql[] = "DROP TABLE IF EXISTS "._DB_PREFIX_."w4wfaq_category";	
  		$sql[] = "DROP TABLE IF EXISTS "._DB_PREFIX_."w4wfaq_category_lang";	

  		foreach($sql as $query)
  		{
  			$ok = $ok && Db::getInstance()->execute($query);
  			if(!$ok)
  			{
  				$this->errors = $this->l('SQL ERROR : Table Destroying Failed.');
  				return false;
  			}
  		}

  		return true;
  	}

  	//Adding CSS & JS to the header.
  	public function hookHeader($params)
  	{
  		//TODO : Make it conditionnal so it's not added on all pages.

      $bootstrap_css = (bool)Configuration::get('W4WFAQ_INCLUDE_BOOTSTRAP_CSS');
      $bootstrap_js = (bool)Configuration::get('W4WFAQ_INCLUDE_BOOTSTRAP_JS');

      //Bootstrap3 CSS style needed for the plugin to work. Only included if the option is checked in module's settings
      if($bootstrap_css)
        $this->context->controller->addCSS($this->_path.'css/w4wfaq.bootstrap.css','all');

      //Bootstrap3 JS code needed for the plugin to work. Only included if the option is checked in module's settings
      if($bootstrap_js)
        $this->context->controller->addJS($this->_path.'js/w4wfaq.bootstrap.js');

      //Font Awesome
      $this->context->controller->addCSS($this->_path.'css/w4wfaq.font-awesome.min.css');

      //generic module css
      $this->context->controller->addCSS($this->_path.'css/w4wfaq.css','all');
  		//module css for user customization (only loaded if exists)
      if(file_exists(dirname(__FILE__).'/css/w4wfaq.custom.css'))
      {
  		  $this->context->controller->addCSS($this->_path.'css/w4wfaq.custom.css','all');
      }
  	}

  	// Get Form Helper for adding / editing FAQ-Categories and FAQ-Entries
  	public function getFormHelper()
  	{
  		// Get default Language
    	$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

    	//Form Helper
        $helper = new HelperForm(); 
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminW4wFaq');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        //Language voodoo
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $languages = Language::getLanguages();

        //Missing data from languages (fix this)
        foreach($languages as $k=>$language)
        {
        	if($languages[$k]['id_lang']==$default_lang)
        	{
        		$languages[$k]['is_default']=1;
        	}else{
        		$languages[$k]['is_default']=0;

        	}
        }
        $helper->languages = $languages;

        $helper->show_toolbar = false;

        return $helper;
  	}

  	//Control buttons for category actions
  	public function getCatControlHtml($base_url,$id_category)
  	{
      $delete_box = "if (confirm('".$this->l('Are you sure you want to delete this category and all its related entries ?')."')){ return true; }else{ event.stopPropagation(); event.preventDefault();};";
      $linkup= '<a href="'.$base_url.'&catup='.$id_category.'" title="'.$this->l('Move Up').'"><img src="../img/admin/up.gif" alt="'.$this->l('Move Up').'"></a>';
      $linkdown= '<a href="'.$base_url.'&catdown='.$id_category.'" title="'.$this->l('Move Down').'"><img src="../img/admin/down.gif" alt="'.$this->l('Move Down').'"></a>';
      $linksupp= '<a href="'.$base_url.'&delcat='.$id_category.'" title="'.$this->l('Delete').'" onclick="'.$delete_box.'"><img src="../img/admin/delete.gif" alt="'.$this->l('Delete').'"></a>';
      $linkentry= '<a href="'.$base_url.'&newentry='.$id_category.'" class="btn btn-xs btn-success"> '.$this->l('New Entry').'</a>';
      $linkedit= '<a href="'.$base_url.'&editcat='.$id_category.'" title="'.$this->l('Edit Category').'"><img src="../img/admin/edit.gif" alt="'.$this->l('Edit Category').'"></a>';
      return array('up'=>$linkup,'down'=>$linkdown,'supp'=>$linksupp,'entry'=>$linkentry,'edit'=>$linkedit);
    }

    //Control buttons for entry actions
    public function getEntryControlHtml($base_url,$id_entry)
    {
      $delete_box = "if (confirm('".$this->l('Are you sure you want to delete this entry ?')."')){ return true; }else{ event.stopPropagation(); event.preventDefault();};";
  		$linkup= '<a href="'.$base_url.'&entryup='.$id_entry.'" title="'.$this->l('Move Up').'"><img src="../img/admin/up.gif" alt="'.$this->l('Move Up').'"></a>';
  		$linkdown= '<a href="'.$base_url.'&entrydown='.$id_entry.'" title="'.$this->l('Move Down').'"><img src="../img/admin/down.gif" alt="'.$this->l('Move Down').'"></a>';
  		$linksupp= '<a href="'.$base_url.'&delentry='.$id_entry.'" title="'.$this->l('Delete').'" onclick="'.$delete_box.'"><img src="../img/admin/delete.gif" alt="'.$this->l('Delete').'"></a>';
  		$linkedit= '<a href="'.$base_url.'&editentry='.$id_entry.'" title="'.$this->l('Edit Entry').'"><img src="../img/admin/edit.gif" alt="'.$this->l('Edit Entry').'"></a>';
  		return array('up'=>$linkup,'down'=>$linkdown,'supp'=>$linksupp,'edit'=>$linkedit);
  	}

  	//Make category/entries listing
  	public function getCategoriesHtml($base_url)
  	{
      //get module configuration form
      $html = $this->getParamForm($base_url);
      $html.='<p></p>';
  		$html .= '<div class="panel"><form><fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Categories').'</legend>';
  		$html .= '<table class="table tableDnD attribute_group" width="100%">';
      //faq categories
  		$faq_cats = FaqCategory::getCategories();
  		$amount = count($faq_cats);
      $cat_even = false;
  		foreach($faq_cats as $key=>$faq_cat)
  		{
        $cat_even = !$cat_even;

  			//get current category control buttons
  			$links = $this->getCatControlHtml($base_url,$faq_cat->id);
  			//get related faq entries
  			$entries = $faq_cat->getEntries();
        if($cat_even)
          $c_class="";
        else
          $c_class=" alt_row";
  			$html.="<tr class=\"row_hover".$c_class."\"><td>".(($faq_cat->fa_icon_category!="")?'<i class="fa '.$faq_cat->fa_icon_category.'"></i> ':'').$faq_cat->name[Context::getContext()->language->id];

  			//Do we need "Up" and "Down" buttons ?
  			$needup = $key!=0;
  			$needdown = $key!=($amount-1);

  			//If only one category left, don't allow to suppress.
  			$needsupp = $amount>1;

  			//Category Control buttons
  			$html.=(($needup||$needdown||$needsupp||1==1)?"</td><td class=\"right\" style=\"text-align:right;\">":'').($needup?$links['up'].' ':'').($needdown?$links['down'].' ':'').($needsupp?$links['supp'].' ':'').$links['edit']."</td>";

  			//Entries
  			$html.="<tr><td colspan=\"2\"><h4>".$this->l('Entries for :').' '.$faq_cat->name[Context::getContext()->language->id]."</h4><table class=\"table tableDnD attribute_group\" width=\"100%\">";
  			if(count($entries)>0)
  			{
  				$amounte = count($entries);
          $entry_even = false;
	  			for($ekey=0;$ekey<$amounte;$ekey++)
	  			{
            $entry_even = !$entry_even;
            if($entry_even)
              $e_class="";
            else
              $e_class="alt_row";
	  				$entry = $entries[$ekey];
	  				//get current entry control buttons
	  				$controls = $this->getEntryControlHtml($base_url,$entry->id);
	  				//Do we need "Up" and "Down" buttons
	  				$eneedup = $ekey!=0;
	  				$eneeddown = $ekey!=($amounte-1);

	  				//Entry Line + Control Buttons
	  				$html.="<tr class=\"".$e_class."\"><td>".(($entry->fa_icon_entry!="")?'<i class="fa '.$entry->fa_icon_entry.'"></i> ':'').$entry->title[Context::getContext()->language->id]."</td><td class=\"right\" style=\"text-align:right;\">".($eneedup?$controls['up'].' ':'').($eneeddown?$controls['down'].' ':'').$controls['supp']." ".$controls['edit']."</td></tr>";
	  			}
  			}
  			//New entry button (for current category)
  			$html.="<tr class=\"\"><td colspan=\"2\" class=\"right\" style=\"text-align:right;\">".$links['entry']."</td></tr></table>";
  			$html.="</td></tr>";
  		}
  		$html .= '</td></tr></table>';
      $html .= "<p><a href=\"".$base_url."&newcat\" title=\"".$this->l('New Category')."\" class=\"btn btn-success\"> ".$this->l('New Category')."</a></p>";
      $html .= '</form></div>';

  		return $html;
  	}

  	//redirect default module configuration page to options controller
  	public function getContent()
  	{
  		Tools::redirectAdmin($this->context->link->getAdminLink('AdminW4wFaq'));
  	}


    //Global module configuration
    public function getParamForm($base_url)
    {
      //this all is a little messy and should be done using form helpers. Coming soon™.

      //Current configuration state
      $current_cms = (int)Configuration::get('W4WFAQ_CMS_HOOK_ID');
      $current_show_phone = (bool)Configuration::get('W4WFAQ_SHOW_PHONE');
      $current_phone_number = Configuration::get('W4WFAQ_PHONE_NUMBER');
      if($current_show_phone)
          $checked = ' checked="checked"';
      else
          $checked = '';
      $bootstrap_css = (bool)Configuration::get('W4WFAQ_INCLUDE_BOOTSTRAP_CSS');
      $bootstrap_js = (bool)Configuration::get('W4WFAQ_INCLUDE_BOOTSTRAP_JS');

      $faq_mode = Configuration::get('W4WFAQ_FAQ_MODE');

      if($bootstrap_js)
          $checked_js = ' checked="checked"';
      else
        $checked_js = '';
      if($bootstrap_css)
          $checked_css = ' checked="checked"';
      else
          $checked_css = '';



      //List of CMS pages we can hook to.
      $cms_list = CMS::getCMSPages(Context::getContext()->language->id,null,false);
      $cms_select_array = array();
      
      foreach($cms_list as $cms_item)
      {
        if($cms_item['id_cms']==$current_cms)
          $selected=true;
        else
          $selected=false;
        $cms_select_array[]=array(
          'id'=>$cms_item['id_cms'],
          'name'=>$cms_item['meta_title'],
          'selected'=>$selected
        );
        $cms_select_array = array_map("unserialize", array_unique(array_map("serialize", $cms_select_array)));
      }

      //Form in itself
      $html = '<div class="panel"><form action="'.$base_url.'" method="post">
      <fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Configuration').'</legend>
        <table style="width:615px; padding-left:15px;"><tr><td>
        <div class="margin-form">
            <p>
            <label for="selected_cms">'.$this->l('CMS Page to Hook FAQ to : ').'</label>
            <select name="selected_cms"><option value="0">'.$this->l('None (disabled)').'</value>';
            foreach($cms_select_array as $cms_item)
            {
              $selected = "";
              if($cms_item['selected'])
                $selected = ' selected="selected"';
              $html.='<option value="'.$cms_item['id'].'"'.$selected.'>'.$cms_item['name'].'</option>';
            }
            $html.='</select>
            </p>
            <p><label for="show_phone">'.$this->l('Show Phone Number : ').'</label>
            <input type="checkbox" name="show_phone"'.$checked.'></p>
            <p><label for="bootstrap_css">'.$this->l('Include Bootstrap CSS (Only use if your front end FAQ display is messy) : ').'</label>
            <input type="checkbox" name="bootstrap_css"'.$checked_css.'></p>
            <p><label for="bootstrap_js">'.$this->l('Include Bootstrap JS (Only use if your front end FAQ behaviour is messy) : ').'</label>
            <input type="checkbox" name="bootstrap_js"'.$checked_js.'></p>
            <p><label for="phone_number">'.$this->l('Phone Number : ').'</label>
            <input type="text" name="phone_number" value="'.$current_phone_number.'"></p>
            <p><label for="faq_mode">'.$this->l('FAQ Display Style : ').'</label>
            <select name="faq_mode">
            <option value="accordion"'.(($faq_mode == 'accordion'||$faq_mode===false)?' selected="selected"':'').'>'.$this->l('Collapsed FAQ Entries').'</option>
            <option value="allopened"'.(($faq_mode == 'allopened')?' selected="selected"':'').'>'.$this->l('All Entries Opened').'</option>
            </select></p>
        </div>
        </td></tr></table>
        <input type="submit" name="submitparams" class="btn pull-left button" value="'.$this->l('Save').'" class="button" />       
        </fieldset>
      </form></div>';
      return $html;
    }

  	//Categories and Entries data retrieval for display in front office
  	public function getFaqContent($id_lang)
  	{
  		$categories = FaqCategory::getCategories($id_lang);
  		$cats = array();
  		foreach($categories as $category)
  		{
  			$cat = array(
  				'name'=>$category->name,
  				'id'=>$category->id,
          'icon'=>$category->fa_icon_category,
  				'entries'=>array()
  			);
  			foreach($category->getEntries($id_lang) as $entry)
  			{
  				$entry_array = array(
  					'id'=>$entry->id,
  					'title'=>$entry->title,
  					'content'=>$entry->content,
  					'has_controls'=>$entry->has_controls,
            'icon'=>$entry->fa_icon_entry
  				);
  				$cat['entries'][]=$entry_array;
  			}
  			//only display a category if it has entries
  			if(count($cat['entries'])>0)
  				$cats[]=$cat;
  		}

      //get module configuration for passing to template
      $context = Context::getContext();
      $contact_link = $context->link->getPageLink('contact',false,$context->language->id);
      $show_phone = (bool)Configuration::get('W4WFAQ_SHOW_PHONE');
      $phone_number = Configuration::get('W4WFAQ_PHONE_NUMBER');
      $faq_mode = Configuration::get('W4WFAQ_FAQ_MODE');
      if($faq_mode===false)
        $faq_mode = 'accordion';

  		//smarty bindings
  		$this->context->smarty->assign(array(
  			'faq_data'=>$cats,
        'faq_contact_link'=>$contact_link,
  			'faq_show_phone'=>$show_phone,
        'faq_phone_number'=>$phone_number,
        'faq_mode'=>$faq_mode
  		));

  		//generate HTML from template
  		$data = $this->display(__FILE__,'simple.tpl');
      //return this html to caller
  		return $data;
  	}

  	// Return data to append to CMS page
  	public function getCmsContent($cms_id,$id_lang)
  	{
  		$target_id = (int)Configuration::get('W4WFAQ_CMS_HOOK_ID');
      //Only return FAQ data if the CMS page calling this is the right one (if there's no right one (=0), it will never be shown)
  		if($cms_id == $target_id)
  		{
  			return $this->getFaqContent($id_lang);
  		}
  		return '';
  	}

    public function getPath()
    {
      return $this->_path;
    }
}