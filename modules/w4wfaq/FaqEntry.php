<?php

Class FaqEntry extends ObjectModel
{
	public $title;
	public $id_w4wfaq_category;
	public $content;
	public $position;
	public $has_controls;
	public $fa_icon_entry='';

	public static $definition = array(
		'table' => 'w4wfaq_entry',
		'primary' => 'id_w4wfaq_entry',
		'multilang' => true,
		'fields' => array(
			'id_w4wfaq_category' 	=> array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'position' 			=> array('type' => self::TYPE_INT),
			'has_controls' 		=> array('type' => self::TYPE_BOOL),
			'fa_icon_entry' 		=> array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString', 'size' => 50, 'default' => ''),

			//Lang Fields
			'title'				=> array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 255),
			'content'			=> array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999),
		),
	);

	//when adding an entry, put it as the last entry of its category
	public function add($autodate = true, $null_values = false)
	{
		$this->position = FaqEntry::getLastPosition((int)$this->id_w4wfaq_category);
		return parent::add($autodate,$null_values);
	}

	//when updating (except if we're moving up/down), ensure positions are good within category
	public function update($null_values = false,$redopos = true)
	{
		if (parent::update($null_values))
		{
			if($redopos)
				return $this->cleanPositions($this->id_w4wfaq_category);
			else
				return true;
		}
		return false;
	}

	//ensure positions are good after deletion
	public function delete()
	{
	 	if (parent::delete())
			return $this->cleanPositions($this->id_w4wfaq_category);
		return false;
	}

	//move an entry up within its category
	public function moveUp()
	{
		$oldpos = $this->position;
		$newpos = $oldpos - 1;
		//if new postion is valid
		if($newpos>=0)
		{
			//get entry occupying target position
			$entry_to_switch_id = FaqEntry::getEntryAtPosition($newpos,$this->id_w4wfaq_category);
			if($entry_to_switch_id!==false)
			{
				//load its data
				$entry_to_switch = new FaqEntry($entry_to_switch_id);
				//change its position to the one of current entry
				$entry_to_switch->position = $oldpos;
				//save to database (without reordering)
				$entry_to_switch->update(false,false);
			}
			//change position of current to (now free) target position
			$this->position = $newpos;
			//update database (without reordering)
			$this->update(false,false);
		}
		//reorder
		FaqEntry::cleanPositions($this->id_w4wfaq_category);
	}

	//move an entry down within its category
	public function moveDown()
	{
		$oldpos = $this->position;
		$newpos = $oldpos + 1;
		//if new position is valid
		if($newpos>=0)
		{
			//get entry occupying target position
			$entry_to_switch_id = FaqEntry::getEntryAtPosition($newpos,$this->id_w4wfaq_category);
			if($entry_to_switch_id!==false)
			{
				//load its data
				$entry_to_switch = new FaqEntry($entry_to_switch_id);
				//change its position to the one of current entry
				$entry_to_switch->position = $oldpos;
				//save to database (no reordering)
				$entry_to_switch->update(false,false);
			}
			//change position of current to (now free) target position
			$this->position = $newpos;
			//save to database (without reordering)
			$this->update(false,false);
		}
		//reorder
		FaqEntry::cleanPositions($this->id_w4wfaq_category);
	}

	//gets id of an entry occupying a given position within a category (for moving up/down purposes)
	public static function getEntryAtPosition($position,$id_category)
	{
		$sql ="SELECT id_w4wfaq_entry FROM `"._DB_PREFIX_."w4wfaq_entry` WHERE id_w4wfaq_category = ".(int)$id_category." AND position = ".(int)$position;
		return Db::getInstance()->getValue($sql);
	}

	//delete entries of a given category
	public static function deleteByCategory($id_category)
	{
		$cat = new FaqCategory($id_category);
		$entries = $cat->getEntries();
		foreach($entries as $entry)
		{
			$entry->delete();
		}
	}

	//get position at which to insert a new element in given category
	public static function getLastPosition($id_category)
	{
		$sql = "SELECT MAX(position) + 1 FROM `"._DB_PREFIX_."w4wfaq_entry` WHERE id_w4wfaq_category = ".(int)$id_category;
		return (Db::getInstance()->getValue($sql));
	}

	//ensure entry order within a category is correct (0->n-1 with no gaps) after update/deletion
	public static function cleanPositions($id_category)
	{
		$sql = "
		SELECT `id_w4wfaq_entry`
		FROM `"._DB_PREFIX_."w4wfaq_entry`
		WHERE `id_w4wfaq_category` = ".(int)$id_category."
		ORDER BY `position`";

		$result = Db::getInstance()->executeS($sql);

		for ($i = 0, $total = count($result); $i < $total; ++$i)
		{
			$sql = "UPDATE `"._DB_PREFIX_."w4wfaq_entry`
					SET `position` = ".(int)$i."
					WHERE `id_w4wfaq_category` = ".(int)$id_category."
						AND `id_w4wfaq_entry` = ".(int)$result[$i]['id_w4wfaq_entry'];
			Db::getInstance()->execute($sql);
		}
		return true;
	}
}