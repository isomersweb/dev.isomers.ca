<?php
/**
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 */

require_once(dirname(__FILE__).'/RPCarrierDiscountsCart.php');

class RPCarrierDiscounts extends ObjectModel
{

    public $id_rpcarrierdiscount;
    public $name;
    public $id_carrier;
    public $min_order;
    public $apply_discount = 0;
    public $id_shop = 0;
    /* public $reduction_tax = 1; */
    public $reduction_currency = 0;
    public $reduction_amount = 0;
    public $reduction_percent = 0;
    public $active = true;

    public static $definition = array(
        'table' => 'rpcarrierdiscount',
        'primary' => 'id_rpcarrierdiscount',
        'multilang' => true,
        'multilang_shop' => false,
        'fields' => array(
            'name' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName',
                'required' => true, 'size' => 128),
            'id_carrier' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
            'min_order' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'apply_discount' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            /* 'reduction_tax' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),*/
            'reduction_currency' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
            'reduction_amount' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'reduction_percent' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'active' => array('type' => self::TYPE_BOOL),
        )
    );

    public function updateValues()
    {
        $this->min_order = str_replace(',', '.', $this->min_order);
        $this->reduction_amount = str_replace(',', '.', $this->reduction_amount);
        $this->reduction_percent = str_replace(',', '.', $this->reduction_percent);
    }

    public function add($autodate = true, $null_values = true)
    {
        $this->updateValues();
        return parent::add($autodate, $null_values);
    }

    public function update($null_values = false)
    {
        $this->updateValues();
        return parent::update($null_values);
    }

    public static function getAvailableDiscountsToCurrier($id_cart, $id_carrier)
    {
        $context = Context::getContext();
        $cart = new Cart($id_cart);
        $amount = Tools::convertPriceFull(
            $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING),
            Context::getContext()->currency,
            new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
        );

        $sql = '
        SELECT *
        FROM `'._DB_PREFIX_.'rpcarrierdiscount` rpcd
        LEFT JOIN `'._DB_PREFIX_.'rpcarrierdiscount_lang` rpcdl
            ON (rpcd.id_rpcarrierdiscount = rpcdl.id_rpcarrierdiscount AND rpcdl.id_lang =
            '.(int)$context->language->id.')
        WHERE (rpcd.`id_shop` = '.(int)$context->shop->id.' OR rpcd.`id_shop` = \'0\')
            AND rpcd.`min_order` <= '.$amount.' AND rpcd.`id_carrier` = '.(int)$id_carrier.'
        ORDER BY rpcd.`min_order` DESC limit 1';

        $result = Db::getInstance()->executeS($sql);

        if (!isset($result[0])) {
            return array();
        }

        foreach ($result as $key => $value) {
            $result[$key]['reduction_amount_converted'] = Tools::convertPriceFull(
                $value['reduction_amount'],
                new Currency($value['reduction_currency']),
                Context::getContext()->currency
            );
        }

        return $result[0];
    }

    public static function getAvailableDiscounts($id_cart)
    {
        $context = Context::getContext();
        $cart = new Cart($id_cart);
        $amount = Tools::convertPriceFull(
            $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING),
            Context::getContext()->currency,
            new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
        );

        $sql = '
        SELECT *
        FROM `'._DB_PREFIX_.'rpcarrierdiscount` rpcd
        LEFT JOIN `'._DB_PREFIX_.'rpcarrierdiscount_lang` rpcdl
            ON (rpcd.id_rpcarrierdiscount = rpcdl.id_rpcarrierdiscount AND rpcdl.id_lang =
             '.(int)$context->language->id.')
        WHERE (rpcd.`id_shop` = '.(int)$context->shop->id.' OR rpcd.`id_shop` = \'0\')
            AND rpcd.`min_order` <= '.$amount.'
        ORDER BY rpcd.`min_order` DESC';

        $result = Db::getInstance()->executeS($sql);

        $id_carriers = array();
        foreach ($result as $key => $value) {
            if (!in_array($value['id_carrier'], $id_carriers)) {
                $id_carriers[] = $value['id_carrier'];
                $result[$key]['reduction_amount_converted'] = Tools::convertPriceFull(
                    $result[$key]['reduction_amount'],
                    new Currency($result[$key]['reduction_currency']),
                    Context::getContext()->currency
                );
            } else {
                unset($result[$key]);
            }
        }
        return $result;
    }

    public static function checkDiscount($id_cart, $id_carrier = null)
    {
        $context = Context::getContext();
        $cart = new Cart($id_cart);
        if (!$id_carrier) {
            $id_carrier = (int)$cart->id_carrier;
        }

        $cart_rules = $cart->getCartRules();

        if (is_array($cart_rules)) {
            $one_is_in_cart = false;
            foreach ($cart_rules as $cart_rule) {
                if ($cart_rule['rpcarrierdiscount_cart'] == 1) {
                    $id_carrier_discount = RPCarrierDiscountsCart::getIdByCartRule($cart_rule['id_cart_rule']);
                    if ($id_carrier_discount > 0) {
                        $carrier_discount = new RPCarrierDiscountsCart($id_carrier_discount);
                        if ($id_cart != $carrier_discount->id_cart || $id_carrier != $carrier_discount->id_carrier ||
                            $one_is_in_cart) {
                            $cart->removeCartRule($cart_rule['id_cart_rule']);
                            $cart_rule = new CartRule($cart_rule['id_cart_rule']);
                            $cart_rule->delete();
                            $carrier_discount->delete();
                        } else {
                            $one_is_in_cart = true;
                        }
                    } else {
                        $cart->removeCartRule($cart_rule['id_cart_rule']);
                        $cart_rule = new CartRule($cart_rule['id_cart_rule']);
                        $cart_rule->delete();
                    }
                }
            }
        }

        $cart_rules = $cart->getCartRules();
        if (is_array($cart_rules)) {
            if (Tools::getValue('free_shipping') == 1) {
                $free_shipping = true;
            } else {
                $free_shipping = false;
            }
            foreach ($cart_rules as $cart_rule) {
                if ($cart_rule['free_shipping'] == 1) $free_shipping = true;
                $id_carrier_discount = RPCarrierDiscountsCart::getIdByCartRule($cart_rule['id_cart_rule']);
                if ($id_carrier_discount > 0) {
//                    p($cart_rule);
                     $carrier_discount = new RPCarrierDiscountsCart($id_carrier_discount);
 //                   echo "#$id_cart#$id_carrier_discount#{$carrier_discount->id_cart} <br>";
                     
                    if ($id_cart != $carrier_discount->id_cart || $free_shipping == true) {
                        $cart->removeCartRule($cart_rule['id_cart_rule']);
                    }
                }
            }
            if ($free_shipping == true) return true;
        }
        if ($id_carrier > 0) {
            $discount = RPCarrierDiscounts::getAvailableDiscountsToCurrier($id_cart, $id_carrier);
            $id_rpcarrierdiscount_cart = RPCarrierDiscountsCart::getIdByCartId($id_cart);

            if (count($discount) == 0) {
                if ($id_rpcarrierdiscount_cart > 0) {
                    $cart_rule_cart = new RPCarrierDiscountsCart($id_rpcarrierdiscount_cart);
                    $cart_rule = new CartRule($cart_rule_cart->id_cart_rule);
                    $cart_rule->delete();
                    $cart_rule_cart->delete();
                }
                return false;
            }

            $cart_rule_cart = new RPCarrierDiscountsCart($id_rpcarrierdiscount_cart);
            if (Validate::isLoadedObject($cart_rule_cart) && $cart_rule_cart->id_carrier != $id_carrier) {
                $cart_rule_cart = new RPCarrierDiscountsCart($id_rpcarrierdiscount_cart);
                $cart_rule = new CartRule($cart_rule_cart->id_cart_rule);
                $cart_rule->delete();
                $cart_rule_cart->delete();
                $id_rpcarrierdiscount_cart = 0;
            }

            $cart = new Cart($id_cart);

            /*$default_country = new Country(Configuration::get('PS_COUNTRY_DEFAULT'),
            Configuration::get('PS_LANG_DEFAULT'));*/

            /*if (isset($cart->id_address_delivery)
                && $cart->id_address_delivery
                && Customer::customerHasAddress($cart->id_customer, $cart->id_address_delivery))
                $id_zone = Address::getZoneById((int)$cart->id_address_delivery);
            else
                $id_zone = (int)$default_country->id_zone;

            $carrier = new Carrier($discount['id_carrier']);*/

            $new_price = 0;

            /*$new_price2 = $carrier->getDeliveryPriceByPrice($cart->getOrderTotal(
            true, Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING),
            $id_zone, (int)Context::getContext()->currency->id);*/
            /*if ($context->cart->id_customer == 44)
            {
                var_dump($context->cart->id);
            }*/
            $new_price2 = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING);

            $summary = $cart->getSummaryDetails();

            if ($discount['apply_discount'] == 1) {
                $new_price = $new_price2 * $discount['reduction_percent'] / 100;
            } elseif ($discount['apply_discount'] == 2) {
                $new_price = $discount['reduction_amount_converted'];
            } elseif ($discount['apply_discount'] == 3) {
                $new_price = $summary['total_products'] * $discount['reduction_percent'] / 100;
            }

            if ($new_price2 < $new_price) {
                $new_price = $new_price2;
            }

            if ($id_rpcarrierdiscount_cart > 0) {
                $cart_rule_cart = new RPCarrierDiscountsCart($id_rpcarrierdiscount_cart);

                $cart_rule = new CartRule($cart_rule_cart->id_cart_rule);
                $cart_rule->reduction_currency = (int)Context::getContext()->currency->id;

                $date_from = time();

                $cart_rule->date_from = date('Y-m-d H:i:s', $date_from);
                $cart_rule->date_to = date('Y-m-d H:i:s', strtotime($cart_rule->date_from.' +1 week'));

                $cart_rule->reduction_amount = (float)$new_price;
                $cart_rule->reduction_tax = 1;
                $cart_rule->priority = (int)Configuration::get('RP_CARRIER_DISCOUNT_PRIORITY');
                /* $cart_rule->reduction_tax = $discount['reduction_tax']; */

                $languages = Language::getLanguages(true);
                foreach ($languages as $language) {
                    $cart_rule->name[(int)$language['id_lang']] = $discount['name'];
                }

                try {
                    $cart_rule->update();
                } catch (Exception $e) {
                    return false;
                }

                $cart_rule_find = false;

                if (is_array($cart_rules)) {
                    foreach ($cart_rules as $cart_rule) {
                        if ($cart_rule['id_cart_rule'] == $cart_rule_cart->id_cart_rule) {
                            $cart_rule_find = true;
                        }
                    }
                }

                if (!$cart_rule_find) {
                    $cart->addCartRule($cart_rule->id);
                }

                return true;
            } else {
                $voucher_code = null;
                do {
                    $voucher_code = Configuration::get('RP_CARRIER_DISCOUNT_PREFIX') . rand(1000, 10000000);
                } while (CartRule::cartRuleExists($voucher_code));

                $cart_rule = new CartRule();

                $cart_rule->rpcarrierdiscount_cart = 1;
                $cart_rule->code = $voucher_code;
                $cart_rule->id_customer = (int)$context->customer->id;

                $cart_rule->reduction_currency = (int)Context::getContext()->currency->id;
                $cart_rule->reduction_amount = (float)$new_price;
                $cart_rule->reduction_tax = 1;
                /* $cart_rule->reduction_tax = $discount['reduction_tax']; */

                $cart_rule->quantity = 1;
                $cart_rule->quantity_per_user = 1;
                $cart_rule->highlight = 0;

                $date_from = time();

                $cart_rule->date_from = date('Y-m-d H:i:s', $date_from);
                $cart_rule->date_to = date('Y-m-d H:i:s', strtotime($cart_rule->date_from.' +1 week'));

                $languages = Language::getLanguages(true);
                foreach ($languages as $language) {
                    $cart_rule->name[(int)$language['id_lang']] = $discount['name'];
                }
                $cart_rule->add();

                $cart_rule_cart = new RPCarrierDiscountsCart();
                $cart_rule_cart->id_cart = $id_cart;
                $cart_rule_cart->id_cart_rule = $cart_rule->id;
                $cart_rule_cart->id_carrier = $id_carrier;
                $cart_rule_cart->add();

                $cart->addCartRule($cart_rule->id);

                return true;
            }
        }

        return false;
    }
}
