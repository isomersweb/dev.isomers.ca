<?php
/**
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__).'/classes/RPCarrierDiscounts.php');

class RPCarrierDiscount extends Module
{
    public $_html = '';
    public $fields_form = array();
    public $toolbar_btn;
    public $fields_value;
    public $bootstrap;

    public function __construct()
    {
        $this->name = 'rpcarrierdiscount';
        $this->tab = 'front_office_features';
        $this->version = '1.2.10';
        $this->author = 'Reservation Partner LT';
        $this->need_instance = 0;
        $this->secure_key = Tools::encrypt($this->name);
        $this->module_key = 'e8012ec929edffbb10cd25b28ebebc3a';

        parent::__construct();

        $this->displayName = $this->l('Carrier discount');
        $this->description = $this->l('Additional carrier (shipping) discount based on order amount.');

        $this->bootstrap = (version_compare(_PS_VERSION_, 1.6) >= 0) ? true : false;
        Context::getContext()->smarty->assign(array(
            'bootstrap' => $this->bootstrap,
        ));
    }

    /**
     * @see Module::install()
     */
    public function install()
    {
        /* Adds Module */
        if (parent::install()
            && $this->registerHook('displayBeforeCarrier')
            && $this->registerHook('actionCarrierProcess')
            && $this->registerHook('actionCartSave')
            && $this->registerHook('actionCarrierUpdate')
        ) {
            /* Sets up configuration */

            /* Creates tables */
            $res = $this->createTables();

            $this->context->controller->getLanguages();

            $tab = new Tab();
            $lang_array = array();
            foreach ($this->context->controller->_languages as $language) {
                $lang_array[(int)$language['id_lang']] = $this->l('Carrier discount');
            }
            $tab->name = $lang_array;
            $tab->class_name = 'AdminRPCarrierDiscountEdit';
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminParentShipping');
            $tab->module = $this->name;
            $tab->add();

            return $res;
        }
        return false;
    }

    /**
     * @see Module::uninstall()
     */
    public function uninstall()
    {
        /* Deletes Module */
        if (parent::uninstall()) {
            $res = $this->deleteTables();

            $tab = new Tab((int)Tab::getIdFromClassName('AdminRPCarrierDiscountEdit'));
            $tab->delete();

            return $res;
        }
        return false;
    }

    /**
     * Creates tables
     */
    protected function createTables()
    {
        /* `reduction_tax` tinyint(4) NOT NULL DEFAULT \'1\', */
        $res = Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'rpcarrierdiscount` (
                `id_rpcarrierdiscount` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_carrier` int(10) unsigned NOT NULL,
                `id_shop` int(10) unsigned NOT NULL,
                `min_order` decimal(17,2) NOT NULL DEFAULT \'0.00\',
                `apply_discount` tinyint(4) NOT NULL DEFAULT \'0\',
                `reduction_currency` int(10) unsigned NOT NULL,
                `reduction_amount` decimal(17,2) NOT NULL DEFAULT \'0.00\',
                `reduction_percent` decimal(5,2) NOT NULL DEFAULT \'0.00\',
                `active` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
                PRIMARY KEY (`id_rpcarrierdiscount`),
                INDEX (`active`, `min_order`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');

        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'rpcarrierdiscount_lang` (
                `id_rpcarrierdiscount_lang` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_rpcarrierdiscount` int(10) unsigned NOT NULL,
                `id_lang` int(10) unsigned NOT NULL,
                `name` varchar(255) NOT NULL,
                PRIMARY KEY (`id_rpcarrierdiscount_lang`),
                INDEX (`id_rpcarrierdiscount`, `id_lang`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');

        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'rpcarrierdiscount_cart` (
                `id_rpcarrierdiscount_cart` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_cart` int(10) unsigned NOT NULL,
                `id_cart_rule` int(10) unsigned NOT NULL,
                `id_carrier` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id_rpcarrierdiscount_cart`),
                KEY `active` (`id_cart`)
            ) ENGINE=' . _MYSQL_ENGINE_ . '  DEFAULT CHARSET=utf8 ;
        ');

        $res &= Db::getInstance()->execute('ALTER TABLE ' . _DB_PREFIX_ . 'cart_rule
            ADD COLUMN `rpcarrierdiscount_cart` tinyint(1) unsigned NOT NULL DEFAULT \'0\' ');

        return $res;
    }

    /**
     * deletes tables
     */
    protected function deleteTables()
    {
        return Db::getInstance()->execute('
                    DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'rpcarrierdiscount`;
                ') && Db::getInstance()->execute('
                    DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'rpcarrierdiscount_lang`;
                ') && Db::getInstance()->execute('
                    DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'rpcarrierdiscount_cart`;
                ') && Db::getInstance()->execute('ALTER TABLE ' . _DB_PREFIX_ . 'cart_rule
                    DROP rpcarrierdiscount_cart');
    }


    public function hookDisplayBeforeCarrier($param)
    {
        $this->hookActionCarrierProcess($param);

        $cart = new Cart($this->context->cart->id);
        $summary = $cart->getSummaryDetails();

        Context::getContext()->smarty->assign(array(
            'module_uri' => _MODULE_DIR_ . $this->name,
            'carrier_discounts' => RPCarrierDiscounts::getAvailableDiscounts(Context::getContext()->cart->id),

            'currency_sign' => Context::getContext()->currency->sign,
            'currency_format' => Context::getContext()->currency->format,
            'currency_decimals' => (int)Context::getContext()->currency->decimals * _PS_PRICE_DISPLAY_PRECISION_,
            'currency_blank' => (Context::getContext()->currency->blank ? ' ' : ''),
            'currency_is_rtl' => Context::getContext()->language->is_rtl,
            'currency_round_mode' => (int)Configuration::get('PS_PRICE_ROUND_MODE'),

            'update_link' => $this->context->link->getModuleLink($this->name, 'default', array('ajax' => 'true')),
            'totalProducts' => $summary['total_products'],
        ));

        return $this->display(__FILE__, 'carrier_hook.tpl');
    }

    public function hookActionCarrierProcess($param)
    {
        if (isset(Context::getContext()->cart)) {
            RPCarrierDiscounts::checkDiscount(Context::getContext()->cart->id, $param['cart']->id_carrier);
            $cart = $param['cart'];
        } else {
            $cart = new Cart((int)Context::getContext()->cookie->id_cart);
        }

        if (Validate::isLoadedObject($cart)) {
            RPCarrierDiscounts::checkDiscount($cart->id, $cart->id_carrier);
        } else {
            RPCarrierDiscounts::checkDiscount(0, 0);
        }
    }

    public function hookActionCartSave($param)
    {
        $this->hookActionCarrierProcess($param);
    }

    public function hookActionCarrierUpdate($params)
    {
        $id_carrier_new = (int)$params['carrier']->id;
        $id_carrier_old = (int)$params['id_carrier'];
        Db::getInstance()->execute(
            'UPDATE ' . _DB_PREFIX_ . 'rpcarrierdiscount
            SET id_carrier=' . $id_carrier_new . '
            WHERE id_carrier=' . $id_carrier_old . ';'
        );
    }

    public function getContent()
    {
        $this->_html = '';
        $this->modulePostProcess();
        $this->displayForm();
        return $this->_html;
    }

    public function initToolbar()
    {
        $this->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
                        '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        return $this->toolbar_btn;
    }

    private function modulePostProcess()
    {
        $this->_errors = array();
        if (Tools::isSubmit('submit' . $this->name)) {
            if (count($this->_errors) < 1) {
                Configuration::updateValue(
                    'RP_CARRIER_DISCOUNT_PRIORITY',
                    (int)Tools::getValue('RP_CARRIER_DISCOUNT_PRIORITY')
                );
                Configuration::updateValue(
                    'RP_CARRIER_DISCOUNT_PREFIX',
                    preg_replace(
                        '/[^A-Za-z0-9\-_]/',
                        '',
                        (string)Tools::getValue('RP_CARRIER_DISCOUNT_PREFIX')
                    )
                );
                $this->_html .= $this->displayConfirmation($this->l('Update success'));
            }
        }
        if (count($this->_errors)) {
            foreach ($this->_errors as $err) {
                $this->_html .= $this->displayError($err);
            }
        }
    }

    private function initForm()
    {
        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->context->controller->_languages;
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->default_form_language = $this->context->controller->default_form_language;
        $helper->allow_employee_form_lang = $this->context->controller->allow_employee_form_lang;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = $this->initToolbar();

        return $helper;
    }

    public function displayForm()
    {
        $this->fields_form = array(
            array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Settings'),
                        'icon' => 'icon-cogs',
                        'image' => (_PS_VERSION_ < 1.6 ? _PS_ADMIN_IMG_ . 'information.png' : null),
                    ),
                    'input' => array(
                        array(
                            'type' => 'text',
                            'label' => $this->l('Generated cart rule priority'),
                            'name' => 'RP_CARRIER_DISCOUNT_PRIORITY',
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('Generated cart rule code prefix'),
                            'name' => 'RP_CARRIER_DISCOUNT_PREFIX',
                        ),
                    ),
                    'submit' => array(
                        'name' => 'submit' . $this->name,
                        'title' => $this->l('Save'),
                        'class' => (_PS_VERSION_ < 1.6 ? 'button' : null)
                    )
                )
            )
        );

        $this->fields_value['RP_CARRIER_DISCOUNT_PRIORITY'] = Configuration::get('RP_CARRIER_DISCOUNT_PRIORITY');
        $this->fields_value['RP_CARRIER_DISCOUNT_PREFIX'] = Configuration::get('RP_CARRIER_DISCOUNT_PREFIX');

        $helper = $this->initForm();
        $helper->submit_action = '';
        $helper->title = $this->l('Carrier discount settings');

        $helper->fields_value = $this->fields_value;
        $this->_html .= $helper->generateForm($this->fields_form);
    }
}
