<?php
/**
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_2()
{
    return (Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'cart_rule
            ADD COLUMN `rpcarrierdiscount_cart` tinyint(1) unsigned NOT NULL DEFAULT \'0\' '));
}
