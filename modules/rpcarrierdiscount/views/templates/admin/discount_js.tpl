{*
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 *}

<script type="text/javascript">

	var bootstrap  = $('#header').hasClass('bootstrap');
	   $selectType = $('#apply_discount');

	if (!bootstrap){

		var $inputPercentage = $('#reduction_percent') .parent(),
			$inputAmount     = $('#reduction_amount')  .parent(),
			$inputCurrency   = $('#reduction_currency').parent(),

			$labelPercentage = $inputPercentage.prev(),
			$labelAmount     = $inputAmount    .prev(),
			$labelCurrency   = $inputCurrency  .prev(),

			$rowPercentage   = $inputPercentage.add($labelPercentage),
			$rowAmount       = $inputAmount    .add($labelAmount),
			$rowCurrency     = $inputCurrency  .add($labelCurrency);

	} else {

		var $rowPercentage   = $('#reduction_percent') .closest('.form-group'),
			$rowAmount       = $('#reduction_amount')  .closest('.form-group'),
			$rowCurrency     = $('#reduction_currency').closest('.form-group');
	}


	var $rowAmountCurrency = $rowAmount.add($rowCurrency);

	$selectType.on('change', function(){

		var typeNum = $selectType.val();

		if (typeNum == 0){
			$rowPercentage.hide();
			$rowAmountCurrency.hide();
		} else if (typeNum == 1 || typeNum == 3) {
			$rowPercentage.show();
			$rowAmountCurrency.hide();
		} else if (typeNum == 2) {
			$rowPercentage.hide();
			$rowAmountCurrency.show();
		}

	}).trigger('change');

</script>