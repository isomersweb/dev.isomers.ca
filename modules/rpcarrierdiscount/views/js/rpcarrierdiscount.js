/**
 * 2014-2015 Reservation Partner LT
 *
 * NOTICE OF LICENSE
 *
 * This source file is a property of Reservation Partner LT.
 * Redistribution or republication of any part of this code is prohibited.
 * A single module license strictly limits the usage of this module
 * to one (1) shop / domain / website.
 * If you want to use this module in more than one shop / domain / website
 * you must purchase additional licenses.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * this module to newer versions in the future.
 *
 *  @author    Reservation Partner LT <info@reservationpartner.com>
 *  @copyright 2014-2015 Reservation Partner LT
 *  @license   Commercial License
 *  Property of Reservation Partner LT
 */

rpcarrierdiscount = new Object();

rpcarrierdiscount.reWriteDiscounts = function(){

	/*console.log(rpcarrierdiscount.discounts);*/
	$('.delivery_option_radio').each(function(){
		var id_carrier = parseInt($(this).val());
		if (typeof rpcarrierdiscount.discounts[id_carrier] !== 'undefined')
		{
			var $rowCarrier = $(this).closest('.delivery_option');
			if ($rowCarrier.find('.delivery_option_price .carrier_old').length == 0)
			{
				var oldPriceText = $rowCarrier.find('div.delivery_option_price').text(),
					price     = rpcarrierdiscount.convertPriceToFloat( oldPriceText ),
					new_price = rpcarrierdiscount.calculateNewPrice(price, rpcarrierdiscount.discounts[id_carrier]);

				if (price != new_price)
				{
					$rowCarrier.find('.delivery_option_price')
					.html('<span class="carrier_old" style="text-decoration: line-through; color: #CB1C00;">' + oldPriceText + '</span><br/>' +
						  '<span>' + rpcarrierdiscount.convertFloatToPrice(new_price) + rpcarrierdiscount.aditional_text + '</span>');
				}
			}
		}
	});
	
	$('#carrierTable input:radio').each(function(){
		var id_carrier = $(this).val();
		var first = parseInt(id_carrier.charAt(0));
		id_carrier = parseInt(id_carrier.substring(1));
		id_carrier = parseInt(id_carrier / Math.pow(10, first + 1));
		
		if (typeof rpcarrierdiscount.discounts[id_carrier] !== 'undefined')
		{
			if ($(this).parent().parent().find('.carrier_price label .carrier_old').length == 0)
			{
				var price = rpcarrierdiscount.convertPriceToFloat($(this).parent().parent().find('.carrier_price label .price').text());
				var new_price = rpcarrierdiscount.calculateNewPrice(price, rpcarrierdiscount.discounts[id_carrier]);
				if (price != new_price)
				{
					$(this).parent().parent().find('.carrier_price label').html('<span class="carrier_old" style="text-decoration: line-through; color: #CB1C00;">' + $(this).parent().parent().find('.carrier_price label').text() + '</span><br/>' +
																		 '<span>' + rpcarrierdiscount.convertFloatToPrice(new_price) + rpcarrierdiscount.aditional_text + '</span>');
				}
			}
		}
	});
};

rpcarrierdiscount.numberFormat = function(number, decimals, dec_point, thousands_sep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}

rpcarrierdiscount.convertPriceToFloat = function(str){
	//console.log('input : '+str);
	var ret = str.replace(rpcarrierdiscount.sign, '').replace(rpcarrierdiscount.blank, '');
	ret = ret.replace(/[^-\d]/g, '');
	if (rpcarrierdiscount.decimals > 0){
		ret = ret.substr(0,ret.length - rpcarrierdiscount.decimals) + '.' + ret.substr(ret.length - rpcarrierdiscount.decimals,rpcarrierdiscount.decimals);

	}
	//console.log('output : '+ret);
	return parseFloat(ret);
}

rpcarrierdiscount.convertFloatToPrice = function(price){
	var is_negative;
	if ((is_negative = (price < 0)))
		price *= -1;
	var precision_factor;
	if (rpcarrierdiscount.decimals == 0)
		precision_factor = 1;
	else
		precision_factor = Math.pow(10, rpcarrierdiscount.decimals);
	var tmp = price * precision_factor;
	var tmp2 = String(tmp);
	if ((tmp2.indexOf('.') !== false) && (tmp2.substr(tmp2.length - 1) != 0)){
		if (rpcarrierdiscount.round_mode == '0')
			price = Math.ceil(tmp) / precision_factor;
		else if (rpcarrierdiscount.round_mode == '1')
			price = Math.floor(tmp) / precision_factor;
		else
			price = Math.round(tmp) / precision_factor;
	}
	if ((rpcarrierdiscount.format == '2') && (rpcarrierdiscount.is_rtl == '1'))
		rpcarrierdiscount.format = '4';
	var ret;
	switch (rpcarrierdiscount.format)
	{
		/* X 0,000.00 */
		case '1':
			ret = rpcarrierdiscount.sign + rpcarrierdiscount.blank + rpcarrierdiscount.numberFormat(price, rpcarrierdiscount.decimals, '.', ',');
			break;
		/* 0 000,00 X*/
		case '2':
			ret = rpcarrierdiscount.numberFormat(price, rpcarrierdiscount.decimals, ',', ' ') + rpcarrierdiscount.blank + rpcarrierdiscount.sign;
			break;
		/* X 0.000,00 */
		case '3':
			ret = rpcarrierdiscount.sign + rpcarrierdiscount.blank + rpcarrierdiscount.numberFormat(price, rpcarrierdiscount.decimals, ',', '.');
			break;
		/* 0,000.00 X */
		case '4':
			ret = rpcarrierdiscount.numberFormat(price, rpcarrierdiscount.decimals, '.', ',') + rpcarrierdiscount.blank + rpcarrierdiscount.sign;
			break;
		/* 0'000.00 X  Added for the switzerland currency */
		case '5':
			ret = rpcarrierdiscount.numberFormat(price, rpcarrierdiscount.decimals, '.', "'") + rpcarrierdiscount.blank + rpcarrierdiscount.sign;
			break;
	}
	if (is_negative)
		ret = '-' + ret;
	return ret;
}

rpcarrierdiscount.calculateNewPrice = function(price_default, rules){
	var new_price = price_default;
	if (rules.apply_discount == 1)
		new_price = price_default - price_default * rules.reduction_percent / 100;
	else if (rules.apply_discount == 2)
		new_price = price_default - rules.reduction_amount_converted;
	else if (rules.apply_discount == 3)
		new_price = price_default - rpcarrierdiscount.totalProducts * rules.reduction_percent / 100;
	if (new_price < 0)
		new_price = 0;
	return new_price;
}

rpcarrierdiscount.updateCartList = function()
{
	setTimeout(function() {
		$.ajax({
			dataType: "json",
			type: 'POST',
			url: rpcarrierdiscount.ajax_link
		}).done(function(data){
			//console.log(data);
			//getCarrierListAndUpdate();
			console.log('update');
			if (typeof updateAddressSelection == 'function')
				updateAddressSelection();
		}).fail(function (jqXHR, textStatus) {
			//console.log(jqXHR, textStatus);
			//getCarrierListAndUpdate();
			console.log('error update');
	
		});
	}, 1000);
}