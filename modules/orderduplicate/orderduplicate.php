<?php
/**
 * OrderDuplicate
 *
 * @category  Module
 * @author    silbersaiten <info@silbersaiten.de>
 * @support   silbersaiten <support@silbersaiten.de>
 * @copyright 2017 silbersaiten
 * @version   1.1.5
 * @link      http://www.silbersaiten.de
 * @license   See joined file licence.txt
 */

class OrderDuplicate extends Module
{
    private $list_hooks = array(
        'displayBackOfficeHeader',
        'displayAdminOrder'
    );
    public $vt = 't17';

    public function __construct()
    {
        $this->name = 'orderduplicate';
        $this->version = '1.1.5';
        $this->tab = 'administration';
        $this->author = 'Silbersaiten';
        $this->module_key = '8f876e5e232b729aceb04072a270f6ab';

        if (version_compare('1.7.0.0', _PS_VERSION_, '>')) {
            $this->vt = 't16';
        }

        parent::__construct();

        $this->displayName = $this->l('Order Duplicator');
        $this->description = $this->l('Clone existing orders');
    }

    public function install()
    {
        if (parent::install()) {
            foreach ($this->list_hooks as $hook) {
                $this->registerHook($hook);
            }

            return true;
        }
        return false;
    }

    public function getContent()
    {
        if (Tools::getIsset('ajax')) {
            $this->ajaxCall();
        }

        Tools::redirectAdmin(
            AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules')
        );
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        unset($params);
        if ($this->context->controller instanceof AdminOrdersController) {
            $this->context->controller->addCSS($this->_path.'views/css/style.css');
            $this->context->controller->addJquery();
            $this->context->controller->addJs($this->_path.'views/js/order.js');
            $this->context->controller->addJqueryPlugin('autocomplete');

            return '
            <script type="text/javascript">
            var duplicator_path = "'.$this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&ajax=true",
                orderduplicateTranslation = {
                    "Duplicate": "'.$this->l('Duplicate').'",
                    "You must select an address (delivery and invoice).": "'.$this->l('You must select an address (delivery and invoice).').'"
                }
            </script>';
        }
    }

    public function hookDisplayAdminOrder($params)
    {

    }

    public function ajaxCall()
    {
        $action = Tools::getValue('action');

        switch ($action) {
            case 'duplicateGetInfo':
                $id_order = Tools::getValue('id_order');
                $check = true;

                $order = new Order((int)$id_order);

                $products = $order->getProducts();
                foreach ($products as $product) {
                    $pr = new Product((int)$product['product_id']);
                    $pr->id_product_attribute = $product['product_attribute_id'];
                    $check &= $pr->checkQty($product['product_quantity']);
                }

                if (!Validate::isLoadedObject($order) || !$check) {
                    $order = false;
                    $customer = false;
                    $address_delivery = false;
                    $address_invoice = false;
                } else {
                    $customer = new Customer((int)$order->id_customer);
                    $address_delivery = new Address((int)$order->id_address_delivery);
                    $address_invoice = new Address((int)$order->id_address_invoice);
                }

                $payment_modules = Module::getPaymentModules();
                foreach ($payment_modules as &$pm) {
                    $obj = Module::getInstanceById($pm['id_module']);
                    $pm['displayName'] = $obj->displayName;
                }
                $order_payment_module = Module::getInstanceByName($order->module);

                $this->context->smarty->assign(array(
                    'order' => $order,
                    'prod' => $check,
                    'customer' => $customer,
                    'address_delivery' => $address_delivery,
                    'address_invoice' => $address_invoice,
                    'states' => OrderState::getOrderStates($this->context->language->id),
                    'methods' => $payment_modules,
                    'selected_method' => $order_payment_module->id
                ));

                die($this->display(__FILE__, 'fancybox.tpl'));
                //break;
            case 'getCustomerList':
                $query = Tools::getValue('q', false);

                $result = Db::getInstance()->executeS(
                    'SELECT
                    `firstname`,
                    `lastname`,
                    `id_customer`
                    FROM
                    `'._DB_PREFIX_.'customer`
                    WHERE
                    `active` = 1
                    AND `firstname` LIKE \'%'.pSQL($query).'%\' OR `lastname` LIKE \'%'.pSQL($query).'%\''
                );

                if ($result) {
                    die(Tools::jsonEncode($result));
                }

                die(Tools::jsonEncode(new stdClass));
                //break;
            case 'getAddressList':
                $id_customer = Tools::getValue('id_customer');
                $customer = new Customer((int)$id_customer);

                if (Validate::isLoadedObject($customer)) {
                    $addresses = $customer->getAddresses($this->context->language->id);

                    if (count($addresses)) {
                        $formatted_addresses = array();

                        foreach ($addresses as $address) {
                            $formatted_addresses[$address['id_address']] = AddressFormat::generateAddress(
                                new Address($address['id_address']),
                                array(),
                                '<br />'
                            );
                        }

                        $this->context->smarty->assign(array(
                            'id_address_delivery_selected' => Tools::getValue('id_address_delivery_selected'),
                            'id_address_invoice_selected' => Tools::getValue('id_address_invoice_selected'),
                            'addresses' => $formatted_addresses
                        ));
                    }
                }

                die($this->display(__FILE__, $this->vt.'_addresses.tpl'));
                //break;
            case 'cloneOrder':
                $id_customer = Tools::getValue('id_customer');
                $id_address_delivery = Tools::getValue('id_address_delivery');
                $id_address_invoice = Tools::getValue('id_address_invoice');
                $id_order = Tools::getValue('id_order');
                $id_order_state = Tools::getValue('id_order_state');
                $id_payment_method = Tools::getValue('id_payment_method');

                $order = new Order((int)$id_order);

                if ($id_payment_method != 0) {
                    $payment_module = Module::getInstanceById((int)$id_payment_method);    
                } else {
                    $payment_module = Module::getInstanceByName($order->module);
                }
                if (Validate::isLoadedObject($order)) {
                    if (Validate::isLoadedObject($payment_module)
                        && $cart = $this->cloneCart(
                            $order->id_cart,
                            $id_customer,
                            $id_address_delivery,
                            $id_address_invoice
                        )
                    ) {
                        $this->context->cart = $cart;

                        try {
                            if ($payment_module->validateOrder(
                                $cart->id,
                                (int)$id_order_state,
                                $order->total_paid_tax_incl,
                                ((int)$id_payment_method != 0 ? $payment_module->displayName : $order->payment)
                            )) {
                                $neworder = new Order((int)$payment_module->currentOrder);
                                $discs = "";
//                                $discs = $neworder->getCartRules();

                                if (count($discs)) {
                                    foreach ($discs as $disc) {
                                        $cr = new CartRule($disc['id_cart_rule']);
                                        $cr->date_from = date('Y-m-d H:i:s', strtotime('-1 hour', strtotime($neworder->date_add)));
                                        $cr->date_to = date('Y-m-d H:i:s', strtotime('+1 hour'));
                                        $cr->name[Configuration::get('PS_LANG_DEFAULT')] = $disc['name'];
                                        $cr->quantity = 0;
                                        $cr->quantity_per_user = 1;
                                        $cr->active = 0;
                                        if ($res = $cr->update()) {
                                            $order_cart_rule = new OrderCartRule($disc['id_order_cart_rule']);

                                            $neworder->total_discounts += $order_cart_rule->value;
                                            $neworder->total_discounts_tax_incl += $order_cart_rule->value;
                                            $neworder->total_discounts_tax_excl += $order_cart_rule->value_tax_excl;
                                            $neworder->total_paid -= $order_cart_rule->value;
                                            $neworder->total_paid_tax_incl -= $order_cart_rule->value;
                                            $neworder->total_paid_tax_excl -= $order_cart_rule->value_tax_excl;
                                            $neworder->update();
                                        } else {
                                            break;
                                        }
                                    }
                                }
                                Hook::exec('actionOrderCloned', array(
                                    'order_old' => $order,
                                    'order_new' => $neworder
                                ));

                                die(Tools::jsonEncode(array('success' => true)));
                            }
                        } catch (PrestaShopException $e) {
                            die(Tools::jsonEncode(array(
                                'error' => $this->l('Unable to clone this order').': '.$e->getMessage()
                            )));
                        }
                    }
                }

                die(Tools::jsonEncode(array('error' => $this->l('Unable to clone this order'))));
                //break;
            case 'deleteOrder':
                if ($this->deleteOrder(Tools::getValue('id_order'))) {
                    die(Tools::jsonEncode(array('success' => true)));
                }

                die(Tools::jsonEncode(array('error' => $this->l('Unable to delete this order'))));
                //break;
        }
    }

    private function cloneCart($id_cart, $id_customer, $id_address_delivery, $id_address_invoice)
    {
        if (Validate::isLoadedObject($cart = new Cart((int)$id_cart))) {
            $cloned = $cart;

            $cloned->id = null;
            $cloned->id_customer = (int)$id_customer;
            $cloned->id_address_delivery = (int)$id_address_delivery;
            $cloned->id_address_invoice = (int)$id_address_invoice;

            //$delivery_option = Tools::unSerialize($cloned->delivery_option);
            $delivery_option = array();
            $delivery_option[$cloned->id_address_delivery] = $cloned->id_carrier.',';
            $cloned->delivery_option = serialize($delivery_option);

            if ($cloned->add()) {
                $new_cart_id = $cloned->id;

                $cart_products = Db::getInstance()->ExecuteS(
                    'SELECT * FROM `'._DB_PREFIX_.'cart_product` WHERE `id_cart` = '.(int)$id_cart
                );
/*
                $cart_rules = Db::getInstance()->ExecuteS(
                    'SELECT * FROM `'._DB_PREFIX_.'cart_cart_rule` WHERE `id_cart` = '.(int)$id_cart
                );
*/
                $cart_rules = "";
                if (count($cart_products)) {
                    foreach ($cart_products as $cart_product) {
                        $cart_product['id_cart'] = $new_cart_id;
                        $cart_product['id_address_delivery'] = (int)$id_address_delivery;

                        Db::getInstance()->insert('cart_product', $cart_product);
                    }
                }

                if (count($cart_rules)) {
                    foreach ($cart_rules as $cart_rule) {
                        $cart_rule['id_cart'] = $new_cart_id;

                        $ncr = new CartRule((int)$cart_rule['id_cart_rule']);
                        $ncr->id_customer = (int)$id_customer;
                        $ncr->date_from = date('Y-m-d H:i:s', time());
                        $ncr->date_to = date('Y-m-d H:i:s', time() + 24 * 36000);
                        $ncr->add();

                        $cart_rule['id_cart_rule'] = $ncr->id;
                        Db::getInstance()->insert('cart_cart_rule', $cart_rule);
                    }
                }

                return $cloned;
            }
        }

        return false;
    }

    private function deleteOrderPayments(Order $order)
    {
        $invoices = $order->getInvoicesCollection()->getResults();

        if ($invoices) {
            $invoice_list = array();

            foreach ($invoices as $invoice) {
                array_push($invoice_list, (int)$invoice->id);
            }

            Db::getInstance()->delete(
                'order_invoice_payment',
                '`id_order_invoice` IN ('.(implode(',', $invoice_list)).')'
            );
        }
    }

    private function deleteOrderInvoices(Order $order)
    {
        $invoices = $order->getInvoicesCollection()->getResults();

        if ($invoices) {
            foreach ($invoices as $invoice) {
                Db::getInstance()->delete('order_invoice', '`id_order_invoice` = '.(int)$invoice->id);
                Db::getInstance()->delete('order_invoice_payment', '`id_order_invoice` = '.(int)$invoice->id);
                Db::getInstance()->delete('order_invoice_tax', '`id_order_invoice` = '.(int)$invoice->id);
            }
        }
    }

    private function deleteOrderSlips(Order $order)
    {
        $slips = $order->getDeliverySlipsCollection()->getResults();

        if ($slips) {
            foreach ($slips as $slip) {
                Db::getInstance()->delete('order_slip', '`id_order_slip` = '.(int)$slip->id);
                Db::getInstance()->delete('order_slip_detail', '`id_order_slip` = '.(int)$slip->id);
            }
        }
    }

    private function deleteOrderReturns(Order $order)
    {
        $order_returns = OrderReturn::getOrdersReturn($order->id_customer, $order->id);

        if ($order_returns) {
            foreach ($order_returns as $order_return) {
                $return_data = OrderReturn::getOrdersReturnDetail($order_return['id_order_return']);

                if ($return_data) {
                    foreach ($return_data as $return_detail) {
                        Db::getInstance()->delete(
                            'order_return_detail',
                            '`id_order_return` = '.(int)$return_detail['id_order_return']
                        );
                    }
                }

                Db::getInstance()->delete(
                    'order_return',
                    '`id_order_return` = '.(int)$return_detail['id_order_return']
                );
            }
        }
    }

    private function deleteOrderHistory(Order $order)
    {
        Db::getInstance()->delete('order_history', '`id_order` = '.(int)$order->id);
    }

    private function deleteOrderDetails(Order $order)
    {
        $order_details = Db::getInstance()->ExecuteS(
            'SELECT
            *
            FROM
            `'._DB_PREFIX_.'order_detail`
            WHERE
            `id_order` = '.(int)$order->id
        );

        if ($order_details) {
            $order_detail_ids = array();

            foreach ($order_details as $order_detail) {
                array_push($order_detail_ids, (int)$order_detail['id_order_detail']);

                if (!StockAvailable::dependsOnStock($order_detail['product_id'])) {
                    StockAvailable::updateQuantity(
                        $order_detail['product_id'],
                        $order_detail['product_attribute_id'],
                        $order_detail['product_quantity']
                    );
                }
            }

            Db::getInstance()->delete('order_detail_tax', '`id_order_detail` IN ('.implode(',', $order_detail_ids).')');
        }

        Db::getInstance()->delete('order_detail', '`id_order` = '.(int)$order->id);
    }

    private function deleteOrderCarriers(Order $order)
    {
        Db::getInstance()->delete('order_carrier', '`id_order` = '.(int)$order->id);
    }

    private function deleteOrderMessages(Order $order)
    {
        $threads = Db::getInstance()->ExecuteS(
            'SELECT
            DISTINCT(`id_customer_thread`)
            FROM
            `'._DB_PREFIX_.'customer_thread`
            WHERE
            `id_order` = '.(int)$order->id
        );

        if ($threads) {
            foreach ($threads as $thread) {
                Db::getInstance()->delete(
                    'customer_thread',
                    '`id_customer_thread` = '.(int)$thread['id_customer_thread']
                );
                Db::getInstance()->delete(
                    'customer_message',
                    '`id_customer_thread` = '.(int)$thread['id_customer_thread']
                );
            }
        }

        $messages = Db::getInstance()->ExecuteS(
            'SELECT * FROM `'._DB_PREFIX_.'message` WHERE `id_order` = '.(int)$order->id
        );

        if ($messages) {
            foreach ($messages as $message) {
                Db::getInstance()->delete('message_readed', '`id_message` = '.(int)$message['id_message']);
                Db::getInstance()->delete('message', '`id_message` = '.(int)$message['id_message']);
            }
        }
    }

    private function deleteOrderCartRules(Order $order)
    {
        $rules = $order->getCartRules();

        if ($rules) {
            foreach ($rules as $rule) {
                Db::getInstance()->delete(
                    'order_cart_rule',
                    '`id_order_cart_rule` = '.(int)$rule['id_order_cart_rule']
                );
            }
        }
    }

    public function deleteOrder($id_order)
    {
        if (Validate::isLoadedObject($order = new Order((int)$id_order))) {
            $this->deleteOrderPayments($order);
            $this->deleteOrderCarriers($order);
            $this->deleteOrderCartRules($order);
            $this->deleteOrderDetails($order);
            $this->deleteOrderHistory($order);
            $this->deleteOrderInvoices($order);
            $this->deleteOrderMessages($order);
            $this->deleteOrderReturns($order);
            $this->deleteOrderSlips($order);

            Db::getInstance()->delete('orders', '`id_order` = '.(int)$order->id);

            Hook::exec('actionOrderDelete', array('order' => $order));

            return true;
        }

        return false;
    }
}
