# OrderDuplicate 1.1.5
# copyright 2017 silbersaiten.de

## Changelog


#### 1.1.5 (27.07.2017):
* added en + fr languages packs

#### 1.1.4 (27.07.2017):
* added notification abouts select address

#### 1.1.3 (24.05.2017):
* added feature to select a payment method when cloning an order

#### 1.1.2 (01.05.2017):
* ajax request now passes through the BO

#### 1.1.1 (23.10.2016):
* bug fixes (added voucher)

#### 1.1.0 (10.10.2016):
* adapted for Prestashop version 1.7

#### 1.0.6 (23.09.2016):

* bug fixes (discount in order)

#### 1.0.5 (17.03.2016):

* bug fixes (update product quantity of the removal order)