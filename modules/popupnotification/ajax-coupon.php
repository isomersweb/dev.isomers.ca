<?php
	include(dirname(__FILE__).'/../../config/config.inc.php');
	include(dirname(__FILE__).'/../../init.php');
	include(dirname(__FILE__).'/popupnotification.php');

	if(Tools::getValue('action') && Tools::getValue('action') == 'nlc_subscribtion'){
		$html = '';
		$nlc = new PopupNotification();
		$languages = Language::getLanguages(false);
		$curencies = Currency::getCurrencies();
		$context = Context::getContext();

		$first_name = trim(Tools::getValue('first_name'));
		$last_name = trim(Tools::getValue('last_name'));
		$email = trim(Tools::getValue('email'));
		$terms = Tools::getValue('popup_terms');

		$nlc->errors = array();
		
	
		if (!$first_name || !Validate::isName($first_name))
			$nlc->errors['first_name'] = $nlc->l('First Name is not valid');
		if (!$last_name || !Validate::isName($last_name))
			$nlc->errors['last_name'] = $nlc->l('Last Name is not valid');
		if (!$email || !Validate::isEmail($email))
			$nlc->errors['email'] = $nlc->l('Email Address is not valid');

		if(Configuration::get('nl_terms_url') && !$terms)
			$nlc->errors['terms'] = $nlc->l('You must agree with Terms of Use');

		if(count($nlc->errors)){
			die(json_encode(array('hasError' => true, 'errors' => $nlc->errors)));
		}else{
		
			$subscribed = $nlc->checkCustomerSubscription($email);
			$voucher = Configuration::get('nl_welcome_coupon');

			// if(!$subscribed){

				if($nlc->isNewsletterRegistered($email)){
					
					$nlc->errors['email'] = $nlc->l('This email address is already registered');
					die (json_encode(array('hasError' => true, 'errors' => $nlc->errors)));
				}else{

					/*
					$valid_days = (int)Configuration::get('nlc_valid');
					
					$voucher = new CartRule();

					if($customer = $nlc->isCustomerExists($email)){
						$voucher->id_customer = $customer;				
					}
					else{
						$voucher->id_customer = 0;
					}

					foreach($languages as $language){
						$voucher->name[$language['id_lang']] = Configuration::get('nlc_name_'.$language['id_lang']);
					}

					$voucher->highlight = Configuration::get('nlc_highlight');
					$voucher->partial_use = Configuration::get('nlc_partial_use');
					
					$date = new DateTime(date("Y-m-d H:i:s"));
					$date->modify("+{$valid_days} day");
					$voucher->date_from = date("Y-m-d H:i:s");
					$voucher->date_to = $date->format('Y-m-d H:i:s');

					$voucher->code = genVoucherCode(16);

					$voucher->minimum_amount = Configuration::get('nlc_minimum_amount');
					$voucher->minimum_amount_currency = Configuration::get('nlc_minimum_amount_currency');
					$voucher->minimum_amount_tax = Configuration::get('nlc_minimum_amount_tax');
					$voucher->minimum_amount_shipping = Configuration::get('nlc_minimum_amount_shipping');

					$voucher->quantity_per_user = Configuration::get('nlc_quantity');
					$voucher->free_shipping = Configuration::get('nlc_free_shipping');

					if(Configuration::get('nlc_apply_discount') == 'percent'){
						$voucher->reduction_percent = Configuration::get('nlc_reduction_percent');
						if(Configuration::get('nlc_apply_discount_to') == 'order'){
							$voucher->reduction_product = 0;
						}else if(Configuration::get('nlc_apply_discount_to') == 'specific'){
							$voucher->reduction_product = Configuration::get('nlc_reduction_product');
						}else if(Configuration::get('nlc_apply_discount_to') == 'cheapest'){
							$voucher->reduction_product = -1;
						}
					}else if(Configuration::get('nlc_apply_discount') == 'amount'){
						$voucher->reduction_amount = Configuration::get('nlc_reduction_amount');
						$voucher->reduction_currency = Configuration::get('nlc_reduction_currency');
						$voucher->reduction_tax = Configuration::get('nlc_reduction_tax');
						if(Configuration::get('nlc_apply_discount_to') == 'order'){
							$voucher->reduction_product = 0;
						}else if(Configuration::get('nlc_apply_discount_to') == 'specific'){
							$voucher->reduction_product = Configuration::get('nlc_reduction_product');
						}else{
							$voucher->reduction_product = 0;
						}
					}
					$voucher->add();
					*/
					if($customer = $nlc->isCustomerExists($email)){
						$nlc->registerUser($email);				
					}
					else{
						$customer = 0;
						$nlc->registerGuest($email);
					}
					$customer = 0;
					if($voucher){
						$isVoucher = true;		
						$html .= '<div class="voucher_fancy" style="width:400px;">
									<p style="font-size: 18px; line-height: 25px; text-align: center;">
										'.Configuration::get('nlc_popup_description_'.$context->language->id).'
									</p>
								</div>';
						$html = str_replace("{code}", $voucher, $html);

						$nlc->sendVoucherEmail($first_name, $last_name, $email, $voucher);
					}else{
						$voucher = 0;
						$isVoucher = false;
						$html .= '<p class="alert alert-success">'.$nlc->l('You have successfully subscribed to newsletter.').'</p>';
					}
					if(!$nlc->isSubscriberExists($email))
						$nlc->insertSubscribedCustomer($customer, $first_name, $last_name, $email, $voucher);
					
					die(json_encode(array('hasError' => false, 'isVoucher' => $isVoucher, 'html' => $html)));
				}
			// }else{
			// 	$nlc->errors['email'] = $nlc->l('This email address is already registered');
			// 	die (json_encode(array('hasError' => true, 'errors' => $nlc->errors)));
			// }
		}
		
	}else{
		echo "bad request";
	}

	function genVoucherCode($number){
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$res = "";
		for ($i = 0; $i < $number; $i++) {
		    $res .= $chars[mt_rand(0, strlen($chars)-1)];
		}
		return $res;
	}
?>