<?php

if(!defined('_PS_VERSION_'))
exit;

class PopupNotification extends Module
{
	public $_sendVoucherEmail;
	public $_popup_gls_enable_responsive;
	public $_popup_gls_resize_start_point;
	public $_popup_gls_hide_start_point;
	public $_popup_login_enable_popup_login;
	public $_popup_login_enable_login;
	public $_popup_login_enable_register;
	/* Facebook Connect */
	public $_popup_login_enable_facebook;
	public $_popup_fb_connect_app_id;
	public $_popup_fb_connect_position_top;
	public $_popup_fb_connect_position_custom;
	/* Twitter Connect */
	public $_popup_login_enable_twitter;
	public $_popup_tw_connect_position_top;
	public $_popup_tw_connect_position_custom;
	public $_popup_tw_connect_consumer_key;
	public $_popup_tw_connect_consumer_secret;
	/* Google Connect */
	public $_popup_login_enable_google;
	public $_popup_gp_connect_client_id;
	public $_popup_gp_connect_position_top;
	public $_popup_gp_connect_position_custom;
	/* Home Page Popup */
	public $home_box_bg;
	public $home_box_bg_repeat;
	/* Product Page Popup */
	public $product_box_bg;
	public $product_box_bg_repeat;
	/* Category Page Popup */
	public $category_box_bg;
	public $category_box_bg_repeat;
	/* Category Page Popup */
	public $custom_box_bg;
	public $custom_box_bg_repeat;
	/* Login Popup */
	public $login_box_bg;
	public $login_box_bg_repeat;
	/* public $_home_box_image; */

	public function __construct(){
		$this->name = 'popupnotification';
		$this->tab = 'front_office_features';
		$this->version = '2.7.1';
		$this->author = 'PrestaFan';
		$this->need_instance = 0;

		if(floatval(substr(_PS_VERSION_,0,3)) >= 1.6)
			$this->bootstrap = true;
		 
		parent::__construct();

		$this->_globalVars();
		 
		$this->displayName = $this->l('Popup Notification');
		$this->description = $this->l('Add popup notification to your shop.');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');


	}

	public function install(){
		if(Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);

		$defaultCurrency = intval(Configuration::get('PS_CURRENCY_DEFAULT'));
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
		
		Db::getInstance()->Execute('
			CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'popup_nl_voucher` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `customer_id` int(11) NOT NULL,
			  `first_name` varchar(50) NOT NULL,
			  `last_name` varchar(50) NOT NULL,
			  `email` varchar(50) NOT NULL,
			  `code` varchar(50) NOT NULL,
			  `date_end` datetime NOT NULL,
			  `used` int NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE = MYISAM DEFAULT CHARSET=utf8;
		');
			
		return parent::install() &&
			$this->registerHook('header') &&
			$this->registerHook('Home') &&
			$this->registerHook('displayNav') &&
			$this->registerHook('footer') &&
			$this->registerHook('popupFacebookConnect') &&
			$this->registerHook('popupTwitterConnect') &&
			$this->registerHook('popupGoogleConnect') &&
			$this->registerHook('DisplayRightColumnProduct') &&
			$this->registerHook('DisplayAdminProductsExtra') &&
			$this->registerHook('ActionProductUpdate') &&
			$this->registerHook('customPopup') &&
			$this->registerHook('categoryPopup') &&
			$this->addProductTableColumn() &&
			$this->addCategoryTableColumn() &&
			$this->installFBDb() &&
			$this->installTWDb() &&
			$this->installGPDb() &&
			// Home Page Popup
			Configuration::updateValue('home_page_popup_enable', 'true') &&
			Configuration::updateValue('home_box_width', '500') &&
			Configuration::updateValue('home_box_height', '350') &&
			Configuration::updateValue('home_box_cookie', '0') &&
			Configuration::updateValue('home_box_start_date', '0000-00-00') &&
			Configuration::updateValue('home_box_end_date', '0000-00-00') &&
			Configuration::updateValue('home_box_autoclose_time', '0') &&
			Configuration::updateValue('home_box_delay', '0') &&
			Configuration::updateValue('home_box_bg', '') &&
			Configuration::updateValue('home_box_bg_repeat', 'no-repeat') &&
			Configuration::updateValue('LOGIN_BOX_BG', '') &&
			Configuration::updateValue('LOGIN_BOX_BG_REPEAT', 'no-repeat') &&
			Configuration::updateValue('PN_SOCIAL_REDIRECT', 'no_redirect') &&
			// Configuration::updateValue('home_box_image', '') &&

			Configuration::updateValue('product_page_popup_enable', 'true') &&
			Configuration::updateValue('each_product_different_content', 'false') &&
			Configuration::updateValue('product_box_width', '500') &&
			Configuration::updateValue('product_box_height', '350') &&
			Configuration::updateValue('product_box_cookie', '0') &&
			Configuration::updateValue('product_box_start_date', '0000-00-00') &&
			Configuration::updateValue('product_box_end_date', '0000-00-00') &&
			Configuration::updateValue('product_box_autoclose_time', '0') &&
			Configuration::updateValue('product_box_delay', '0') &&
			Configuration::updateValue('product_box_bg', '') &&
			Configuration::updateValue('product_box_bg_repeat', 'no-repeat') &&
			Configuration::updateValue('custom_page_popup_enable', 'true') &&
			Configuration::updateValue('custom_box_width', '500') &&
			Configuration::updateValue('custom_box_height', '350') &&
			Configuration::updateValue('custom_box_cookie', '0') &&
			Configuration::updateValue('custom_box_start_date', '0000-00-00') &&
			Configuration::updateValue('custom_box_end_date', '0000-00-00') &&
			Configuration::updateValue('custom_box_autoclose_time', '0') &&
			Configuration::updateValue('custom_box_delay', '0') &&
			Configuration::updateValue('custom_box_bg', '') &&
			Configuration::updateValue('custom_box_bg_repeat', 'no-repeat') &&
			Configuration::updateValue('category_page_popup_enable', 'true') &&
			Configuration::updateValue('category_box_width', '500') &&
			Configuration::updateValue('category_box_height', '350') &&
			Configuration::updateValue('category_box_start_date', '0000-00-00') &&
			Configuration::updateValue('category_box_end_date', '0000-00-00') &&
			Configuration::updateValue('category_box_autoclose_time', '0') &&
			Configuration::updateValue('category_box_delay', '0') &&
			Configuration::updateValue('category_box_bg', '') &&
			Configuration::updateValue('category_box_bg_repeat', 'no-repeat') &&
			Configuration::updateValue('enable_product_extra_tabs', 'true') &&
			
			Configuration::updateValue('home_box_effect', '') &&
			Configuration::updateValue('product_box_effect', '') &&
			Configuration::updateValue('category_box_effect', '') &&
			Configuration::updateValue('custom_box_effect', '') &&
			
			Configuration::updateValue('nl_welcome_coupon', '') &&
			Configuration::updateValue('nl_terms_url', '') &&

			/*Configuration::updateValue('nl_verification_email', '0') &&
			Configuration::updateValue('nl_confirmation_email', '0') &&
			Configuration::updateValue('nl_coupon', '0') &&

			Configuration::updateValue('nlc_valid', '1') &&
			Configuration::updateValue('nlc_highlight', '1') &&
			Configuration::updateValue('nlc_partial_use', '0') &&
			Configuration::updateValue('nlc_minimum_amount', '0') &&
			Configuration::updateValue('nlc_minimum_amount_currency', $defaultCurrency) &&
			Configuration::updateValue('nlc_minimum_amount_tax', '0') &&
			Configuration::updateValue('nlc_minimum_amount_shipping', '0') &&
			Configuration::updateValue('nlc_quantity', '1') &&
			Configuration::updateValue('nlc_free_shipping', '0') &&
			Configuration::updateValue('nlc_apply_discount', 'off') &&
			Configuration::updateValue('nlc_reduction_percent', '0') &&
			Configuration::updateValue('nlc_reduction_amount', '0') &&
			Configuration::updateValue('nlc_reduction_currency', $defaultCurrency) &&
			Configuration::updateValue('nlc_reduction_tax', '0') &&
			Configuration::updateValue('nlc_apply_discount_to', 'order') &&
			Configuration::updateValue('nlc_reduction_product', '') &&
			Configuration::updateValue('nlc_name_'.$defaultLanguage, 'Subscribtion Discount') && */
			Configuration::updateValue('nlc_popup_description_'.$defaultLanguage, 'Your voucher code is {code}') &&

			//Responsive Settings
			Configuration::updateValue('popup_gls_enable_responsive', 'true') &&
			Configuration::updateValue('popup_gls_resize_start_point', '768') &&
			Configuration::updateValue('popup_gls_hide_start_point', '0') &&

			//Login Settings
			Configuration::updateValue('popup_login_enable_popup_login', 'false') &&
			Configuration::updateValue('popup_login_enable_login', 'true') &&
			Configuration::updateValue('popup_login_enable_register', 'true') &&

			//Facebook Connect
			Configuration::updateValue('popup_login_enable_facebook', 'true') &&
			Configuration::updateValue('popup_fb_connect_app_id', '') &&
			Configuration::updateValue('popup_fb_connect_position_top', 'true') &&
			Configuration::updateValue('popup_fb_connect_position_custom', 'false') &&

			//Twitter Connect
			Configuration::updateValue('popup_login_enable_twitter', 'true') &&
			Configuration::updateValue('popup_tw_connect_consumer_key', '') &&
			Configuration::updateValue('popup_tw_connect_consumer_secret', '') &&
			Configuration::updateValue('popup_tw_connect_position_top', 'true') &&
			Configuration::updateValue('popup_tw_connect_position_custom', 'false') &&

			//Google Connect
			Configuration::updateValue('popup_login_enable_google', 'false') &&
			Configuration::updateValue('popup_gp_connect_client_id', '') &&
			Configuration::updateValue('popup_gp_connect_position_top', 'true') &&
			Configuration::updateValue('popup_gp_connect_position_custom', 'false') &&

			//Update Version
			Configuration::updateValue('pn_version_update', 6);
	
	}
	
	public function uninstall(){
		$languages = Language::getLanguages(false);
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));

		DB::getInstance()->execute("DROP TABLE IF EXISTS "._DB_PREFIX_."popup_nl_voucher");

		foreach($languages as $language){
			Configuration::deleteByName('home_description_'.$language['id_lang']);
			Configuration::deleteByName('product_description_'.$language['id_lang']);
		}
		if(
			!parent::uninstall() ||
			!$this->dropProductTableColumn() ||
			!$this->dropCategoryTableColumn() ||
			//Home Page Popup
			!Configuration::deleteByName('home_page_popup_enable') ||
			!Configuration::deleteByName('home_box_width') ||
			!Configuration::deleteByName('home_box_height') ||
			!Configuration::deleteByName('home_box_cookie') ||
			!Configuration::deleteByName('home_box_start_date') ||
			!Configuration::deleteByName('home_box_end_date') ||
			!Configuration::deleteByName('home_box_autoclose_time') ||
			!Configuration::deleteByName('home_box_delay') ||
			!Configuration::deleteByName('home_box_bg') ||
			!Configuration::deleteByName('home_box_bg_repeat') ||
			!Configuration::deleteByName('LOGIN_BOX_BG') ||
			!Configuration::deleteByName('LOGIN_BOX_BG_REPEAT') ||
			!Configuration::deleteByName('PN_SOCIAL_REDIRECT') ||
			// !Configuration::deleteByName('home_box_image') ||

			!Configuration::deleteByName('product_page_popup_enable') ||
			!Configuration::deleteByName('each_product_different_content') ||
			!Configuration::deleteByName('product_box_width') ||
			!Configuration::deleteByName('product_box_height') ||
			!Configuration::deleteByName('product_box_cookie') ||
			!Configuration::deleteByName('product_box_start_date') ||
			!Configuration::deleteByName('product_box_end_date') ||
			!Configuration::deleteByName('product_box_autoclose_time') ||
			!Configuration::deleteByName('product_box_delay') ||
			!Configuration::deleteByName('product_box_bg') ||
			!Configuration::deleteByName('product_box_bg_repeat') ||
			!Configuration::deleteByName('custom_page_popup_enable') ||
			!Configuration::deleteByName('custom_box_width') ||
			!Configuration::deleteByName('custom_box_height') ||
			!Configuration::deleteByName('custom_box_cookie') ||
			!Configuration::deleteByName('custom_box_start_date') ||
			!Configuration::deleteByName('custom_box_end_date') ||
			!Configuration::deleteByName('custom_box_autoclose_time') ||
			!Configuration::deleteByName('custom_box_delay') ||
			!Configuration::deleteByName('custom_box_bg') ||
			!Configuration::deleteByName('custom_box_bg_repeat') ||
			!Configuration::deleteByName('category_page_popup_enable') ||
			!Configuration::deleteByName('category_box_width') ||
			!Configuration::deleteByName('category_box_height') ||
			!Configuration::deleteByName('category_box_start_date') ||
			!Configuration::deleteByName('category_box_end_date') ||
			!Configuration::deleteByName('category_box_autoclose_time') ||
			!Configuration::deleteByName('category_box_delay') ||
			!Configuration::deleteByName('category_box_bg') ||
			!Configuration::deleteByName('category_box_bg_repeat') ||
			!Configuration::deleteByName('enable_product_extra_tabs') ||
			!Configuration::deleteByName('home_box_effect') ||
			!Configuration::deleteByName('product_box_effect') ||
			!Configuration::deleteByName('category_box_effect') ||
			!Configuration::deleteByName('custom_box_effect') ||
			!Configuration::deleteByName('nl_welcome_coupon') ||
			!Configuration::deleteByName('nl_terms_url') ||
			/*
			!Configuration::deleteByName('nl_verification_email') ||
			!Configuration::deleteByName('nl_confirmation_email') ||
			!Configuration::deleteByName('nl_coupon') ||

			!Configuration::deleteByName('nlc_valid') ||
			!Configuration::deleteByName('nlc_highlight') ||
			!Configuration::deleteByName('nlc_partial_use') ||
			!Configuration::deleteByName('nlc_minimum_amount') ||
			!Configuration::deleteByName('nlc_minimum_amount_currency') ||
			!Configuration::deleteByName('nlc_minimum_amount_tax') ||
			!Configuration::deleteByName('nlc_minimum_amount_shipping') ||
			!Configuration::deleteByName('nlc_quantity') ||
			!Configuration::deleteByName('nlc_free_shipping') ||
			!Configuration::deleteByName('nlc_apply_discount') ||
			!Configuration::deleteByName('nlc_reduction_percent') ||
			!Configuration::deleteByName('nlc_reduction_amount') ||
			!Configuration::deleteByName('nlc_reduction_currency') ||
			!Configuration::deleteByName('nlc_reduction_tax') ||
			!Configuration::deleteByName('nlc_apply_discount_to') ||
			!Configuration::deleteByName('nlc_reduction_product') ||
			!Configuration::deleteByName('nlc_name_'.$defaultLanguage) || */
			!Configuration::deleteByName('nlc_popup_description_'.$defaultLanguage) ||
			
			//Responsive Settings
			!Configuration::deleteByName('popup_gls_enable_responsive') ||
			!Configuration::deleteByName('popup_gls_resize_start_point') ||
			!Configuration::deleteByName('popup_gls_hide_start_point') ||

			//Login Settings
			!Configuration::deleteByName('popup_login_enable_popup_login') ||
			!Configuration::deleteByName('popup_login_enable_login') ||
			!Configuration::deleteByName('popup_login_enable_register') ||

			//Facebook Connect settings
			!Configuration::deleteByName('popup_login_enable_facebook') ||
			!Configuration::deleteByName('popup_fb_connect_app_id') ||
			!Configuration::deleteByName('popup_fb_connect_position_top') ||
			!Configuration::deleteByName('popup_fb_connect_position_custom') ||

			//Twitter Connect Settings
			!Configuration::deleteByName('popup_login_enable_twitter') ||
			!Configuration::deleteByName('popup_tw_connect_consumer_key') ||
			!Configuration::deleteByName('popup_tw_connect_consumer_secret') ||
			!Configuration::deleteByName('popup_tw_connect_position_top') ||
			!Configuration::deleteByName('popup_tw_connect_position_custom') ||

			//Google Connect
			!Configuration::deleteByName('popup_login_enable_google') ||
			!Configuration::deleteByName('popup_gp_connect_client_id') ||
			!Configuration::deleteByName('popup_gp_connect_position_top') ||
			!Configuration::deleteByName('popup_gp_connect_position_custom') ||


			!Configuration::deleteByName('pn_version_update')
			)

			return false;
		return true;

	}

	public function _globalVars(){
		if((int)Configuration::get('pn_version_update') < 1){
			Configuration::updateValue('pn_version_update', 1);
			Configuration::updateValue('pn_send_voucher_email', 1);			
		}
		if((int)Configuration::get('pn_version_update') < 2){
			Configuration::updateValue('pn_version_update', 2);
			Configuration::updateValue('popup_gls_enable_responsive', 'true');
			Configuration::updateValue('popup_gls_resize_start_point', '768');
			Configuration::updateValue('popup_gls_hide_start_point', '0');		
		}
		// Configuration::updateValue('pn_version_update', 2);
		if((int)Configuration::get('pn_version_update') < 3){
			$this->installFBDb();
			$this->installTWDb();
			Configuration::updateValue('pn_version_update', 3);
			Configuration::updateValue('popup_login_enable_popup_login', 'false');
			Configuration::updateValue('popup_login_enable_login', 'true');
			Configuration::updateValue('popup_login_enable_register', 'true');
			Configuration::updateValue('popup_login_enable_facebook', 'true');		
			Configuration::updateValue('popup_login_enable_twitter', 'true');		
			Configuration::updateValue('popup_fb_connect_app_id', '');		
			Configuration::updateValue('popup_fb_connect_position_top', 'true');		
			Configuration::updateValue('popup_fb_connect_position_custom', 'false');		
		}
		if((int)Configuration::get('pn_version_update') < 4){
			$this->installGPDb();
			Configuration::updateValue('popup_login_enable_google', 'false');
			Configuration::updateValue('popup_gp_connect_client_id', '');
			Configuration::updateValue('popup_gp_connect_position_top', 'true');
			Configuration::updateValue('popup_gp_connect_position_custom', 'false');
			Configuration::updateValue('pn_version_update', 4);
		}
		if((int)Configuration::get('pn_version_update') < 5){
			// Configuration::updateValue('home_box_image', '');
			Configuration::updateValue('pn_version_update', 5);
		}
		if((int)Configuration::get('pn_version_update') < 6){
			Configuration::updateValue('home_box_bg', '');
			Configuration::updateValue('home_box_bg_repeat', 'no-repeat');
			Configuration::updateValue('product_box_bg', '');
			Configuration::updateValue('product_box_bg_repeat', 'no-repeat');
			Configuration::updateValue('category_box_bg', '');
			Configuration::updateValue('category_box_bg', 'no-repeat');
			Configuration::updateValue('custom_box_bg', '');
			Configuration::updateValue('custom_box_bg_repeat', 'no-repeat');
			Configuration::updateValue('pn_version_update', 6);
		}
		// Home Page Popup
		$this->home_box_bg = Configuration::get('home_box_bg');
		$this->home_box_bg_repeat = Configuration::get('home_box_bg_repeat');
		// $this->_home_box_image = Configuration::get('home_box_image');
		// Product Page Popup
		$this->product_box_bg = Configuration::get('product_box_bg');
		$this->product_box_bg_repeat = Configuration::get('product_box_bg_repeat');
		// Category Page Popup
		$this->category_box_bg = Configuration::get('category_box_bg');
		$this->category_box_bg_repeat = Configuration::get('category_box_bg_repeat');
		// Custom Page Popup
		$this->custom_box_bg = Configuration::get('custom_box_bg');
		$this->custom_box_bg_repeat = Configuration::get('custom_box_bg_repeat');

		// Login Popup
		$this->login_box_bg = Configuration::get('LOGIN_BOX_BG');
		$this->login_box_bg_repeat = Configuration::get('LOGIN_BOX_BG_REPEAT');

		$this->_sendVoucherEmail = (int)Configuration::get('pn_send_voucher_email');
		//Responsive Settings
		$this->_popup_gls_enable_responsive = Configuration::get('popup_gls_enable_responsive');
		$this->_popup_gls_resize_start_point = Configuration::get('popup_gls_resize_start_point');
		$this->_popup_gls_hide_start_point = Configuration::get('popup_gls_hide_start_point');

		//Login Settings
		$this->_popup_login_enable_popup_login = Configuration::get('popup_login_enable_popup_login');
		$this->_popup_login_enable_login = Configuration::get('popup_login_enable_login');
		$this->_popup_login_enable_register = Configuration::get('popup_login_enable_register');

		//Facebook Connect
		$this->_popup_login_enable_facebook = Configuration::get('popup_login_enable_facebook');
		$this->_popup_fb_connect_app_id = Configuration::get('popup_fb_connect_app_id');
		$this->_popup_fb_connect_position_top = Configuration::get('popup_fb_connect_position_top');
		$this->_popup_fb_connect_position_custom = Configuration::get('popup_fb_connect_position_custom');

		//Twitter Connect
		$this->_popup_login_enable_twitter = Configuration::get('popup_login_enable_twitter');
		$this->_popup_tw_connect_consumer_key = Configuration::get('popup_tw_connect_consumer_key');
		$this->_popup_tw_connect_consumer_secret = Configuration::get('popup_tw_connect_consumer_secret');
		$this->_popup_tw_connect_position_top = Configuration::get('popup_tw_connect_position_top');
		$this->_popup_tw_connect_position_custom = Configuration::get('popup_tw_connect_position_custom');

		//Google Connect
		$this->_popup_login_enable_google = Configuration::get('popup_login_enable_google');
		$this->_popup_gp_connect_client_id = Configuration::get('popup_gp_connect_client_id');
		$this->_popup_gp_connect_position_top = Configuration::get('popup_gp_connect_position_top');
		$this->_popup_gp_connect_position_custom = Configuration::get('popup_gp_connect_position_custom');

	}

	private function addProductTableColumn(){
		$sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'product_lang ADD `popup_notification` TEXT NOT NULL';
	    if(!Db::getInstance()->Execute($sql))
	        return false;
	    return true;

	}
	private function addCategoryTableColumn(){
		$sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'category_lang ADD `popup_notification` TEXT NOT NULL';
	    if(!Db::getInstance()->Execute($sql))
	        return false;
	    return true;
	}
	private function dropProductTableColumn(){
		$sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'product_lang DROP COLUMN `popup_notification`';
	    if(!Db::getInstance()->Execute($sql))
	        return false;
	    return true;

	}
	private function dropCategoryTableColumn(){
		$sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'category_lang DROP COLUMN `popup_notification`';
	    if(!Db::getInstance()->Execute($sql))
	        return false;
	    return true;
	}
	private function installFBDb (){
		$sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pf_popup_facebook_users` (
		   `id` INT NOT NULL AUTO_INCREMENT ,
		   `id_user` VARCHAR (100) NOT NULL  ,
		   `first_name` VARCHAR( 100 ) NOT NULL ,
		   `last_name` VARCHAR( 100 ) NOT NULL ,
		   `email` VARCHAR( 100 ) NOT NULL ,
		   `gender` VARCHAR( 100 ) NOT NULL ,
		   `birthday` DATE NOT NULL ,
		   `date_add` DATE NOT NULL ,
		   `date_upd` DATE NOT NULL ,
		   PRIMARY KEY ( `id` )
		   ) ENGINE = MYISAM DEFAULT CHARSET=utf8;';
		return Db::getInstance()->Execute(trim($sql));
	}
	private function uninstallFBDb(){
		return DB::getInstance()->execute("DROP TABLE IF EXISTS "._DB_PREFIX_."pf_popup_facebook_users");
	}

	private function installGPDb (){
		$sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pf_popup_google_users` (
		   `id` INT NOT NULL AUTO_INCREMENT ,
		   `id_user` VARCHAR (100) NOT NULL  ,
		   `first_name` VARCHAR( 100 ) NOT NULL ,
		   `last_name` VARCHAR( 100 ) NOT NULL ,
		   `email` VARCHAR( 100 ) NOT NULL ,
		   `gender` VARCHAR( 100 ) NOT NULL ,
		   `date_add` DATE NOT NULL ,
		   `date_upd` DATE NOT NULL ,
		   PRIMARY KEY ( `id` )
		   ) ENGINE = MYISAM DEFAULT CHARSET=utf8;';
		return Db::getInstance()->Execute(trim($sql));
	}
	private function uninstallGPDb(){
		return DB::getInstance()->execute("DROP TABLE IF EXISTS "._DB_PREFIX_."pf_popup_google_users");
	}

	private function installTWDb (){
		$sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pf_popup_twitter_users` (
		   `id` INT NOT NULL AUTO_INCREMENT ,
		   `id_user` VARCHAR (100) NOT NULL  ,
		   `first_name` VARCHAR( 100 ) NOT NULL ,
		   `last_name` VARCHAR( 100 ) NOT NULL ,
		   `screen_name` VARCHAR( 100 ) NOT NULL ,
		   `email` VARCHAR( 100 ) NOT NULL ,				   
		   `active` INT NOT NULL ,
		   `date_add` DATE NOT NULL ,
		   `date_upd` DATE NOT NULL ,
		   PRIMARY KEY ( `id` )
		   ) ENGINE = MYISAM DEFAULT CHARSET=utf8;';
		return Db::getInstance()->Execute(trim($sql));
	}
	private function uninstallTWDb(){
		return DB::getInstance()->execute("DROP TABLE IF EXISTS "._DB_PREFIX_."pf_popup_twitter_users");
	}
	
	public function getContent(){
		$output = null;
		
		if(Tools::isSubmit('submithomepagepopup')){
				$languages = Language::getLanguages(false);

				Configuration::updateValue('home_page_popup_enable', strval(Tools::getValue('home_page_popup_enable')));
				Configuration::updateValue('home_box_width', strval(Tools::getValue('home_box_width')));
				Configuration::updateValue('home_box_height', strval(Tools::getValue('home_box_height')));
				Configuration::updateValue('home_box_cookie', strval(Tools::getValue('home_box_cookie')));
				Configuration::updateValue('home_content_type', strval(Tools::getValue('home_content_type')));
				Configuration::updateValue('home_gmaps_link', strval(Tools::getValue('home_gmaps_link')));
				Configuration::updateValue('home_facebook_link', strval(Tools::getValue('home_facebook_link')));
				Configuration::updateValue('home_box_start_date', strval(Tools::getValue('home_box_start_date')));
				Configuration::updateValue('home_box_end_date', strval(Tools::getValue('home_box_end_date')));
				Configuration::updateValue('home_box_autoclose_time', intval(Tools::getValue('home_box_autoclose_time')));
				Configuration::updateValue('home_box_delay', intval(Tools::getValue('home_box_delay')));
				Configuration::updateValue('home_box_effect', strval(Tools::getValue('home_box_effect')));
				Configuration::updateValue('home_box_bg_repeat', strval(Tools::getValue('home_box_bg_repeat')));

				foreach($languages as $language){
					Configuration::updateValue('home_description_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('home_description_'.$language['id_lang']))));
					Configuration::updateValue('home_youtube_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('home_youtube_link_'.$language['id_lang']))));
					Configuration::updateValue('home_vimeo_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('home_vimeo_link_'.$language['id_lang']))));
					Configuration::updateValue('home_image_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('home_image_link_'.$language['id_lang']))));

					if(isset($_FILES['home_image_input_'.$language['id_lang']])){
						
						$type = strtolower(substr(strrchr($_FILES['home_image_input_'.$language['id_lang']]['name'], '.'), 1));
						$imagesize = array();
						$imagesize = @getimagesize($_FILES['home_image_input_'.$language['id_lang']]['tmp_name']);
					
						if (isset($_FILES['home_image_input_'.$language['id_lang']]['tmp_name']) &&
								!empty($_FILES['home_image_input_'.$language['id_lang']]['tmp_name']) &&
								!empty($imagesize) &&
								in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
								in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

								$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
								$salt = sha1(microtime());
								$name = Tools::encrypt($_FILES['home_image_input_'.$language['id_lang']]['name'].$salt).'.'.$type;
								if ($error = ImageManager::validateUpload($_FILES['home_image_input_'.$language['id_lang']]))
									$errors[] = $error;
								elseif (!$temp_name || !move_uploaded_file($_FILES['home_image_input_'.$language['id_lang']]['tmp_name'], $temp_name))
									return false;
								elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/uploads/'.$name, null, null, $type))
									$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
								if (isset($temp_name))
									@unlink($temp_name);

								Configuration::updateValue('home_box_image_'.$language['id_lang'], $name);
						}
					}
					if(isset($_FILES['home_box_bg'])){
						
						$type = strtolower(substr(strrchr($_FILES['home_box_bg']['name'], '.'), 1));
						$imagesize = array();
						$imagesize = @getimagesize($_FILES['home_box_bg']['tmp_name']);
					
						if (isset($_FILES['home_box_bg']['tmp_name']) &&
								!empty($_FILES['home_box_bg']['tmp_name']) &&
								!empty($imagesize) &&
								in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
								in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

								$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
								$salt = sha1(microtime());
								$name = Tools::encrypt($_FILES['home_box_bg']['name'].$salt).'.'.$type;
								if ($error = ImageManager::validateUpload($_FILES['home_box_bg']))
									$errors[] = $error;
								elseif (!$temp_name || !move_uploaded_file($_FILES['home_box_bg']['tmp_name'], $temp_name))
									return false;
								elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/uploads/bg/'.$name, null, null, $type))
									$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
								if (isset($temp_name))
									@unlink($temp_name);

								Configuration::updateValue('home_box_bg', $name);
						}
					}
				
				}
				$output .= $this->displayConfirmation($this->l('Home page popup settings updated'));

		}elseif(Tools::isSubmit('submitproductpagepopup')){
				$languages = Language::getLanguages(false);

				Configuration::updateValue('product_page_popup_enable', strval(Tools::getValue('product_page_popup_enable')));
				Configuration::updateValue('product_box_width', strval(Tools::getValue('product_box_width')));
				Configuration::updateValue('product_box_height', strval(Tools::getValue('product_box_height')));
				Configuration::updateValue('product_box_cookie', strval(Tools::getValue('product_box_cookie')));
				Configuration::updateValue('each_product_different_content', strval(Tools::getValue('enable_multi_content')));
				Configuration::updateValue('product_content_type', strval(Tools::getValue('product_content_type')));
				Configuration::updateValue('product_gmaps_link', strval(Tools::getValue('product_gmaps_link')));
				Configuration::updateValue('product_facebook_link', strval(Tools::getValue('product_facebook_link')));
				Configuration::updateValue('product_box_start_date', strval(Tools::getValue('product_box_start_date')));
				Configuration::updateValue('product_box_end_date', strval(Tools::getValue('product_box_end_date')));
				Configuration::updateValue('product_box_autoclose_time', intval(Tools::getValue('product_box_autoclose_time')));
				Configuration::updateValue('product_box_delay', intval(Tools::getValue('product_box_delay')));
				Configuration::updateValue('product_box_effect', strval(Tools::getValue('product_box_effect')));
				Configuration::updateValue('product_box_bg_repeat', strval(Tools::getValue('product_box_bg_repeat')));

				foreach($languages as $language){
					Configuration::updateValue('product_description_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('product_description_'.$language['id_lang']))));
					Configuration::updateValue('product_youtube_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('product_youtube_link_'.$language['id_lang']))));
					Configuration::updateValue('product_vimeo_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('product_vimeo_link_'.$language['id_lang']))));
					Configuration::updateValue('product_image_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('product_image_link_'.$language['id_lang']))));

					if(isset($_FILES['product_image_input_'.$language['id_lang']])){
						
						$type = strtolower(substr(strrchr($_FILES['product_image_input_'.$language['id_lang']]['name'], '.'), 1));
						$imagesize = array();
						$imagesize = @getimagesize($_FILES['product_image_input_'.$language['id_lang']]['tmp_name']);
					
						if (isset($_FILES['product_image_input_'.$language['id_lang']]['tmp_name']) &&
								!empty($_FILES['product_image_input_'.$language['id_lang']]['tmp_name']) &&
								!empty($imagesize) &&
								in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
								in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

								$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
								$salt = sha1(microtime());
								$name = Tools::encrypt($_FILES['product_image_input_'.$language['id_lang']]['name'].$salt).'.'.$type;
								if ($error = ImageManager::validateUpload($_FILES['product_image_input_'.$language['id_lang']]))
									$errors[] = $error;
								elseif (!$temp_name || !move_uploaded_file($_FILES['product_image_input_'.$language['id_lang']]['tmp_name'], $temp_name))
									return false;
								elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/uploads/'.$name, null, null, $type))
									$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
								if (isset($temp_name))
									@unlink($temp_name);

								Configuration::updateValue('product_box_image_'.$language['id_lang'], $name);
						}
					}
					if(isset($_FILES['product_box_bg'])){
						
						$type = strtolower(substr(strrchr($_FILES['product_box_bg']['name'], '.'), 1));
						$imagesize = array();
						$imagesize = @getimagesize($_FILES['product_box_bg']['tmp_name']);
					
						if (isset($_FILES['product_box_bg']['tmp_name']) &&
								!empty($_FILES['product_box_bg']['tmp_name']) &&
								!empty($imagesize) &&
								in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
								in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

								$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
								$salt = sha1(microtime());
								$name = Tools::encrypt($_FILES['product_box_bg']['name'].$salt).'.'.$type;
								if ($error = ImageManager::validateUpload($_FILES['product_box_bg']))
									$errors[] = $error;
								elseif (!$temp_name || !move_uploaded_file($_FILES['product_box_bg']['tmp_name'], $temp_name))
									return false;
								elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/uploads/bg/'.$name, null, null, $type))
									$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
								if (isset($temp_name))
									@unlink($temp_name);

								Configuration::updateValue('product_box_bg', $name);
						}
					}
				}
				$output .= $this->displayConfirmation($this->l('Product page popup settings updated'));

		}
		elseif(Tools::isSubmit('submitcategorypagepopup')){
				$languages = Language::getLanguages(false);

				Configuration::updateValue('category_page_popup_enable', strval(Tools::getValue('category_page_popup_enable')));
				Configuration::updateValue('category_box_width', strval(Tools::getValue('category_box_width')));
				Configuration::updateValue('category_box_height', strval(Tools::getValue('category_box_height')));
				Configuration::updateValue('category_box_cookie', strval(Tools::getValue('category_box_cookie')));
				Configuration::updateValue('category_box_start_date', strval(Tools::getValue('category_box_start_date')));
				Configuration::updateValue('category_box_end_date', strval(Tools::getValue('category_box_end_date')));
				Configuration::updateValue('category_box_autoclose_time', intval(Tools::getValue('category_box_autoclose_time')));
				Configuration::updateValue('category_box_delay', intval(Tools::getValue('category_box_delay')));
				Configuration::updateValue('category_box_effect', strval(Tools::getValue('category_box_effect')));
				Configuration::updateValue('category_box_bg_repeat', strval(Tools::getValue('category_box_bg_repeat')));

				if(isset($_FILES['category_box_bg'])){
					$type = strtolower(substr(strrchr($_FILES['category_box_bg']['name'], '.'), 1));
					$imagesize = array();
					$imagesize = @getimagesize($_FILES['category_box_bg']['tmp_name']);
					if (isset($_FILES['category_box_bg']['tmp_name']) &&
							!empty($_FILES['category_box_bg']['tmp_name']) &&
							!empty($imagesize) &&
							in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
							in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

							$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
							$salt = sha1(microtime());
							$name = Tools::encrypt($_FILES['category_box_bg']['name'].$salt).'.'.$type;
							if ($error = ImageManager::validateUpload($_FILES['category_box_bg']))
								$errors[] = $error;
							elseif (!$temp_name || !move_uploaded_file($_FILES['category_box_bg']['tmp_name'], $temp_name))
								return false;
							elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/uploads/bg/'.$name, null, null, $type))
								$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
							if (isset($temp_name))
								@unlink($temp_name);

							Configuration::updateValue('category_box_bg', $name);
					}
				}

				$output .= $this->displayConfirmation($this->l('Category page popup settings updated'));

		}
		elseif(Tools::isSubmit('submitcustompagepopup')){
				$languages = Language::getLanguages(false);

				Configuration::updateValue('custom_page_popup_enable', strval(Tools::getValue('custom_page_popup_enable')));
				Configuration::updateValue('custom_box_width', strval(Tools::getValue('custom_box_width')));
				Configuration::updateValue('custom_box_height', strval(Tools::getValue('custom_box_height')));
				Configuration::updateValue('custom_box_cookie', strval(Tools::getValue('custom_box_cookie')));
				Configuration::updateValue('custom_content_type', strval(Tools::getValue('custom_content_type')));
				Configuration::updateValue('custom_gmaps_link', strval(Tools::getValue('custom_gmaps_link')));
				Configuration::updateValue('custom_facebook_link', strval(Tools::getValue('custom_facebook_link')));
				Configuration::updateValue('custom_box_start_date', strval(Tools::getValue('custom_box_start_date')));
				Configuration::updateValue('custom_box_end_date', strval(Tools::getValue('custom_box_end_date')));
				Configuration::updateValue('custom_box_autoclose_time', intval(Tools::getValue('custom_box_autoclose_time')));
				Configuration::updateValue('custom_box_delay', intval(Tools::getValue('custom_box_delay')));
				Configuration::updateValue('custom_box_effect', strval(Tools::getValue('custom_box_effect')));
				Configuration::updateValue('custom_box_bg_repeat', strval(Tools::getValue('custom_box_bg_repeat')));

				foreach($languages as $language){
					Configuration::updateValue('custom_description_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('custom_description_'.$language['id_lang']))));
					Configuration::updateValue('custom_youtube_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('custom_youtube_link_'.$language['id_lang']))));
					Configuration::updateValue('custom_vimeo_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('custom_vimeo_link_'.$language['id_lang']))));
					Configuration::updateValue('custom_image_link_'.$language['id_lang'], stripslashes(htmlspecialchars(Tools::getValue('custom_image_link_'.$language['id_lang']))));

					if(isset($_FILES['custom_image_input_'.$language['id_lang']])){
						
						$type = strtolower(substr(strrchr($_FILES['custom_image_input_'.$language['id_lang']]['name'], '.'), 1));
						$imagesize = array();
						$imagesize = @getimagesize($_FILES['custom_image_input_'.$language['id_lang']]['tmp_name']);
					
						if (isset($_FILES['custom_image_input_'.$language['id_lang']]['tmp_name']) &&
								!empty($_FILES['custom_image_input_'.$language['id_lang']]['tmp_name']) &&
								!empty($imagesize) &&
								in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
								in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

								$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
								$salt = sha1(microtime());
								$name = Tools::encrypt($_FILES['custom_image_input_'.$language['id_lang']]['name'].$salt).'.'.$type;
								if ($error = ImageManager::validateUpload($_FILES['custom_image_input_'.$language['id_lang']]))
									$errors[] = $error;
								elseif (!$temp_name || !move_uploaded_file($_FILES['custom_image_input_'.$language['id_lang']]['tmp_name'], $temp_name))
									return false;
								elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/uploads/'.$name, null, null, $type))
									$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
								if (isset($temp_name))
									@unlink($temp_name);

								Configuration::updateValue('custom_box_image_'.$language['id_lang'], $name);
						}
					}
				}
				if(isset($_FILES['custom_box_bg'])){
					$type = strtolower(substr(strrchr($_FILES['custom_box_bg']['name'], '.'), 1));
					$imagesize = array();
					$imagesize = @getimagesize($_FILES['custom_box_bg']['tmp_name']);
					if (isset($_FILES['custom_box_bg']['tmp_name']) &&
							!empty($_FILES['custom_box_bg']['tmp_name']) &&
							!empty($imagesize) &&
							in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
							in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

							$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
							$salt = sha1(microtime());
							$name = Tools::encrypt($_FILES['custom_box_bg']['name'].$salt).'.'.$type;
							if ($error = ImageManager::validateUpload($_FILES['custom_box_bg']))
								$errors[] = $error;
							elseif (!$temp_name || !move_uploaded_file($_FILES['custom_box_bg']['tmp_name'], $temp_name))
								return false;
							elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/uploads/bg/'.$name, null, null, $type))
								$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
							if (isset($temp_name))
								@unlink($temp_name);

							Configuration::updateValue('custom_box_bg', $name);
					}
				}
				$output .= $this->displayConfirmation($this->l('Custom page popup settings updated'));

		}elseif(Tools::isSubmit('submitnl')){
			$languages = Language::getLanguages(false);
			$psv = floatval(substr(_PS_VERSION_,0,3));

			/*Configuration::updateValue('nl_verification_email', strval(Tools::getValue('nl_verification_email')));
			Configuration::updateValue('nl_confirmation_email', strval(Tools::getValue('nl_confirmation_email')));
			Configuration::updateValue('nl_coupon', strval(Tools::getValue('nl_coupon')));

			Configuration::updateValue('nlc_valid', intval(Tools::getValue('nlc_valid')));
			Configuration::updateValue('nlc_highlight', intval(Tools::getValue('nlc_highlight')));
			Configuration::updateValue('nlc_partial_use', intval(Tools::getValue('nlc_partial_use')));
			Configuration::updateValue('nlc_minimum_amount', intval(Tools::getValue('nlc_minimum_amount')));
			Configuration::updateValue('nlc_minimum_amount_currency', intval(Tools::getValue('nlc_minimum_amount_currency')));
			Configuration::updateValue('nlc_minimum_amount_tax', intval(Tools::getValue('nlc_minimum_amount_tax')));
			Configuration::updateValue('nlc_minimum_amount_shipping', intval(Tools::getValue('nlc_minimum_amount_shipping')));
			Configuration::updateValue('nlc_quantity', intval(Tools::getValue('nlc_quantity')));
			Configuration::updateValue('nlc_free_shipping', intval(Tools::getValue('nlc_free_shipping')));
			Configuration::updateValue('nlc_apply_discount', strval(Tools::getValue('nlc_apply_discount')));
			Configuration::updateValue('nlc_reduction_percent', floatval(Tools::getValue('nlc_reduction_percent')));
			Configuration::updateValue('nlc_reduction_amount', floatval(Tools::getValue('nlc_reduction_amount')));
			Configuration::updateValue('nlc_reduction_currency', intval(Tools::getValue('nlc_reduction_currency')));
			Configuration::updateValue('nlc_reduction_tax', intval(Tools::getValue('nlc_reduction_tax')));
			Configuration::updateValue('nlc_apply_discount_to', strval(Tools::getValue('nlc_apply_discount_to')));
			
			if(Tools::getValue('nlc_apply_discount_to') == 'specific'){
				$product = (int)trim(Tools::getValue('nlc_reduction_product'));

				$error = false;
				if ($psv >= 1.5){
					$product_row = Db::getInstance()->ExecuteS("SELECT pl.id_product FROM `"._DB_PREFIX_."product_lang` pl LEFT JOIN `"._DB_PREFIX_."product_shop` ps on ps.id_product = pl.id_product WHERE pl.id_product = '$product'");
				}else{
					$product_row = Db::getInstance()->ExecuteS("SELECT id_product FROM `"._DB_PREFIX_."product_lang` WHERE id_product = '$product'");
				}
				if(!is_array($product_row) || sizeof($product_row) == 0) 
				{
					$error = true;
				}

				if(!$error){
					Configuration::updateValue('nlc_reduction_product', $product);	
				}else{
					$output .= '<div class="error">Invalid product id</div>';
				}
			}
			*/
			Configuration::updateValue('pn_send_voucher_email', strval(trim(Tools::getValue('pn_send_voucher_email'))));
			Configuration::updateValue('nl_welcome_coupon', strval(trim(Tools::getValue('nl_welcome_coupon'))));
			Configuration::updateValue('nl_terms_url', strval(trim(Tools::getValue('nl_terms_url'))));
			foreach($languages as $language){
				//Configuration::updateValue('nlc_name_'.$language['id_lang'], strval(Tools::getValue('nlc_name_'.$language['id_lang'])));
				Configuration::updateValue('nlc_popup_description_'.$language['id_lang'], strval(Tools::getValue('nlc_popup_description_'.$language['id_lang'])));
			}

			$output .= $this->displayConfirmation($this->l('Newsletter settings has been updated'));

		}elseif(Tools::isSubmit('submitresponsivesettings')){
			Configuration::updateValue('popup_gls_enable_responsive', strval(trim(Tools::getValue('popup_gls_enable_responsive'))));
			Configuration::updateValue('popup_gls_resize_start_point', strval(trim(Tools::getValue('popup_gls_resize_start_point'))));
			Configuration::updateValue('popup_gls_hide_start_point', strval(trim(Tools::getValue('popup_gls_hide_start_point'))));

			$output .= $this->displayConfirmation($this->l('Responsive settings has been updated'));
		
		}elseif(Tools::isSubmit('submitloginsettings')){
			Configuration::updateValue('popup_login_enable_popup_login', Tools::getValue('popup_login_enable_popup_login'));
			Configuration::updateValue('popup_login_enable_login', Tools::getValue('popup_login_enable_login'));
			Configuration::updateValue('popup_login_enable_register', Tools::getValue('popup_login_enable_register'));
			Configuration::updateValue('LOGIN_BOX_BG_REPEAT', Tools::getValue('login_box_bg_repeat'));

			if(isset($_FILES['login_box_bg'])){
				$type = strtolower(substr(strrchr($_FILES['login_box_bg']['name'], '.'), 1));
				$imagesize = array();
				$imagesize = @getimagesize($_FILES['login_box_bg']['tmp_name']);
				if (isset($_FILES['login_box_bg']['tmp_name']) &&
						!empty($_FILES['login_box_bg']['tmp_name']) &&
						!empty($imagesize) &&
						in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
						in_array($type, array('jpg', 'gif', 'jpeg', 'png'))){

						$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
						$salt = sha1(microtime());
						$name = Tools::encrypt($_FILES['login_box_bg']['name'].$salt).'.'.$type;
						if ($error = ImageManager::validateUpload($_FILES['login_box_bg']))
							$errors[] = $error;
						elseif (!$temp_name || !move_uploaded_file($_FILES['login_box_bg']['tmp_name'], $temp_name))
							return false;
						elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/uploads/bg/'.$name, null, null, $type))
							$errors[] = $this->displayError($this->l('An error occurred during the image upload process.'));
						if (isset($temp_name))
							@unlink($temp_name);

						Configuration::updateValue('LOGIN_BOX_BG', $name);
				}
			}

			$output .= $this->displayConfirmation($this->l('Login settings has been updated'));

		}elseif(Tools::isSubmit('submitfbconnectsettings')){
			Configuration::updateValue('popup_login_enable_facebook', Tools::getValue('popup_login_enable_facebook'));
			Configuration::updateValue('popup_fb_connect_app_id', strval(trim(Tools::getValue('popup_fb_connect_app_id'))));
			Configuration::updateValue('popup_fb_connect_position_top', Tools::getValue('popup_fb_connect_position_top'));
			Configuration::updateValue('popup_fb_connect_position_custom', Tools::getValue('popup_fb_connect_position_custom'));
			Configuration::updateValue('PN_SOCIAL_REDIRECT', Tools::getValue('social_redirect'));

			$output .= $this->displayConfirmation($this->l('Facebook Connect settings has been updated'));

		}elseif(Tools::isSubmit('submittwconnectsettings')){
			Configuration::updateValue('popup_login_enable_twitter', Tools::getValue('popup_login_enable_twitter'));
			Configuration::updateValue('popup_tw_connect_consumer_key', strval(trim(Tools::getValue('popup_tw_connect_consumer_key'))));
			Configuration::updateValue('popup_tw_connect_consumer_secret', strval(trim(Tools::getValue('popup_tw_connect_consumer_secret'))));
			Configuration::updateValue('popup_tw_connect_position_top', Tools::getValue('popup_tw_connect_position_top'));
			Configuration::updateValue('popup_tw_connect_position_custom', Tools::getValue('popup_tw_connect_position_custom'));
			Configuration::updateValue('PN_SOCIAL_REDIRECT', Tools::getValue('social_redirect'));

			$output .= $this->displayConfirmation($this->l('Twitter Connect settings has been updated'));

		}elseif(Tools::isSubmit('submitgpconnectsettings')){
			Configuration::updateValue('popup_login_enable_google', Tools::getValue('popup_login_enable_google'));
			Configuration::updateValue('popup_gp_connect_client_id', strval(trim(Tools::getValue('popup_gp_connect_client_id'))));
			Configuration::updateValue('popup_gp_connect_position_top', Tools::getValue('popup_gp_connect_position_top'));
			Configuration::updateValue('popup_gp_connect_position_custom', Tools::getValue('popup_gp_connect_position_custom'));
			Configuration::updateValue('PN_SOCIAL_REDIRECT', Tools::getValue('social_redirect'));

			$output .= $this->displayConfirmation($this->l('Google Connect settings has been updated'));

		}elseif(Tools::isSubmit('remove_home_box_bg')){
			$file = _PS_MODULE_DIR_.$this->name.'/uploads/bg/'.$this->home_box_bg;
			if(file_exists($file))
				unlink($file);
			Configuration::updateValue('home_box_bg', '');

			$output .= $this->displayConfirmation($this->l('Home page popup Background successfully removed'));
		
		}elseif(Tools::isSubmit('remove_product_box_bg')){
			$file = _PS_MODULE_DIR_.$this->name.'/uploads/bg/'.$this->product_box_bg;
			if(file_exists($file))
				unlink($file);
			Configuration::updateValue('product_box_bg', '');

			$output .= $this->displayConfirmation($this->l('Product page popup Background successfully removed'));

		}elseif(Tools::isSubmit('remove_category_box_bg')){
			$file = _PS_MODULE_DIR_.$this->name.'/uploads/bg/'.$this->category_box_bg;
			if(file_exists($file))
				unlink($file);
			Configuration::updateValue('category_box_bg', '');

			$output .= $this->displayConfirmation($this->l('Category page popup Background successfully removed'));

		}elseif(Tools::isSubmit('remove_custom_box_bg')){
			$file = _PS_MODULE_DIR_.$this->name.'/uploads/bg/'.$this->custom_box_bg;
			if(file_exists($file))
				unlink($file);
			Configuration::updateValue('custom_box_bg', '');

			$output .= $this->displayConfirmation($this->l('Custom page popup Background successfully removed'));

		}elseif(Tools::isSubmit('remove_login_box_bg')){
			$file = _PS_MODULE_DIR_.$this->name.'/uploads/bg/'.$this->login_box_bg;
			if(file_exists($file))
				unlink($file);
			Configuration::updateValue('LOGIN_BOX_BG', '');

			$output .= $this->displayConfirmation($this->l('Login popup Background successfully removed'));
		}

		$this->_globalVars();


		return $output.$this->displayForm();
		
	}

	public function _getModuleUrl(){
		$psv = (float)substr(_PS_VERSION_,0,3);

		if($psv >= 1.6){
			$module_host_domain = Tools::getAdminUrl();
			$admin_web = $this->context->controller->admin_webpath;
		}else{
			$module_host_domain = Tools::getHttpHost(true).__PS_BASE_URI__;
			$admin_web = substr(str_ireplace(_PS_ROOT_DIR_, '', getcwd()),1);
		}
		$module_token = $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;

		$module_url = $module_host_domain.$admin_web."/".$module_token;

		return $module_url;
	}

	protected function getTinymceJS()
	{
		$psv = (float)substr(_PS_VERSION_,0,3);
		$ps_version_array = explode('.', _PS_VERSION_);
		if($psv >= 1.6 && (((int)$ps_version_array[2] == 0 && (int)$ps_version_array[3] >= 13) || (int)$ps_version_array[2] > 0))
			$path = 'admin/';
		else
			$path = '';

		return '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/'.$path.'tinymce.inc.js"></script>';
	}

	public function displayForm(){
		$output = '';
		$this->context->controller->addCSS($this->_path.'css/admin_style.css', 'all');
		$this->context->controller->addJS($this->_path.'js/admin_script.js');
		$this->context->controller->addJqueryUI("ui.datepicker");

		$languages = Language::getLanguages(false);
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));

		$psv = (float)substr(_PS_VERSION_,0,3);

		$output .= '
		<div class="row">
			<div class="popupTabs col-lg-2">
				<div class="list-group">
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'homePagePopup' || !Tools::getValue('pfpopuptab')) ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=homePagePopup">'
						.$this->l('Home Page Popup').'
					</a>
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'productPagePopup') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=productPagePopup">'
						.$this->l('Product Page Popup').'
					</a>
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'categoryPagePopup') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=categoryPagePopup">'
						.$this->l('Category Page Popup').'
					</a>
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'customPagePopup') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=customPagePopup">'
						.$this->l('Custom Page Popup').'
					</a>
					<!--
                    <a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'newsletter') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=newsletter">'
						.$this->l('Newsletter').'
					</a>
                    -->
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'responsive') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=responsive">'
						.$this->l('Responsive Settings').'
					</a>
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'loginRegister') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=loginRegister">'
						.$this->l('Login And Register Settings').'
					</a>
					<!--
                    <a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'facebookConnect') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=facebookConnect">'
						.$this->l('Facebook Connect').'
					</a>
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'twitterConnect') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=twitterConnect">'
						.$this->l('Twitter Connect').'
					</a>
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'googleConnect') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=googleConnect">'
						.$this->l('Google+ Connect').'
					</a>
					<a class="list-group-item '.((Tools::getValue('pfpopuptab') == 'help') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=help">'
						.$this->l('Help').'
					</a>
                    -->
				</div>
			</div>
			
			<div class="form-horizontal col-lg-10">';

				if(!Tools::getValue('pfpopuptab') || Tools::getValue('pfpopuptab') == 'homePagePopup')
					include (_PS_MODULE_DIR_.$this->name.'/admin/homePagePopup.php');
				elseif(Tools::getValue('pfpopuptab') == 'productPagePopup')
					include (_PS_MODULE_DIR_.$this->name.'/admin/productPagePopup.php');
				elseif(Tools::getValue('pfpopuptab') == 'categoryPagePopup')
					include (_PS_MODULE_DIR_.$this->name.'/admin/categoryPagePopup.php');
				elseif(Tools::getValue('pfpopuptab') == 'customPagePopup')
					include (_PS_MODULE_DIR_.$this->name.'/admin/customPagePopup.php');
				elseif(Tools::getValue('pfpopuptab') == 'newsletter')
					include (_PS_MODULE_DIR_.$this->name.'/admin/newsletter.php');
				elseif(Tools::getValue('pfpopuptab') == 'responsive')
					include (_PS_MODULE_DIR_.$this->name.'/admin/responsive.php');
				elseif(Tools::getValue('pfpopuptab') == 'loginRegister')
					include (_PS_MODULE_DIR_.$this->name.'/admin/loginRegister.php');
				elseif(Tools::getValue('pfpopuptab') == 'facebookConnect')
					include (_PS_MODULE_DIR_.$this->name.'/admin/facebookConnect.php');
				elseif(Tools::getValue('pfpopuptab') == 'twitterConnect')
					include (_PS_MODULE_DIR_.$this->name.'/admin/twitterConnect.php');
				elseif(Tools::getValue('pfpopuptab') == 'googleConnect')
					include (_PS_MODULE_DIR_.$this->name.'/admin/googleConnect.php');
				elseif(Tools::getValue('pfpopuptab') == 'help')
					include (_PS_MODULE_DIR_.$this->name.'/admin/help.php');
				

				$output .= '
			</div>

		</div>
		'.($psv < 1.6?'
		<style type="text/css">
			.col-lg-2 {width: 16.66667%;float:left;}
			.col-lg-10{width: 83.33333%;float:left;}
			a.list-group-item {
				color: #555;
				position: relative;
				display: block;
				padding: 10px 15px;
				margin-bottom: -1px;
				background-color: #fff;
				border: 1px solid #ddd;
				webkit-box-sizing: border-box;
				-moz-box-sizing: border-box;
				box-sizing: border-box;
			}
			.list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus{
				z-index: 2;
				color: #fff;
				background-color: #00aff0;
				border-color: #00aff0;
			}

			</style>
		':'').'';
		global $cookie;
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		$output .= '
		<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
			'.$this->getTinymceJS().'
		<script type="text/javascript">
			$(document).ready(function(){

				//small hack for PS 1.5
				if ( window.tinySetup ) {
					tinySetup({
						editor_selector :"autoload_rte"
					});
				}
			});
		</script>';
		return $output;
	
	}
	protected function getHomePopupContent($id_lang){
		return stripcslashes(htmlspecialchars_decode(Configuration::get('home_description_'.$id_lang)));
	}
	protected function getProductPopupContent($id_lang){
		return stripcslashes(htmlspecialchars_decode(Configuration::get('product_description_'.$id_lang)));
	}
	protected function getCustomPageContent($id_lang){
		return stripcslashes(htmlspecialchars_decode(Configuration::get('custom_description_'.$id_lang)));	
	}
	protected function getPopupContentByProductId($id_product){
	    $result = Db::getInstance()->ExecuteS('SELECT popup_notification, id_lang FROM '._DB_PREFIX_.'product_lang WHERE id_product = ' . (int)$id_product);
	    if(!$result)
	        return array();
	 
	    foreach ($result as $field) {
	        $fields[$field['id_lang']] = $field['popup_notification'];
	    }
	 
	    return $fields;
	}
	protected function getProductContentByProductIdAndLang($id_product, $id_lang){
		$result = Db::getInstance()->ExecuteS('SELECT popup_notification FROM '._DB_PREFIX_.'product_lang WHERE id_product = '.(int)$id_product.' AND id_lang = '.(int)$id_lang);
		return $result[0]['popup_notification'];
	}
	protected function getCategoryContentByIdAndLang($id_category, $id_lang){
		$result = Db::getInstance()->ExecuteS('SELECT popup_notification FROM '._DB_PREFIX_.'category_lang WHERE id_category = '.(int)$id_category.' AND id_lang = '.(int)$id_lang);
		return $result[0]['popup_notification'];
	}
	protected function getFacebookLikeBox($page_link, $width, $height){
		return '<div id="fb-root"></div>
						<script>
							(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=595097330556590";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, \'script\', \'facebook-jssdk\'));
						</script>
						<div class="fb-like-box" data-href="'.$page_link.'" data-width="'.($width-30).'" data-height="'.($height-30).'" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>';
	}

	protected function getNewsletterBlock(){
		return '<form action="" method="post" id="popup_subscribe_form">
					<h1>'.(Configuration::get('nl_welcome_coupon')?$this->l('Subscribe and Get Discount Coupon!'):$this->l('Subscribe to our Newsletter')).'</h1>
					<div class="form-group">
						<input class="form-control grey" type="text" name="popup_first_name" id="popup_first_name" placeholder="'.$this->l('First Name').'">
					</div>
					<div class="form-group">
						<input class="form-control grey" type="text" name="popup_last_name" id="popup_last_name" placeholder="'.$this->l('Last Name').'">
					</div>
					<div class="form-group">
						<input class="form-control grey" type="text" name="popup_email" id="popup_email" placeholder="'.$this->l('Email Address').'">
					</div>
					'.(Configuration::get('nl_terms_url')?'
						<div class="form-group">
							<input type="hidden" name="popup_terms" value="0">
							<input type="checkbox" id="popup_terms" name="popup_terms" value="1">
							<label for="popup_terms" title="'.$this->l('Terms of Use').'">'.$this->l('I agree the').' <a href="'.Configuration::get('nl_terms_url').'" target="_blank">'.$this->l('Terms of Use').'</a>.</label>
						</div>
					':'').'
					<div class="form-group">
						<center>
							<button type="submit" name="submitPopupNewsletter">
								<img src="'._MODULE_DIR_.'/'.$this->name.'/images/ajax-loader.gif" class="popup-ajax-loader">
								'.$this->l('Subscribe').'
							</button>
							<!--<input type="submit" name="submitPopupNewsletter" value="'.$this->l('Subscribe').'">-->
						</center>
						<input type="hidden" name="action" value="0">
					</div>
				</form>';
	}

	protected function getRegisterForm(){
		$html = '';
		$callback_url = Tools::getHTTPHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/twitterconnect.php';

		if(!$this->context->customer->isLogged())
			$html .= '
					<div id="pf_popup_login_box" class="pf_popup_only_register">
						<div id="pf_popup_login_content">
		                	<div id="pf_popup_login_box_left" class="pf_popup_login_avgrund-blur">
			                    <h1>'.$this->l('Create an account').'</h1>

			                    '.($this->_popup_login_enable_facebook == 'true' || $this->_popup_login_enable_twitter == 'true'?'
				                	<!-- Social Buttons -->
				                    <div class="pf_popup_login_social">
				                        Login using social network:<br>
				                        '.($this->_popup_login_enable_twitter == 'true'?'
				                        	<div class="pf_popup_login_twt pf_connect pf_twitter">
					                            <a href="#" onclick="twitterPopup = window.open(\''.$callback_url.'\', \'_blank\', \'toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=300, width=700, height=600\');">'.$this->l('Sign In with Twitter').'</a>
					                        </div>
				                    	':'').
				                        ($this->_popup_login_enable_facebook == 'true'?'
				                        	<div class="pf_popup_login_fb pf_connect pf_facebook">
					                            <a href="#" onclick="fb_login();">'.$this->l('Sign In with Facebook').'</a>
					                        </div>
				                    	':'').'
				                    </div>
				            	':'').'

			                    <div id="pf_popup_login_login">'
			                    	.($this->_popup_login_enable_facebook == 'true' || $this->_popup_login_enable_twitter == 'true'?'
				                    	'.$this->l('Or').'
				                	':'').'
			                        '.$this->l('Insert your account information:').'<br>
			                        
			                        <!-- Register Form -->
			                        <form name="pf_popup_login_login" id="pf_popup_login_register_form" action="#" method="POST" accept-charset="utf-8">
			                            <input type="text" placeholder="'.$this->l('First Name').'" id="pf_popup_login_first_name" name="pf_popup_login_first_name" class="pf_popup_login_register pf_popup_login_user" title="'.$this->l('First Name').'">
			                            <input type="text" placeholder="'.$this->l('Last Name').'" id="pf_popup_login_last_name" name="pf_popup_login_last_name" class="pf_popup_login_register pf_popup_login_user" title="'.$this->l('Last Name').'">
			                            <input type="email" placeholder="'.$this->l('Email').'" id="pf_popup_login_email" class="pf_popup_login_register pf_popup_login_email" title="'.$this->l('Email').'">
			                            <input type="password" name="pf_popup_login_password" id="pf_popup_login_register_password" placeholder="'.$this->l('Password').'" class="pf_popup_login_register pf_popup_login_password" title="'.$this->l('Password').'">
			                            <!-- Label for PopupBox -->
			                            <!--<label for="pf_popup_login_terms" class="pf_popup_login_check_terms" id="pf_popup_login_lblAssociateWithCheckBox" title="Terms of Use"><input type="checkbox" class="pf_popup_login_check_box" id="pf_popup_login_terms"> 
			                            		'.$this->l('I agree the').' <a href="#" id="pf_popup_login_tou">'.$this->l('Terms of Use').'</a>.</label>-->
			                            <input type="submit" value="'.$this->l('Register').'" class="pf_popup_login_btn_register">
			                        </form>
			                        
			                    </div>
			                </div>
		            	</div>
	            	</div>
		            <style type="text/css">
		            	.popup_remove_padding .white-popup{margin0;padding:0;}
		            	#login-and-register-popup.white-popup{margin0;padding:0;}
		            	.pf_popup_only_register #pf_popup_login_box_left{width:100% !important;float:none;}
		            	#pf_popup_login_box.pf_popup_only_register .pf_popup_login_register, 
		            	#pf_popup_login_box.pf_popup_only_register .pf_popup_login_login{
		            		width:100%;
		            		-webkit-box-sizing: border-box;
							-moz-box-sizing: border-box;
							box-sizing: border-box;
		            	}
		            </style>';

        return $html;
	}

	protected function getLoginForm(){
		$html = '';
		$callback_url = Tools::getHTTPHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/twitterconnect.php';

		if(!$this->context->customer->isLogged())
			$html .= '
				<div id="pf_popup_login_box" class="pf_popup_only_login">
					<div id="pf_popup_login_content">
			            <div id="pf_popup_login_box_right" class="pf_popup_login_avgrund-blur">
			                <!-- Title -->
			                <h1>'.$this->l('Login').'</h1>
			                
			                '.($this->_popup_login_enable_facebook == 'true' || $this->_popup_login_enable_twitter == 'true'?'
			                	<!-- Social Buttons -->
			                    <div class="pf_popup_login_social">
			                        Login using social network:<br>
			                        '.($this->_popup_login_enable_twitter == 'true'?'
			                        	<div class="pf_popup_login_twt pf_connect pf_twitter">
				                            <a href="#" onclick="twitterPopup = window.open(\''.$callback_url.'\', \'_blank\', \'toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=300, width=700, height=600\');">'.$this->l('Sign In with Twitter').'</a>
				                        </div>
			                    	':'').
			                        ($this->_popup_login_enable_facebook == 'true'?'
			                        	<div class="pf_popup_login_fb pf_connect pf_facebook">
				                            <a href="#">'.$this->l('Sign In with Facebook').'</a>
				                        </div>
			                    	':'').'
			                    </div>
			            	':'').'
			                
			                <!-- Login Box -->
			                <div id="pf_popup_login_login">
			                    <!-- Login Subtitle -->
			                    '.($this->_popup_login_enable_facebook == 'true' || $this->_popup_login_enable_twitter == 'true'?'
			                    	'.$this->l('Or').'
			                	':'').'
			                		'.$this->l('insert your account information:').'<br>
			                    
			                    <!-- Login Form -->
			                    <form name="pf_popup_login_login_form" id="pf_popup_login_login_form" action="#" method="POST">
			                        <input type="text" placeholder="'.$this->l('Email').'" id="pf_popup_login_l_user" class="pf_popup_login_login pf_popup_login_user" title="'.$this->l('Email').'">
			                        <input type="password" name="pf_popup_login_password" placeholder="'.$this->l('Password').'" id="pf_popup_login_l_password" class="pf_popup_login_login pf_popup_login_password" title="'.$this->l('Password').'">
			                        <input type="submit" value="Login" class="pf_popup_login_btn_login">
			                    </form>                    
			                </div>
			                
			                <!-- Login Bottom Text -->
			                <div id="pf_popup_login_bottom_text">
			                    '.$this->l('Don\'t have an account?').' <a href="#" title="'.$this->l('Create new account').'">'.$this->l('Create new account').'</a>.<br>
			                    '.$this->l('Forgot your').' <a href="#" title="'.$this->l('Recovery Password').'">'.$this->l('Password').'</a>?
			                </div>
			            </div>
		            </div>
		        </div>
	            <style type="text/css">
	            	.popup_remove_padding .white-popup{margin0;padding:0;}
	            	#login-and-register-popup.white-popup{margin0;padding:0;}
	            	.pf_popup_only_login #pf_popup_login_box_right{
	            		float:none;
	            		width:100%;
	        		}
	        		.pf_popup_only_login #pf_popup_login_box_right input[type="text"], 
	        		.pf_popup_only_login #pf_popup_login_box_right input[type="password"]{
	        			width:100%;
	        			-webkit-box-sizing: border-box;
						-moz-box-sizing: border-box;
						box-sizing: border-box;
	        		}
	        		.pf_popup_only_login #pf_popup_login_box_right input[type="text"]{
	        			margin-right:0;
	        		}
	            </style>';

            return $html;
	}

	protected function getLoginAndRegisterForms($header = false){
		$html = '';
		$link = new link();
		$callback_url = Tools::getHTTPHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/twitterconnect.php';

		if(!$this->context->customer->isLogged())
			$html .= '
					<div id="pf_popup_login_box" class="pf_popup_login_both">
						<div id="pf_popup_login_content">
		                	<div id="pf_popup_login_box_left" class="pf_popup_login_avgrund-blur">
			                    <h1>'.$this->l('Register').'</h1>
			                    <div id="pf_popup_login_login">
			                        '.$this->l('Insert your account informations:').'<br>
			                        
			                        <!-- Register Form -->
			                        <form name="pf_popup_login_login'.($header?'_header':'').'" id="pf_popup_login_register_form" action="#" method="POST" accept-charset="utf-8">
			                            <input type="text" placeholder="'.$this->l('First Name').'" id="pf_popup_login_first_name" name="pf_popup_login_first_name" class="pf_popup_login_register pf_popup_login_user" title="'.$this->l('First Name').'">
			                            <input type="text" placeholder="'.$this->l('Last Name').'" id="pf_popup_login_last_name" name="pf_popup_login_last_name" class="pf_popup_login_register pf_popup_login_user" title="'.$this->l('Last Name').'">
			                            <input type="email" placeholder="'.$this->l('Email').'" id="pf_popup_login_email" class="pf_popup_login_register pf_popup_login_email" title="'.$this->l('Email').'">
			                            <input type="password" name="pf_popup_login_password" id="pf_popup_login_register_password" placeholder="'.$this->l('Password').'" class="pf_popup_login_register pf_popup_login_password" title="'.$this->l('Password').'">
			                            <input type="submit" value="'.$this->l('Register').'" class="pf_popup_login_btn_register">
			                        </form>
			                        
			                    </div>
			                </div>
			                
		                	<!-- Right Box (Login form) -->
			                <div id="pf_popup_login_box_right" class="pf_popup_login_avgrund-blur">
			                    <h1>'.$this->l('Login').'</h1>
			                    
			                    '.($this->_popup_login_enable_facebook == 'true' || $this->_popup_login_enable_twitter == 'true' || $this->_popup_login_enable_google == 'true'?'
			                    	<!-- Social Buttons -->
				                    <div class="pf_popup_login_social">
				                        '.$this->l('Login using social network:').'<br>
				                        '.($this->_popup_login_enable_twitter == 'true'?'
				                        	<div class="pf_popup_login_twt pf_connect pf_twitter">
					                            <a href="#" onclick="twitterPopup = window.open(\''.$callback_url.'\', \'_blank\', \'toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=300, width=700, height=600\');">'.$this->l('Sign In with Twitter').'</a>
					                        </div>
			                        	':'').
				                        ($this->_popup_login_enable_facebook == 'true'?'
				                        	<div class="pf_popup_login_fb pf_connect pf_facebook">
					                            <a href="#">'.$this->l('Sign In with Facebook').'</a>
					                        </div>
			                        	':'').
			                        	($this->_popup_login_enable_google == 'true'?'
				                        	<div class="pf_popup_login_gp pf_connect pf_googlePlus" style="float:left;">
												<span
												    class="g-signin"
												    data-callback="gpConnectCallback"
												    data-clientid="'.$this->_popup_gp_connect_client_id.'"
												    data-cookiepolicy="single_host_origin"	    
												    data-scope="email">'.$this->l('Sign In with Google').'
												</span>
											</div>
			                        	':'').'
				                    </div>
		                    	':'').'
			                    
			                    <!-- Login Box -->
			                    <div id="pf_popup_login_login" style="clear:both;">
				                    <!-- Login Subtitle -->
				                    '.($this->_popup_login_enable_facebook == 'true' || $this->_popup_login_enable_twitter == 'true' || $this->_popup_login_enable_google == 'true'?'
				                    	'.$this->l('Or').'
				                	':'').'
				                		'.$this->l('insert your account information:').'<br>
				                    
				                    <!-- Login Form -->
				                    <form name="pf_popup_login_login_form'.($header?'_header':'').'" id="pf_popup_login_login_form" action="#" method="POST">
				                        <input type="text" placeholder="'.$this->l('Email').'" id="pf_popup_login_l_user" class="pf_popup_login_login pf_popup_login_user" title="'.$this->l('Email').'">
				                        <input type="password" name="pf_popup_login_password" placeholder="'.$this->l('Password').'" id="pf_popup_login_l_password" class="pf_popup_login_login pf_popup_login_password" title="'.$this->l('Password').'">
				                        <input type="submit" value="'.$this->l('Login').'" class="pf_popup_login_btn_login">
				                    </form>                    
				                </div>
				                
				                <!-- Login Bottom Text -->
				                <div id="pf_popup_login_bottom_text">
				                    '.$this->l('Don\'t have an account?').' <a href="javascript:document.getElementById(\'pf_popup_login_first_name\').focus()" title="'.$this->l('Create new account').'">'.$this->l('Create new account').'</a>.<br>
				                    '.$this->l('Forgot your').' <a href="'.$link->getPageLink('password').'" title="'.$this->l('Recovery Password').'">'.$this->l('Password').'</a>?
				                </div>
			                </div>
		            	</div>
	            	</div>
	            <style type="text/css">
	            	.popup_remove_padding .white-popup{margin0;padding:0;}
	            	#login-and-register-popup.white-popup{margin0;padding:0;}
	            	.pf_popup_login_both .pf_popup_login_register, 
	            	.pf_popup_login_both .pf_popup_login_login{
	            		width:100%;
	            	}
	            	#pf_popup_login_box.pf_popup_login_both input{
	            		-webkit-box-sizing: border-box;
						-moz-box-sizing: border-box;
						box-sizing: border-box;
	            	}
	            	.pf_popup_login_both #pf_popup_login_box_right input[type="text"], 
	            	.pf_popup_login_both #pf_popup_login_box_right input[type="password"]{
	            		width:100%;
	            	}
	            </style>';
	            if($this->login_box_bg != '')
					$html .= '
						<style type="text/css">
							#login-and-register-popup{
								background: url('._MODULE_DIR_.$this->name.'/uploads/bg/'.$this->login_box_bg.');
								background-repeat: '.$this->login_box_bg_repeat.';
							}
							#pf_popup_login_box{background: transparent;}
						</style>';

            return $html;
	}

	public function isNewsletterRegistered($email){
		$sql1 = 'SELECT `email`, `active`
				FROM '._DB_PREFIX_.'newsletter
				WHERE `email` = \''.pSQL($email).'\'
				AND id_shop = '.$this->context->shop->id;


		$sql2 = 'SELECT `newsletter`
				FROM '._DB_PREFIX_.'customer
				WHERE `email` = \''.pSQL($email).'\'
				AND id_shop = '.$this->context->shop->id;

		$registered_news = Db::getInstance()->getRow($sql1);
		$registered_cust = Db::getInstance()->getRow($sql2);

		if(($registered_cust && isset($registered_cust['newsletter']) && $registered_cust['newsletter'] == '1') || ($registered_news && $registered_news['active'] == 1))
			return true;

		return false;
	}

	public function isCustomerExists($email){
		$sql = 'SELECT `id_customer`
				FROM '._DB_PREFIX_.'customer
				WHERE `email` = \''.pSQL($email).'\'
				AND id_shop = '.$this->context->shop->id;

		if (!$registered = Db::getInstance()->getRow($sql))
			return false;
		else
			return $registered['id_customer'];
	}

	public function checkCustomerSubscription($email){
		$customer = Db::getInstance()->ExecuteS("SELECT * FROM `"._DB_PREFIX_."popup_nl_voucher` WHERE email = '$email'");
		if(is_array($customer) && sizeof($customer) > 0)
			return $customer;
		else
			return false;
	}

	public function registerGuest($email, $active = true)
	{
		$sql1 = 'SELECT `email`, `active`
				FROM '._DB_PREFIX_.'newsletter
				WHERE `email` = \''.pSQL($email).'\'
				AND id_shop = '.$this->context->shop->id;

		$registered_news = Db::getInstance()->getRow($sql1);

		if($registered_news && isset($registered_news['email']) && $registered_news['active'] == 0){
			$update_sql = 'UPDATE '._DB_PREFIX_.'newsletter
				SET `active` = 1
				WHERE `email` = \''.pSQL($email).'\'
				AND id_shop = '.$this->context->shop->id;
			return Db::getInstance()->execute($update_sql);
		}else{

			$sql = 'INSERT INTO '._DB_PREFIX_.'newsletter (id_shop, id_shop_group, email, newsletter_date_add, ip_registration_newsletter, http_referer, active)
					VALUES
					('.$this->context->shop->id.',
					'.$this->context->shop->id_shop_group.',
					\''.pSQL($email).'\',
					NOW(),
					\''.pSQL(Tools::getRemoteAddr()).'\',
					(
						SELECT c.http_referer
						FROM '._DB_PREFIX_.'connections c
						WHERE c.id_guest = '.(int)$this->context->customer->id.'
						ORDER BY c.date_add DESC LIMIT 1
					),
					'.(int)$active.'
					)';

			return Db::getInstance()->execute($sql);
		}
	}

	public function registerUser($email)
	{
		$sql = 'UPDATE '._DB_PREFIX_.'customer
				SET `newsletter` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.pSQL(Tools::getRemoteAddr()).'\'
				WHERE `email` = \''.pSQL($email).'\'
				AND id_shop = '.$this->context->shop->id;

		return Db::getInstance()->execute($sql);
	}

	public function insertSubscribedCustomer($customer_id, $first_name, $last_name, $email, $code){
		return Db::getInstance()->Execute("INSERT INTO `"._DB_PREFIX_."popup_nl_voucher` (`customer_id`, `first_name`, `last_name`, `email`, `code`, `date_end`, `used`) VALUES ('$customer_id', '$first_name', '$last_name', '$email', '$code', '0', '0')");
	}

	public function isSubscriberExists($email){
		$sql = 'SELECT *
				FROM '._DB_PREFIX_.'popup_nl_voucher
				WHERE `email` = \''.pSQL($email).'\'';
				
		$registered_customer = Db::getInstance()->getRow($sql);

		if($registered_customer)
			return true;
		
		return false;
	}

	public function sendVoucherEmail($first_name, $last_name, $email, $voucher){
		if($this->_sendVoucherEmail){
			
			$template_vars = array(
				'{firstname}' => $first_name, 
				'{lastname}' => $last_name,
				'{voucher}' => $voucher,
				'{shop_name}' => Configuration::get('PS_SHOP_NAME')
            );

			Mail::Send(
				intval($this->context->cookie->id_lang), 
				'voucher', 
				$this->l('New Voucher Code'),
                $template_vars, 
                $email, 
                $first_name.' '.$last_name,
                null,
				null,
				null,
				null,
				_PS_ROOT_DIR_.'/modules/'.$this->name.'/mails/'
            );
                                    
		}
	}

	/**
	 * sendConfirmationMail
	 * @param Customer $customer
	 * @return bool
	 */
	public function sendConfirmationMail(Customer $customer, $password)
	{
		if (!Configuration::get('PS_CUSTOMER_CREATION_EMAIL'))
			return true;

		return Mail::Send(
			$this->context->language->id,
			'account',
			Mail::l('Welcome!'),
			array(
				'{firstname}' => $customer->firstname,
				'{lastname}' => $customer->lastname,
				'{email}' => $customer->email,
				'{passwd}' => $password),
			$customer->email,
			$customer->firstname.' '.$customer->lastname
		);
	}

	public function isTwitterCustomerActive($mail){
		$result = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."pf_popup_twitter_users WHERE email ='$mail'");
		if(empty($result))
			return true;
		elseif(isset($result[0]["email"]) && $result[0]["active"])
			return true;
		elseif(isset($result[0]["email"]) && !$result[0]["active"])
			return false;			
	}

	public function hookDisplayAdminProductsExtra($params){
		if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))){
			$psv = (float)substr(_PS_VERSION_,0,3);

			$this->context->smarty->assign(array(
		        'input_value' => $this->getPopupContentByProductId((int)Tools::getValue('id_product')),
		        'input_name' => 'popup_notification',
		        'languages' => $this->context->controller->_languages,
		        'default_language' => (int)Configuration::get('PS_LANG_DEFAULT')
		    ));
		    if($psv == 1.5){
        		return $this->display(__FILE__, 'hookadminproductsextra15.tpl');
        	}elseif($psv == 1.6){
        		return $this->display(__FILE__, 'hookadminproductsextra16.tpl');
        	}
   	 	}else{
   	 		return $this->display(__FILE__, 'hookadminextraaddproduct.tpl');
   	 	}

	}
	
	public function hookActionProductUpdate($params)
	{
	 
	    $id_product = (int)Tools::getValue('id_product');
	 
	    $languages = Language::getLanguages(true);
	    foreach ($languages as $lang) {
	        if(!Db::getInstance()->update('product_lang', array('popup_notification'=> pSQL(Tools::getValue('popup_notification_'.$lang['id_lang']), true)) ,'id_lang = ' . $lang['id_lang'] .' AND id_product = ' .$id_product ))
	            $this->context->controller->_errors[] = Tools::displayError('Error: ').mysql_error();
	    }
	 
	}

	public function hookDisplayHeader(){

			$this->context->controller->addCSS($this->_path.'css/magnific-popup.css', 'all');
			$this->context->controller->addCSS($this->_path.'css/animation_effects.css', 'all');
			$this->context->controller->addJS(($this->_path).'js/jquery.magnific-popup.min.js');
			$this->context->controller->addJS(($this->_path).'js/jquery.cookie.js');
			$this->context->controller->addJS(($this->_path).'js/popup.notification.js');

			$psv = (float)substr(_PS_VERSION_,0,3);
			if($psv < 1.6)
				$this->context->controller->addJqueryPlugin(array('fancybox'));

			$css = '';

			if(Dispatcher::getInstance()->getController() == 'index'){
				$css = '<style type="text/css">
							.white-popup{width:'.Configuration::get("home_box_width").'px; height:'.Configuration::get("home_box_height").'px;}
							.mfp-content{width:'.Configuration::get("home_box_width").'px !important; height:'.Configuration::get("product_box_height").'px;}
						</style>';
				if($this->home_box_bg != '')
					$css .= '
						<style type="text/css">
							#home-popup-notification{
								background: url('._MODULE_DIR_.$this->name.'/uploads/bg/'.$this->home_box_bg.');
								background-repeat: '.$this->home_box_bg_repeat.';
							}
							#pf_popup_login_box{background: transparent;}
						</style>';
			}elseif(Dispatcher::getInstance()->getController() == 'product'){
				$css = '<style type="text/css">
							.white-popup{width:'.Configuration::get("product_box_width").'px; height:'.Configuration::get("product_box_height").'px;}
							.mfp-content{width:'.Configuration::get("product_box_width").'px !important; height:'.Configuration::get("product_box_height").'px;}
						</style>';
				if($this->product_box_bg != '')
					$css .= '
						<style type="text/css">
							#product-popup-notification{
								background: url('._MODULE_DIR_.$this->name.'/uploads/bg/'.$this->product_box_bg.');
								background-repeat: '.$this->product_box_bg_repeat.';
							}
							#pf_popup_login_box{background: transparent;}
						</style>';
			}elseif(Dispatcher::getInstance()->getController() == 'category'){
				$css = '<style type="text/css">
							.white-popup{width:'.Configuration::get("category_box_width").'px; height:'.Configuration::get("category_box_height").'px;}
						</style>';
				if($this->category_box_bg != '')
					$css .= '
						<style type="text/css">
							#category-popup-notification{
								background: url('._MODULE_DIR_.$this->name.'/uploads/bg/'.$this->category_box_bg.');
								background-repeat: '.$this->category_box_bg_repeat.';
							}
							#pf_popup_login_box{background: transparent;}
						</style>';
			}

			$popup_width = '';
			$popup_height = '';
			$description = '';
			if($this->_popup_login_enable_login == 'true' && $this->_popup_login_enable_register == 'true'){
				$popup_width = 850;
				$popup_height = 360;
				$description = $this->getLoginAndRegisterForms();
			}elseif($this->_popup_login_enable_login == 'true' && $this->_popup_login_enable_register == 'false'){
				$popup_width = 460;
				$popup_height = 340;
				$description = $this->getLoginForm();
			}elseif($this->_popup_login_enable_login == 'false' && $this->_popup_login_enable_register == 'true'){
				$popup_width = 460;
				$popup_height = 430;
				$description = $this->getRegisterForm();
			}
			// print_r($description);
			$sign_in_popup_styles = '
				<style type="text/css">
					#login-and-register-popup.white-popup{width: '.$popup_width.'px; height: '.$popup_height.'px;}
					.pf_popup_signin_popup .mfp-content{width: '.$popup_width.'px !important; height: '.$popup_height.'px;}
				</style>';
			$link = new Link();
			if($this->_popup_login_enable_popup_login == 'true' && ($this->_popup_login_enable_login == 'true' || $this->_popup_login_enable_register == 'true'))
				$enable_header_popup_login = 'true';
			else
				$enable_header_popup_login = 'false';
			// print_r($sign_in_popup_css);
			$this->context->smarty->assign(
				array(
					'redirect' => Configuration::get('PN_SOCIAL_REDIRECT'),
					'enable_responsive' => $this->_popup_gls_enable_responsive,
					'resize_start_point' => $this->_popup_gls_resize_start_point,
					'hide_start_point' => $this->_popup_gls_hide_start_point,
					'enable_facebook_login' => $this->_popup_login_enable_facebook,
					'enable_google_login' => $this->_popup_login_enable_google,
					'facebook_app_id' => $this->_popup_fb_connect_app_id,
					'my_account_url' => $link->getPageLink('my-account'),
					'popup_login_enable_popup_login' => $enable_header_popup_login,
					'description' => $sign_in_popup_styles.$description

				)
			);

			return $css.$this->display(__FILE__, 'header.tpl');

	}

	public function hookDisplayFooter(){
		$active = true;
		if($this->context->customer->isLogged()){
			if(!$this->isTwitterCustomerActive($this->context->customer->email)){
				$active = false;
				$this->context->smarty->assign('email', $this->context->customer->email);
			}	
			$this->context->smarty->assign('email_active',$active);

			return $this -> display(__FILE__,'footer.tpl');
		}
	}

	public function hookDisplayNav(){

		$enable_fb = false;
		$enable_tw = false;
		$enable_gp = false;
		if(!$this->context->customer->isLogged() && $this->_popup_login_enable_facebook == 'true' && $this->_popup_fb_connect_app_id != '' && $this->_popup_fb_connect_position_top == 'true')
			$enable_fb = true;
		if(!$this->context->customer->isLogged() && $this->_popup_login_enable_twitter == 'true' && $this->_popup_tw_connect_consumer_key != '' && $this->_popup_tw_connect_consumer_secret != '' && $this->_popup_tw_connect_position_top == 'true')
			$enable_tw = true;
		if(!$this->context->customer->isLogged() && $this->_popup_login_enable_google == 'true' && $this->_popup_gp_connect_client_id != '' && $this->_popup_gp_connect_position_top == 'true')
			$enable_gp = true;

		$this->context->smarty->assign(
			array(
				'enable_fb' => $enable_fb,
				'enable_tw' => $enable_tw,
				'enable_gp' => $enable_gp,
				'gp_client_id' => $this->_popup_gp_connect_client_id,
				'callback_url' => Tools::getHTTPHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/twitterconnect.php'
			)
		);
		return $this->display(__FILE__, 'top.tpl');
	}

	public function hookPopupFacebookConnect(){
		$psv = floatval(substr(_PS_VERSION_,0,3));
		if($psv >= 1.6)
			$enable_custom_position = $this->_popup_fb_connect_position_custom;
		else
			$enable_custom_position = 'true';

		if(!$this->context->customer->isLogged() && $this->_popup_login_enable_facebook == 'true' && $this->_popup_fb_connect_app_id != '' && $enable_custom_position == 'true')
		return $this->display(__FILE__, 'facebookCustom.tpl');
	}

	public function hookPopupTwitterConnect(){
		$psv = floatval(substr(_PS_VERSION_,0,3));
		if($psv >= 1.6)
			$enable_custom_position = $this->_popup_tw_connect_position_custom;
		else
			$enable_custom_position = 'true';

		if(!$this->context->customer->isLogged() && $this->_popup_login_enable_twitter == 'true' && $this->_popup_tw_connect_consumer_key != '' && $this->_popup_tw_connect_consumer_secret != '' && $enable_custom_position == 'true'){
			$this->context->smarty->assign(array(
				'callback_url' => Tools::getHTTPHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/twitterconnect.php'
			));
			return $this->display(__FILE__, 'twitterCustom.tpl');
		}
	}

	public function hookPopupGoogleConnect(){
		$psv = floatval(substr(_PS_VERSION_,0,3));
		if($psv >= 1.6)
			$enable_custom_position = $this->_popup_gp_connect_position_custom;
		else
			$enable_custom_position = 'true';

		if(!$this->context->customer->isLogged() && $this->_popup_login_enable_google == 'true' && $this->_popup_gp_connect_client_id != '' && $enable_custom_position == 'true'){
			$link = new Link();
			$this->context->smarty->assign(array(
				'gp_client_id' => $this->_popup_gp_connect_client_id,
				'my_account_url' => $link->getPageLink('my-account')
			));
			return $this->display(__FILE__, 'googleCustom.tpl');
		}
	}
	
	public function hookDisplayHome($params){
		// print_r(Configuration::get("home_content_type"));
		if(Configuration::get("home_page_popup_enable") == "true"){
			$home_content_type = Configuration::get("home_content_type");
			
			
			$home_box_start_date = Configuration::get('home_box_start_date');
			$home_box_end_date = Configuration::get('home_box_end_date');
			$current_date = date("Y-m-d");
			if(($home_box_start_date == '0000-00-00') && ($home_box_end_date == '0000-00-00')){
				$home_box_valid = 1;
			}else{
				if(($home_box_start_date <= $current_date) && ($home_box_end_date >= $current_date))
					$home_box_valid = 1;
				else
					$home_box_valid = 0;
			}

			$content = '';
			switch($home_content_type){
				case 'html':
					$content = $this->getHomePopupContent($this->context->language->id);
					break;
				case 'youtube':
					$content = Configuration::get("home_youtube_link_".$this->context->language->id);
					break;
				case 'vimeo':
					$content = Configuration::get("home_vimeo_link_".$this->context->language->id);
					break;
				case 'gmaps':
					$content = Configuration::get("home_gmaps_link");
					break;
				case 'facebook':
					$content = $this->getFacebookLikeBox(Configuration::get("home_facebook_link"), Configuration::get("home_box_width"), Configuration::get("home_box_height"));
					break;
				case 'newsletter':
					$content = $this->getNewsletterBlock();
					break;
				case 'login':
					$content = $this->getLoginForm();
					break;
				case 'register':
					$content = $this->getRegisterForm();
					break;
				case 'login_and_register':
					$content = $this->getLoginAndRegisterForms();
					break;
				case 'image':
					$content = Configuration::get("home_box_image_".$this->context->language->id);
					break;
				default:
					break;
			}
			// print_r($content);
			$this->context->smarty->assign(
				array(
					'description' => $content,
					'home_box_cookie' => Configuration::get("home_box_cookie"),
					'home_content_type' => Configuration::get("home_content_type"),
					'home_box_valid' => $home_box_valid,
					'home_box_delay' => Configuration::get("home_box_delay"),
					'home_box_autoclose_time' => Configuration::get("home_box_autoclose_time"),
					'home_box_effect' => Configuration::get("home_box_effect"),
					'image_dir' => _MODULE_DIR_.$this->name.'/uploads',
					'home_image_link' => Configuration::get("home_image_link_".$this->context->language->id),
				)
			);

			return $this->display(__FILE__, 'homepagepopup.tpl');
		}

	}

	public function hookDisplayRightColumnProduct($params){
		if(Configuration::get("product_page_popup_enable") == "true"){

			$product_box_start_date = Configuration::get('product_box_start_date');
			$product_box_end_date = Configuration::get('product_box_end_date');
			$current_date = date("Y-m-d");
			if(($product_box_start_date == '0000-00-00') && ($product_box_end_date == '0000-00-00')){
				$product_box_valid = 1;
			}else{
				if(($product_box_start_date <= $current_date) && ($product_box_end_date >= $current_date))
					$product_box_valid = 1;
				else
					$product_box_valid = 0;
			}
			
			if(Configuration::get("each_product_different_content") == "true"){
				$product_id = (int)Tools::getValue('id_product');
				$lang_id = $this->context->language->id;
				$this->context->smarty->assign(
					array(
						'description' => $this->getProductContentByProductIdAndLang($product_id, $lang_id),
						'product_box_valid' => $product_box_valid,
						'product_box_delay' => Configuration::get("product_box_delay"),
						'product_box_autoclose_time' => Configuration::get("product_box_autoclose_time"),
						'product_box_effect' => Configuration::get("product_box_effect")
					)
				);

				return $this->display(__FILE__, 'singleproductpopup.tpl');
			
			}else{
				$product_content_type = Configuration::get("product_content_type");

				$content = '';
				switch($product_content_type){
					case 'html':
						$content = $this->getProductPopupContent($this->context->language->id);
						break;
					case 'youtube':
						$content = Configuration::get("product_youtube_link_".$this->context->language->id);
						break;
					case 'vimeo':
						$content = Configuration::get("product_vimeo_link_".$this->context->language->id);
						break;
					case 'gmaps':
						$content = Configuration::get("product_gmaps_link");
						break;
					case 'facebook':
						$content = $this->getFacebookLikeBox(Configuration::get("product_facebook_link"), Configuration::get("product_box_width"), Configuration::get("product_box_height"));
						break;
					case 'newsletter':
						$content = $this->getNewsletterBlock();
						break;
					case 'image':
						$content = Configuration::get("product_box_image_".$this->context->language->id);
						break;
					default:
						break;
				}
				$this->context->smarty->assign(
					array(
						'description' => $content,
						'product_box_cookie' => Configuration::get("product_box_cookie"),
						'product_content_type' => Configuration::get("product_content_type"),
						'product_box_valid' => $product_box_valid,
						'product_box_delay' => Configuration::get("product_box_delay"),
						'product_box_autoclose_time' => Configuration::get("product_box_autoclose_time"),
						'product_box_effect' => Configuration::get("product_box_effect"),
						'image_dir' => _MODULE_DIR_.$this->name.'/uploads',
						'product_image_link' => Configuration::get("product_image_link_".$this->context->language->id)
					)
				);

				return $this->display(__FILE__, 'productpagepopup.tpl');

			}
		}
	}

	public function hookCategoryPopup($params){
		if(Configuration::get("category_page_popup_enable") == "true"){
			$category_id = (int)Tools::getValue('id_category');
			$lang_id = $this->context->language->id;

			$category_box_start_date = Configuration::get('category_box_start_date');
			$category_box_end_date = Configuration::get('category_box_end_date');
			$current_date = date("Y-m-d");
			if(($category_box_start_date == '0000-00-00') && ($category_box_end_date == '0000-00-00')){
				$category_box_valid = 1;
			}else{
				if(($category_box_start_date <= $current_date) && ($category_box_end_date >= $current_date))
					$category_box_valid = 1;
				else
					$category_box_valid = 0;
			}
			$this->context->smarty->assign(
				array(
					'description' => $this->getCategoryContentByIdAndLang($category_id, $lang_id),
					'category_box_valid' => $category_box_valid,
					'category_box_delay' => Configuration::get("category_box_delay"),
					'category_box_autoclose_time' => Configuration::get("category_box_autoclose_time"),
					'category_box_effect' => Configuration::get("category_box_effect")
				)
			);

			return $this->display(__FILE__, 'categorypagepopup.tpl');
		}
	}

	public function hookCustomPopup($params){
		if(Configuration::get("custom_page_popup_enable") == "true"){
			$custom_content_type = Configuration::get("custom_content_type");

			$custom_box_start_date = Configuration::get('custom_box_start_date');
			$custom_box_end_date = Configuration::get('custom_box_end_date');
			$current_date = date("Y-m-d");
			if(($custom_box_start_date == '0000-00-00') && ($custom_box_end_date == '0000-00-00')){
				$custom_box_valid = 1;
			}else{
				if(($custom_box_start_date <= $current_date) && ($custom_box_end_date >= $current_date))
					$custom_box_valid = 1;
				else
					$custom_box_valid = 0;
			}

			$content = '';
			switch($custom_content_type){
				case 'html':
					$content = $this->getCustomPageContent($this->context->language->id);
					break;
				case 'youtube':
					$content = Configuration::get("custom_youtube_link_".$this->context->language->id);
					break;
				case 'vimeo':
					$content = Configuration::get("custom_vimeo_link_".$this->context->language->id);
					break;
				case 'gmaps':
					$content = Configuration::get("custom_gmaps_link");
					break;
				case 'facebook':
					$content = $this->getFacebookLikeBox(Configuration::get("custom_facebook_link"), Configuration::get("custom_box_width"), Configuration::get("custom_box_height"));
					break;
				case 'newsletter':
					$content = $this->getNewsletterBlock();
					break;
				case 'image':
					$content = Configuration::get("custom_box_image_".$this->context->language->id);
					break;
				default:
					break;
			}
			$bg_style = '';
			if($this->custom_box_bg != '')
				$bg_style .= '
						#custom-popup-notification{
							background: url('._MODULE_DIR_.$this->name.'/uploads/bg/'.$this->custom_box_bg.');
							background-repeat: '.$this->custom_box_bg_repeat.';
						}
						#pf_popup_login_box{background: transparent;}';
			$this->context->smarty->assign(
				array(
					'description' => $content,
					'custom_box_cookie' => Configuration::get("custom_box_cookie"),
					'custom_box_width' => Configuration::get("custom_box_width"),
					'custom_box_height' => Configuration::get("custom_box_height"),
					'custom_content_type' => Configuration::get("custom_content_type"),
					'custom_box_valid' => $custom_box_valid,
					'custom_box_delay' => Configuration::get("custom_box_delay"),
					'custom_box_autoclose_time' => Configuration::get("custom_box_autoclose_time"),
					'custom_box_effect' => Configuration::get("custom_box_effect"),
					'image_dir' => _MODULE_DIR_.$this->name.'/uploads',
					'custom_image_link' => Configuration::get("custom_image_link_".$this->context->language->id),
					'bg_style' => $bg_style,
				)
			);

			return $this->display(__FILE__, 'custompagepopup.tpl');
		}
	}
	
	
}


?>