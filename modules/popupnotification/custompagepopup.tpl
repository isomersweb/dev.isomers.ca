{if isset($custom_box_valid) && $custom_box_valid == 1}
	{if $custom_content_type == 'html' || $custom_content_type == 'facebook' || $custom_content_type == 'newsletter'}
	<div  id="custom-popup-notification" class="white-popup mfp-with-anim mfp-hide">
		{$description}
	</div>
	{if $custom_content_type == 'newsletter'}
		{include file="./newsletter.tpl"}
	{/if}
	<a href="#custom-popup-notification" class="open-popup-notification"></a>
	<script type="text/javascript">
		$('.open-popup-notification').magnificPopup({
			  	type:'inline',
			  	midClick: true,
			  	removalDelay: 500,
				callbacks: {
				    beforeOpen: function() {
				       this.st.mainClass = '{$custom_box_effect}';
				    },
				    open: function(){
				    	PF_addResponsivnes(resize_start_point, enable_responsive);
				    }
				}
		});

		var custom_box_cookie = '{$custom_box_cookie|intval}';
		var custom_box_delay = '{$custom_box_delay|intval}';
		var custom_box_autoclose_time = '{$custom_box_autoclose_time|intval}';

			if(custom_box_cookie == "0"){
				$.cookie("custom_box_cookie", "0");
			}
			$(window).load(function(){
				if($.trim($("#custom-popup-notification").html()).length != 0){
					if($.cookie("custom_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(custom_box_cookie != 0){
								$.cookie("custom_box_cookie", "1", { expires: parseInt(custom_box_cookie) });
							}	
						}, custom_box_delay*1000);
						if(custom_box_autoclose_time != 0){
							var hide_time = (parseInt(custom_box_delay)+parseInt(custom_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
						
					}
				}

			});
	</script>
	{else if $custom_content_type == 'youtube' || $custom_content_type == 'vimeo' || $custom_content_type == 'gmaps'}
		<a href="{$description}" class="open-popup-notification"></a>
		<script type="text/javascript">
			$('.open-popup-notification').magnificPopup({
			  	type:'iframe',
			 	midClick: true,
			 	removalDelay: 500,
			  	callbacks: {
				    beforeOpen: function() {
				       this.st.mainClass = '{$custom_box_effect}';
				    },
				    open: function(){
				    	PF_addResponsivnes(resize_start_point, enable_responsive);
				    }
			  	},
			  	iframe: {
				  	markup: '<div class="mfp-with-anim" style="height:100%;">'+
				  			'<div class="mfp-iframe-scaler">'+
				            '<div class="mfp-close"></div>'+
				            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
				          '</div></div>'
		      	}
			});

			var custom_box_cookie = '{$custom_box_cookie|intval}';
			var custom_box_delay = '{$custom_box_delay|intval}';
			var custom_box_autoclose_time = '{$custom_box_autoclose_time|intval}';

			if(custom_box_cookie == "0"){
				$.cookie("custom_box_cookie", "0");
			}
			$(window).load(function(){
				if($.trim($(".open-popup-notification").attr('href')).length != 0){
					if($.cookie("custom_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(custom_box_cookie != 0){
								$.cookie("custom_box_cookie", "1", { expires: parseInt(custom_box_cookie) });
							}
						}, custom_box_delay*1000);
						if(custom_box_autoclose_time != 0){
							var hide_time = (parseInt(custom_box_delay)+parseInt(custom_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
					}
				}

			});
		</script>
	{else if $custom_content_type == 'image'}
		<a href="{if $description != ''}{$image_dir}/{$description}{/if}" class="open-popup-notification"></a>
		<script type="text/javascript">
			var custom_image_link = '{$custom_image_link}';

			if(custom_image_link != '')
				var html = '<a href="'+custom_image_link+'"><div class="mfp-img"></div></a>';
			else
				var html = '<div class="mfp-img"></div>';

			$('.open-popup-notification').magnificPopup({
			  	type:'image',
			 	midClick: true,
			 	removalDelay: 500,
			  	callbacks: {
				    beforeOpen: function() {
				       this.st.mainClass = '{$custom_box_effect}';
				    },
				    open: function(){
				    	PF_addResponsivnes(resize_start_point, enable_responsive);
				    }
			  	},
			  	image: {
				  	markup: '<div class="mfp-figure">'+
				            	'<div class="mfp-close"></div>'+
				            	html+
					            '<div class="mfp-bottom-bar">'+
					              	'<div class="mfp-title"></div>'+
					              	'<div class="mfp-counter"></div>'+
					            '</div>'+
				          	'</div>'
				}
			});

			var custom_box_cookie = '{$custom_box_cookie|intval}';
			var custom_box_delay = '{$custom_box_delay|intval}';
			var custom_box_autoclose_time = '{$custom_box_autoclose_time|intval}';

			if(custom_box_cookie == "0"){
				$.cookie("custom_box_cookie", "0");
			}
			$(window).load(function(){
				if($.trim($(".open-popup-notification").attr('href')).length != 0){
					if($.cookie("custom_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(custom_box_cookie != 0){
								$.cookie("custom_box_cookie", "1", { expires: parseInt(custom_box_cookie) });
							}	
						}, custom_box_delay*1000);
						if(custom_box_autoclose_time != 0){
							var hide_time = (parseInt(custom_box_delay)+parseInt(custom_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
					}
				}

			});
		</script>
	{/if}
	<style type="text/css">
		{literal}
		.white-popup{width:{/literal}{$custom_box_width|intval}px{literal}; height:{/literal}{$custom_box_height|intval}px{literal};}
		.mfp-content{width:{/literal}{$custom_box_width|intval}px !important{literal}; height:{/literal}{$custom_box_height|intval}px{literal};}
		{/literal}
		{$bg_style}
	</style>
{/if}