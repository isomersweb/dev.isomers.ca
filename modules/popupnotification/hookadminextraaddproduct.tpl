<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert">×</button>
		{l s='There is 1 warning.'}
	<ul style="display:block;" id="seeMore">
		<li>{l s='You must save this product before managing Popups.'}</li>
	</ul>
</div>