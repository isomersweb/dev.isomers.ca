<?php
	include(dirname(__FILE__).'/../../config/config.inc.php');
	include(dirname(__FILE__).'/../../init.php');
	include(dirname(__FILE__).'/popupnotification.php');
	include(dirname(__FILE__).'/classes/twitterUsers.php');
	
	$popupnotification = new PopupNotification();

	session_start();
	if( Tools::getValue("oauth_token") && Tools::getValue("oauth_verifier") ){
		$_SESSION['oauth_token_bac'] = Tools::getValue('oauth_token');
		$_SESSION['oauth_verifier'] = Tools::getValue('oauth_verifier');
	}

	define ('TWITTER_CONSUMER_KEY', $popupnotification->_popup_tw_connect_consumer_key);
	define ('TWITTER_CONSUMER_SECRET', $popupnotification->_popup_tw_connect_consumer_secret);
	define ('TWITTER_URL_CALLBACK', Tools::getHTTPHost(true).__PS_BASE_URI__.'modules/'.$popupnotification->name.'/twitterconnect.php');
	define ('URL_REQUEST_TOKEN', 'https://api.twitter.com/oauth/request_token');
	define ('URL_AUTHORIZE', 'https://api.twitter.com/oauth/authorize');
	define ('URL_ACCESS_TOKEN', 'https://api.twitter.com/oauth/access_token');
	define ('URL_ACCOUNT_DATA', 'https://api.twitter.com/1.1/users/show.json');


	$oauth_nonce = md5(uniqid(rand(), true));
	$oauth_timestamp = time();
	$oauth_base_text = "GET&";
	$oauth_base_text .= urlencode(URL_REQUEST_TOKEN)."&";
	$oauth_base_text .= urlencode("oauth_callback=".urlencode(TWITTER_URL_CALLBACK)."&");
	$oauth_base_text .= urlencode("oauth_consumer_key=".TWITTER_CONSUMER_KEY."&");
	$oauth_base_text .= urlencode("oauth_nonce=".$oauth_nonce."&");
	$oauth_base_text .= urlencode("oauth_signature_method=HMAC-SHA1&");
	$oauth_base_text .= urlencode("oauth_timestamp=".$oauth_timestamp."&");
	$oauth_base_text .= urlencode("oauth_version=1.0");
		
	$key = TWITTER_CONSUMER_SECRET."&";
	$oauth_signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));
	$url1 = URL_REQUEST_TOKEN;
	$url1 .= '?oauth_callback='.urlencode(TWITTER_URL_CALLBACK);
	$url1 .= '&oauth_consumer_key='.TWITTER_CONSUMER_KEY;
	$url1 .= '&oauth_nonce='.$oauth_nonce;
	$url1 .= '&oauth_signature='.urlencode($oauth_signature);
	$url1 .= '&oauth_signature_method=HMAC-SHA1';
	$url1 .= '&oauth_timestamp='.$oauth_timestamp;
	$url1 .= '&oauth_version=1.0';

	$response = file_get_contents($url1);
	parse_str($response, $result);
	$_SESSION['oauth_token'] = $oauth_token = $result['oauth_token'];
	$_SESSION['oauth_token_secret'] = $oauth_token_secret = $result['oauth_token_secret'];
	$_SESSION['oauth_token_secret'] = $oauth_token_secret = $result['oauth_token_secret'];
	$url1 = URL_AUTHORIZE;
	$url1 .= '?oauth_token='.$oauth_token;

	if((Tools::getValue("oauth_token") && Tools::getValue("oauth_verifier")) || (isset($_SESSION['oauth_token_bac']) && isset($_SESSION['oauth_verifier']))){

		if(isset($_SESSION['oauth_token_bac']) && isset($_SESSION['oauth_verifier'])){
			$oauth_token = $_SESSION['oauth_token_bac'];
			$oauth_verifier = $_SESSION['oauth_verifier'];
		}else{
			$oauth_token = Tools::getValue('oauth_token_bac');
			$oauth_verifier = Tools::getValue('oauth_verifier');
		}
		
																			
		$oauth_token_secret = $_SESSION['oauth_token_secret'];			
		$oauth_base_text = "GET&";
		$oauth_base_text .= urlencode(URL_ACCESS_TOKEN)."&";
		$oauth_base_text .= urlencode("oauth_consumer_key=".TWITTER_CONSUMER_KEY."&");
		$oauth_base_text .= urlencode("oauth_nonce=".$oauth_nonce."&");
		$oauth_base_text .= urlencode("oauth_signature_method=HMAC-SHA1&");
		$oauth_base_text .= urlencode("oauth_token=".$oauth_token."&");
		$oauth_base_text .= urlencode("oauth_timestamp=".$oauth_timestamp."&");
		$oauth_base_text .= urlencode("oauth_verifier=".$oauth_verifier."&");
		$oauth_base_text .= urlencode("oauth_version=1.0");

		$key = TWITTER_CONSUMER_SECRET."&".$oauth_token_secret;
		$oauth_signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));
		$url = URL_ACCESS_TOKEN;
		$url .= '?oauth_nonce='.$oauth_nonce;
		$url .= '&oauth_signature_method=HMAC-SHA1';
		$url .= '&oauth_timestamp='.$oauth_timestamp;
		$url .= '&oauth_consumer_key='.TWITTER_CONSUMER_KEY;
		$url .= '&oauth_token='.urlencode($oauth_token);
		$url .= '&oauth_verifier='.urlencode($oauth_verifier);
		$url .= '&oauth_signature='.urlencode($oauth_signature);
		$url .= '&oauth_version=1.0';

		if(!$response = file_get_contents($url))
			header('Location: '.$url1.'');

		parse_str($response, $result);
		$oauth_nonce = md5(uniqid(rand(), true));
		$oauth_timestamp = time();
		$oauth_token = $result['oauth_token'];
		$oauth_token_secret = $result['oauth_token_secret'];
		$screen_name = $result['screen_name'];

		$oauth_base_text = "GET&";
		$oauth_base_text .= urlencode(URL_ACCOUNT_DATA).'&';
		$oauth_base_text .= urlencode('oauth_consumer_key='.TWITTER_CONSUMER_KEY.'&');
		$oauth_base_text .= urlencode('oauth_nonce='.$oauth_nonce.'&');
		$oauth_base_text .= urlencode('oauth_signature_method=HMAC-SHA1&');
		$oauth_base_text .= urlencode('oauth_timestamp='.$oauth_timestamp."&");
		$oauth_base_text .= urlencode('oauth_token='.$oauth_token."&");
		$oauth_base_text .= urlencode('oauth_version=1.0&');
		$oauth_base_text .= urlencode('screen_name=' . $screen_name);

		$key = TWITTER_CONSUMER_SECRET . '&' . $oauth_token_secret;
		$signature = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));
		$url = URL_ACCOUNT_DATA;
		$url .= '?oauth_consumer_key=' . TWITTER_CONSUMER_KEY;
		$url .= '&oauth_nonce=' . $oauth_nonce;
		$url .= '&oauth_signature=' . urlencode($signature);
		$url .= '&oauth_signature_method=HMAC-SHA1';
		$url .= '&oauth_timestamp=' . $oauth_timestamp;
		$url .= '&oauth_token=' . urlencode($oauth_token);
		$url .= '&oauth_version=1.0';
		$url .= '&screen_name=' . $screen_name;
		$response = file_get_contents($url);
		$user_data = json_decode($response);

		$result = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'pf_popup_twitter_users WHERE id_user ='.$user_data->id );	
		$customer = new Customer();
		$mail = $user_data->screen_name.'@twitter.com';
		$customer_email = $customer->getByEmail($mail);
		
		if(empty($result) && empty($customer_email)){
			$twitter_users = new twitterUsers();
			$twitter_users->id_user = $user_data->id;
			$user_name = explode(" ", $user_data->name);
			$twitter_users->first_name = $user_name[0];
			$twitter_users->last_name = isset($user_name[1])?$user_name[1]:$user_name[0];
			$twitter_users->screen_name = $user_data->screen_name;
			$mail = $user_data->screen_name.'@twitter.com';
			$twitter_users->email = $mail;
			$twitter_users->active = 0;
			$twitter_users->add();				
		
			$customer = new Customer();

			Hook::exec('actionBeforeSubmitAccount');

			$customer->firstname = $user_name[0];
			$customer->lastname = isset($user_name[1])?$user_name[1]:$user_name[0];
			$customer->email = $mail;
			//$customer->passwd=Tools::passwdGen();
			$password = Tools::passwdGen();
			$customer->passwd = md5(pSQL(_COOKIE_KEY_.$password));
			$customer->add();
								
			
			$context = Context::getContext();
		  	$context->cookie->id_customer = (int)($customer->id);
		   	$context->cookie->customer_lastname = $customer->lastname;
		   	$context->cookie->customer_firstname = $customer->firstname;
		   	$context->cookie->logged = 1;
		   	$customer->logged = 1;
		   	$context->cookie->passwd = $customer->passwd;
		   	$context->cookie->email = $mail;
		}else{									
			$customer = new Customer();
			$customer->getByEmail($result[0]["email"], null, true);
				if($customer){
					$context = Context::getContext(); 
					Hook::exec('actionBeforeAuthentication');
					$context->cookie->id_compare = isset($context->cookie->id_compare) ? $context->cookie->id_compare: CompareProduct::getIdCompareByIdCustomer($customer->id);
					$context->cookie->id_customer = (int)($customer->id);
					$context->cookie->customer_lastname = $customer->lastname;
					$context->cookie->customer_firstname = $customer->firstname;
					$context->cookie->logged = 1;
					$customer->logged = 1;
					$context->cookie->is_guest = $customer->isGuest();
					$context->cookie->passwd = $customer->passwd;
					$context->cookie->email = $customer->email;
						
					  // Add customer to the context
					$context->customer = $customer;
						
					if(Configuration::get('PS_CART_FOLLOWING') && (empty($context->cookie->id_cart) || Cart::getNbProducts($context->cookie->id_cart) == 0) && $id_cart = (int)Cart::lastNoneOrderedCart($context->customer->id))
					   $context->cart = new Cart($id_cart);
					else{
						$id_carrier = (int)$context->cart->id_carrier;
						$context->cart->id_carrier = 0;
						$context->cart->setDeliveryOption(null);
						$context->cart->id_address_delivery = (int)Address::getFirstCustomerAddressId((int)($customer->id));
						$context->cart->id_address_invoice = (int)Address::getFirstCustomerAddressId((int)($customer->id));
					}
					   $context->cart->id_customer = (int)$customer->id;
						$context->cart->secure_key = $customer->secure_key;
						$context->cart->save();
						$context->cookie->id_cart = (int)$context->cart->id;
						$context->cookie->write();
						$context->cart->autosetProductAddress();
						Hook::exec('actionAuthentication');
					  // Login information have changed, so we check if the cart rules still apply
						CartRule::autoRemoveFromCart($context);
						CartRule::autoAddToCart($context);

			 }					   
		}

		echo ('<script type="text/javascript">
			window.onunload = refreshParent;
			function refreshParent() {
					window.opener.location.reload();
			}
			setTimeout(function(){
				self.close();	
			}, 500)
		</script>');
		
	}else{
		header('Location: '.$url1.'');
	}
?>