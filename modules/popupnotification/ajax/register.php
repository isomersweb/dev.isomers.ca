<?php

	include(dirname(__FILE__).'/../../../config/config.inc.php');
	include(dirname(__FILE__).'/../../../init.php');
	include(dirname(__FILE__).'/../popupnotification.php');
	if(Tools::isSubmit('submitAccount')){
		$PFPopup = new PopupNotification();
		$errors = array();

		if (!Validate::isEmail($email = Tools::getValue('email')) || empty($email) || Customer::customerExists($email))
			$errors['email'] = $PFPopup->l('An email address is not valid');

		if (!Validate::isName($first_name = Tools::getValue('customer_firstname')) || empty($first_name))
			$errors['customer_firstname'] = $PFPopup->l('First Name is invalid');

		if (!Validate::isName($last_name = Tools::getValue('customer_lastname')) || empty($last_name))
			$errors['customer_lastname'] = $PFPopup->l('Last Name is invalid');

		if (!Validate::isPasswd($passwd = Tools::getValue('passwd')) || empty($passwd))
			$errors['passwd'] = $PFPopup->l('Password is invalid');

		if(!count($errors)){
			$customer = new Customer();			

			Hook::exec('actionBeforeSubmitAccount');
		
			$customer->firstname = Tools::getValue('customer_firstname');
			$customer->lastname  = Tools::getValue('customer_lastname');
			$customer->email     = Tools::getValue('email');
			$customer->passwd    = md5(pSQL(_COOKIE_KEY_.Tools::getValue('passwd')));
		
			$customer->is_guest = 0;
			$customer->active = 1;

			
			$customer->add();
			$context = Context::getContext();
			$context->customer = $customer;
			$context->cookie->id_customer = (int)$customer->id;
			$context->cookie->customer_lastname = $customer->lastname;
			$context->cookie->customer_firstname = $customer->firstname;
			$context->cookie->passwd = $customer->passwd;
			$context->cookie->logged = 1;
			$customer->logged = 1;
			$context->cookie->email = $customer->email;
			$context->cookie->is_guest = $customer->is_guest;
			// Update cart address
			$context->cart->secure_key = $customer->secure_key;
			$context->cookie->update();
			$context->cart->update();

			$return = array(
				'hasError' => false
			);
			die(Tools::jsonEncode($return));

		}else{
			$return = array(
				'hasError' => true,
				'errors' => $errors
			);
			die(Tools::jsonEncode($return));
		}
	}else{
		die(0);
	}