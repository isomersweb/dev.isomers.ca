<?php

if($psv >= 1.6)
	$output .='
		<div class="col-lg-12" style="margin-bottom:10px">
			<a class="list-group-item col-lg-2 no_top_radius '.(Tools::getValue('pfpopupsubtab') == 'settings' || !Tools::getValue('pfpopupsubtab') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=googleConnect&pfpopupsubtab=settings">
				'.$this->l('Google Connect').'
			</a>
			<a class="list-group-item col-lg-2 no_bottom_radius '.(Tools::getValue('pfpopupsubtab') == 'users' ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=googleConnect&pfpopupsubtab=users">
				'.$this->l('Users').'
			</a>
		</div>';
else
	$output .= '
		<div class="clearfix" style="height: 36px;width: 900px;margin: 0 auto;clear: both;margin-bottom:10px;">
			<a class="list-group-item col-lg-2 margin-right '.(Tools::getValue('pfpopupsubtab') == 'settings' || !Tools::getValue('pfpopupsubtab') ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=googleConnect&pfpopupsubtab=settings" style="margin-right:10px;">
				'.$this->l('Google Connect').'
			</a>
			<a class="list-group-item col-lg-2 '.(Tools::getValue('pfpopupsubtab') == 'users' ? 'active' : '').'" href="'.$this->_getModuleUrl().'&pfpopuptab=googleConnect&pfpopupsubtab=users">
				'.$this->l('Users').'
			</a>
		</div>';

if (Tools::getValue('pfpopupsubtab') == 'settings' || !Tools::getValue('pfpopupsubtab')){
	$output .= '
	<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" '.($psv == 1.5?'style="width:900px; margin:0 auto;clear:both;"':'style="clear:both;"').($psv >= 1.6?'class="defaultForm  form-horizontal"':'sky-form').'>'.
		($psv >= 1.6?'<div class="panel">':'<fieldset>').
			($psv < 1.6 ? '<legend>': '<div class="panel-heading">')
				.($psv >=1.6?'<i class="icon-cogs"></i> ':' ').$this->l('Google Connect Settings').
			($psv < 1.6 ? '</legend>': '</div>').'

			'.($psv < 1.6?'<label for="popup_login_enable_google">'.$this->l("Enable Google Sign In").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="popup_login_enable_google" class="control-label col-lg-3">'.$this->l("Enable Google Sign in").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="popup_login_enable_google" id="popup_login_enable_google_on" value="true" '.(($this->_popup_login_enable_google == "true") ? 'checked="checked" ' : '').'>
							<label for="popup_login_enable_google_on">'.$this->l('Yes').'</label>
							<input type="radio" name="popup_login_enable_google" id="popup_login_enable_google_off" value="false" '.(($this->_popup_login_enable_google == "false") ? 'checked="checked" ' : '').'>
							<label for="popup_login_enable_google_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
					':'
					<input type="hidden" name="popup_login_enable_google" value="false" />
					<input type="checkbox" id="popup_login_enable_google" name="popup_login_enable_google" value="true" '.(($this->_popup_login_enable_google == "true") ? 'checked="checked" ' : '').' >
					').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="popup_gp_connect_client_id">'.$this->l("Client ID").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="popup_gp_connect_client_id" class="control-label col-lg-3">'.$this->l("Client ID").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-2 input-group">':'').'
					<input type="text" id="popup_gp_connect_client_id" name="popup_gp_connect_client_id" class="fixed-width-xl" value="'.$this->_popup_gp_connect_client_id.'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label>'.$this->l("Redirect After Login").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Redirect After Login").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<select name="social_redirect" id="social_redirect" class="fixed-width-xl">
						<option value="no_redirect" '.(Configuration::get('PN_SOCIAL_REDIRECT') == 'no_redirect'?'selected':'').'>'.$this->l('No Redirect').'</option>
						<option value="my_account" '.(Configuration::get('PN_SOCIAL_REDIRECT') == 'my_account'?'selected':'').'>'.$this->l('My Account Page').'</option>
					</select>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label>'.$this->l("Positions to display").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Positions to display").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					'.($psv >= 1.6?'
						<label class="control-label gp_connect_position_label">'.$this->l('Top').'</label>
						<a href="#" class="gp_connect_position '.($this->_popup_gp_connect_position_top == 'true'?'checked':'').'">
							<i class="icon-check-empty '.($this->_popup_gp_connect_position_top == 'true'?'icon-check-sign':'').'"></i>
							<input type="hidden" name="popup_gp_connect_position_top" value="'.$this->_popup_gp_connect_position_top.'">
						</a>
						<br />
						<label class="control-label gp_connect_position_label">'.$this->l('Custom').'</label>
						<a href="#" class="gp_connect_position '.($this->_popup_gp_connect_position_custom == 'true'?'checked':'').'">
							<i class="icon-check-empty '.($this->_popup_gp_connect_position_custom == 'true'?'icon-check-sign':'').'"></i>
							<input type="hidden" name="popup_gp_connect_position_custom" value="'.$this->_popup_gp_connect_position_custom.'">
						</a>
						<p class="help-block">'.$this->l('Add {hook h=\'popupGoogleConnect\'} to your page tpl file where you want to display.').'</p>
						<script type="text/javascript">
							$(document).ready(function(){
								$(".gp_connect_position").on("click", function(){
									if($(this).hasClass("checked")){
										$(this).removeClass("checked");
										$(this).find("i").removeClass("icon-check-sign");
										$(this).find("input").val("false");
									}else{
										$(this).addClass("checked");
										$(this).find("i").addClass("icon-check-sign");
										$(this).find("input").val("true");
									}
									return false;
								});
								
								$(".gp_connect_position_label").on("click", function(){
									$(this).next().click();
								});
							});
						</script>
					':'
						<p class="help-block">'.$this->l('Add {hook h=\'popupGoogleConnect\'} to your page tpl file where you want to display.').'</p>
					').
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>'.

			($psv >= 1.6?'<div class="panel-footer"><button type="submit" value="1" name="submitgpconnectsettings" class="btn btn-default pull-right"><i class="process-icon-save"></i> '.$this->l('Update Settings').'</button></div>':'<input type="submit" name="submitgpconnectsettings" value="'.$this->l('Update Settings').'" class="button" />').

		($psv < 1.6 ? '</fieldset>': '</div>').'
	</form>';
}
if (Tools::getValue('pfpopupsubtab') == 'users'){
	if($psv >= 1.6){
		$output .='
			<div class="col-lg-12">
				<div class="panel">
					<div class="panel-heading"> <i class="icon-cogs"> </i> '.$this->l('Users').' </div>				
					<div class="form-wrapper ">
						<div class="responsive-row">';
							$result = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."pf_popup_google_users");
							if (empty($result)){
								$output .='
									<div> 
										<p class="help-block"> '.$this->l('Oops, No one here...').' </p>
									</div>';
							}else{
								$output .='
									<table class="table product">
										<tr>
											<th col-ld-1>'.$this->l('ID').'</th>
											<th col-ld-3>'.$this->l('ID user').'</th>
											<th col-ld-2>'.$this->l('First name').'</th>
											<th col-ld-2>'.$this->l('Last name').'</th>
											<th col-ld-2>'.$this->l('Email').'</th>
											<th col-ld-1>'.$this->l('Gender').'</th>
											<th col-ld-1>'.$this->l('Date add').'</th>
										</tr>';
										
										foreach($result as $res){
											$output .='
												<tr>
													<td class="pointer">'.$res['id'].'</td>
													<td class="pointer">'.$res['id_user'].'</td>
													<td class="pointer">'.$res['first_name'].'</td>
													<td class="pointer">'.$res['last_name'].'</td>
													<td class="pointer">'.$res['email'].'</td>
													<td class="pointer">'.$res['gender'].'</td>
													<td class="pointer">'.$res['date_add'].'</td>
												</tr>';
										}
										$output .='
									</table>';
							}
							$output .='
						</div>
					</div>
				</div>
			</div>';
	}else{
		$output .='
			<div style="width: 900px;margin: 0 auto;clear: both;">';
				$result = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."pf_popup_google_users");
				if (empty($result)){
					$output .='
						<div> 
							<div class="warning warn">'.$this->l('Oops, No one here...').'</div> 
						</div>';
				}else{
					$output .='
						<table class="table product">
							<tr>
								<th>'.$this->l('ID').'</th>
								<th>'.$this->l('ID User').'</th>
								<th>'.$this->l('First name').'</th>
								<th>'.$this->l('Last name').'</th>
								<th>'.$this->l('Email').'</th>
								<th>'.$this->l('Gender').'</th>
								<th>'.$this->l('Birthday').'</th>
								<th>'.$this->l('Date Add').'</th>
							</tr>';
							foreach($result as $res){
								$output .='
								<tr>
									<td class="pointer">'.$res['id'].'</td>
									<td class="pointer">'.$res['id_user'].'</td>
									<td class="pointer">'.$res['first_name'].'</td>
									<td class="pointer">'.$res['last_name'].'</td>
									<td class="pointer">'.$res['email'].'</td>
									<td class="pointer">'.$res['gender'].'</td>
									<td class="pointer">'.$res['birthday'].'</td>
									<td class="pointer">'.$res['date_add'].'</td>
								</tr>';
							}
							$output .='
						</table>';
				}
				$output .='
			</div>';
	}
}
$output .='
		<style type="text/css">
			.no_top_radius{
				border-top-right-radius: 0px !important;
				border-top-left-radius:  0px !important;
			}
			.no_bottom_radius{
				margin-left : 5px;
				border-bottom-right-radius: 0px !important;
				border-bottom-left-radius:  0px !important;
			}
		</style>';