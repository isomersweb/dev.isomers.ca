<?php

$output .= '
					
	<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" '.($psv == 1.5?'style="width:900px; margin:0 auto;"':'').($psv >= 1.6?'class="defaultForm  form-horizontal"':'sky-form').'>'.
		($psv >= 1.6?'<div class="panel">':'<fieldset>').
			($psv < 1.6 ? '<legend>': '<h3>')
				.($psv >=1.6?'<i class="icon-cogs"></i>':'').$this->l(' Responsive Settings').
			($psv < 1.6 ? '</legend>': '</h3>').'

			'.($psv < 1.6?'<label for="popup_gls_enable_responsive">'.$this->l("Enable responsiveness").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="popup_gls_enable_responsive" class="control-label col-lg-3">'.$this->l("Enable responsiveness").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="popup_gls_enable_responsive" id="popup_gls_enable_responsive_on" value="true" '.(($this->_popup_gls_enable_responsive == "true") ? 'checked="checked" ' : '').'>
							<label for="popup_gls_enable_responsive_on">'.$this->l('Yes').'</label>
							<input type="radio" name="popup_gls_enable_responsive" id="popup_gls_enable_responsive_off" value="false" '.(($this->_popup_gls_enable_responsive == "false") ? 'checked="checked" ' : '').'>
							<label for="popup_gls_enable_responsive_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>								
					</div>
					':'
					<input type="hidden" name="popup_gls_enable_responsive" value="false" />
					<input type="checkbox" id="popup_gls_enable_responsive" name="popup_gls_enable_responsive" value="true" '.(($this->_popup_gls_enable_responsive == "true") ? 'checked="checked" ' : '').' >
					').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="popup_gls_resize_start_point">'.$this->l("Popup Resize Start Point").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="popup_gls_resize_start_point" class="control-label col-lg-3">'.$this->l("Popup Resize Start Point").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-2 input-group">':'').'
					<input type="text" id="popup_gls_resize_start_point" name="popup_gls_resize_start_point" class="fixed-width-xl" value="'.$this->_popup_gls_resize_start_point.'" />'.
				($psv >= 1.6?'<span class="input-group-addon">px</span></div>':'').'
				<p class="help-block" style="margin-left:25%;">'.$this->l('Specify the window width').'</p>
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="popup_gls_hide_start_point">'.$this->l("Popup hide start point").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="custom_box_height" class="control-label col-lg-3">'.$this->l("Popup hide start point").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-2 input-group">':'').'
					<input type="text" id="popup_gls_hide_start_point" name="popup_gls_hide_start_point" class="fixed-width-xl" value="'.$this->_popup_gls_hide_start_point.'" />'.
				($psv >= 1.6?'<span class="input-group-addon">px</span></div>':'').'
				<p class="help-block" style="margin-left:25%;">'.$this->l('Specify the window width when to hide the popup').'</p>
			</div>
			<div class="clear"></div>'.

			($psv >= 1.6?'<div class="panel-footer"><button type="submit" value="1" name="submitresponsivesettings" class="btn btn-default pull-right"><i class="process-icon-save"></i> '.$this->l('Update Settings').'</button></div>':'<input type="submit" name="submitresponsivesettings" value="'.$this->l('Update Settings').'" class="button" />').

		($psv < 1.6 ? '</fieldset>': '</div>').'
	</form>';