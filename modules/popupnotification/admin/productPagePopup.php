<?php

$output .='
	<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" '.($psv == 1.5?'style="width:900px; margin:0 auto;"':'').($psv >= 1.6?'class="defaultForm  form-horizontal"':'sky-form').' enctype="multipart/form-data">'.
	($psv >= 1.6?'<div class="panel">':'<fieldset>').
		($psv < 1.6 ? '<legend>': '<h3>')
			.($psv >=1.6?'<i class="icon-cogs"></i>':'').$this->l(' Product Page Popup').
		($psv < 1.6 ? '</legend>': '</h3>').'

			'.($psv < 1.6?'<label for="product_page_popup_enable">'.$this->l("Enable product page popup").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="product_page_popup_enable" class="control-label col-lg-3">'.$this->l("Enable product page popup").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="product_page_popup_enable" id="product_page_popup_enable_on" value="true" '.((Configuration::get('product_page_popup_enable') == "true") ? 'checked="checked" ' : '').'>
							<label for="product_page_popup_enable_on">'.$this->l('Yes').'</label>
							<input type="radio" name="product_page_popup_enable" id="product_page_popup_enable_off" value="false" '.((Configuration::get('product_page_popup_enable') == "false") ? 'checked="checked" ' : '').'>
							<label for="product_page_popup_enable_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>								
					</div>
					':'
					<input type="hidden" name="product_page_popup_enable" value="false" />
					<input type="checkbox" id="product_page_popup_enable" name="product_page_popup_enable" value="true" '.((Configuration::get('product_page_popup_enable') == "true") ? 'checked="checked" ' : '').' >
					').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="enable_multi_content">'.$this->l("Use different popups for each product").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="enable_multi_content" class="control-label col-lg-3">'.$this->l("Use different popups for each product").'</label>':'').
				($psv >= 1.6?'
					<div class="col-lg-9 ">
						<span class="switch prestashop-switch fixed-width-lg">
							<input type="radio" name="enable_multi_content" id="enable_multi_content_on" value="true" '.((Configuration::get('each_product_different_content') == "true") ? 'checked="checked" ' : '').'>
							<label for="enable_multi_content_on">'.$this->l('Yes').'</label>
							<input type="radio" name="enable_multi_content" id="enable_multi_content_off" value="false" '.((Configuration::get('each_product_different_content') == "false") ? 'checked="checked" ' : '').'>
							<label for="enable_multi_content_off">'.$this->l('No').'</label>
							<a class="slide-button btn"></a>
						</span>								
					</div>
					':'
					<input type="hidden" name="enable_multi_content" value="false" />
					<input type="checkbox" id="enable_multi_content" name="enable_multi_content" value="true" '.((Configuration::get('each_product_different_content') == "true") ? 'checked="checked" ' : '').' >
					').'
					<p class="help-block">'.$this->l('If you have enabled this, you have to add popup content in Catalog->Products->Edit').'</p>
				
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="product_box_width">'.$this->l("Product Box Width").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="product_box_width" class="control-label col-lg-3">'.$this->l("Product Box Width").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" id="product_box_width" name="product_box_width" class="fixed-width-xl" value="'.Configuration::get('product_box_width').'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="product_box_height">'.$this->l("Product Box Height").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="product_box_height" class="control-label col-lg-3">'.$this->l("Product Box Height").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" id="product_box_height" name="product_box_height" class="fixed-width-xl" value="'.Configuration::get('product_box_height').'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label>'.$this->l("Content Type").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Content Type").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<select name="product_content_type" id="product_content_type" class="fixed-width-xl">
						<option value="html" '.(Configuration::get('product_content_type') == 'html'?'selected':'').'>'.$this->l('HTML').'</option>
						<option value="youtube" '.(Configuration::get('product_content_type') == 'youtube'?'selected':'').'>'.$this->l('Youtube').'</option>
						<option value="vimeo" '.(Configuration::get('product_content_type') == 'vimeo'?'selected':'').'>'.$this->l('Vimeo').'</option>
						<option value="gmaps" '.(Configuration::get('product_content_type') == 'gmaps'?'selected':'').'>'.$this->l('Google Maps').'</option>
						<option value="facebook" '.(Configuration::get('product_content_type') == 'facebook'?'selected':'').'>'.$this->l('Facebook Like Box').'</option>
						<option value="newsletter" '.(Configuration::get('product_content_type') == 'newsletter'?'selected':'').'>'.$this->l('Newsletter Subscribtion').'</option>
						<option value="image" '.(Configuration::get('product_content_type') == 'image'?'selected':'').'>'.$this->l('Image').'</option>
					</select>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			<div id="product_html" class="hide-pop">
				'.($psv < 1.6?'<label>'.$this->l("Description").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Description").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'');
						foreach($languages as $language){
							$output .= '
								<div id="product_description_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
									<textarea cols="100" rows="'.($psv<1.6?'30':'10').'" class="rte autoload_rte"  name="product_description_'.$language['id_lang'].'">'.Configuration::get('product_description_'.$language['id_lang']).'</textarea></div>';
						}
						$output .= $this->displayFlags($languages, $defaultLanguage, 'product_description', 'product_description', true);
					$output .=
					($psv >= 1.6?'</div>':'').'
				</div>
			</div>
			<div class="clear"></div>

			<div id="product_youtube" class="hide-pop">
				'.($psv < 1.6?'<label>'.$this->l("Youtube Link").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Youtube Link").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'');
						foreach($languages as $language){
							$output .= '
							<div id="product_youtube_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
								<input type="text" size="70" name="product_youtube_link_'.$language['id_lang'].'" id="product_youtube_link_'.$language['id_lang'].'" class="fixed-width-xxl" value="'.Configuration::get('product_youtube_link_'.$language['id_lang']).'" />
							</div>';
						}
						$output .= $this->displayFlags($languages, $defaultLanguage, 'product_youtube', 'product_youtube', true);
					$output .=
					($psv >= 1.6?'</div>':'').'
				</div>
				<br />
			</div>
			<div class="clear"></div>

			<div id="product_vimeo" class="hide-pop">
				'.($psv < 1.6?'<label>'.$this->l("Viemo Link").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Viemo Link").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'');
						foreach($languages as $language){
							$output .= '
							<div id="product_vimeo_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
								<input type="text" size="70" name="product_vimeo_link_'.$language['id_lang'].'" id="product_vimeo_link_'.$language['id_lang'].'" class="fixed-width-xxl" value="'.Configuration::get('product_vimeo_link_'.$language['id_lang']).'" />
							</div>';
						}
						$output .= $this->displayFlags($languages, $defaultLanguage, 'product_vimeo', 'product_vimeo', true);
					$output .=
					($psv >= 1.6?'</div>':'').'
				</div>
				<br />
			</div>
			<div class="clear"></div>

			<div id="product_gmaps" class="hide-pop">
				'.($psv < 1.6?'<label>'.$this->l("Google Maps Link").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Google Maps Link").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'').'
						<input type="text" size="80" name="product_gmaps_link" class="fixed-width-xxl" id="product_gmaps_link" value="'.Configuration::get('product_gmaps_link').'" />'.
					($psv >= 1.6?'</div>':'').'
				</div>
			</div>
			<div class="clear"></div>

			<div id="product_facebook" class="hide-pop">
				'.($psv < 1.6?'<label>'.$this->l("Facebook Page Link").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Facebook Page Link").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'').'
						<input type="text" size="70" name="product_facebook_link" class="fixed-width-xxl" id="product_facebook_link" value="'.Configuration::get('product_facebook_link').'" />'.
					($psv >= 1.6?'</div>':'').'
				</div>
			</div>

			<div id="product_image" class="hide-pop">
				'.($psv < 1.6?'<label>'.$this->l("Upload Image").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'
						<label class="control-label col-lg-3">'.$this->l("Upload Image").'</label>
							<div class="col-lg-9">':'');
								if($psv >= 1.6){
									foreach($languages as $language){
										$output .= '
										<div id="product_box_image_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
											<div class="col-sm-5">';

												$output .= ' 
												<input id="product_image_input_'.$language['id_lang'].'" type="file" name="product_image_input_'.$language['id_lang'].'" class="hide">
												<div class="dummyfile input-group">
													<span class="input-group-addon"><i class="icon-file"></i></span>
													<input id="product_image_input_'.$language['id_lang'].'-name" type="text" name="filename_'.$language['id_lang'].'" readonly="">
													<span class="input-group-btn">
														<button id="product_image_input_'.$language['id_lang'].'-selectbutton" type="button" name="submitAddAttachments_'.$language['id_lang'].'" class="btn btn-default">
															<i class="icon-folder-open"></i> '.$this->l('Add file').'
														</button>
													</span>
												</div>
											</div>';

											if(Configuration::get('product_box_image_'.$language['id_lang']) && Configuration::get('product_box_image_'.$language['id_lang']) != '')
													$output .= '<img style="max-width:50%; clear:both; display:block;" src="'._MODULE_DIR_.$this->name.'/uploads/'.Configuration::get('product_box_image_'.$language['id_lang']).'" />';

											$output .= '<script type="text/javascript">
												$(document).ready(function(){
													$(\'#product_image_input_'.$language['id_lang'].'-selectbutton\').click(function(e) {
														$(\'#product_image_input_'.$language['id_lang'].'\').trigger(\'click\');
													});

													$(\'#product_image_input_'.$language['id_lang'].'-name\').click(function(e) {
														$(\'#product_image_input_'.$language['id_lang'].'\').trigger(\'click\');
													});

													$(\'#product_image_input_'.$language['id_lang'].'-name\').on(\'dragenter\', function(e) {
														e.stopPropagation();
														e.preventDefault();
													});

													$(\'#product_image_input_'.$language['id_lang'].'-name\').on(\'dragover\', function(e) {
														e.stopPropagation();
														e.preventDefault();
													});

													$(\'#product_image_input_'.$language['id_lang'].'-name\').on(\'drop\', function(e) {
														e.preventDefault();
														var files = e.originalEvent.dataTransfer.files;
														$(\'#product_image_input_'.$language['id_lang'].'\')[0].files = files;
														$(this).val(files[0].name);
													});

													$(\'#product_image_input_'.$language['id_lang'].'\').change(function(e) {
														if ($(this)[0].files !== undefined)
														{
															var files = $(this)[0].files;
															var name  = \'\';

															$.each(files, function(index, value) {
																name += value.name+\', \';
															});

															$(\'#product_image_input_'.$language['id_lang'].'-name\').val(name.slice(0, -2));
														}
														else // Internet Explorer 9 Compatibility
														{
															var name = $(this).val().split(/[\\/]/);
															$(\'#product_image_input_'.$language['id_lang'].'-name\').val(name[name.length-1]);
														}
													});
												});
											</script>
										</div>';
									}
									$output .= $this->displayFlags($languages, $defaultLanguage, 'product_box_image', 'product_box_image', true);
								}
							if($psv >= 1.6)
								$output .= '</div>';
							else{
								foreach($languages as $language){
									$output .= '
									<div id="product_box_image_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
										<input type="file" name="product_image_input_'.$language['id_lang'].'" id="product_image_input_'.$language['id_lang'].'" value="'.Configuration::get('product_image_input_'.$language['id_lang']).'" />
									</div>';

									if(Configuration::get('product_box_image_'.$language['id_lang']) && Configuration::get('product_box_image_'.$language['id_lang']) != '')
										$output .= '<img style="max-width:50%; clear:both; display:block;" src="'._MODULE_DIR_.$this->name.'/uploads/'.Configuration::get('product_box_image_'.$language['id_lang']).'" />';
								}
								$output .= $this->displayFlags($languages, $defaultLanguage, 'product_box_image', 'product_box_image', true);
							}
				$output .= '</div><div class="clear"></div><br />

				'.($psv < 1.6?'<label>'.$this->l("Link on Image").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Link on Image").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'');
						foreach($languages as $language){
							$output .= '
								<div id="product_img_link_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
									<input type="text" size="70" name="product_image_link_'.$language['id_lang'].'" id="product_image_link_'.$language['id_lang'].'" class="fixed-width-xxl" value="'.Configuration::get('product_image_link_'.$language['id_lang']).'" />
									<p class="help-block">'.$this->l('Leave blank to disable by default').'</p>
								</div>';
						}
						$output .= $this->displayFlags($languages, $defaultLanguage, 'product_img_link', 'product_img_link', true);
					$output .=
					($psv >= 1.6?'</div>':'').'
				</div>

			</div>
			<div class="clear"></div>

			<div id="product_box_bg_container">
				'.($psv < 1.6?'<br /><label>'.$this->l("Background Image").'</label>':'').'
				<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
					'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Background Image").'</label>':'').
					($psv >= 1.6?'<div class="col-lg-9">':'');

						if($this->product_box_bg != ''){
							$output .= '<img style="max-width:50%; clear:both; display:block; float:left;" src="'._MODULE_DIR_.$this->name.'/uploads/bg/'.$this->product_box_bg.'" />';
							$output .= '<button type="submit" value="1" name="remove_product_box_bg" class="btn btn-default" style="float:left;"><i class="icon-remove"></i> '.$this->l('Delete').'</button>';
						}

						if($psv < 1.6)
							$output .= '
								<div class="clear"></div>
								<input type="file" name="product_box_bg" id="product_box_bg" />';

						if($psv >= 1.6)
							$output .= '
							<div class="col-sm-5" style="clear:left;">
								<input id="product_box_bg" type="file" name="product_box_bg" class="hide">
								<div class="dummyfile input-group">
									<span class="input-group-addon"><i class="icon-file"></i></span>
									<input id="product_box_bg-name" type="text" name="filename_product_box_bg" readonly="">
									<span class="input-group-btn">
										<button id="product_box_bg-selectbutton" type="button" name="submitAddAttachments_product_box_bg" class="btn btn-default">
											<i class="icon-folder-open"></i> '.$this->l('Add file').'
										</button>
									</span>
								</div>
							</div>
							<script type="text/javascript">
								$(document).ready(function(){
									$(\'#product_box_bg-selectbutton\').click(function(e) {
										$(\'#product_box_bg\').trigger(\'click\');
									});

									$(\'#product_box_bg-name\').click(function(e) {
										$(\'#product_box_bg\').trigger(\'click\');
									});

									$(\'#product_box_bg-name\').on(\'dragenter\', function(e) {
										e.stopPropagation();
										e.preventDefault();
									});

									$(\'#product_box_bg-name\').on(\'dragover\', function(e) {
										e.stopPropagation();
										e.preventDefault();
									});

									$(\'#product_box_bg-name\').on(\'drop\', function(e) {
										e.preventDefault();
										var files = e.originalEvent.dataTransfer.files;
										$(\'#product_box_bg\')[0].files = files;
										$(this).val(files[0].name);
									});

									$(\'#product_box_bg\').change(function(e) {
										if ($(this)[0].files !== undefined)
										{
											var files = $(this)[0].files;
											var name  = \'\';

											$.each(files, function(index, value) {
												name += value.name+\', \';
											});

											$(\'#product_box_bg-name\').val(name.slice(0, -2));
										}
										else // Internet Explorer 9 Compatibility
										{
											var name = $(this).val().split(/[\\/]/);
											$(\'#product_box_bg-name\').val(name[name.length-1]);
										}
									});
								});
							</script>';
					$output .=
					($psv >= 1.6?'</div>':'');
					if ($psv >= 1.6)
						$output .= '
						<div class="form-group">
							<label class="control-label col-lg-3">'.$this->l('Repeat').'</label>
							<div class="col-lg-9">
								<div class="radio">
									<label for="repeat_x">
										<input type="radio" name="product_box_bg_repeat" id="repeat_x" value="repeat-x" '.($this->product_box_bg_repeat == 'repeat-x'?'checked == "checked"':'').'>
										'.$this->l('Repeat-x').'
									</label>
								</div>
								<div class="radio">
									<label for="repeat_y">
										<input type="radio" name="product_box_bg_repeat" id="repeat_y" value="repeat-y" '.($this->product_box_bg_repeat == 'repeat-y'?'checked == "checked"':'').'>
										'.$this->l('Repeat-y').'
									</label>
								</div>
								<div class="radio">
									<label for="repeat_x_y">
										<input type="radio" name="product_box_bg_repeat" id="repeat_x_y" value="repeat" '.($this->product_box_bg_repeat == 'repeat'?'checked == "checked"':'').'>
										'.$this->l('Repeat-x-y').'
									</label>
								</div>
								<div class="radio">
									<label for="no_repeat">
										<input type="radio" name="product_box_bg_repeat" id="no_repeat" value="no-repeat" '.($this->product_box_bg_repeat == 'no-repeat'?'checked == "checked"':'').'>
										'.$this->l('No Repeat').'
									</label>
								</div>
							</div>
						</div>';
				$output .= '
				</div>
				'.($psv < 1.6?'
				<div class="clear"></div>
				<label>'.$this->l('Repeat').'</label>
				<div class="margin-form">
					&nbsp;&nbsp;
					<input type="radio" name="product_box_bg_repeat" id="repeat_x" value="repeat-x" '.($this->product_box_bg_repeat == 'repeat-x'?'checked == "checked"':'').'>
					<label class="t" for="repeat_x">'.$this->l('Repeat-x').'</label>
					&nbsp;&nbsp;
					<input type="radio" name="product_box_bg_repeat" id="repeat_y" value="repeat-y" '.($this->product_box_bg_repeat == 'repeat-y'?'checked == "checked"':'').'>
					<label class="t" for="repeat_y">'.$this->l('Repeat-y').'</label>
					&nbsp;&nbsp;
					<input type="radio" name="product_box_bg_repeat" id="repeat_x_y" value="repeat" '.($this->product_box_bg_repeat == 'repeat'?'checked == "checked"':'').'>
					<label class="t" for="repeat_x_y">'.$this->l('Repeat-x-y').'</label>
					<input type="radio" name="product_box_bg_repeat" id="no_repeat" value="no-repeat" '.($this->product_box_bg_repeat == 'no-repeat'?'checked == "checked"':'').'>
					<label class="t" for="no_repeat">'.$this->l('No Repeat').'</label>
				</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="product_box_cookie">'.$this->l("Cookie Time (Days)").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="product_box_cookie" class="control-label col-lg-3">'.$this->l("Cookie Time (Days)").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="product_box_cookie" id="product_box_cookie" class="fixed-width-xl" value="'.Configuration::get("product_box_cookie").'" />
					<p class="help-block">'.$this->l('"0" for no cookie').'</p>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="product_box_start_date">'.$this->l("Start Date").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="product_box_start_date" class="control-label col-lg-3">'.$this->l("Start Date").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="product_box_start_date" id="product_box_start_date" class="fixed-width-xl" value="'.Configuration::get("product_box_start_date").'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="product_box_end_date">'.$this->l("End Date").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="product_box_end_date" class="control-label col-lg-3">'.$this->l("End Date").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="product_box_end_date" id="product_box_end_date" class="fixed-width-xl" value="'.Configuration::get("product_box_end_date").'" />'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="product_box_autoclose_time">'.$this->l("Close popup automatically").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="product_box_autoclose_time" class="control-label col-lg-3">'.$this->l("Close popup automatically").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="product_box_autoclose_time" id="product_box_autoclose_time" class="fixed-width-xl" value="'.Configuration::get("product_box_autoclose_time").'" />
					<p class="help-block">'.$this->l('"0" for no autoclose (seconds)').'</p>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label for="product_box_delay">'.$this->l("Display popup with delay").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label for="product_box_delay" class="control-label col-lg-3">'.$this->l("Display popup with delay").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<input type="text" name="product_box_delay" id="product_box_delay" class="fixed-width-xl" value="'.Configuration::get("product_box_delay").'" />
					<p class="help-block">'.$this->l('"0" for no delay (seconds)').'</p>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>

			'.($psv < 1.6?'<label>'.$this->l("Animation Effect").'</label>':'').'
			<div class="'.($psv >= 1.6?'form-group':'margin-form').'">
				'.($psv >= 1.6?'<label class="control-label col-lg-3">'.$this->l("Animation Effect").'</label>':'').
				($psv >= 1.6?'<div class="col-lg-9">':'').'
					<select name="product_box_effect" id="product_box_effect" class="fixed-width-xl">
						<option value="" '.(Configuration::get('product_box_effect') == ''?'selected':'').'>'.$this->l('None').'</option>
						<option value="mfp-zoom-in" '.(Configuration::get('product_box_effect') == 'mfp-zoom-in'?'selected':'').'>'.$this->l('Zoom').'</option>
						<option value="mfp-newspaper" '.(Configuration::get('product_box_effect') == 'mfp-newspaper'?'selected':'').'>'.$this->l('Newspaper').'</option>
						<option value="mfp-move-horizontal" '.(Configuration::get('product_box_effect') == 'mfp-move-horizontal'?'selected':'').'>'.$this->l('Horizontal move').'</option>
						<option value="mfp-move-from-top" '.(Configuration::get('product_box_effect') == 'mfp-move-from-top'?'selected':'').'>'.$this->l('Move from top').'</option>
						<option value="mfp-3d-unfold" '.(Configuration::get('product_box_effect') == 'mfp-3d-unfold'?'selected':'').'>'.$this->l('3d unfold').'</option>
						<option value="mfp-zoom-out" '.(Configuration::get('product_box_effect') == 'mfp-zoom-out'?'selected':'').'>'.$this->l('Zoom-out').'</option>
					</select>'.
				($psv >= 1.6?'</div>':'').'
			</div>
			<div class="clear"></div>'.

			($psv >= 1.6?'<div class="panel-footer"><button type="submit" value="1" name="submitproductpagepopup" class="btn btn-default pull-right"><i class="process-icon-save"></i> '.$this->l('Update Settings').'</button></div>':'<input type="submit" name="submitproductpagepopup" value="'.$this->l('Update Settings').'" class="button" />').

		($psv < 1.6 ? '</fieldset>': '</div>').'
	</form>';