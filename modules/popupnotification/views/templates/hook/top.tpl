{if $enable_fb}
	<div class="pf_popup_login_fb pf_connect pf_facebook pf_connect_top">
		<a href="#">{l s='Sign In' mod='popupnotification'}</a>
	</div>
{/if}

{if $enable_tw}
	<div class="pf_popup_login_tw pf_connect pf_twitter pf_connect_top">
		<a href="#" onclick="twitterPopup = window.open('{$callback_url}', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=300, width=700, height=600');">{l s='Sign In' mod='popupnotification'}</a>
	</div>
	<script>
		{literal}
			!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
		{/literal}
	</script>
{/if}

{if $enable_gp}
	<div class="pf_popup_login_gp pf_connect pf_googlePlus pf_connect_top">
		<span
		    class="g-signin"
		    data-callback="gpConnectCallback"
		    data-clientid="{$gp_client_id}"
		    data-cookiepolicy="single_host_origin"	    
		    data-scope="email">{l s='Sign In' mod='popupnotification'}
		</span>
	</div>
{/if}