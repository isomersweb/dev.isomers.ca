<style type="text/css">
	 @media (max-width: {$hide_start_point}px) {
	 	.mfp-wrap, .mfp-bg{
	 		display: none;
	 	}
	 }
</style>
{if $popup_login_enable_popup_login == "true"}
	<div id="login-and-register-popup" class="white-popup mfp-with-anim mfp-hide">
		{$description}
	</div>
{/if}

{if $enable_google_login == "true"}
	<script src="https://apis.google.com/js/client:platform.js" async defer></script>
	<script>
		var my_account_url = '{$my_account_url}';
		var pn_social_redirect = '{$redirect}';
		function gpConnectCallback(authResult) {
			if(!authResult.error && authResult.status.method == 'PROMPT' && authResult.access_token){
				$.ajax({
					type: "POST",
					url: baseDir+"modules/popupnotification/ajax/gp_login.php",
					data: {
						access_token: authResult.access_token 
					},
					success: function(data){
						if(pn_social_redirect == 'my_account')
							window.location.href = my_account_url;
						else
							window.location.reload();
					}
				});
			}
		}
	</script>
{/if}

<script type="text/javascript">
	var enable_responsive = '{$enable_responsive}';
	var resize_start_point = '{$resize_start_point}';
	var enable_facebook_login = '{$enable_facebook_login}';
	//var enable_google_login = '{$enable_google_login}';
	var facebook_app_id = '{$facebook_app_id}';
	var pn_social_redirect = '{$redirect}';
	var my_account_url = '{$my_account_url}';
	var popup_login_enable_popup_login = '{$popup_login_enable_popup_login}';

	if(enable_facebook_login == "true"){
		window.fbAsyncInit = function() {
			FB.init({
				appId: facebook_app_id,
				scope: 'email, user_birthday',
				cookie: true,
				xfbml: true
			});
		};

		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	}

	function fb_login(){
		FB.login(function(response) {
			if (response.authResponse) {
				access_token = response.authResponse.accessToken; //get access token
				user_id = response.authResponse.userID; //get FB UID
				FB.api('/me?fields=email,birthday,first_name,last_name,gender', function(response) {
					$.ajax({
						type: "POST",
						url: baseDir+"modules/popupnotification/ajax/fb_login.php",
						data: {
							firstname: response.first_name, 
							lastname: response.last_name, 
							email: response.email, 
							id: response.id, 
							gender: response.gender,
							birthday: response.birthday
						},
						success:  function(data){
							if(pn_social_redirect == 'my_account')
								window.location.href = my_account_url;
							else
								window.location.reload();
						}
					});
				});
			} else {
				//user hit cancel button
				console.log('User cancelled login or did not fully authorize.');
			}		
		},
		{
			scope: 'public_profile, email'
		});
	}
	$(document).ready(function(){
		$('.pf_popup_login_fb').click(function(){
			fb_login();
			return false;
		});
		if(popup_login_enable_popup_login == "true"){
			$('.login').attr('href','#login-and-register-popup');
			$('.login').magnificPopup({
			  type:'inline',
			  midClick: true,
			  removalDelay: 500,
			  callbacks: {
			  	beforeOpen: function() {
			       this.st.mainClass = 'pf_popup_signin_popup';
			    },
			    open: function(){
			    	PF_addResponsivnes(resize_start_point, enable_responsive);
			    }
			  }
			});
		}

		{literal}
			$('#pf_popup_login_register_form, #pf_popup_login_register_form_header').on('submit', function(){
				$.ajax({
    				type: 'POST',
          			dataType : 'json',
	    			data:{
	    				submitAccount:'',
	    				customer_firstname: $('#pf_popup_login_first_name').val(),
	    				customer_lastname: $('#pf_popup_login_last_name').val(),
	    				email: $('#pf_popup_login_email').val(),
	    				passwd: $('#pf_popup_login_register_password').val()
	    			},
    				url: baseDir + 'modules/popupnotification/ajax/register.php',
    				success: function(data){
    					if(data.hasError){
    						if(data.errors.customer_firstname)
    							$('#pf_popup_login_first_name').addClass('pf_popup_login_invalid');
    						else
    							$('#pf_popup_login_first_name').removeClass('pf_popup_login_invalid');

    						if(data.errors.customer_lastname)
    							$('#pf_popup_login_last_name').addClass('pf_popup_login_invalid');
    						else
    							$('#pf_popup_login_last_name').removeClass('pf_popup_login_invalid');

    						if(data.errors.email)
    							$('#pf_popup_login_email').addClass('pf_popup_login_invalid');
    						else
    							$('#pf_popup_login_email').removeClass('pf_popup_login_invalid');

    						if(data.errors.passwd)
    							$('#pf_popup_login_register_password').addClass('pf_popup_login_invalid');
    						else
    							$('#pf_popup_login_register_password').removeClass('pf_popup_login_invalid');

    						$( "#pf_popup_login_box" ).shake(5,5,5);
    					}else{
    						window.location.href = my_account_url;
    					}
    					// console.log(data);
    				}
    			});
    			return false;
    		});

			$('#pf_popup_login_login_form, #pf_popup_login_login_form_header').on('submit', function(){
				$.ajax({
    				type: 'POST',
          			dataType : 'json',
	    			data:{
	    				email: $('#pf_popup_login_l_user').val(),
						passwd: $('#pf_popup_login_l_password').val(),
						SubmitLogin: '',
						ajax: ''
	    			},
    				url: baseUri + '?controller=authentication',
    				success: function(data){
    					if(data.hasError){	
							$('#pf_popup_login_l_user').addClass('pf_popup_login_invalid');
							$('#pf_popup_login_l_password').addClass('pf_popup_login_invalid');
    						$("#pf_popup_login_box").shake(5,5,5);
    					}else{
    						window.location.href = my_account_url;
    					}
    				}
    			});
    			return false;
    		});
		{/literal}
	});
</script>