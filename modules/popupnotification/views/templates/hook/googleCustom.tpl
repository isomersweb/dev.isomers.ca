<div class="pf_popup_login_gp pf_connect pf_googlePlus">
	<span
	    class="g-signin"
	    data-callback="gpConnectCallback"
	    data-clientid="{$gp_client_id}"
	    data-cookiepolicy="single_host_origin"	    
	    data-scope="email">{l s='Sign In with Google' mod='popupnotification'}
	</span>
</div>