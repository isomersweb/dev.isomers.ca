{if isset($product_box_valid) && $product_box_valid == 1}	
	{if $product_content_type == 'html' || $product_content_type == 'facebook' || $product_content_type == 'newsletter'}
	<div  id="product-popup-notification" class="white-popup mfp-with-anim mfp-hide">
		{$description}
	</div>
	{if $product_content_type == 'newsletter'}
		{include file="./newsletter.tpl"}
	{/if}
	<a href="#product-popup-notification" class="open-popup-notification"></a>
	<script type="text/javascript">
		$('.open-popup-notification').magnificPopup({
			type:'inline',
			midClick: true,
			removalDelay: 500,
			callbacks: {
			    beforeOpen: function() {
			       this.st.mainClass = '{$product_box_effect}';
			    },
			    open: function(){
			    	PF_addResponsivnes(resize_start_point, enable_responsive);
			    }
			}
		});

		var product_box_cookie = '{$product_box_cookie|intval}';
		var product_box_delay = '{$product_box_delay|intval}';
		var product_box_autoclose_time = '{$product_box_autoclose_time|intval}';

			if(product_box_cookie == "0"){
				$.cookie("product_box_cookie", "0", { path: '/' });
			}
			$(window).load(function(){
				if($.trim($("#product-popup-notification").html()).length != 0){
					if($.cookie("product_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(product_box_cookie != 0){
								$.cookie("product_box_cookie", "1", { expires: parseInt(product_box_cookie), path: '/' });
							}	
						}, product_box_delay*1000);
						if(product_box_autoclose_time != 0){
							var hide_time = (parseInt(product_box_delay)+parseInt(product_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
						
					}
				}

			});
	</script>
	{else if $product_content_type == 'youtube' || $product_content_type == 'vimeo' || $product_content_type == 'gmaps'}
		<a href="{$description}" class="open-popup-notification"></a>
		<script type="text/javascript">
			$('.open-popup-notification').magnificPopup({
			  	type:'iframe',
			 	midClick: true,
			 	removalDelay: 500,
			  	callbacks: {
				    beforeOpen: function() {
				       this.st.mainClass = '{$product_box_effect}';
				    },
				    open: function(){
				    	PF_addResponsivnes(resize_start_point, enable_responsive);
				    }
			  	},
			  	iframe: {
				  	markup: '<div class="mfp-with-anim" style="height:100%;">'+
				  			'<div class="mfp-iframe-scaler">'+
				            '<div class="mfp-close"></div>'+
				            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
				          '</div></div>'
		      	}
			});

			var product_box_cookie = '{$product_box_cookie|intval}';
			var product_box_delay = '{$product_box_delay|intval}';
			var product_box_autoclose_time = '{$product_box_autoclose_time|intval}';

			if(product_box_cookie == "0"){
				$.cookie("product_box_cookie", "0", { path: '/' });
			}
			$(window).load(function(){
				if($.trim($(".open-popup-notification").attr('href')).length != 0){
					if($.cookie("product_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(product_box_cookie != 0){
								$.cookie("product_box_cookie", "1", { expires: parseInt(product_box_cookie), path: '/' });
							}	
						}, product_box_delay*1000);
						if(product_box_autoclose_time != 0){
							var hide_time = (parseInt(product_box_delay)+parseInt(product_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
					}
				}

			});
		</script>
	{else if $product_content_type == 'image'}
		<a href="{if $description != ''}{$image_dir}/{$description}{/if}" class="open-popup-notification"></a>
		<script type="text/javascript">
			var product_image_link = '{$product_image_link}';

			if(product_image_link != '')
				var html = '<a href="'+product_image_link+'"><div class="mfp-img"></div></a>';
			else
				var html = '<div class="mfp-img"></div>';

			$('.open-popup-notification').magnificPopup({
			  	type:'image',
			 	midClick: true,
			 	removalDelay: 500,
			  	callbacks: {
				    beforeOpen: function() {
				       this.st.mainClass = '{$product_box_effect}';
				    },
				    open: function(){
				    	PF_addResponsivnes(resize_start_point, enable_responsive);
				    }
			  	},
			  	image: {
				  	markup: '<div class="mfp-figure">'+
				            	'<div class="mfp-close"></div>'+
				            	html+
					            '<div class="mfp-bottom-bar">'+
					              	'<div class="mfp-title"></div>'+
					              	'<div class="mfp-counter"></div>'+
					            '</div>'+
				          	'</div>'
				}
			});

			var product_box_cookie = '{$product_box_cookie|intval}';
			var product_box_delay = '{$product_box_delay|intval}';
			var product_box_autoclose_time = '{$product_box_autoclose_time|intval}';

			if(product_box_cookie == "0"){
				$.cookie("product_box_cookie", "0");
			}
			$(window).load(function(){
				if($.trim($(".open-popup-notification").attr('href')).length != 0){
					if($.cookie("product_box_cookie") != "1"){
						setTimeout(function(){
							$('.open-popup-notification').click();
							if(product_box_cookie != 0){
								$.cookie("product_box_cookie", "1", { expires: parseInt(product_box_cookie) });
							}	
						}, product_box_delay*1000);
						if(product_box_autoclose_time != 0){
							var hide_time = (parseInt(product_box_delay)+parseInt(product_box_autoclose_time))*1000;
							setTimeout(function(){
								$.magnificPopup.close();
							}, hide_time);	
						}
					}
				}

			});
		</script>
	{/if}
{/if}