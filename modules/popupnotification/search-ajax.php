<?php

include(dirname(__FILE__).'/../../config/config.inc.php');

if (Tools::getValue('productFilter') AND $value = urldecode(Tools::getValue('q')) AND !is_array($value))
{
	$search = Search::find(intval(Tools::getValue('id_lang')), $value, 1, 10, 'position', 'desc', true);

	foreach ($search as $product){
		print $product['id_product'].'|'.$product['pname'].'|'.$product['cname']."\n";
	}
}

?>