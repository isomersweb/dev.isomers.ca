<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class StatsCommisions extends ModuleGrid
{
	private $html;
	private $query;
	private $columns;
	private $default_sort_column;
	private $default_sort_direction;
	private $empty_message;
	private $paging_message;

	public function __construct()
	{
		$this->name = 'statscommisions';
		$this->tab = 'analytics_stats';
		$this->version = '1.0.0';
		$this->author = 'Isomers';
		$this->need_instance = 0;

		parent::__construct();

        $this->default_sort_column = 'cname';
		$this->default_sort_direction = 'DESC';
		$this->empty_message = $this->l('Empty recordset returned.');
		$this->paging_message = sprintf($this->l('Displaying %1$s of %2$s'), '{0} - {1}', '{2}');

		$this->columns = array(
			array(
				'id' => 'cname',
				'header' => $this->l('Customer Name'),
				'dataIndex' => 'cname',
				'align' => 'left'
			),			
			array(
				'id' => 'address',
				'header' => $this->l('Address'),
				'dataIndex' => 'address',
				'align' => 'left'
			),			
			array(
				'id' => 'city',
				'header' => $this->l('City'),
				'dataIndex' => 'city',
				'align' => 'left'
			),			
			array(
				'id' => 'state',
				'header' => $this->l('State'),
				'dataIndex' => 'state',
				'align' => 'left'
			),			
			array(
				'id' => 'country',
				'header' => $this->l('Country'),
				'dataIndex' => 'country',
				'align' => 'left'
			),			
			array(
				'id' => 'sum_paid',
				'header' => $this->l('Sum Paid'),
				'dataIndex' => 'sum_paid',
				'align' => 'right'
			),			
			array(
				'id' => 'date_add',
				'header' => $this->l('Date Paid'),
				'dataIndex' => 'date_add',
				'align' => 'center'
			)			
		);

		$this->displayName = $this->l('Commissions Report Paid');
		//$this->description = $this->l('Adds a list of the best vouchers to the Stats dashboard.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{            
		return (parent::install() && $this->registerHook('AdminStatsModules'));
	}

	public function hookAdminStatsModules($params)
	{
		$engine_params = array(
			'id' => 'id_product',
			'title' => $this->displayName,
			'columns' => $this->columns,
			'defaultSortColumn' => $this->default_sort_column,
			'defaultSortDirection' => $this->default_sort_direction,
			'emptyMessage' => $this->empty_message,
			'pagingMessage' => $this->paging_message
		);

		if (Tools::getValue('export'))
			$this->csvExport($engine_params);

		$this->html = '
			<div class="panel-heading">
				'.$this->displayName.'
			</div>
			'.$this->engine($engine_params).'
			<a class="btn btn-default export-csv" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=1').'">
				<i class="icon-cloud-upload"></i> '.$this->l('CSV Export').'
			</a>';

		return $this->html;
	}

	public function getData()
	{
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
		/*$this->query = 'SELECT SQL_CALC_FOUND_ROWS cr.code, ocr.name, cr.date_from, cr.date_to,  ROUND(SUM(o.total_shipping), 2) as total_shipping, COUNT(ocr.id_cart_rule) as total, ROUND(SUM(o.total_paid_real) / o.conversion_rate,2) as ca, 
        IFNULL((SELECT COUNT(ocr1.id_customer_group) FROM '._DB_PREFIX_.'order_cart_rule ocr1 WHERE ocr.id_cart_rule = ocr1.id_cart_rule AND ocr1.id_customer_group = 3 GROUP BY ocr1.id_customer_group), 0) as sales_ha, 
        IFNULL((SELECT COUNT(ocr1.id_customer_group) FROM '._DB_PREFIX_.'order_cart_rule ocr1 WHERE ocr.id_cart_rule = ocr1.id_cart_rule AND ocr1.id_customer_group = 5 GROUP BY ocr1.id_customer_group), 0) as sales_st, 
        IFNULL((SELECT COUNT(ocr1.id_customer_group) FROM '._DB_PREFIX_.'order_cart_rule ocr1 WHERE ocr.id_cart_rule = ocr1.id_cart_rule AND ocr1.id_customer_group = 7 GROUP BY ocr1.id_customer_group), 0) as sales_rf
				FROM '._DB_PREFIX_.'order_cart_rule ocr
				LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ocr.id_order
				LEFT JOIN '._DB_PREFIX_.'customer_group cg ON o.id_customer = cg.id_customer
				LEFT JOIN '._DB_PREFIX_.'cart_rule cr ON cr.id_cart_rule = ocr.id_cart_rule
				WHERE o.valid = 1
					'.Shop::addSqlRestriction(Shop::SHARE_ORDER, 'o').'
					AND o.invoice_date BETWEEN '.$this->getDate().'
				GROUP BY ocr.id_cart_rule';
*/
        $this->query = 'SELECT SQL_CALC_FOUND_ROWS CONCAT(c.firstname, \' \', c.lastname) AS cname, ad.address1 As address, ad.postcode, ad.city, ad.phone, co.name AS country, st.name AS state, ROUND(rp.credits, 2) as sum_paid, rp.date_add
				FROM '._DB_PREFIX_.'rewards_payment rp
                LEFT JOIN '._DB_PREFIX_.'rewards r ON r.id_payment = rp.id_payment
                LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = r.id_customer
                LEFT JOIN (SELECT ad.* FROM '._DB_PREFIX_.'address ad
                    LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = ad.id_customer
                    LEFT JOIN '._DB_PREFIX_.'rewards r ON r.id_customer = ad.id_customer
                    LEFT JOIN '._DB_PREFIX_.'rewards_payment rp ON rp.id_payment = r.id_payment
                    WHERE r.id_reward_state = 8) ad ON ad.id_customer = r.id_customer
                LEFT JOIN '._DB_PREFIX_.'country_lang co ON co.id_country = ad.id_country
                LEFT JOIN '._DB_PREFIX_.'state st ON st.id_state = ad.id_state
				WHERE r.id_reward_state = 8
                    AND ad.active = 1
                    AND ad.deleted = 0
                    AND co.id_lang = 1
                    AND rp.date_add BETWEEN '.$this->getDate().'
				GROUP BY rp.id_payment';
                
		/*if (Validate::IsName($this->_sort))
		{
			$this->query .= ' ORDER BY `'.bqSQL($this->_sort).'`';
			if (isset($this->_direction) && (Tools::strtoupper($this->_direction) == 'ASC' || Tools::strtoupper($this->_direction) == 'DESC'))
				$this->query .= ' '.pSQL($this->_direction);
		}*/

		if (($this->_start === 0 || Validate::IsUnsignedInt($this->_start)) && Validate::IsUnsignedInt($this->_limit))
			$this->query .= ' LIMIT '.(int)$this->_start.', '.(int)$this->_limit;

		$values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($this->query);
		foreach ($values as &$value)
			$value['sum_paid'] = Tools::displayPrice($value['sum_paid'], $currency);

		$this->_values = $values;
//        echo "<pre>";print_r($this->_values);die;
		$this->_totalCount = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT FOUND_ROWS()');
	}
}
