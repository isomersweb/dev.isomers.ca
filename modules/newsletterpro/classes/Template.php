<?php
/**
* 2013-2015 Ovidiu Cimpean
*
* Ovidiu Cimpean - Newsletter Pro © All rights reserved.
*
* DISCLAIMER
*
* Do not edit, modify or copy this file.
* If you wish to customize it, contact us at addons4prestashop@gmail.com.
*
* @author    Ovidiu Cimpean <addons4prestashop@gmail.com>
* @copyright 2013-2015 Ovidiu Cimpean
* @version   Release: 3
* @license   Do not edit, modify or copy this file
*/

require_once dirname(__FILE__).'/ExtendTemplateVars.php';

class NewsletterProTemplate
{
	const ALLOW_UNREGISTRED_USERS = true;
	const ERROR_CODE_NO_FILE = 100;

	public $module;
	public $context;

	public $errors = array();
	public $user = false;
	public $type;

	/* type == 'file' */
	public $name;

	public $path_templates;
	public $path_html;
	public $url_html;
	public $filename;
	public $extension;

	/* type == 'history'*/
	public $id_history = 0;

	public $content;
	public $configuration;

	public $dynamic_vars_data = array();

	public $is_forwarder = false;
	public $forwarder_data = array();

	public static $replace_vars;
	public static $replace_chimp_vars;

	/**
	 * If there are no arguments, the template will be added from database configuration
	 *
	 * 	$template = new NewsletterproTemplate(array(
	 *		'data' => ('test.html' | file_get_contents('test.html') | (int)$id_history ),
	 *		'type' => ( 'file' | 'content' | 'history' ),
	 *
	 *		'user' => array(null, 'employee'), this will generate a test user of the current employee
	 *		'user' => array('test@test.com', 'employee'), this will create a test user of the current employee with the email test@test.com
	 *		'user' => array('test@test.com', 'unregistred'), this will create a test user with the email test@test.com
	 *		'user' => 'test@test.com', this will search the email test@test.com it the tables, ps_customers, ps_newsletter and ps_newsletter_pro_email and then a new user will be created
	 *
	 *		'tpl_vars' => array(
	 *			'firstname' => 'John',
	 *			'lastname' => 'Smith',
	 *		),
	 *	));
	 *
	 * @param array  $cfg
	 * @param string $type
	 */

	public function __construct($cfg = array())
	{
		$this->module = NewsletterPro::getInstance();

		if (!$this->module)
			throw new Exception('Cannot get instance of the newsletterpro module!');

		if (!isset($cfg['data']))
			$cfg['data'] = null;

		if (!isset($cfg['user']))
			$cfg['user'] = null;

		if (!isset($cfg['type']))
			$cfg['type'] = 'file';

		if (!isset($cfg['tpl_vars']))
			$cfg['tpl_vars'] = array();

		$this->context =& $this->module->context;

		$this->configuration = $cfg;
		$this->type = $cfg['type'];

		switch ($this->type)
		{
			// $data is template name
			case 'file':

				if (!isset($cfg['data']))
					$cfg['data'] = $this->getConfiguration('NEWSLETTER_TEMPLATE');

				$this->setInfo($cfg['data']);
				$this->setContent($this->getContentByType());
				break;

			// $data is content
			case 'content':

				$this->setContent($this->getContentByType());
				break;

			// $data is an history id
			case 'history':

				$this->id_history = (int)$cfg['data'];
				$this->setContent($this->getContentByType());
				break;
		}
	}

	public function setInfo($name)
	{
		$pathinfo = pathinfo($name);

		$this->name = $name;
		$this->filename = $pathinfo['filename'];

		if (isset($pathinfo['extension']))
			$this->extension = $pathinfo['extension'];

		$this->path_templates = dirname(dirname(__FILE__)).'/mails/newsletter/';
		$this->path_html = $this->path_templates.$this->name;
		$this->url_html = $this->module->url_location.'mails/newsletter/'.$this->name;
	}

	public function getInitUser()
	{
		$user = false;
		$cfg_user     = $this->configuration['user'];
		$cfg_tpl_vars = $this->configuration['tpl_vars'];

		switch (gettype($cfg_user))
		{
			case 'string':
					$user = $this->createUser($cfg_user, $cfg_tpl_vars);
				break;

			case 'array':

					$email = null;
					$user_type = 'unregistred';

					if (isset($cfg_user[0]))
						$email = $cfg_user[0];

					if (isset($cfg_user[1]))
						$user_type = $cfg_user[1];

					$user = $this->createTestUser($email, $user_type, $cfg_tpl_vars);
				break;

			default:
				$user = $this->createTestUser(null, 'unregistred', $cfg_tpl_vars);
				break;
		}

		if (!isset($user->id_lang))
			$user->id_lang = $this->getConfiguration('PS_LANG_DEFAULT');

		if (!isset($user->id_shop))
			$user->id_shop = $this->getConfiguration('PS_SHOP_DEFAULT');

		return $user;
	}

	public function getType()
	{
		return $this->type;
	}

	public function isFile()
	{
		return ($this->type == 'file');
	}

	public function isHistory()
	{
		return ($this->type == 'history');
	}

	public function isContent()
	{
		return ($this->type == 'content');
	}

	public function templateExists()
	{
		if ($this->extension != '' && file_exists($this->path_html))
			return true;
		return false;
	}

	public function getInfo()
	{
		$info = array(
			'name'           => $this->name,
			'filename'       => $this->filename,
			'extension'      => $this->extension,
			'path_templates' => $this->path_templates,
			'path_html'      => $this->path_html,
			'url_html'       => $this->url_html,
			'file_exists'    => $this->templateExists(),
			'content_css'    => $this->getNewsletterTemplateCSSArray(),
		);

		return (isset($this->name) ? $info : false);
	}

	private function getTitle($content)
	{
		$str = '';
		if (preg_match('/<\s*?(title)\s*?>(?P<content>.*?)<\s*?\/\s*?\1\s*?>/', $content, $match))
			$str = $match['content'];
		return $str;
	}

	private function getBody($content)
	{
		$str = '';
		if (preg_match('/<\s*?(body)\s*.*?>(?P<content>[\s\S]*)<s*?\/\s*?\1\s*?>/i', $content, $match))
			$str = $match['content'];
		return $str;
	}

	private function getHeader($content)
	{
		$str = '';
		if (preg_match('/(?P<content>^[\s\S]*?<\s*?body\s*?.*?>)/i', $content, $match))
			$str = $match['content'];
		return $str;
	}

	private function getFooter($content)
	{
		$str = '';
		if (preg_match('/(?P<content><\s*?\/\s*?\s*?body\s*?>[\s\S]*)/i', $content, $match))
			$str = $match['content'];
		return $str;
	}

	public static function addBodyStyle(&$body, $style = '')
	{
		if (preg_match('/<\s*?body\s*[^>]?style="[^>]*>/', $body))
			$body = preg_replace('/(<\s*?body\s*.*?(?:style=")?)([^">]*)(.*?>)/', '$1$2 '.$style.' $3', $body);
		else
			$body = preg_replace('/(<\s*?body\s*.*?(?:\s+)?)(>)/', '$1 style="'.$style.'" $2', $body);
	}

	public function getContentByType()
	{
		$content = '';
		if ($this->isFile())
		{
			if (file_exists($this->path_html))
			{
				if (($data_content = Tools::file_get_contents($this->path_html)) !== false)
					$content = $data_content;
				else
					$this->addError( sprintf($this->l('The file "%s" cannot be read, check the CHMOD!'), $this->name) );
			}
			else
				$this->addError( sprintf($this->l('File "%s" not exists!'), $this->name), self::ERROR_CODE_NO_FILE);
		}
		else if ($this->isContent())
			$content = $this->configuration['data'];
		else if ($this->isHistory())
		{
			$sql = 'SELECT * FROM `'._DB_PREFIX_.'newsletter_pro_tpl_history`
			WHERE `id_newsletter_pro_tpl_history`='.(int)$this->id_history;

			$row = Db::getInstance()->getRow($sql);
			if (isset($row['template']))
				$content = $row['template'];
			else
				$this->addError($this->l('Cannot get the template content from the history!'));
		}
		return $content;
	}

	private function stripComments(&$str)
	{
		$str = preg_replace('/\/\*[\s\S]*?\*\//', '', $str);
	}

	public function setContent($content)
	{
		$this->content = $content;
	}

	public function setContentBuild($content)
	{
		$this->setContent(self::buildContent($content));
	}

	public function getContent($get = 'all')
	{
		$content = array(
			'title' => '',
			'header' => '',
			'body' => '',
			'footer' => '',
			'full' => '',
		);

		$render = array(
			'title' => '',
			'header' => '',
			'body' => '',
			'footer' => '',
			'full' => '',
		);

		$response = array(
			'status' => false,
			'info' => $this->getInfo(),
			'content' => &$content,
			'render' => &$render,
		);

		switch ($get)
		{
			case 'all':
				$content['title'] = $this->getTitle($this->content);
				$content['header'] = $this->getHeader($this->content);
				$content['body'] = $this->getBody($this->content);
				$content['footer'] = $this->getFooter($this->content);
				$content['full'] = $this->content;
				break;

			case 'title':
				$content['title'] = $this->getTitle($this->content);
				break;

			case 'header':
				$content['header'] = $this->getHeader($this->content);
				break;

			case 'body':
				$content['body'] = $this->getBody($this->content);
				break;

			case 'footer':
				$content['footer'] = $this->getFooter($this->content);
				break;

			case 'full':
				$content['full'] = $this->content;
				break;
		}

		try
		{
			$data_render = $this->render();

			switch ($get)
			{
				case 'all':
					$render['title'] = $this->getTitle($data_render);
					$render['header'] = $this->getHeader($data_render);
					$render['body'] = $this->getBody($data_render);
					$render['footer'] = $this->getFooter($data_render);
					$render['full'] = $data_render;
					break;

				case 'title':
					$render['title'] = $this->getTitle($data_render);
					break;

				case 'header':
					$render['header'] = $this->getHeader($data_render);
					break;

				case 'body':
					$render['body'] = $this->getBody($data_render);
					break;

				case 'footer':
					$render['footer'] = $this->getFooter($data_render);
					break;

				case 'full':
					$render['full'] = $data_render;
					break;
			}
		}
		catch (Exception $e)
		{
			$this->addError($e->getMessage());
		}

		if (empty($this->errors))
			$response['status'] = true;
		if ($get != 'all')
		{
			return array(
				'content' => $response['content'][$get],
				'render' => $response['render'][$get],
			);
		}
		else
			return $response;
	}

	public function getErrors()
	{
		return $this->errors;
	}

	public function grepErrors()
	{
		$errors = array();
		foreach ($this->errors as $error)
			$errors[] = $error['message'];
		return $errors;
	}

	public function getErrorByConde($code)
	{
		foreach ($this->errors as $error)
			if ($error['code'] == $code)
				return $error;

		return false;
	}

	public function hasErrors()
	{
		return !empty($this->errors);
	}

	public function addError($message, $code = null)
	{
		$this->errors[] = array(
			'code' => $code,
			'message' => $message,
		);
	}

	public function render()
	{
		$content = $this->content;

		$all_vars   = $this->getTemplateVars();

		$vars       = $all_vars['default'];
		$chimp_vars = $all_vars['chimp'];

		$regex_if = '/\{(?:\s+)?if\s[\s\S]*?(\{(?:\s+)?\/(?:\s+)?if(?:\s+)?\})/';
		preg_match_all($regex_if, $content, $matches);
		$matches = $matches[0];

		foreach ($matches as $key => $value)
		{
			preg_match('/\{(?:\s+)?if\s(?:\s+)?(?P<content>[^}]+)\}/i', $value, $condition);
			$match_condition = trim(preg_replace('/\{if\s+|\}/', '', $condition['content']));
			$condition = str_replace(array('"', "'"), '', $match_condition);

			$sign = '';
			if (preg_match('/==/', $condition))
				$sign = '==';
			else if (preg_match('/isset/', $condition))
				$sign = 'isset';

			$regex_start_if = '/\{(?:\s+)?if\s(?:\s+)?[^}]+\}/';
			$regex_end_if = '/\{(?:\s+)?\/(?:\s+)?if(?:\s+)?\}/';

			switch ($sign)
			{
				case '==':
					$evaluate = explode('==', $condition);
					$evaluate[0] = isset($vars[$evaluate[0]]) ? $vars[$evaluate[0]] : $evaluate[0];

					if (trim($evaluate[0]) == trim($evaluate[1]))
						$line = preg_replace( array($regex_start_if, $regex_end_if), '', $value );
					else
						$line = preg_replace( array($regex_if), '', $value );

					break;

				case 'isset':
					$evaluate[0] = preg_replace('/isset|\(|\)/', '', $condition);
					$evaluate[1] = isset($vars[$evaluate[0]]) ? $vars[$evaluate[0]] : null;

					if (isset($evaluate[1]))
						$line = preg_replace( array($regex_start_if, $regex_end_if), '', $value );
					else
						$line = preg_replace( array($regex_if), '', $value );

					break;
			}

			$vars['{if '.$match_condition.'}'] = $line;
		}

		foreach ($vars as $key => $value)
		{
			preg_match('/\{(?:\s+)?if\s(?:\s+)?[^}]+\}/', $key, $match);
			if (!empty($match))
			{
				$sub = addcslashes(Tools::substr($match[0], 1, -1), '(){}[].');
				$sub = str_replace('if ', 'if\s(?:\s+)?', $sub);
				$regex_replace = '/\{(?:\s+)?'.$sub.'(?:\s+)?\}[\s\S]*?\{(?:\s+)?\/(?:\s+)?if(?:\s+)?\}/';
				$content = preg_replace($regex_replace, $value, $content);
			}
		}

		$content = $this->replaceVars($content, $vars, $chimp_vars);
		$content = $this->renderDynamicVars($content);

		if ($title = $this->getTitle($content))
		{
			$content = str_replace('{newsletter_title}', urlencode($title), $content);

			if (Tools::isSubmit('testCampaign'))
				ddd($content);
		}

		if ($this->isHistory())
		{
			$content = str_replace('{id_newsletter_pro_tpl_history}', $this->id_history, $content);

			// viewInBrowser will remove the opened email tracking if the browser is viewed by admin
			if (!Tools::isSubmit('viewInBrowser'))
			{
				$tracking_link = $this->module->url_location.'opened_email.php?email='.(string)$vars['email'].'&token='.$this->module->getTokenByIdHistory((int)$this->id_history);
				$tracking_img = '<img style="display: none; margin: 0; padding: 0; float: left; position: absolute;" src="'.$tracking_link.'" height="1" width="1" />';

				if ($body_end = $this->getEndBodyStrPos($content))
					$content = substr_replace($content, $tracking_img, $body_end, 0);
				else
					$content .= $tracking_img;
			}
		}
		else
			$content = str_replace('&id_newsletter={id_newsletter_pro_tpl_history}', '', $content);

		return $content;
	}

	private function getEndBodyStrPos($content)
	{
		if (preg_match('/(?P<body>\<\s*?\/\s*?body\s*?\>)/', $content, $match))
			return (int)strpos($content, $match['body']);
		return false;
	}

	public static function getDynamicVarsKyes($vars)
	{
		$result = array();
		foreach (array_keys($vars) as $var_name)
			$result[] = '{'.$var_name.'}';
		return $result;
	}

	public static function getDynamicVarsValues($vars)
	{
		return array_values($vars);
	}

	public function getProductLang($id_product, $data, $product)
	{
		$sql = 'SELECT p.`id_product`, pl.`id_lang`, pl.`name`, pl.`description`, pl.`description_short`, m.`name` AS manufacturer_name
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`)
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (p.`id_manufacturer` = m.`id_manufacturer`)
			WHERE p.`id_product` = '.(int)$id_product.'
			AND pl.`id_shop` = '.(int)$data['id_shop'];

		$product_db_lang = Db::getInstance()->executeS($sql);
		$product_lang = array();

		if (!empty($product_db_lang))
		{
			foreach ($product_db_lang as $pl)
				$product_lang[$pl['id_lang']] = $pl;
		}
		else
		{
			foreach ($data['languages_key_ids'] as $pl)
			{
				$product_lang[$pl['id_lang']] = array(
					'name'              => $product['name'],
					'description'       => $product['description'],
					'description_short' => $product['description_short'],
					'manufacturer_name' => $product['manufacturer_name'],
				);
			}
		}

		return $product_lang;
	}

	public function setStringVar($name, $product_name, &$vars, $product_lang, $settings)
	{
		$vars[$name] = strip_tags($product_lang[$settings['id_lang']][$product_name]);
		if ($settings['type'] == 'string')
		{
			$init_name = $vars[$name];
			if (isset($settings['trim_length']))
				$vars[$name] = Tools::substr($vars[$name], 0, $settings['trim_length']);

			if (Tools::strlen($init_name) >= $settings['trim_length'])
				$vars[$name] = $vars[$name].$settings['trim_end'];
		}
	}

	public function getDynamicVars($id_product, $available_vars)
	{
		$vars        = array();
		if (!empty($available_vars))
		{
			$data = $this->getDynamicVarsData();
			$dinamyc_vars = array();

			foreach ($available_vars as $value)
			{
				$exp_currency = explode('|', Tools::strtoupper($value));
				$exp_language = explode('|', Tools::strtolower($value));
				$exp = explode('|', $value);
				$var = $exp[0];

				$current_currency = array_intersect($data['currencies_iso'], $exp_currency);

				$dinamyc_vars[$value]['replace']     = $value;
				$dinamyc_vars[$value]['var']         = $var;
				$dinamyc_vars[$value]['id_currency'] = $data['id_currency'];
				$dinamyc_vars[$value]['id_lang']     = $data['id_lang'];

				if (!empty($current_currency))
				{
					$dinamyc_vars[$value]['type'] = 'price';

					$current_currency = array_values($current_currency);
					$iso_code = $current_currency[0];

					$get_currency = array_search($iso_code, $data['currencies_iso']);
					$dinamyc_vars[$value]['id_currency'] = ($get_currency !== false ? (int)$get_currency : $data['id_currency']);
				}
				else if (isset($exp[1]) && preg_match('/(?P<trim_length>\d+)?(?:\((?P<trim_end>[^)]+)\))?/', $exp[1], $match))
				{
					$len = null;
					if (isset($match['trim_length']))
						$len = $match['trim_length'];

					$end = '';
					if (isset($match['trim_end']))
						$end = $match['trim_end'];

					$dinamyc_vars[$value]['type'] = 'string';
					$dinamyc_vars[$value]['trim_length'] = $len;
					$dinamyc_vars[$value]['trim_end'] = $end;

					$current_lang = array_intersect($data['languages_iso'], $exp_language);

					if (!empty($current_lang))
					{
						$current_lang = array_values($current_lang);
						$iso_code = $current_lang[0];

						$get_lang = array_search($iso_code, $data['languages_iso']);
						$dinamyc_vars[$value]['id_lang'] = ($get_lang !== false ? (int)$get_lang : $data['id_lang']);
					}
				}
				else
					$dinamyc_vars[$value]['type'] = null;
			}

			$product = $this->module->getProductById($id_product, $data['id_lang']);

			if ($product && isset($product['dynamic_vars']))
			{
				$product_lang = $this->getProductLang($id_product, $data, $product);

				$vars =& $product['dynamic_vars'];

				$vars['$currency']                = $data['currencies_key_ids'][$data['id_currency']]['sign'];
				$vars['$price_convert']           = Tools::convertPrice((float)$product['price'], $data['id_currency']);
				$vars['$price_display']           = Tools::displayPrice((float)$vars['$price_convert'], $data['id_currency']);

				$vars['$price_without_reduction']         = $product['price_without_reduction'];
				$vars['$price_without_reduction_convert'] = Tools::convertPrice((float)$vars['$price_without_reduction'], (int)$data['id_currency']);
				$vars['$price_without_reduction_display'] = Tools::convertPrice((float)$vars['$price_without_reduction_convert'], (int)$data['id_currency']);

				$vars['$price_tax_exc']         = $product['price_tax_exc'];
				$vars['$price_tax_exc_convert'] = Tools::convertPrice((float)$vars['$price_tax_exc'], (int)$data['id_currency']);
				$vars['$price_tax_exc_display'] = Tools::displayPrice((float)$vars['$price_tax_exc_convert'], (int)$data['id_currency']);

				$vars['$wholesale_price']         = $product['wholesale_price'];
				$vars['$wholesale_price_convert'] = Tools::convertPrice((float)$vars['$wholesale_price'], (int)$data['id_currency']);
				$vars['$wholesale_price_display'] = Tools::displayPrice((float)$vars['$wholesale_price_convert'], (int)$data['id_currency']);

				$decimal = 2;
				foreach ($dinamyc_vars as $replace => $settings)
				{
					$target = $settings['var'];
					switch ($target)
					{
						case '$currency':
								$vars[$replace] = $data['currencies_key_ids'][$settings['id_currency']]['sign'];
							break;

						case '$price_convert':
								$vars[$replace] = number_format(Tools::convertPrice((float)$product['price'], $settings['id_currency']), $decimal);
							break;

						case '$price_display':
								$price_convert  = Tools::convertPrice((float)$product['price'], $settings['id_currency']);
								$vars[$replace] = Tools::displayPrice((float)$price_convert, $settings['id_currency']);
							break;

						case '$price_without_reduction_convert':
								$vars[$replace] = number_format(Tools::convertPrice((float)$vars['$price_without_reduction'], (int)$settings['id_currency']), $decimal);
							break;

						case '$price_without_reduction_display':
								$price_without_reduction_convert = Tools::convertPrice((float)$vars['$price_without_reduction'], (int)$settings['id_currency']);
								$vars[$replace]                  = Tools::displayPrice((float)$price_without_reduction_convert, (int)$settings['id_currency']);
							break;

						case '$price_tax_exc_convert':
								$vars[$replace] = number_format(Tools::convertPrice((float)$vars['$price_tax_exc'], (int)$settings['id_currency']), $decimal);
							break;

						case '$price_tax_exc_display':
								$price_tax_exc_convert = Tools::convertPrice((float)$vars['$price_tax_exc'], (int)$settings['id_currency']);
								$vars[$replace]        = Tools::displayPrice((float)$price_tax_exc_convert, (int)$settings['id_currency']);
							break;

						case '$wholesale_price_convert':
								$vars[$replace] = number_format(Tools::convertPrice((float)$vars['$wholesale_price'], (int)$settings['id_currency']), $decimal);
							break;

						case '$wholesale_price_display':
								$wholesale_price_convert = Tools::convertPrice((float)$vars['$wholesale_price'], (int)$settings['id_currency']);
								$vars[$replace]          = Tools::displayPrice((float)$wholesale_price_convert, (int)$settings['id_currency']);
							break;

						case '$name':
								$this->setStringVar($replace, 'name', $vars, $product_lang, $settings);
							break;

						case '$description':
								$this->setStringVar($replace, 'description', $vars, $product_lang, $settings);
							break;

						case '$description_short':
								$this->setStringVar($replace, 'description_short', $vars, $product_lang, $settings);
							break;

						case '$manufacturer_name':
							$this->setStringVar($replace, 'manufacturer_name', $vars, $product_lang, $settings);
							break;

						default:
							break;
					}
				}
			}
		}

		return $vars;
	}

	public function setDynamicVarsData($data)
	{
		$this->dynamic_vars_data = $data;
	}

	public function getDynamicVarsData()
	{
		return $this->dynamic_vars_data;
	}

	public function setupDynamicVarsData()
	{
		$id_currency = (int)$this->module->getConfiguration('CURRENCY');
		$id_lang     = (int)$this->module->getConfiguration('LANG');
		$id_shop     = (int)$this->context->shop->id;

		if (property_exists($this->context, 'customer') && $this->context->customer)
		{
			$prop = get_object_vars($this->context->customer);

			if (isset($prop['id_lang']))
				$id_lang = (int)$this->context->customer->id_lang;
			else
			{
				$c_id_lang = $this->module->getCustomerIdLang($this->context->customer->id);

				if ($c_id_lang)
					$id_lang = $c_id_lang;
			}

			$id_shop = (int)$this->context->customer->id_shop;
		}

		$currencies = NewsletterPro::getCurrenciesByIdShop($this->context->shop->id);

		$currencies_iso = array();
		$currencies_key_ids = array();

		foreach ($currencies as $currency)
		{
			$currencies_iso[$currency['id_currency']]     = $currency['iso_code'];
			$currencies_key_ids[$currency['id_currency']] = $currency;
		}

		$languages = Language::getLanguages(false);
		$languages_iso = array();
		$languages_key_ids = array();

		foreach ($languages as $language)
		{
			$languages_iso[$language['id_lang']]     = $language['iso_code'];
			$languages_key_ids[$language['id_lang']] = $language;
		}

		$this->setDynamicVarsData(array(
			'id_currency'        => $id_currency,
			'id_lang'            => $id_lang,
			'id_shop'            => $id_shop,
			'currencies'         => $currencies,
			'currencies_iso'     => $currencies_iso,
			'currencies_key_ids' => $currencies_key_ids,
			'languages'          => $languages,
			'languages_iso'      => $languages_iso,
			'languages_key_ids'  => $languages_key_ids,
		));
	}

	public function renderDynamicVars($content)
	{
		$current_encode = mb_detect_encoding($content, 'UTF-8, ISO-8859-1, ISO-8859-15, HTML-ENTITIES', true);
		// this will solve the trim last row bug in phpQuery plugin
		$content = html_entity_decode($content, ENT_COMPAT | ENT_HTML401 | ENT_HTML5, $current_encode);
		$content = mb_convert_encoding($content, 'UTF-8', $current_encode);

		// old
		// this will solve the trim last row bug in phpQuery plugin
		// $content = html_entity_decode($content);
		// $content = mb_convert_encoding($content, "UTF-8", mb_detect_encoding($content, "UTF-8, ISO-8859-1, ISO-8859-15, HTML-ENTITIES", true));

		$doc = NewsletterProphpQuery::newDocumentHTML($content);
		$dom_products = pq('.newsletter-pro-product');

		// this part is not required on the send email process but at the moment is working
		$dom_body = pq('body');
		$dom_container = pq('.newsletter-pro-container');
		if (count($dom_container) > 0 && count($dom_body) > 0)
		{
			$style = $dom_container->attr('style');
			$exp = explode(';', $style);
			$gr = preg_grep('/background-color|background/', $exp);
			if (count($gr) > 0)
			{
				$bg = trim($gr[key($gr)]);

				$last_char = Tools::substr($bg, -1);
				if ($last_char != ';')

				$bg .= ';';
			}

			if (isset($bg))
			{
				$body_style = trim($dom_body->attr('style'));

				$last_char = Tools::substr($body_style, -1);
				if ($last_char != ';')
					$body_style .= ';';

				$body_style .= $bg;

				$dom_body->attr('style', ltrim($body_style, ';'));
			}
		}

		$this->setupDynamicVarsData();

		foreach ($dom_products as $dom_product)
		{
			$id_product           = (int)pq($dom_product)->attr('data-id');
			$html_product         = pq($dom_product)->html();

			preg_match_all('/\{(?P<var>\$[^}]+)\}/', $html_product, $matches);
			$vars                 = $this->getDynamicVars($id_product, $matches['var']);

			// if the id_product doesn't exist in database, the template variables will become an empty string
			if (empty($vars) && !empty($matches['var']))
				$html_product_replace = preg_replace('/\{(?P<var>\$[^}]+)\}/', '', $html_product);
			else
				$html_product_replace = str_replace(self::getDynamicVarsKyes($vars), self::getDynamicVarsValues($vars), $html_product);

			pq($dom_product)->html($html_product_replace);
		}

		$html_return = html_entity_decode(str_replace(array('%7B', '%7D'), array('{', '}'), $doc->htmlOuter()));
		return $html_return;
	}

	public function getConfiguration($name)
	{
		return $this->module->getConfiguration($name);
	}

	public function updateConfiguration($name, $value)
	{
		return $this->module->updateConfiguration($name, $value);
	}

	public function relplaceAdminLink($path)
	{
		return $this->module->relplaceAdminLink($path);
	}

	public function l($value)
	{
		return $this->module->l($value);
	}

	public function replaceVars($template, $variables = array(), $chimp_vars = array())
	{
		self::$replace_vars       = $variables;
		$template = preg_replace_callback('/\{(?P<tag>\w+)\}/', 
			array($this, 'replaceCallback'),
			$template);

		self::$replace_chimp_vars = $chimp_vars;
		$template = preg_replace_callback('/\*\|(?P<tag>[^|]+)\|\*/',
			array($this, 'replaceChimpCallback'),
			$template);

		return $template;
	}

	public function replaceCallback($matches)
	{
		$tag = $matches[1]; // tag
		return (isset(self::$replace_vars[$tag])) ? self::$replace_vars[$tag] : '{'.$tag.'}';
	}

	public function replaceChimpCallback($matches)
	{
		$tag = $matches[1]; // tag
		return (isset(self::$replace_chimp_vars[$tag])) ? self::$replace_chimp_vars[$tag] : '*|'.$tag.'|*';
	}

	public function getTemplateVars()
	{
		$user = $this->getInitUser();

		if ($user)
		{
			$base_uri = $this->relplaceAdminLink(__PS_BASE_URI__);
			$default_shop_url = Tools::getHttpHost(true).$base_uri;
			$user->default_shop_url = $default_shop_url;

			$user->module_url = $default_shop_url.'modules/newsletterpro/';

			$shop_url = $this->module->getShopUrl();

			$user->shop_url = $shop_url;

			$shop_logo_url = $this->module->getShopLogoUrl($user->id_shop);

			$user->shop_logo_url = $shop_logo_url;

			$sql = 'SELECT `name`
					FROM `'._DB_PREFIX_.'shop` s
					WHERE s.`id_shop` = '.(int)$user->id_shop;

			$row = Db::getInstance()->getRow( $sql );
			$shop_name = isset($row['name']) ? $row['name'] : Tools::safeOutput($this->getConfiguration('PS_SHOP_NAME'));
			$user->shop_name = $shop_name;

			$shop_logo = '<a title="'.$user->shop_name.'" href="'.$user->shop_url.'"> <img style="border: none;" src="'.$user->shop_logo_url.'" alt="'.$user->shop_name.'" /> </a>';
			$user->shop_logo = $shop_logo;

			$unsubscribe_link = $shop_url.'index.php?fc=module&module=newsletterpro&controller=unsubscribe&email='.$user->email.'&u_token='.Tools::encrypt($user->email);
			$user->unsubscribe_link = $unsubscribe_link;

			$user->front_subscription_link = NewsletterProSubscriptionTpl::getFrontLink((int)$user->id_lang, (int)$user->id_shop);
			$user->front_subscription = '<a href="'.$user->front_subscription_link.'" target="_blank">'.$this->l('subscribe').'</a>';

			$unsubscribe_link_redirect = $shop_url.'index.php?fc=module&module=newsletterpro&controller=unsubscribe&email='.$user->email.'&msg=false';
			$user->unsubscribe_link_redirect = $unsubscribe_link_redirect;

			$unsubscribe = '<a href="'.$unsubscribe_link.'" target="_blank">'.$this->l('unsubscribe').'</a>';
			$user->unsubscribe = $unsubscribe;

			$unsubscribe_redirect = '<a href="'.$unsubscribe_link_redirect.'" target="_blank">'.$this->l('unsubscribe').'</a>';
			$user->unsubscribe_redirect = $unsubscribe_redirect;

			$subscribe_link = $shop_url.'index.php?fc=module&module=newsletterpro&controller=subscribe&email='.$user->email.'&token='.Tools::encrypt($user->email);
			$user->subscribe_link = $subscribe_link;

			$subscribe = '<a href="'.$subscribe_link.'" target="_blank">'.$this->l('subscribe').'</a>';
			$user->subscribe = $subscribe;

			$forward_link = $shop_url.'index.php?fc=module&module=newsletterpro&controller=forward&email='.$user->email.'&token='.Tools::encrypt($user->email);
			$user->forward_link = $forward_link;

			$forward = '<a href="'.$forward_link.'" target="_blank">'.$this->l('forward').'</a>';
			$user->forward = $forward;

			$user->date = date($user->date);
			$user->date_full = date($user->date_full);

			$user->is_forwarder    = (string)$this->isForwarder();
			$user->forwarder_email = (string)$this->getForwarderData('forwarder_email');

			$user->page_index_link       = $this->context->link->getPageLink('index', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_contact_link     = $this->context->link->getPageLink('contact', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_new_products     = $this->context->link->getPageLink('new-products', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_best_sales       = $this->context->link->getPageLink('best-sales', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_sitemap          = $this->context->link->getPageLink('sitemap', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_my_account       = $this->context->link->getPageLink('my-account', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_my_orders        = $this->context->link->getPageLink('history', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_my_order_slip    = $this->context->link->getPageLink('order-slip', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_my_addresses     = $this->context->link->getPageLink('addresses', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_my_personal_info = $this->context->link->getPageLink('identity', null, $user->id_lang, null, false, $user->id_shop);
			$user->page_my_vouchers      = $this->context->link->getPageLink('discount', null, $user->id_lang, null, false, $user->id_shop);
	
			if ($this->isHistory())
			{
				$user->unsubscribe_link = $user->unsubscribe_link.'&token='.$this->module->getTokenByIdHistory((int)$this->id_history);
				$user->unsubscribe_link_redirect = $user->unsubscribe_link_redirect.'&token='.$this->module->getTokenByIdHistory((int)$this->id_history);

				$view_in_browser_link = $this->module->url_location.'newsletter.php?&email='.$user->email.'&token_tpl='.$this->module->getTokenByIdHistory((int)$this->id_history);
				$user->view_in_browser_link = $view_in_browser_link;
				$user->view_in_browser = '<a href="'.$view_in_browser_link.'" target="_blank">'.$this->l('Click here').'</a>';
			}
			else
			{
				$user->view_in_browser_link = '';
				$user->view_in_browser = '';
			}

			$extend_vars = new NewsletterProExtendTemplateVars();
			$extend_vars->set( $user );
			$vars = get_object_vars($user);

			ksort($vars);

			$chimp_vars = $this->getMailChimpVars($vars);

			return array(
				'default' => $vars,
				'chimp'   => $chimp_vars,
			);
		}
		else
			$this->addError($this->l('Invalid user creation'));

		return array(
			'default' => array(),
			'chimp'   => array(),
		);
	}

	public function getMailChimpVars($vars)
	{
		$chimp_vars = array();

		if (isset($vars['email']))
			$chimp_vars['EMAIL'] = $vars['email'];

		if (isset($vars['firstname']))
			$chimp_vars['FNAME'] = $vars['firstname'];

		if (isset($vars['lastname']))
			$chimp_vars['LNAME'] = $vars['lastname'];

		if (isset($vars['shop_name']))
			$chimp_vars['SHOP'] = $vars['shop_name'];

		if (isset($vars['active']))
			$chimp_vars['SUBSCRIBED'] = $vars['active'];

		if (isset($vars['user_type']))
			$chimp_vars['USER_TYPE'] = $vars['user_type'];

		if (isset($vars['language']))
			$chimp_vars['LANGUAGE'] = $vars['language'];

		return $chimp_vars;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function createTestUser($email = null, $user_type = 'unregistred', $tpl_vars = array())
	{
		switch ($user_type)
		{
			case 'employee':

				$id_lang = (int)$this->context->language->id;
				$firstname = '';
				$lastname = '';

				$email = $email == true ? $email : $this->getConfiguration('PS_SHOP_EMAIL');
				$id = 0;

				if (isset($this->context->employee))
				{
					$id_lang = (int)$this->context->employee->id_lang;
					$firstname = $this->context->employee->firstname;
					$lastname = $this->context->employee->lastname;
					$id = (int)$this->context->employee->id;
				}

				break;

			case 'unregistred':

				$id_lang = (int)$this->getConfiguration('PS_LANG_DEFAULT');
				$firstname = '';
				$lastname = '';

				if (!isset($email))
					$this->addError($this->l('Invalid unregistered email'));

				$id = 0;
				break;

			default:
				throw new Exception('Invalid user type '.$user_type.'. Only the "employee", "unregistred" values are allowed.');
		}

		$user = new stdClass();
		$user->active = 1;
		$user->email = $email;

		$user->user_type = $user_type;
		$user->firstname = $firstname;
		$user->lastname = $lastname;
		$user->id = $id;
		$user->id_lang = $id_lang;

		$user->id_default_group = '';
		$user->id_shop = (int)$this->context->shop->id;

		if (file_exists(_PS_IMG_DIR_.'l/'.(int)$this->context->cookie->id_lang.'.jpg'))
			$user->img_path = $this->relplaceAdminLink( Tools::getHttpHost(true)._PS_IMG_.'l/'.(int)$id_lang.'.jpg' );
		else
			$user->img_path = $this->relplaceAdminLink( Tools::getHttpHost(true)._PS_IMG_.'l/none.jpg' );

		$user->ip = '127.0.0.1';

		$sql = 'SELECT `name`, `date_format_lite` AS `date`, `date_format_full` AS `date_full` 
				FROM `'._DB_PREFIX_.'lang` 
				WHERE `id_lang` = '.(int)$id_lang;

		$row = Db::getInstance()->getRow($sql);
		$user->language = isset($row['name']) ? $row['name'] : 'Language';
		$user->date = isset($row['name']) ? date($row['date']) : date('Y-m-d');
		$user->date_full = isset($row['name']) ? date($row['date_full']) : date('Y-m-d H:i:s');

		$user->newsletter_date_add = date('Y:m:d H:i:s');

		$sql = 'SELECT `name` FROM `'._DB_PREFIX_.'shop` 
				WHERE `id_shop` = '.(int)$this->context->shop->id;
		$row = Db::getInstance()->getRow($sql);
		$user->shop_name = isset($row['name']) ? $row['name'] : $this->getConfiguration('PS_SHOP_NAME');

		foreach ($tpl_vars as $key => $var)
			$user->{$key} = $var;

		$this->user = $user;
		return $this->user;
	}

	public function createUser($email, $tpl_vars = array())
	{
		$default_lang  = (int)$this->getConfiguration('PS_LANG_DEFAULT');
		$lang_img_path = Tools::getHttpHost(true)._PS_IMG_.'l/';
		$lang_img_dir  = _PS_IMG_DIR_.'l/';

		$sql_customer     = 'SELECT count(*) FROM `'._DB_PREFIX_.'customer` WHERE `email` = "'.pSQL($email).'"';
		$sql_added        = 'SELECT count(*) FROM `'._DB_PREFIX_.'newsletter_pro_email` WHERE `email` = "'.pSQL($email).'"';
		$sql_registred_np = 'SELECT count(*) FROM `'._DB_PREFIX_.'newsletter_pro_subscribers` WHERE `email` = "'.pSQL($email).'"';
		$sql_registred    = 'SELECT count(*) FROM `'._DB_PREFIX_.'newsletter` WHERE `email` = "'.pSQL($email).'"';

		$type = '';
		if (Db::getInstance()->getValue($sql_customer))
			$type = 'customer';
		else if (!((bool)$this->getConfiguration('SUBSCRIPTION_ACTIVE')) && Db::getInstance()->getValue($sql_registred))
			$type = 'registred';
		else if (((bool)$this->getConfiguration('SUBSCRIPTION_ACTIVE')) && Db::getInstance()->getValue($sql_registred_np))
			$type = 'registred_np';
		else if (Db::getInstance()->getValue($sql_added))
			$type = 'added';
		else
			$type = 'unregistred';

		switch ($type)
		{
			case 'customer':

				$sql = 'SELECT c.`firstname`, c.`lastname`, c.`email`, c.`id_customer` AS `id`, c.`id_lang`, c.`id_shop`,
						c.`id_default_group`, c.`newsletter_date_add`, c.`ip_registration_newsletter` AS `ip`, c.`active` , 
						lg.`name` AS `language`, lg.`date_format_lite` AS `date`, lg.`date_format_full` AS `date_full`, 
						sh.`name` AS `shop_name`
						FROM `'._DB_PREFIX_.'customer` c
						LEFT JOIN `'._DB_PREFIX_.'lang` lg ON (c.`id_lang` = lg.`id_lang`)
						INNER JOIN `'._DB_PREFIX_.'shop` sh ON (c.`id_shop` = sh.`id_shop`)
						WHERE c.`email` = "'.$email.'"';

				$row = Db::getInstance()->getRow($sql);

				if (isset($row['id']) && (int)$row['id'] > 0)
				{
					$user = Tools::jsonDecode(Tools::jsonEncode($row, ENT_QUOTES));

					$customer = new Customer((int)$user->id);
					if (Validate::isLoadedObject($customer))
						$this->context->customer = $customer;

					if ($user == null)
						$user = Tools::jsonDecode(Tools::jsonEncode($row));

					if (file_exists($lang_img_dir.$default_lang.'.jpg'))
						$user->img_path = $this->relplaceAdminLink( $lang_img_path.$default_lang.'.jpg' );
					else
						$user->img_path = $this->relplaceAdminLink( $lang_img_path.'none.jpg' );

					$user->user_type = 'customer';
				}

				break;

			case 'registred':

				$sql = 'SELECT n.`id`, n.`id_shop`, n.`ip_registration_newsletter` AS `ip`, n.`email`, n.`newsletter_date_add` , 
						s.`name` AS `shop_name`, l.`date_format_lite` AS `date`, l.`date_format_full` AS `date_full`, l.`name` AS `language`
						FROM `'._DB_PREFIX_.'newsletter` n
						LEFT JOIN `'._DB_PREFIX_.'lang` l ON (l.id_lang = '.$default_lang.')
						LEFT JOIN `'._DB_PREFIX_.'shop` s ON (s.`id_shop` = n.`id_shop`)
						WHERE n.`email` = "'.$email.'"';

				$row = Db::getInstance()->getRow($sql);

				if (isset($row['id']) && (int)$row['id'] > 0)
				{
					$user = Tools::jsonDecode(Tools::jsonEncode($row, ENT_QUOTES));

					if ($user == null)
						$user = Tools::jsonDecode(Tools::jsonEncode($row));

					if (file_exists($lang_img_dir.$default_lang.'.jpg'))
						$user->img_path = $this->relplaceAdminLink( $lang_img_path.$default_lang.'.jpg' );
					else
						$user->img_path = $this->relplaceAdminLink( $lang_img_path.'none.jpg' );

					$user->user_type        = 'registred';

					$user->firstname        = '';
					$user->lastname         = '';
					$user->id_lang          = $default_lang;
					$user->id_default_group = '';
					$user->active           = '1';
				}

				break;

			case 'added':

				$sql = 'SELECT 	n.`id_newsletter_pro_email` AS `id`,
								n.`email`, n.`firstname`, n.`lastname`, n.`id_lang`, n.`id_shop`, 
								n.`ip_registration_newsletter` AS `ip`, n.`date_add` AS `newsletter_date_add`, n.`active`,
								s.`name` AS `shop_name`,
								l.`date_format_lite` AS `date`, l.`date_format_full` AS `date_full`, l.`name` AS `language`
						FROM `'._DB_PREFIX_.'newsletter_pro_email` n
						LEFT JOIN `'._DB_PREFIX_.'lang` l ON (l.`id_lang` = '.$default_lang.')
						LEFT JOIN `'._DB_PREFIX_.'shop` s ON (s.`id_shop` = n.`id_shop`)
						WHERE n.`email` = "'.$email.'"';

				$row = Db::getInstance()->getRow($sql);

				if (isset($row['id']) && (int)$row['id'] > 0)
				{
					$user = Tools::jsonDecode(Tools::jsonEncode($row, ENT_QUOTES));

					if ($user == null)
						$user = Tools::jsonDecode(Tools::jsonEncode($row));

					if (file_exists($lang_img_dir.$default_lang.'.jpg'))
						$user->img_path = $this->relplaceAdminLink( $lang_img_path.$default_lang.'.jpg' );
					else
						$user->img_path = $this->relplaceAdminLink( $lang_img_path.'none.jpg' );

					$user->user_type = 'added';
					$user->id_default_group = '';
				}

				break;

			// the type is registred
			case 'registred_np':
				$sql = 'SELECT 	n.`id_newsletter_pro_subscribers` AS `id`,
								n.`email`, n.`firstname`, n.`lastname`, n.`id_lang`, n.`id_shop`, 
								n.`ip_registration_newsletter` AS `ip`, n.`date_add` AS `newsletter_date_add`, n.`active`,
								s.`name` AS `shop_name`,
								l.`date_format_lite` AS `date`, l.`date_format_full` AS `date_full`, l.`name` AS `language`
						FROM `'._DB_PREFIX_.'newsletter_pro_subscribers` n
						LEFT JOIN `'._DB_PREFIX_.'lang` l ON (l.`id_lang` = '.$default_lang.')
						LEFT JOIN `'._DB_PREFIX_.'shop` s ON (s.`id_shop` = n.`id_shop`)
						WHERE n.`email` = "'.$email.'"';

				$row = Db::getInstance()->getRow($sql);

				if (isset($row['id']) && (int)$row['id'] > 0)
				{
					$user = Tools::jsonDecode(Tools::jsonEncode($row, ENT_QUOTES));

					if ($user == null)
						$user = Tools::jsonDecode(Tools::jsonEncode($row));

					if (file_exists($lang_img_dir.$default_lang.'.jpg'))
						$user->img_path = $this->relplaceAdminLink( $lang_img_path.$default_lang.'.jpg' );
					else
						$user->img_path = $this->relplaceAdminLink( $lang_img_path.'none.jpg' );

					$user->user_type = 'registred_np';
					$user->id_default_group = '';
				}

				break;

			default:
				if (self::ALLOW_UNREGISTRED_USERS)
					$user = $this->createTestUser($email, $type = 'unregistred');
				else
					$this->addError($this->l('Unregistered user are not allowed'));
				break;
		}

		if (isset($user))
		{
			foreach ($tpl_vars as $key => $var)
				$user->{$key} = $var;

			$this->user = $user;
		}
		else
			$this->user = false;

		return $this->user;
	}

	public function save($name = null)
	{
		if (isset($name))
			$this->setInfo($name);

		if ($info = $this->getInfo())
		{
			$full_content = $this->getContent();
			if (!$this->hasErrors())
			{
				$html_content = $this->buildContent($full_content['content']);

				if (file_put_contents($info['path_html'], $html_content) === false)
					$this->addError($this->l('A problem occurred on saving the template. Please check the CHMOD permissions.'));
				else
					return true;
			}
		}
		else
			$this->addError($this->l('Failed save. Invalid info assigned to the template.'));
		return false;
	}

	public static function buildContent($content)
	{
		$str = '';
		if (isset($content['header']))
			$str .= $content['header'];

		if (isset($content['body']))
			$str .= $content['body'];

		if (isset($content['footer']))
			$str .= $content['footer'];
		return $str;
	}

	public function setForwarder($value)
	{
		$this->is_forwarder = $value;
	}

	public function getForwarderData($name)
	{
		if (isset($this->forwarder_data[$name]))
			return $this->forwarder_data[$name];
		return '';
	}

	public function setForwarderData($value)
	{
		$this->forwarder_data = $value;
	}

	public function isForwarder()
	{
		return $this->is_forwarder;
	}

	public function getNewsletterTemplateCSS()
	{
		$link = _MODULE_DIR_.'newsletterpro/css.php?getNewsletterTemplateCSS&name='.$this->name.'&uid='.uniqid();
		return $link;
	}

	public function getNewsletterTemplateCSSArray()
	{
		return array(
			$this->getNewsletterTemplateCSS()
		);
	}

	public function getCSSStyle()
	{
		$style = '';
		$header = $this->getHeader($this->content);
		if (preg_match_all('/<(?:\s*?)style[^>]*>(?P<content>[\s\S]*?)(<(?:\s*?)\/(?:\s*?)style(?:\s*?)>)/', $header, $match))
			$style = implode("\n", $match['content']);

		return $style;
	}
}
?>