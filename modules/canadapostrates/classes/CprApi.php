<?php
/**
*  2014 Zack Hussain
*
*  @author      Zack Hussain <me@zackhussain.ca>
*  @copyright   2014 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*/

class CprApi
{
    const PREFIX = 'CPR_';

    /* Array containing rate information */
    public $rate;

    /* Array containing available services */
    public $services;

    /* Call the Canada Post REST API */
    public function call($method, $url, $data = false, $header = false)
    {
        if (Tools::substr($url, 0, 5) == 'https')
            $url = $url;
        elseif (Configuration::get(self::PREFIX.'MODE') == 0)
            $url = 'https://ct.'. $url;
        else
            $url = 'https://'. $url;

        if ($method == 'PDF')
            sleep(3);

        $curl = curl_init($url);

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, true);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "GET":
                break;
            case "PDF":
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname(__FILE__).'/../cert/cacert.pem'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        if (Configuration::get(self::PREFIX.'MODE') != 0) {
            //$header[] = 'Platform-id: '.Configuration::get(self::PREFIX.'PLATFORM_ID');
            curl_setopt($curl, CURLOPT_USERPWD, Configuration::get(self::PREFIX.'PROD_API_USER').':'.Configuration::get(self::PREFIX.'PROD_API_PASS'));
        } else {
            curl_setopt($curl, CURLOPT_USERPWD, Configuration::get(self::PREFIX.'DEV_API_USER').':'.Configuration::get(self::PREFIX.'DEV_API_PASS'));
        }

////p($data);

        if ($header)
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            
        $response = curl_exec($curl);

        if ($response === false) {
            throw new Exception(curl_error($curl), 1);
        }
////p($url);        
////p($response);
        $content = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);

        if ($method == 'PDF')
            if (strpos($content, 'application/pdf') !== false)
                return $response;

        $json = Tools::jsonEncode(simplexml_load_string($response));
        $array = Tools::jsonDecode($json, true);

        curl_close($curl);

        return $array;
    }

    /* Accepts an array of shipment parameters, returns an array containing rate details */
    public function getRate($params)
    {
        $tags = '<mailing-scenario xmlns="http://www.canadapost.ca/ws/ship/rate-v3"></mailing-scenario>';

        $url = 'soa-gw.canadapost.ca/rs/ship/price';

        $data = $this->toXML($params, $tags);

        $header = array('Content-Type: application/vnd.cpc.ship.rate-v3+xml', 'Accept: application/vnd.cpc.ship.rate-v3+xml');

        if ($rate = $this->call('POST', $url, $data, $header))
            return $rate;
        else
            return false;
    }

    /* Accepts a country-code, returns an array containing available shipping methods to that location */
    public function getCountryServices($country)
    {
        $url = 'soa-gw.canadapost.ca/rs/ship/service?country='.$country;

        $data = false;

        $header = array('Accept: application/vnd.cpc.ship.rate-v3+xml');

        if ($services = $this->call('GET', $url, $data, $header))
            return $services;
        else
            return false;
    }

    /* Accepts a service and country, returns an array containing available services */
    public function getServices($service, $country)
    {
        $url = 'soa-gw.canadapost.ca/rs/ship/service/'.$service.'?country='.$country;

        $data = false;

        $header = array('Accept: application/vnd.cpc.ship.rate-v3+xml');

        if ($this->services = $this->call('GET', $url, $data, $header))
            return CPTools::putOptionsInArray($this->services);
        else
            return false;
    }

    public function getErrors($array)
    {
        $message = false;
        if (array_key_exists('message', $array)) {
            if (CPTools::countArray($array['message']) > 1)
            {
                $message = "";
                foreach ($array['message'] as $e)
                {
                    $message .= $e['code'].": ".$e['description'].' ';
                }
            }
            else if (CPTools::countArray($array['message']) == 1)
            {
                $message = $array['message']['code'].": ".$array['message']['description'];
            }
        }
        return $message;
    }

    /* Converts associative array into Canada Post's XML format */
    public function toXML($ar, $tags)
    {
        $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".$tags);
        $f = create_function('$f,$c,$a','
                foreach($a as $k=>$v) {
                    if(is_array($v)) {
                        if (!is_numeric($k)) {
                            $ch=$c->addChild($k);
                            $f($f,$ch,$v);
                        } else {
                            $f($f,$c,$v);
                        }
                    } else {
                        $c->addChild($k,$v);
                    }
                }');
        $f($f,$xml,$ar);
        return $xml->asXML();
    }
}