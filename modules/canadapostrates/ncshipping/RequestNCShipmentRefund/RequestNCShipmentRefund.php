<?php
/**
 * Sample code for the RequestNCShipmentRefund Canada Post service.
 * 
 * The RequestNCShipmentRefund service is used to request a refund for 
 * a shipment that has been transmitted. Making this call indicates 
 * that the previously printed label is spoiled or will otherwise not be used.
 * 
 * Note that these are “requests for refund�?, as refunding a shipment requires 
 * proper verification before being actioned (i.e. ensure label has not been used, for example).
 * 
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 */

// Your username and password are imported from the following file
// CPCWS_SOAP_NCShipping_PHP_Samples\SOAP\ncshipping\user.ini
$userProperties = parse_ini_file(realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../user.ini');

$wsdl = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../wsdl/ncshipment.wsdl';

$hostName = 'ct.soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/ncshipment/v4';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	$mailedBy = $userProperties['customerNumber'];
	$email = 'user@host.com';

	// Execute Request
	$result = $client->__soapCall('RequestNCShipmentRefund', array(
	    'request-non-contract-shipment-refund-request' => array(
			'locale'		=> 'FR',
			'mailed-by'		=> $mailedBy,
			'non-contract-shipment-refund-request' => array(
				'email'		=> $email
				)
			)
		), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'non-contract-shipment-refund-request-info'}) ) {
	    echo  'Service Ticket ID: ' . $result->{'non-contract-shipment-refund-request-info'}->{'service-ticket-id'} . "\n";
		echo  'Service Ticket Date: ' . $result->{'non-contract-shipment-refund-request-info'}->{'service-ticket-date'} . "\n";
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
}

?>

