<?php
 /**
 * Sample code for the CreateShipment Canada Post service.
 * 
 * The CreateShipment service is used to initiate generation of a shipping label
 * by providing shipment details. Use of this service indicates an intention to 
 * pay for shipment of an item.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

/*
 Need to override SoapClient because the abstract element 'groupIdOrTransmitShipment' is expected to be in the request in order for validation to pass.

So, we give it what it expects, but in __doRequest we modify the request by removing the abstract element and add the correct element.

*/
class MySoapClient extends SoapClient {

	function __construct($wsdl, $options = null) {
		parent::__construct($wsdl, $options);
	}

	function __doRequest($request, $location, $action, $version, $one_way = NULL) {
		$dom = new DOMDocument('1.0');
		$dom->loadXML($request);

		//get element name and values of group-id or transmit-shipment.
		$groupIdOrTransmitShipment =  $dom->getElementsByTagName("groupIdOrTransmitShipment")->item(0);
		$element = $groupIdOrTransmitShipment->firstChild->firstChild->nodeValue;
		$value = $groupIdOrTransmitShipment->firstChild->firstChild->nextSibling->firstChild->nodeValue;

		//remove bad element
		$newDom = $groupIdOrTransmitShipment->parentNode->removeChild($groupIdOrTransmitShipment);

		//append correct element with namespace
		$body =  $dom->getElementsByTagName("shipment")->item(0);
		$newElement = $dom->createElement($element, $value);
		$body->appendChild($newElement);

		//save $dom to string
		$request = $dom->saveXML();

		//echo $request;

		//doRequest
		return parent::__doRequest($request, $location, $action, $version);
	}
}


// Your username and password are imported from the following file
// CPCWS_SOAP_Shipping_PHP_Samples\SOAP\shipping\user.ini
$userProperties = parse_ini_file(realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../user.ini');

$wsdl = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../wsdl/shipment.wsdl';

$hostName = 'ct.soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/shipment/v8';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new MySoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	$mailedBy = $userProperties['customerNumber'];
	$groupId = '4326432';
	$requestedShippingPoint = 'H2B1A0';
	$mailingDate = '2017-02-24';
	$contractId = $userProperties['contractId'];

	// Execute Request
	$result = $client->__soapCall('CreateShipment', array(
	    'create-shipment-request' => array(
			'locale'			=> 'EN',
			'mailed-by'			=> $mailedBy,
			'shipment' 				=> array(
				//The validation expects this structure. However, this element will be removed and replaced only with ns1:group-id or ns1:transmit-shipment. 
				'groupIdOrTransmitShipment' => array(
					'ns1:group-id'	=> $groupId
					//'ns1:transmit-shipment' => 'true'
				),
				'requested-shipping-point'	=> $requestedShippingPoint,
				'cpc-pickup-indicator'	=> 'true',
				'expected-mailing-date'		=> $mailingDate,	
				'delivery-spec'		=> array(
					'service-code'		=> 'DOM.EP',
					'sender'			=> array(
						'name'				=> 'Bulma',	
						'company'			=> 'company',	
						'contact-phone'		=> 'contact-phone',	
						'address-details'	=> array(
							'address-line-1'	=> '502 MAIN ST N',	
							'city'				=> 'MONTREAL',	
							'prov-state'		=> 'QC',	
							'country-code'		=> 'CA',	
							'postal-zip-code'	=> 'H2B1A0'		
						)
					),
					'destination'			=> array(
						'name'				=> 'John Doe',	
						'company'			=> 'ACME Corp.',	
						'address-details'	=> array(
							'address-line-1'	=> '123 Postal Drive',	
							'city'				=> 'Ottawa',	
							'prov-state'		=> 'ON',	
							'country-code'		=> 'CA',	
							'postal-zip-code'	=> 'K1P5Z9'		
						)					
					),
					'options' 			=> array(
						'option' 				=> array(
							'option-code'			=> 'DC'
						)
					),
					'parcel-characteristics'	=> array(
						'weight'		=> 15,
						'dimensions'	=> array(
							'length'		=> 6,
							'width'			=> 12,
							'height'		=> 9
						),
						'unpackaged'	=> false,
						'mailing-tube'	=> false
					),
					'notification' 	=> array(
						'email'			=> 'ryuko.saito@kubere.com',
						'on-shipment'	=> true,
						'on-exception'	=> false,
						'on-delivery'	=> true
					),
					'print-preferences' => array(
						'output-format'		=> '8.5x11'
					),
					'preferences' 	=> array(
						'show-packing-instructions'	=> true,
						'show-postage-rate'			=> false,
						'show-insured-value'		=> true
					),													
					'settlement-info' => array(
						'contract-id'					=> $contractId,
						'intended-method-of-payment'	=> 'Account'
					),
					'references' 	=> array(
						'cost-centre'	=> 'ccent',
						'customer-ref-1'	=> 'custref1',
						'customer-ref-2'	=> 'custref2'
					)
				)
			)
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'shipment-info'}) ) {
	    echo  'Shipment Id: ' . $result->{'shipment-info'}->{'shipment-id'} . "\n";                 
		echo  'Tracking Pin: ' . $result->{'shipment-info'}->{'tracking-pin'} . "\n";                 
		foreach ( $result->{'shipment-info'}->{'artifacts'}->{'artifact'} as $artifact ) {  
			echo 'Artifact Id: ' . $artifact->{'artifact-id'} . "\n";
			echo 'Page Index: ' . $artifact->{'page-index'} . "\n\n";
		}
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
}

?>

