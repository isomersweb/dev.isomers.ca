<?php
 /**
 * Sample code for the GetShipmentReceipt Canada Post service.
 * 
 * The GetShipmentReceipt service is used to retrieve the shipment receipt 
 * in XML format for a no-manifest shipment paied by Credit Card created by a prior "create shipment" call.
 *
 * This sample is configured to access the Developer Program sandbox environment. 
 * Use your development key username and password for the web service credentials.
 * 
 **/

// Your username and password are imported from the following file
// CPCWS_SOAP_Shipping_PHP_Samples\SOAP\shipping\user.ini
$userProperties = parse_ini_file(realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../user.ini');

$wsdl = realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../wsdl/shipment.wsdl';

$hostName = 'ct.soa-gw.canadapost.ca';

// SOAP URI
$location = 'https://' . $hostName . '/rs/soap/shipment/v8';

// SSL Options
$opts = array('ssl' =>
	array(
		'verify_peer'=> false,
		'cafile' => realpath(dirname($_SERVER['SCRIPT_FILENAME'])) . '/../../../third-party/cert/cacert.pem',
		'CN_match' => $hostName
	)
);

$ctx = stream_context_create($opts);	
$client = new SoapClient($wsdl,array('location' => $location, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'stream_context' => $ctx));

// Set WS Security UsernameToken
$WSSENS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
$usernameToken = new stdClass(); 
$usernameToken->Username = new SoapVar($userProperties['username'], XSD_STRING, null, null, null, $WSSENS);
$usernameToken->Password = new SoapVar($userProperties['password'], XSD_STRING, null, null, null, $WSSENS);
$content = new stdClass(); 
$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $WSSENS);
$header = new SOAPHeader($WSSENS, 'Security', $content);
$client->__setSoapHeaders($header); 

try {
	$mailedBy = $userProperties['customerNumber'];
	// Execute Request
	$result = $client->__soapCall('GetShipmentReceipt', array(
	    'get-shipment-receipt-request' => array(
			'locale'			=> 'FR',
			'mailed-by'			=> $mailedBy,
			'shipment-id'		=> '832091378834466920'
		)
	), NULL, NULL);
	
	// Parse Response
	if ( isset($result->{'shipment-receipt'}) ) {
		if ( isset($result->{'shipment-receipt'}->{'shipment-receipt'}) ) {
			echo 'Auth Code: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'auth-code'} . "\n";
			echo 'Auth Timestamp: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'auth-timestamp'} . "\n";
			echo 'Charge Amount: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'charge-amount'} . "\n";
			echo 'Currency: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'currency'} . "\n";
			echo 'Merchant Name: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'merchant-name'} . "\n";
			echo 'Merchant URL: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'merchant-url'} . "\n";
			echo 'Name on Card: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'name-on-card'} . "\n";
			echo 'Card Type: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'card-type'} . "\n";
			echo 'Transaction Type: ' . $result->{'shipment-receipt'}->{'cc-receipt-details'}->{'transaction-type'} . "\n";
		} 
		else if ( isset($result->{'shipment-receipt'}->{'supplier-account-receipt-details'})) {
			echo 'Auth Code: ' . $result->{'shipment-receipt'}->{'supplier-account-receipt-details'}->{'auth-code'} . "\n";
			echo 'Auth Timestamp: ' . $result->{'shipment-receipt'}->{'supplier-account-receipt-details'}->{'auth-timestamp'} . "\n";
			echo 'Charge Amount: ' . $result->{'shipment-receipt'}->{'supplier-account-receipt-details'}->{'charge-amount'} . "\n";
			echo 'Currency: ' . $result->{'shipment-receipt'}->{'supplier-account-receipt-details'}->{'currency'} . "\n";
			echo 'Merchant Name: ' . $result->{'shipment-receipt'}->{'supplier-account-receipt-details'}->{'merchant-name'} . "\n";
			echo 'Merchant URL: ' . $result->{'shipment-receipt'}->{'supplier-account-receipt-details'}->{'merchant-url'} . "\n";
			echo 'Supplier Id: ' . $result->{'shipment-receipt'}->{'supplier-account-receipt-details'}->{'supplier-id'} . "\n";
			echo 'Transaction Type: ' . $result->{'shipment-receipt'}->{'supplier-account-receipt-details'}->{'transaction-type'} . "\n";
		}
		
	} else {
		foreach ( $result->{'messages'}->{'message'} as $message ) {
			echo 'Error Code: ' . $message->code . "\n";
			echo 'Error Msg: ' . $message->description . "\n\n";
		}
	}
	
} catch (SoapFault $exception) {
	echo 'Fault Code: ' . trim($exception->faultcode) . "\n"; 
	echo 'Fault Reason: ' . trim($exception->getMessage()) . "\n"; 
}

?>

