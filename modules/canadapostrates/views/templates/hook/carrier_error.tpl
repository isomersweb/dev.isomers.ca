{*
*  2014 Zack Hussain
*
*  @author      Zack Hussain <me@zackhussain.ca>
*  @copyright   2014 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*}
{if isset($cp_error) && $cp_error != false}
<p class="alert alert-danger">
    {l s='We could not get rates from Canada Post due to the following error: ' mod='canadapostrates'} {$cp_error|escape:'htmlall':'UTF-8'}
</p>
{/if}