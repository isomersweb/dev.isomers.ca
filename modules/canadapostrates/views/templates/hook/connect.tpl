{*
*  2014 Zack Hussain
*
*  @author      Zack Hussain <me@zackhussain.ca>
*  @copyright   2014 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*}
<div class="alert alert-info">
<img src="{$module_dir|escape:'htmlall':'UTF-8'}logo.png" style="float:left; margin-right:15px;">
<br>
<p>{l s='Click the button below to connect your account.' mod='canadapostrates'}</p>
</div>
<form action="https://www.canadapost.ca/cpotools/apps/drc/merchant" method="post">
	<input type="hidden" name="return-url" value="{$return_url|escape:'htmlall':'UTF-8'}" />
	<input type="hidden" name="token-id" value="{$token_id|escape:'htmlall':'UTF-8'}" />
	<input type="hidden" name="platform-id" value="{$platform_id|escape:'htmlall':'UTF-8'}" />
	<button class="btn btn-default" name="submitConnect" type="submit"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logo_icon.png" alt=""> {l s='Sign in with Canada Post' mod='canadapostrates'}</button>
</form>
<br>