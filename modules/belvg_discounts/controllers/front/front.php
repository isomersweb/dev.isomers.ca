<?php

/**
 * Belvg_discountsFrontModuleFrontController
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class Belvg_discountsFrontModuleFrontController extends ModuleFrontController
{
    public $ssl = TRUE;
    public $shop_id = 1;

    /**
     * Belvg_discountsFrontModuleFrontController::init()
     * 
     * @return void
     */
    public function init()
    {
        parent::init();
        require_once ($this->module->getLocalPath() . 'belvg_discounts.php');
    }

    /**
     * Belvg_discountsFrontModuleFrontController::getPointsAction()
     * 
     * @return void
     */
    public function getPointsAction()
    {
        $this->response = array();
        $ids = Tools::getValue('ids');
        if (is_array($ids)) {
            foreach ($ids as $id) {
                $product = new BelvgDiscountProduct($id, $this->shop_id);
                $data = $product->getData();
                $this->response[$id] = array(
                    'points' => ((int)$data['points'] > 0 ? ('<br><small class="belvg_discounts_catalog_points">' .
                        $product->getModule()->l('Get ', 'front') . ' ' .
                        $data['points'] . ' ' . $product->getModule()->l(' points(s)', 'front') . '</small>') : ''),
                    'buy_points' => ((int)$data['buy_points'] > 0 ? ('<br><span class="belvg_discounts_catalog_buy_points">' .
                        $product->getModule()->l('or ', 'front') . ' ' .
                        $data['buy_points'] . ' ' . $product->getModule()->l('point(s)', 'front') . '</span>') : ''),
                    );
            }
        }

        $this->response = json_encode($this->response);
    }

    /**
     * Belvg_discountsFrontModuleFrontController::getProductPointsAction()
     * 
     * @return void
     */
    public function getProductPointsAction()
    {
        $id = (int)Tools::getValue('id');
        if ($id) {
            $product = new BelvgDiscountProduct($id, $this->shop_id);
            $data = $product->getData();
            $this->response['link'] = '';
            if ($points = (int)$data['points']) {
                $this->response['link'] = '<a href="' . $product->getModule()->getConfig('desc_link') .
                    '">' . $product->getModule()->l('Get', 'front') . ' ' . $points . ' ' . $product->getModule()->l(' point(s) after purchase this product', 'front') .
                    '</a>';
            }

            $this->response['buy'] = '';
            if ($buy = (int)$data['buy_points']) {
                $this->response['buy'] = '<br><span class="belvg_discounts_catalog_buy_points">' .
                $product->getModule()->l('or ', 'front') . ' ' . $buy . ' ' . $product->getModule()->l(' point(s)', 'front') . '</span>';
            }
        }

        $this->response = json_encode($this->response);
    }

    /**
     * Belvg_discountsFrontModuleFrontController::run()
     * 
     * @return void
     */
    public function run()
    {
        if (!is_null(Shop::getContextShopID())) {
            $this->shop_id = Shop::getContextShopID();
        }

        $this->response = '';
        if (Tools::isSubmit('action')) {
            $actionName = Tools::getValue('action') . 'Action';
            if (method_exists($this, $actionName)) {
                $this->$actionName();
            }
        }

        echo $this->response;
    }
}
