<?php

/**
 * ParentOrderController
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class ParentOrderController extends ParentOrderControllerCore
{
    /**
     * ParentOrderController::init()
     * 
     * @return void
     */
    public function init()
    {
        parent::init();
        if (!Module::isEnabled('belvg_discounts')) {
            return;
        }

        $this->context->smarty->assign('belvg_discounts_tpl_dir', $this->
            getBelvgDiscountsTplDir());
    }

    /**
     * ParentOrderController::getBelvgDiscountsTplDir()
     * 
     * @return string
     */
    public function getBelvgDiscountsTplDir()
    {
        return _PS_MODULE_DIR_ . 'belvg_discounts/tpl/front/';
    }

    /**
     * ParentOrderController::setTemplate()
     * 
     * @param mixed $template
     * @return parent::setTemplate($template)
     */
    public function setTemplate($template)
    {        
        if (!Module::isEnabled('belvg_discounts')) {
            return parent::setTemplate($template);
        }

        if (substr($template, -17) == 'shopping-cart.tpl') {
            return parent::setTemplate($this->getBelvgDiscountsTplDir() .
                'shopping-cart.tpl');
        }

        if (substr($template, -13) == 'order-opc.tpl') {
            return parent::setTemplate($this->getBelvgDiscountsTplDir() . 'order-opc.tpl');
        }

        if (substr($template, -17) == 'order-payment.tpl') {
            return parent::setTemplate($this->getBelvgDiscountsTplDir() .
                'order-payment.tpl');
        }

        return parent::setTemplate($template);
    }
}
