<?php

/**
 * AdminOrdersController
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class AdminOrdersController extends AdminOrdersControllerCore
{
    /**
     * AdminOrdersController::renderView()
     * 
     * @return string
     */
    public function renderView()
    {
        if (!Module::isEnabled('belvg_discounts')) {
            return parent::renderView();
        }

        parent::renderView();
        $helper = new HelperView($this);
        $this->setHelperDisplay($helper);
        $helper->base_folder = _PS_MODULE_DIR_ . 'belvg_discounts/tpl/admin/';
        $this->tpl_view_vars['product_line_tpl'] = $helper->base_folder;
        $helper->tpl_vars = $this->tpl_view_vars;
        $helper->base_tpl = 'adminOrder.tpl';
        $view = $helper->generateView();

        return $view;
    }
}
