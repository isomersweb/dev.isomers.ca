(function($){
	$(document).ready(function(){
		var _$ = jQuery('#belvg_discounts');
		var addition_html = _$.find('.add-new-product').html();

		_$.find('.add-product').live('click', function(){
			$(this).hide();
			_$.find('.add-new-product').html(addition_html).show();
		});
		
		_$.find('.add-new-product .add').live('click', function(){
		
			var err = '';
			if (_$.find('input[name="belvg_add_id_product"]').val() < 1) {
				err += 'Select product!\n';
			}
			
			var belvg_add_new_discount = parseFloat(_$.find('input[name="belvg_add_new_discount"]').val());
			if (isNaN(belvg_add_new_discount)) {
				err += 'Field "Discount" is not valid.\n';
			} else {
				$(this).val(belvg_add_new_discount);
			}
	
			if (err == '') {
				_$.find('#belvg_is_add_new_product').val('1');
				$('#desc-product-save-and-stay').click();
			} else {
				alert(err);
			}
		});
		
		_$.find('.add-new-product .cancel').live('click', function(){
			_$.find('#belvg_is_add_new_product').val('0');
			_$.find('.add-new-product').hide();
			_$.find('.add-product').show();
		});
		
		_$.find('#belvg_product_autocomplete_input').live('keyup', function(){
			var val = $.trim($(this).val());
			if (val.length) {
				$.ajax({
					url: 'ajax_products_list.php',
					data: {q: encodeURIComponent(val), limit: 20, excludeIds: id_product},
					success: function(data) {
						_$.find('.ajax-result').html('').hide();
						var products = data.split('\n');
						for (var i = 0; i < products.length - 1; i++) {
							var _product = products[i].split('|');
							_$.find('.ajax-result').append('<a href="javascript:;" rel="'+_product[1]
								+'"><img src="../img/admin/add.gif"> <span>'+_product[0]+'</span></a><br>').show();
						}
					}
				});
			} else {
				_$.find('.ajax-result').html('').hide();
			}
		});
		
		_$.find('#belvg_product_autocomplete_input').live('blur', function(){
			setTimeout(function(){
				_$.find('.ajax-result').hide();
			}, 200);
		});
		
		_$.find('.ajax-result a').live('click', function(){
			var id = $(this).attr('rel');
			var name = $(this).find('span').text();
			$(this).parents('td:first').html('<input type="hidden" name="belvg_add_id_product" value="'+id+'" />'+name);
		});
		
		_$.find('table .edit').live('click', function(){
			var id = $(this).attr('rel');
			var tr = $(this).parents('tr:first');
			var name = tr.find('.name').text();
			_$.find('.find-product').html('<input type="hidden" name="belvg_add_id_product" value="'+id+'" />'+name);
			_$.find('input[name="belvg_add_new_discount"]').val($.trim(tr.find('.discount').text()));
			_$.find('input[name="belvg_add_new_points"]').val($.trim(tr.find('.points').text()));
			_$.find('.add-new-product').show();
			_$.find('.add-product').hide();
		});
		
		_$.find('table .del').live('click', function(){
			if (confirm('Delete selected item?')){
				var id = $(this).attr('rel');
				_$.find('input[name="belvg_delete"]').val(id);
				$('#desc-product-save-and-stay').click();
			}
		});
		
	});
})(jQuery);