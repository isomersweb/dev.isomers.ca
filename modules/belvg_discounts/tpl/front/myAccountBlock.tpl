<li class="belvg_discounts">
	<a href="{Configuration::get('belvg_discounts_desc_link')}" title="{l s='My points' mod='belvg_discounts'}">
		{if !$in_footer}<img width="26" src="{$belvg_discounts_dir}img/points.png" class="ui-li-icon ui-li-thumb" class="icon" alt="{l s='My points' mod='belvg_discounts'}"/>{/if}
		{l s='You have ' mod='belvg_discounts'}{Context::getContext()->cart->getCustomerPoints()}{l s=' point(s)!' mod='belvg_discounts'}
	</a>
</li>