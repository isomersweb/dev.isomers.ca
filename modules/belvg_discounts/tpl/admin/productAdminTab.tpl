<link href="../modules/belvg_discounts/css/admin.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="../modules/belvg_discounts/js/admin.js"></script>
<div id="belvg_discounts">
	<input type="hidden" name="belvg_discounts" value="1" />
	<input type="hidden" name="belvg_id_shop" value="{$storeId}" />
	<input type="hidden" name="belvg_is_add_new_product" id="belvg_is_add_new_product" value="0" />
	<input type="hidden" name="belvg_delete" value="0" />
	<h4>{l s='Points' mod='belvg_discounts'}</h4>
	<div class="separation"></div>
	<table>
		<tbody>
			<tr>
				<td class="col-left" style="width: 300px">
					<label>{l s='Number of points received after purchase:' mod='belvg_discounts'}</label>
				</td>
				<td style="padding-bottom:5px;">
					<input size="11" maxlength="14" name="belvg_product_points" type="text" value="{$belvg_discounts.points.points}">
					<p class="preference_description"></p>
				</td>
			</tr>
			<tr>
				<td class="col-left" style="width: 300px">
					<label>{l s='Number of points for which you can buy the product:' mod='belvg_discounts'}</label>
				</td>
				<td style="padding-bottom:5px;">
					<input size="11" maxlength="14" name="belvg_product_buy" type="text" value="{$belvg_discounts.points.buy_points}">
					<p class="preference_description">Set "0" for disable feature</p>
				</td>
			</tr>
		</tbody>
	</table>
	<h4>{l s='Group with additional products' mod='belvg_discounts'}</h4>
	<div class="separation"></div>

	<table class="table" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom:10px;">
		<thead>
			<tr class="nodrag nodrop" style="height: 40px">
				<th class="center">
				</th>
				<th class="left">
					<span class="title_box">
						{l s='Product ID' mod='belvg_discounts'}
					</span>
				</th>
				<th class="left">
					<span class="title_box">
						{l s='Product Name' mod='belvg_discounts'}
					</span>
				</th>
				<th class="left">
					<span class="title_box">
						{l s='Discount' mod='belvg_discounts'}
					</span>
				</th>
				<th class="left">
					<span class="title_box">
						{l s='Points' mod='belvg_discounts'}
					</span>
				</th>
				<th class="center">{l s='Actions' mod='belvg_discounts'}</th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$belvg_discounts.addons item=addon}
				<tr class="alt_row row_hover">
					<td class="center">
					</td>
					<td class="left">
						{$addon.id_addon}
					</td>
					<td class="left name">
						{$addon.product_name}
					</td>
					<td class="left discount">
						{number_format($addon.discount, 2)}
					</td>
					<td class="left points">
						{$addon.points}
					</td>
					<td class="center" style="white-space: nowrap;">
						<a href="javascript:;" rel="{$addon.id_addon}" class="edit" title="Edit">
							<img src="../img/admin/edit.gif" alt="Edit">
						</a>
						<a href="javascript:;" class="del" rel="{$addon.id_addon}" title="Delete">
							<img src="../img/admin/delete.gif" alt="Delete">
						</a>
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="6" class="center">
						<b>{l s='No products' mod='belvg_discounts'}</b>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>

	<div class="add-new-product" style="display: none">
		<h4>{l s='Add product' mod='belvg_discounts'}</h4>
		<div class="separation"></div>
		<table>
			<tbody>
				<tr>
					<td class="col-left" style="width: 300px">
						<label>{l s='Product:' mod='belvg_discounts'}</label>
					</td>
					<td style="padding-bottom:5px;" class="find-product">
						<input type="hidden" name="belvg_add_id_product" value="" />
						<input type="text" value="" id="belvg_product_autocomplete_input">
						<div class="ajax-result" style="display:none"></div>
						<p class="preference_description">
							{l s='Begin typing the first letters of the product name, then select the product from the drop-down list' mod='belvg_discounts'}
						</p>
					</td>
				</tr>
				<tr>
					<td class="col-left" style="width: 300px">
						<label>{l s='Discount:' mod='belvg_discounts'}</label>
					</td>
					<td style="padding-bottom:5px;">
						<input size="11" maxlength="14" name="belvg_add_new_discount" type="text" value="0">
					</td>
				</tr>
				<tr>
					<td class="col-left" style="width: 300px">
						<label>{l s='Points:' mod='belvg_discounts'}</label>
					</td>
					<td style="padding-bottom:5px;">
						<input size="11" maxlength="14" name="belvg_add_new_points" type="text" value="0">
					</td>
				</tr>
				<tr>
					<td class="col-left" style="width: 300px"></td>
					<td style="padding-bottom:5px;">
						<a class="button bt-icon add" href="javascript:void(0)">
							<span>{l s='Save' mod='belvg_discounts'}</span>
						</a>
						<a class="button bt-icon cancel" href="javascript:void(0)">
							<span>{l s='Cancel' mod='belvg_discounts'}</span>
						</a>
					</td>
				</tr>
			</tbody>	
		</table>
		<div class="separation"></div>
	</div>

	<a class="button bt-icon add-product" href="javascript:void(0)">
		<img src="../img/admin/add.gif">
		<span>{l s='Add product' mod='belvg_discounts'}</span>
	</a>
</div>