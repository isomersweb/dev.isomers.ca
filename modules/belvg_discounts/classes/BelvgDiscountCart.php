<?php

require_once _PS_MODULE_DIR_ .
    'belvg_discounts/classes/BelvgDiscountAbstract.php';

/**
 * BelvgDiscountCart
 *
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class BelvgDiscountCart extends BelvgDiscountAbstract
{
    protected $_id_cart = NULL;
    protected $_id_product = NULL;
    protected $_id_product_attribute = NULL;
    protected $_id_shop = NULL;

    /**
     * BelvgDiscountCart::__construct()
     *
     * @param mixed $id_cart
     * @param mixed $id_product
     * @param mixed $id_product_attribute
     * @return
     */
    public function __construct($id_cart, $id_product = NULL, $id_product_attribute = NULL, $id_shop = 1)
    {
        $this->_id_cart = (int)$id_cart;
        $this->_id_product = (int)$id_product;
        $this->_id_product_attribute = (int)$id_product_attribute;
        $this->_id_shop = (int)$id_shop;
    }

    /**
     * BelvgDiscountCart::createCartItemId()
     *
     * @param mixed $id_product
     * @param mixed $id_product_attribute
     * @return string
     */
    public static function createCartItemId($id_product, $id_product_attribute)
    {
        return (int)$id_product . '_' . (int)$id_product_attribute;
    }

    /**
     * BelvgDiscountCart::_getWhere()
     *
     * @param string $delimetr
     * @return string
     */
    protected function _getWhere($delimetr = 'AND')
    {
        $delimetr = ' ' . $delimetr;
        $where = ' `id_cart` = ' . $this->_id_cart;
        if ($this->_id_product) {
            $where .= $delimetr . ' `id_product` = ' . $this->_id_product;
        }

        if ($this->_id_product_attribute) {
            $where .= $delimetr . ' `id_product_attribute` = ' . $this->
                _id_product_attribute;
        }

        return $where;
    }

    /**
     * BelvgDiscountCart::addData()
     *
     * @param mixed $data
     * @return array
     */
    public function addData($data)
    {
        $_data = $this->getData();
        $addons = array();
        if (is_array($_data) AND isset($_data[0]['addons'])) {
            if ($_data[0]['addons']) {
                $addons = unserialize($_data[0]['addons']);
            }
        }

        foreach ($data['ids_addons'] as $addon) {
            if (array_key_exists($addon, $addons)) {
                $addons[$addon]++;
            } else {
                $addons[$addon] = 1;
            }
        }

        return Db::getInstance()->Execute('
            REPLACE INTO `' . $this->getModule()->getDbPrefix() . 'cart`
            SET' . $this->_getWhere(',') . ', `addons` = "' . pSQL(serialize($addons)) .
            '",
            `qty_by_points` = ' . (int)$_data[0]['qty_by_points']);
    }

    /**
     * BelvgDiscountCart::getData()
     *
     * @return array
     */
    public function getData()
    {
        return Db::getInstance()->ExecuteS('
            SELECT * FROM `' . $this->getModule()->getDbPrefix() . 'cart`
            WHERE' . $this->_getWhere());
    }

    /**
     * BelvgDiscountCart::removeData()
     *
     * @return bool
     */
    public function removeData()
    {
        return Db::getInstance()->Execute('
            DELETE FROM `' . $this->getModule()->getDbPrefix() . 'cart`
            WHERE' . $this->_getWhere());
    }

    /**
     * BelvgDiscountCart::removeCartAddon()
     *
     * @param mixed $customId
     * @return NULL
     */
    public function removeCartAddon($customId)
    {
        $data = $this->getData();
        foreach ($data as $_data) {
            if ($_data['addons']) {
                $serialize = unserialize($_data['addons']);
                if (isset($serialize[$customId])) {
                    unset($serialize[$customId]);
                    Db::getInstance()->Execute('
                        UPDATE `' . $this->getModule()->getDbPrefix() . 'cart`
                        SET `addons` = "' . pSQL(serialize($serialize)) . '"
                        WHERE `id_cart` = ' . $this->_id_cart . '
                        AND `id_product` = ' . $_data['id_product'] . '
                        AND `id_product_attribute` = ' . $_data['id_product_attribute']);
                }
            }
        }
    }

    /**
     * BelvgDiscountCart::getQtyByPoints()
     *
     * @return int
     */
    public function getQtyByPoints()
    {
        return (int)Db::getInstance()->getValue('
            SELECT `qty_by_points` FROM `' . $this->getModule()->getDbPrefix() .
            'cart`
            WHERE' . $this->_getWhere());
    }

    /**
     * BelvgDiscountCart::setQtyByPoints()
     *
     * @param mixed $qty
     * @return bool
     */
    public function setQtyByPoints($qty)
    {
        $addons = pSQL(serialize(array()));
        $data = $this->getData();
        if (isset($data[0]['addons'])) {
            $addons = pSQL($data[0]['addons']);
        }

        return Db::getInstance()->Execute('
            REPLACE INTO `' . $this->getModule()->getDbPrefix() . 'cart`
            SET `qty_by_points` = ' . $qty . ', ' . $this->_getWhere(',') .
            ', `addons` = "' . $addons . '"');
    }

    /**
     * BelvgDiscountCart::calculateCartData()
     *
     * @param mixed $products
     * @return array
     */
    public function calculateCartData($products = NULL)
    {
        $shop_id = $this->_id_shop;

        if (is_NULL($products)) {
            $cart = new Cart($this->_id_cart);
            $products = $cart->getProducts();
        }

        $rules = new self($this->_id_cart, NULL, NULL, $shop_id);
        $_products = array();
        $qtyByPoints = array();
        foreach ($products as $product) {
            $customId = $product['id_product'] . '_' . $product['id_product_attribute'];
            $qtyByPoints[$customId] = new self($this->_id_cart, $product['id_product'], $product['id_product_attribute'], $shop_id);
            $_products[$customId] = array(
                'qty' => $product['cart_quantity'] - $qtyByPoints[$customId]->getQtyByPoints(),
                'qty_by_points' => $qtyByPoints[$customId]->getQtyByPoints(),
                'qty_by_money' => $product['cart_quantity'] - $qtyByPoints[$customId]->
                    getQtyByPoints(),
                'qty_discount' => 0,
                'discount' => 0,
                'points' => 0,
                'all_qty' => $product['cart_quantity'],
                );
        }

        foreach ($rules->getData() as $_rule) {
            if (!array_key_exists($_rule['id_product'] . '_' . $_rule['id_product_attribute'],
                $_products)) {
                /*$this->_id_product = $_rule['id_product'];
                $this->_id_product_attribute = $_rule['id_product_attribute'];
                $this->removeData();
                return $this->calculateCartData($products);*/
                continue;
            }

            $belvgProduct = new BelvgDiscountProduct($_rule['id_product'], $shop_id);
            $addons = $belvgProduct->getProductAddons();
            $ruleAddons = array();
            if ($_rule['addons']) {
                $ruleAddons = unserialize($_rule['addons']);
            }

            foreach ($ruleAddons as $addonId => &$ruleAddon) {
                if (array_key_exists($addonId, $_products)) {
                    $ids = explode('_', $addonId);

                    foreach ($addons as $addon) {
                        if ($addon['id_addon'] == $_rule['id_product']) {
                            continue;
                        }

                        if ($addon['id_addon'] == $ids[0]) {
                            $_products[$addonId]['discount'] += $addon['discount'] * $ruleAddon;
                            $_products[$addonId]['points'] += $addon['points'] * $ruleAddon;
                            $_products[$addonId]['qty'] -= $ruleAddon;
                            $_products[$addonId]['qty_discount'] += $ruleAddon;
                        }
                    }
                } /*else {
                    $update = new self($this->_id_cart, NULL, NULL, $shop_id);
                    $update->removeCartAddon($addonId);
                    return $this->calculateCartData($products);
                }*/
            }
        }

        $err = 0;
        $points = 0;
        $discount = 0;
        $qty_by_points = 0;
        foreach ($_products as $customId => &$_product) {
            $ids = explode('_', $customId);
            $belvgProduct = new BelvgDiscountProduct($ids[0], $shop_id);
            $data = $belvgProduct->getProductData();

            if ($_product['qty'] > 0) {
                $_product['points'] += (int)$_product['qty'] * (int)$data['points']['points'];
            }

            $qty_by_points += (int)$_product['qty_by_points'] * (int)$data['points']['buy_points'];

            if ($_product['all_qty'] < $_product['qty_discount']) {
                $update = new self($this->_id_cart, NULL, NULL, $shop_id);
                $update->removeCartAddon($customId);
                $err = 1;
            }

            if ($_product['qty_by_money'] > 0) {
                $discount += $_product['discount'];
                $points += $_product['points'];
            } else {
                $_product['discount'] = 0;
                $_product['points'] = 0;
            }
        }

        if ($err) {
            return $this->calculateCartData($products);
        }

        return array(
            'products' => $_products,
            'points' => $points,
            'discount' => $discount,
            'qty_by_points' => $qty_by_points,
            );
    }

    /**
     * BelvgDiscountCart::getCalculateDataByName()
     *
     * @param mixed $param
     * @param mixed $products
     * @return array
     */
    public function getCalculateDataByName($param, $products = NULL)
    {
        $data = $this->calculateCartData($products);
        return $data[$param];
    }
}
