<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_ . 'belvg_discounts/belvg_discounts.php';

/**
 * BelvgDiscountAbstract
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
abstract class BelvgDiscountAbstract
{
    protected $_moduleObj = NULL;

    /**
     * BelvgDiscountAbstract::getModule()
     * 
     * @return belvg_discounts
     */
    public function getModule()
    {
        if (is_NULL($this->_moduleObj)) {
            $this->_moduleObj = new belvg_discounts;
        }

        return $this->_moduleObj;
    }
}
