<?php

/**
 * Cart
 *
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class Cart extends CartCore
{
    protected $_belvgCart = NULL;

    /**
     * Cart::__construct()
     *
     * @param mixed $id
     * @param mixed $id_lang
     * @return
     */
    public function __construct($id = NULL, $id_lang = NULL)
    {
        if (!Module::isEnabled('belvg_discounts')) {
            return parent::__construct($id, $id_lang);
        }

        parent::__construct($id, $id_lang);

        require_once _PS_MODULE_DIR_ .
            'belvg_discounts/classes/BelvgDiscountProduct.php';
        require_once _PS_MODULE_DIR_ .
            'belvg_discounts/classes/BelvgDiscountCustomer.php';
        require_once _PS_MODULE_DIR_ . 'belvg_discounts/classes/BelvgDiscountCart.php';
        $this->_belvgCart = new BelvgDiscountCart($id, NULL, NULL, $this->id_shop);
    }

    /**
     * Cart::getYouGetPoints()
     *
     * @return int
     */
    public function getYouGetPoints()
    {
        return $this->_belvgCart->getCalculateDataByName('points');
    }

    /**
     * Cart::getCustomerPoints()
     *
     * @return int
     */
    public function getCustomerPoints()
    {
        $result = 0;
        if (Context::getContext()->customer->isLogged()) {
            $_customer = new BelvgDiscountCustomer(Context::getContext()->customer->id);
            $result = $_customer->getPoints() - $this->getTotalPoints();
        }

        return $result;
    }

    /**
     * Cart::getTotalPoints()
     *
     * @return int
     */
    public function getTotalPoints()
    {
        return $this->_belvgCart->getCalculateDataByName('qty_by_points');
    }

    /**
     * Cart::getTotalDiscount()
     *
     * @return float
     */
    public function getTotalDiscount($products = NULL)
    {
        if (is_null($products)) {
            return $this->_belvgCart->getCalculateDataByName('discount');
        }

        $discount = 0;
        $_products = $this->_belvgCart->getCalculateDataByName('products');
        foreach ($products as $product) {
            $customId = $product['id_product'] . '_' . $product['id_product_attribute'];
            if (array_key_exists($customId, $_products)) {
                $discount += $_products[$customId]['discount'];
            }
        }

        return $discount;
    }

    /**
     * Cart::getProductData()
     *
     * @param mixed $productId
     * @return array
     */
    public function getProductData($productId)
    {
        $_product = new BelvgDiscountProduct($productId, $this->id_shop);
        return $_product->getData();
    }

    /**
     * Cart::getProductBuyPoints()
     *
     * @param mixed $productId
     * @return int
     */
    public function getProductBuyPoints($productId)
    {
        $data = $this->getProductData($productId);
        return $data['buy_points'];
    }

    /**
     * Cart::getProductPoints()
     *
     * @param mixed $customId
     * @return int
     */
    public function getProductPoints($customId)
    {
        $_products = $this->_belvgCart->getCalculateDataByName('products');
        if (array_key_exists($customId, $_products)) {
            return $_products[$customId]['points'];
        }

        $productId = explode('_', $customId);
        $data = $this->getProductData($productId[0]);
        $products = $this->getProducts(FALSE, $productId[0]);
        return $data['points'] * $products[0]['quantity'];
    }

    /**
     * Cart::getProductDiscount()
     *
     * @param mixed $customId
     * @return float
     */
    public function getProductDiscount($customId)
    {
        $_products = $this->_belvgCart->getCalculateDataByName('products');
        if (array_key_exists($customId, $_products)) {
            return $_products[$customId]['discount'];
        }

        return 0;
    }

    /**
     * Cart::getOrderTotal()
     *
     * @param bool $with_taxes
     * @param mixed $type
     * @param mixed $_products
     * @param mixed $id_carrier
     * @param bool $use_cache
     * @return float
     */
    public function getOrderTotal($with_taxes = TRUE, $type = Cart::BOTH, $_products = NULL,
        $id_carrier = NULL, $use_cache = TRUE)
    {
        $orderTotal = parent::getOrderTotal($with_taxes, $type, $_products, $id_carrier,
            $use_cache);

        if (!Module::isEnabled('belvg_discounts')) {
            return $orderTotal;
        }

        $availableTypes = array(
            Cart::ONLY_PRODUCTS,
            Cart::BOTH,
            Cart::BOTH_WITHOUT_SHIPPING,
            Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING,
            Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING,
        );

        /*if (!in_array($type, $availableTypes)) {
            return $orderTotal;
        }*/

        $orderTotal -= $this->getTotalDiscount($_products);

        $belvgProducts = $this->_belvgCart->getCalculateDataByName('products', $_products);
        if (is_null($_products)) {
            $_products = $this->getProducts();
        }

        $discount = 0;
        foreach ($_products as $_product) {
            $customId = $_product['id_product'] . '_' . $_product['id_product_attribute'];
            if (array_key_exists($customId, $belvgProducts)) {
                $discount += (int)$belvgProducts[$customId]['qty_by_points'] * (float)$_product['price'];
            }
        }

        $orderTotal -= $discount;

        if ($orderTotal < 1) {
            return 0;
        }

        return $orderTotal;
    }

    /**
     * Cart::getProductQtyByPoints()
     *
     * @param mixed $customId
     * @return int
     */
    public function getProductQtyByPoints($customId)
    {
        $_products = $this->_belvgCart->getCalculateDataByName('products');
        if (array_key_exists($customId, $_products)) {
            return $_products[$customId]['qty_by_points'];
        }

        return 0;
    }

    /**
     * Cart::getProductQtyByMoney()
     *
     * @param mixed $customId
     * @return int
     */
    public function getProductQtyByMoney($customId)
    {
        $_products = $this->_belvgCart->getCalculateDataByName('products');
        if (array_key_exists($customId, $_products)) {
            return $_products[$customId]['qty_by_money'];
        }

        return 0;
    }
}
