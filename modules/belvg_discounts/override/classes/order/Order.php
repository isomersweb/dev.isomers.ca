<?php

/**
 * Order
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class Order extends OrderCore
{
    protected $_documents = NULL;

    /**
     * Order::getDocuments()
     * 
     * @return parent::getDocuments()
     */
    public function getDocuments()
    {
        if (is_NULL($this->_documents)) {
            $this->_documents = parent::getDocuments();
        }

        return $this->_documents;
    }
}
