<?php

/**
 * OrderDetailController
 * 
 * @package Presta15_Discounts
 * @author Dzianis Yurevich
 * @copyright 2013
 * @version 1.0.0
 * @access public
 */
class OrderDetailController extends OrderDetailControllerCore
{
    /**
     * OrderDetailController::getBelvgDiscountsTplDir()
     * 
     * @return string
     */
    public function getBelvgDiscountsTplDir()
    {
        return _PS_MODULE_DIR_ . 'belvg_discounts/tpl/front/';
    }

    /**
     * OrderDetailController::setTemplate()
     * 
     * @param mixed $template
     * @return parent::setTemplate($template)
     */
    public function setTemplate($template)
    {
        if (!Module::isEnabled('belvg_discounts')) {
            return parent::setTemplate($template);
        }

        if (substr($template, -16) == 'order-detail.tpl') {
            return parent::setTemplate($this->getBelvgDiscountsTplDir() . 'order-detail.tpl');
        }

        return parent::setTemplate($template);
    }
}
