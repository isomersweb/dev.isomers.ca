<?php

/*
$address = "105a Tycos Drive";
//$address = "105 Tycos Drive";
$street = "";
$str_num = "";

$result = array();
if(preg_match('/(^\d+)\s?(.+)/', $address, $result)){
    $street = $result[2];
    $str_num = $result[1];
} else if ( preg_match('/([^\d]+)\s?(.+)/i', $address, $result)) {
    $street = $result[1];
    $str_num = $result[2];    
} else { // no number found, it is only address
    $street = $address;
    $str_num = 1;
}
echo "<pre>";
print_r($result);
echo "<br>$street # $str_num";
die;
*/
p($_SESSION);
session_start();
if ($_SESSION['payment']['payment_method'] != 'cc_payment') return false;
$backend = false;
if (strpos(Tools::getValue('payment_method'), 'Moneris') !== false) {
    $backend = true;
}
include_once(dirname(__FILE__). '/../../config/config.inc.php');
include_once(dirname(__FILE__). '/../../init.php');
include_once(dirname(__FILE__). '/monerishosted.php');    

$monerishosted = new MonerisHosted();

$cart = Context::getContext()->cart;

/* Create random transaction id with store abbreviation prefix */

$store_abbr = strtolower(substr(Configuration::get('PS_SHOP_NAME'), 0, 3)) . "-";
$store_abbr = str_replace(" ", "", $store_abbr);
$summary = $cart->getSummaryDetails();
if (strlen($store_abbr) < 3) {
	$store_abbr = "ord-";
}
$order_id = $store_abbr . $cart->id . "-" .substr(number_format(time() * rand(),0,'',''),0,10);

if (!Validate::isLoadedObject($cart))
{
	Logger::addLog('Cart loading failed for cart '.(int)$order_id, 4);
	die('Fatal error with the cart '.(int)$order_id);
}

//require_once "./mpgClasses.php";
include_once(dirname(__FILE__). '/mpgClasses.php');

/************************ Request Variables ***************************/

$store_id=Configuration::get('MH_STORE_ID');
$api_token=Configuration::get('MH_HPP_KEY');


/********************* Transactional Variables ************************/
$type='purchase';
//$order_id='ord-'.date("dmy-G:i:s");
//$cust_id='my cust id';
$cust_id= '';
if ($backend) {
    $amount = number_format((float)Tools::getValue('payment_amount'), 2, '.', '');
}
else {
    $amount=number_format((float)$cart->getOrderTotal(true, 3), 2, '.', '');    
}
//$amount = '10.21';
$pan=$_SESSION['payment']['cc_num'];
$holder=$_SESSION['payment']['cc_owner'];		
$expiry_date=$_SESSION['payment']['cc_exp'];		
//$pan='4242424242424242'; 
//$expiry_date='1111';
$crypt='7';

/******************* Customer Information Variables ********************/

$first_name = $summary['invoice']->firstname;
$last_name = $summary['invoice']->lastname;
$company_name = $summary['invoice']->company;
$address = trim($summary['invoice']->address1);
$city = $summary['invoice']->city;
$province = $summary['invoice_state'];
$postal_code = $summary['invoice']->postcode;
$country = $summary['invoice']->country;
$phone_number = $summary['invoice']->phone;
$fax = '';
$tax1 = $summary['total_tax'];
$tax2 = '';
$tax3 = '';
$shipping_cost = $summary['total_shipping'];
$email = $customer->email;
$instructions =$summary->message;

$street = trim($summary['invoice']->address1);
$str_num = trim($summary['invoice']->address2);



/*
$result = array();
if(preg_match('/(^\d+)\s?(.+)/', $address, $result)){
    $street = $result[2];
    $str_num = $result[1];
} else if ( preg_match('/([^\d]+)\s?(.+)/i', $address, $result)) {
    $street = $result[1];
    $str_num = $result[2];    
} else { // no number found, it is only address
    $street = $address;
    $str_num = 1;
}
*/

$inv_first_name = $summary['invoice']->firstname;
$inv_last_name = $summary['invoice']->lastname;
$inv_company_name = $summary['invoice']->company;
$inv_address = $summary['invoice']->address1;
$inv_city = $summary['invoice']->city;
$inv_province = $summary['invoice_state'];
$inv_postal_code = $summary['invoice']->postcode;
$inv_country = $summary['invoice']->country;
$inv_phone_number = $summary['invoice']->phone;

/******************** Customer Information Object *********************/

$mpgCustInfo = new mpgCustInfo();

/********************** Set Customer Information **********************/

$billing = array(
				 'first_name' => $inv_first_name,
                 'last_name' => $inv_last_name,
                 'company_name' => $inv_company_name,
                 'address' => $inv_address,
                 'city' => $inv_city,
                 'province' => $inv_province,
                 'postal_code' => $inv_postal_code,
                 'country' => $inv_country,
                 'phone_number' => $inv_phone_number,
                 'fax' => $fax,
                 'tax1' => $tax1,
                 'tax2' => $tax2,
                 'tax3' => $tax3,
                 'shipping_cost' => $shipping_cost
                 );

$mpgCustInfo->setBilling($billing);

$shipping = array(
				 'first_name' => $first_name,
                 'last_name' => $last_name,
                 'company_name' => $company_name,
                 'address' => $address,
                 'city' => $city,
                 'province' => $province,
                 'postal_code' => $postal_code,
                 'country' => $country,
                 'phone_number' => $phone_number,
                 'fax' => $fax,
                 'tax1' => $tax1,
                 'tax2' => $tax2,
                 'tax3' => $tax3,
                 'shipping_cost' => $shipping_cost
                 );

$mpgCustInfo->setShipping($shipping);

$mpgCustInfo->setEmail($email);
//$mpgCustInfo->setInstructions($instructions);

/*********************** Set Line Item Information *********************/

$i = 0;
foreach ($summary['products'] as $product) {
    $item[$i] = array(
	   'name'=>$product['name'],
       'quantity'=>$product['cart_quantity'],
       'product_code'=>$product['reference'],
       'extended_amount'=>$product['total_wt']
       );
    $mpgCustInfo->setItems($item[$i]);    
    if (!empty($product['recurring']) && $product['recurring'] > 0) {
        $recurUnit = 'day'; 
        $startDate = date('Y/m/d'); 
        $numRecurs = '99'; 
        $recurInterval = $product['recurring']; 
        $recurAmount = $product['total_wt']; 
        $startNow = 'false'; 
        
        $recurArray[$i] = array('recur_unit'=>$recurUnit, // (day | week | month) 
             'start_date'=>$startDate, //yyyy/mm/dd 
             'num_recurs'=>$numRecurs, 
             'start_now'=>$startNow, 
             'period' => $recurInterval, 
             'recur_amount'=> $recurAmount 
             ); 
    }
    $i++;    
}

/***************** Transactional Associative Array ********************/

$txnArray=array(
    'type'=>$type,
    'order_id'=>$order_id,
    'cust_id'=>$cust_id,
    'amount'=>$amount,
    'pan'=>$pan,
    'expdate'=>$expiry_date,
    'crypt_type'=>$cryаpt
);

/************************** AVS Variables *****************************/

$avs_street_number = $str_num;
$avs_street_name = $street;
$avs_zipcode = $inv_postal_code;
$avs_email = $email;
$avs_hostname = $_SERVER['SERVER_NAME'];
$avs_browser = $_SERVER['HTTP_USER_AGENT'];
$avs_shiptocountry = $country;
$avs_merchprodsku = '123456';
$avs_custip = $_SERVER['REMOTE_ADDR'];
$avs_custphone = $inv_phone_number;

/************************** CVD Variables *****************************/

$cvd_indicator = '1';
$cvd_value = $_SESSION['payment']['cc_cvd'];

/********************** AVS Associative Array *************************/

$avsTemplate = array(
	'avs_street_number'=>$avs_street_number,
     'avs_street_name' =>$avs_street_name,
     'avs_zipcode' => $avs_zipcode,
     'avs_hostname'=>$avs_hostname,
	 'avs_browser' =>$avs_browser,
	 'avs_shiptocountry' => $avs_shiptocountry,
	 'avs_merchprodsku' => $avs_merchprodsku,
	 'avs_custip'=>$avs_custip,
	 'avs_custphone' => $avs_custphone
    );
/********************** CVD Associative Array *************************/

$cvdTemplate = array(
 'cvd_indicator' => $cvd_indicator,
 'cvd_value' => $cvd_value
);

//p($avsTemplate);
//d($cvdTemplate);

/************************** AVS Object ********************************/
if (Configuration::get('MH_AVS') == 1) {
    $mpgAvsInfo = new mpgAvsInfo ($avsTemplate);   
}

/************************** CVD Object ********************************/

if (Configuration::get('MH_CVD') == 1) {
    $mpgCvdInfo = new mpgCvdInfo ($cvdTemplate); 
}
/********************** Transaction Object ****************************/

$mpgTxn = new mpgTransaction($txnArray);

/******************** Set Customer Information ************************/

$mpgTxn->setCustInfo($mpgCustInfo);

/************************* Request Object *****************************/

$mpgTxn->setAvsInfo($mpgAvsInfo); 
$mpgTxn->setCvdInfo($mpgCvdInfo); 

$mpgRequest = new mpgRequest($mpgTxn);

/************************ HTTPS Post Object ***************************/

$mpgHttpPost  =new mpgHttpsPost($store_id,$api_token,$mpgRequest);


if (isset($recurArray) && !empty($recurArray)) {
    foreach ($recurArray as $val) {
        $mpgRecur = new mpgRecur($val); 
        $mpgTxn->setRecur($val);       
    }
}


/****************8********** Response *********************************/

//unset($_SESSION['payment']);
$mpgResponse=$mpgHttpPost->getMpgResponse();
$response_code = $mpgResponse->getResponseCode();

/*
echo '<pre>';
print("\nCardType = " . $mpgResponse->getCardType());
print("\nTransAmount = " . $mpgResponse->getTransAmount());
print("\nTxnNumber = " . $mpgResponse->getTxnNumber());
print("\nReceiptId = " . $mpgResponse->getReceiptId());
print("\nTransType = " . $mpgResponse->getTransType());
print("\nReferenceNum = " . $mpgResponse->getReferenceNum());
print("\nResponseCode = " . $mpgResponse->getResponseCode());
print("\nISO = " . $mpgResponse->getISO());
print("\nMessage = " . $mpgResponse->getMessage());
print("\nIsVisaDebit = " . $mpgResponse->getIsVisaDebit());
print("\nAuthCode = " . $mpgResponse->getAuthCode());
print("\nComplete = " . $mpgResponse->getComplete());
print("\nTransDate = " . $mpgResponse->getTransDate());
print("\nTransTime = " . $mpgResponse->getTransTime());
print("\nTicket = " . $mpgResponse->getTicket());
print("\nTimedOut = " . $mpgResponse->getTimedOut());
print("\nAVSResponse = " . $mpgResponse->getAvsResultCode());
print("\nCVDResponse = " . $mpgResponse->getCvdResultCode());
print("\nITDResponse = " . $mpgResponse->getITDResponse());
echo '</pre>';
        echo "$message <pre>";
        print_r($avsTemplate);
        print_r($cvdTemplate);
        echo "ResponseCode: $response_code<br>";
        echo 'AVS error: '.mpgResponse::ERROR_AVS.'<br>';
        echo 'CVD error: '.mpgResponse::ERROR_CVD.'<br>';
        p($_SESSION);
        die;
*/
$message = '';
//$message = "ERROR: $response_code, ";
//$message .= $mpgResponse->getMessage();

$cardType =$mpgResponse->getCardType();
$transaction_id = $mpgResponse->getReferenceNum();
if ($cardType == 'V') $cardType = 'VISA';
elseif ($cardType == 'M') $cardType = 'MasterCard';
elseif ($cardType == 'D') $cardType = 'Discover';
elseif ($cardType == 'A') $cardType = 'American Express';

//p($mpgResponse->getMessage());
//d($response_code);

if (isset($response_code)) {
	if ($response_code < 50 && (int) $response_code != 0)
	{
/*
$AvsResultCode = $mpgResponse->getAvsResultCode();
if (!empty($AvsResultCode )) {
    $message .= "<br>REASON: AVS Code $AvsResultCode<br>";    
}
$CvdResultCode = $mpgResponse->getCvdResultCode();
if (!empty($CvdResultCode )) {
    $message .= "<br>REASON: CVD Code $CvdResultCode<br>";    
}

	   if ($cardType == 'V') {
	       $avs_status = array("D", "F", "M", "Y", "P", "Z");
	       if (!in_array($AvsResultCode, $avs_status)) {
	           
	       }
	   }
*/
	   if (!$backend) {
            if (Configuration::get('MH_CVD') == 1) {
                $CvdResultCode = $mpgResponse->getCvdResultCode();
                if ($CvdResultCode != "1M" ) {
                        checkStatus($message, $mpgResponse);  
                }
            }
    //p($mpgResponse->_successful_cvd_codes);        
    //d($CvdResultCode);        
            if (Configuration::get('MH_AVS') == 1) {
               $AvsResultCode = $mpgResponse->getAvsResultCode();
               $visa_status = array("D", "F", "G", "M", "Y", "P", "Z", "U");
               $mc_status = array("X", "T", "W", "Z", "S", "U");
               $amex_status = array("M", "W", "L", "Z");
               $CType =$mpgResponse->getCardType();
               if ((!in_array($AvsResultCode, $visa_status) && $CType == 'V') 
                || (!in_array($AvsResultCode, $mc_status) && $CType == 'M')  
                || (!in_array($AvsResultCode, $amex_status) && $CType == 'A')
                || $AvsResultCode == '') {
                    checkStatus($message, $mpgResponse);            
               }
            }  
/*
//checkStatus($message, $mpgResponse);  
p($CvdResultCode);
p($AvsResultCode);  
echo 'CC num: '. $pan.'<br>';
echo 'AVS ZIP error: '.mpgResponse::ERROR_AVS_POSTAL_CODE.'<br>';      
echo 'AVS ADDRESS error: '.mpgResponse::ERROR_AVS_ADDRESS.'<br>';      
echo 'AVS error: '.mpgResponse::ERROR_AVS.'<br>';
echo 'CVD error: '.mpgResponse::ERROR_CVD.'<br>';
d('ERROR');
*/            
    	   $extra_fields = array(
                'transaction_id' => $transaction_id, 
                'card_brand' => $cardType,
                'card_number' => substr($pan, 0, 4). '  **** **** ' . substr($pan, -4),
                'card_holder' => $holder,
            );
    		$monerishosted->validateOrder((int)$cart->id, Configuration::get('PS_OS_PAYMENT'), $amount, $monerishosted->displayName, $instructions, $extra_fields, NULL, false,	$cart->secure_key);
      }
      else {
        if ($order_has_invoice) {
            $order_invoice = new OrderInvoice(Tools::getValue('payment_invoice'));
        } else {
            $order_invoice = null;
        }
        $currency = new Currency((int)$cart->id_currency);        
        if (!$order->addOrderPayment($amount, Tools::getValue('payment_method'), $transaction_id, $currency, Tools::getValue('payment_date'), $order_invoice)) {
            $this->errors[] = Tools::displayError('An error occurred during payment.');
            return false;
        }               
      }
        
        $sql = '
			UPDATE `'._DB_PREFIX_.'order_payment`
			SET `card_brand` = \''.$cardType.'\',
			`card_number` = \''.substr($pan, 0, 4). '  **** **** ' . substr($pan, -4).'\',
			`card_holder` = \''.$holder.'\' 
			`card_expiration` = \''.$expiry_date.'\' 
			WHERE `transaction_id` = \''.$transaction_id.'\' LIMIT 1';
        Db::getInstance()->execute($sql);

        $order_id2 = Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'orders WHERE id_cart = '.(int)$cart->id);
        $details = array();
        $currency = new Currency((int)$cart->id_currency);        
        $details['amount'] = $amount;
		$details['id_transaction'] = $transaction_id;
		$details['tnx_number'] = $mpgResponse->getTxnNumber();
		$details['moneris_order'] = $order_id;
		$details['fee'] = 0;        
        $details['id_customer'] = $customer->id;
		$details['date_add'] = date('Y-m-d H:i:s');
		$details['source'] = '';
		$details['currency'] = $currency->iso_code;
        $details['cc_exp'] = $expiry_date;
        $details['cc_type'] = $cardType;
        $details['cc_last_digits'] = substr($pan, -4);
		$details['id_shop'] = (int)$cart->id_shop;
        $details['id_order'] = $order_id2;
        $details['id_cart'] = $cart->id;
        $monerishosted->addTransaction('payment', $details);
        
        if ($backend) {
            Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=4&token='.$this->token);
            return true;
        }
        
        if (Configuration::get('MH_AVS') == 1) {
            $ERROR_AVS = '';
            $ERROR_AVS = mpgResponse::ERROR_AVS;
            if (isset($ERROR_AVS) & !empty($ERROR_AVS)) {
//                $ERROR_AVS = getError($mpgResponse);
                $ERROR_AVS = "\n AVS ERROR: $ERROR_AVS";           
                $old_message = Message::getMessageByCartId((int)$cart->id);
                if ($old_message) {
                    $update_message = new Message((int)$old_message['id_message']);
                    $update_message->message .= $ERROR_AVS;
                    $update_message->update();
                }
                else {
                    $msg = new Message();
                    $msg->message = $ERROR_AVS;
                    $msg->id_cart = $cart->id;
                    $msg->id_customer = $customer->id;
                    $msg->id_order = $order_id2;
                    $msg->private = 1;
                    $msg->add();
                }
            }
        }
        

        foreach ($summary['products'] as $product) {
            if (!empty($product['recurring']) && $product['recurring'] > 0) {
                $sql = '
            		UPDATE `'._DB_PREFIX_.'order_detail`
            		SET `recurring` = \''.$product['recurring'].'\' 
            		WHERE `product_id` = '.$product['id_product'].' AND `id_order` = '.$order_id2.' LIMIT 1';
                Db::getInstance()->execute($sql);
            }
        }
        
		$url = 'index.php?controller=order-confirmation&';
		if (_PS_VERSION_ < '1.5')
			$url = 'order-confirmation.php?';

		Tools::redirect($url.'id_module='.(int)$monerishosted->id.'&id_cart='.(int)$cart->id.'&key='.$customer->secure_key);
	}

	if ($response_code >= 50 || (int) $response_code == 0)
	{
    	switch ($response_code) {
    	    case '100':$message .= ' '.'Unable to process transaction';break;
            case '101':
            case '102':
                $message .= ' '.'Place call';break;
            case '103':$message .= ' '.'NEG file problem';break;
            case '104':$message .= ' '.'CAF problem';break;
            case '105':$message .= ' '.'Card not supported';break;
            case '106':$message .= ' '.'Amount over maximum';break;
            case '107':$message .= ' '.'Over daily limit';break;
            case '108':$message .= ' '.'CAF Problem';break;
            case '109':$message .= ' '.'Advance less than minimum';break;
            case '110':$message .= ' '.'Number of times used exceeded';break;
            case '111':$message .= ' '.'Delinquent';break;
            case '112':$message .= ' '.'Over table limit';break;
            case '113':$message .= ' '.'Timeout';break;
            case '115':$message .= ' '.'PTLF error';break;
            case '121':$message .= ' '.'Administration file problem';break;
            case '122':$message .= ' '.'Unable to validate PIN: security module down';break;
            case '150':$message .= ' '.'Merchant not on file';break;
            case '200':$message .= ' '.'Invalid account';break;
            case '201':$message .= ' '.'Incorrect PIN';break;
            case '202':$message .= ' '.'Advance less than minimum';break;
            case '203':$message .= ' '.'Administrative card needed';break;
            case '204':$message .= ' '.'Amount over maximum';break;
            case '205':$message .= ' '.'Invalid Advance amount';break;
            case '206':$message .= ' '.'CAF not found';break;
            case '207':$message .= ' '.'Invalid transaction date';break;
            case '208':$message .= ' '.'Invalid expiration date';break;
            case '209':$message .= ' '.'Invalid transaction code';break;
            case '210':$message .= ' '.'PIN key sync error';break;
            case '212':$message .= ' '.'Destination not available';break;
            case '251':$message .= ' '.'Error on cash amount';break;
            case '252':$message .= ' '.'Debit not supported';break;
            case '426':$message .= ' '.'AMEX - Denial 12';break;
            case '427':$message .= ' '.'AMEX - Invalid merchant';break;
            case '429':$message .= ' '.'AMEX - Account error';break;
            case '430':$message .= ' '.'AMEX - Expired card';break;
            case '431':$message .= ' '.'AMEX - Call Amex';break;
            case '434':$message .= ' '.'AMEX - Call 03';break;
            case '435':$message .= ' '.'AMEX - System down';break;
            case '436':$message .= ' '.'AMEX - Call 05';break;
            case '437':$message .= ' '.'AMEX - Declined';break;
            case '438':$message .= ' '.'AMEX - Declined';break;
            case '439':$message .= ' '.'AMEX - Service error';break;
            case '440':$message .= ' '.'AMEX - Call Amex';break;
            case '441':$message .= ' '.'AMEX - Amount error';break;
            case '475':$message .= ' '.'CREDIT CARD - Invalid expiration date';break;
            case '476':$message .= ' '.'CREDIT CARD - Invalid transaction, rejected';break;
            case '477':$message .= ' '.'CREDIT CARD - Refer Call';break;
            case '478':$message .= ' '.'CREDIT CARD - Decline, Pick up card, Call';break;
            case '479':$message .= ' '.'CREDIT CARD - Decline, Pick up card';break;
            case '480':$message .= ' '.'CREDIT CARD - Decline, Pick up card';break;
            case '481':$message .= ' '.'CREDIT CARD - Decline';break;
            case '482':$message .= ' '.'CREDIT CARD - Expired Card';break;
            case '483':$message .= ' '.'CREDIT CARD - Refer';break;
            case '484':$message .= ' '.'CREDIT CARD - Expired card - refer';break;
            case '485':$message .= ' '.'CREDIT CARD - Not authorized';break;
            case '486':$message .= ' '.'CREDIT CARD - CVV Cryptographic error';break;
            case '487':$message .= ' '.'CREDIT CARD - Invalid CVV';break;
            case '489':$message .= ' '.'CREDIT CARD - Invalid CVV';break;
            case '490':$message .= ' '.'CREDIT CARD - Invalid CVV';break;
            case '50':$message .= ' '.'Decline';break;
            case '51':$message .= ' '.'Expired Card';break;
            case '52':$message .= ' '.'PIN retries exceeded';break;
            case '53':$message .= ' '.'No sharing';break;
            case '54':$message .= ' '.'No security module';break;
            case '55':$message .= ' '.'Invalid transaction';break;
            case '56':$message .= ' '.'No Support';break;
            case '57':$message .= ' '.'Lost or stolen card';break;
            case '58':$message .= ' '.'Invalid status';break;
            case '59':$message .= ' '.'Restricted Card';break;
            case '60':$message .= ' '.'No Chequing account';break;
            case '60':$message .= ' '.'No Savings account';break;
            case '61':$message .= ' '.'No PBF';break;
            case '62':$message .= ' '.'PBF update error';break;
            case '63':$message .= ' '.'Invalid authorization type';break;
            case '64':$message .= ' '.'Bad Track 2';break;
            case '65':$message .= ' '.'Adjustment not allowed';break;
            case '66':$message .= ' '.'Invalid credit card advance increment';break;
            case '67':$message .= ' '.'Invalid transaction date';break;
            case '68':$message .= ' '.'PTLF error';break;
            case '69':$message .= ' '.'Bad message error';break;
            case '70':$message .= ' '.'No IDF';break;
            case '71':$message .= ' '.'Invalid route authorization';break;
            case '72':$message .= ' '.'Card on National NEG file';break;
            case '73':$message .= ' '.'Invalid route service (destination)';break;
            case '74':$message .= ' '.'Unable to authorize';break;
            case '75':$message .= ' '.'Invalid PAN length';break;
            case '76':$message .= ' '.'Low funds';break;
            case '77':$message .= ' '.'Pre-auth full';break;
            case '78':$message .= ' '.'Duplicate transaction';break;
            case '79':$message .= ' '.'Maximum online refund reached';break;
            case '80':$message .= ' '.'Maximum offline refund reached';break;
            case '800':$message .= ' '.'Bad format';break;
            case '801':$message .= ' '.'Bad data';break;
            case '802':$message .= ' '.'Invalid Clerk ID';break;
            case '809':$message .= ' '.'Bad close';break;
            case '81':$message .= ' '.'Maximum credit per refund reached';break;
            case '810':$message .= ' '.'System timeout';break;
            case '811':$message .= ' '.'System error';break;
            case '82':$message .= ' '.'Number of times used exceeded';break;
            case '821':$message .= ' '.'Bad response length';break;
            case '83':$message .= ' '.'Maximum refund credit reached';break;
            case '84':$message .= ' '.'Duplicate transaction - authorization number has already been corrected by host.';break;
            case '85':$message .= ' '.'Inquiry not allowed';break;
            case '86':$message .= ' '.'Over floor limit';break;
            case '87':$message .= ' '.'Maximum number of refund credit by retailer';break;
            case '877':$message .= ' '.'Invalid PIN block';break;
            case '878':$message .= ' '.'PIN length error';break;
            case '88':$message .= ' '.'Place call';break;
            case '880':$message .= ' '.'Final packet of a multi-packet transaction';break;
            case '881':$message .= ' '.'Intermediate packet of a multi-packet transaction';break;
            case '889':$message .= ' '.'MAC key sync error';break;
            case '89':$message .= ' '.'CAF status inactive or closed';break;
            case '898':$message .= ' '.'Bad MAC value';break;
            case '899':$message .= ' '.'Bad sequence number - resend transaction';break;
            case '90':$message .= ' '.'Referral file full';break;
            case '900':$message .= ' '.'Capture - PIN Tries Exceeded';break;
            case '901':$message .= ' '.'Capture - Expired Card';break;
            case '902':$message .= ' '.'Capture - NEG Capture';break;
            case '903':$message .= ' '.'Capture - CAF Status 3';break;
            case '904':$message .= ' '.'Capture - Advance < Minimum';break;
            case '905':$message .= ' '.'Capture - Num Times Used';break;
            case '906':$message .= ' '.'Capture - Delinquent';break;
            case '907':$message .= ' '.'Capture - Over Limit Table';break;
            case '908':$message .= ' '.'Capture - Amount Over Maximum';break;
            case '909':$message .= ' '.'Capture - Capture';break;
            case '91':$message .= ' '.'NEG file problem';break;
            case '92':$message .= ' '.'Advance less than minimum';break;
            case '93':$message .= ' '.'Delinquent';break;
            case '94':$message .= ' '.'Over table limit';break;
            case '95':$message .= ' '.'Amount over maximum';break;
            case '96':$message .= ' '.'PIN required';break;
            case '97':$message .= ' '.'Mod 10 check failure';break;
            case '98':$message .= ' '.'Force Post';break;
            case '99':$message .= ' '.'Bad PBF';break;
    
    		default:
    			$message .= ' '.mpgResponse::ERROR;
    	}
        if ($backend == 1) {
            $this->errors[] = $message;
            return false;
         
        }return false;
        
        checkStatus($message, $mpgResponse);

	}
}
return;

function checkStatus($message = '', $mpgResponse) {
//    $cvd_default_error_msg = mpgResponse::ERROR_CVD;
    $avs_default_error_msg = $cvd_default_error_msg = 'We were unable to process your credit card payment.  Please verify your card details and billing address and try again. If the problem persists, contact us to complete your order.';    
    if (Configuration::get('MH_AVS') == 1) {
        $AvsResultCode = $mpgResponse->getAvsResultCode();        
        if (!in_array($AvsResultCode, $mpgResponse->_successful_avs_codes)) {
//            $message .= getError($mpgResponse, $avs_default_error_msg);   
//            $message .= "<br>REASON: $avs_default_error_msg";     
        }
        $message = $avs_default_error_msg;
    }
    if (Configuration::get('MH_CVD') == 1) {
        $CvdResultCode = $mpgResponse->getCvdResultCode();
         if ($CvdResultCode != "1M" ) {
//                $message .= '<br>REASON: CVV Code '. $cvd_default_error_msg;
        }
        $message = $avs_default_error_msg;
    }
    $error_message = urlencode($message);
	$checkout_type = Configuration::get('PS_ORDER_PROCESS_TYPE') ?
		'order-opc' : 'order';
	$url = _PS_VERSION_ >= '1.5' ?
		'index.php?controller='.$checkout_type.'&' : $checkout_type.'.php?';
	$url .= 'step=4&cgv=1&monerror=1&message='.$error_message;
        
	if (!isset($_SERVER['HTTP_REFERER']) || strstr($_SERVER['HTTP_REFERER'], 'order'))
		Tools::redirect($url);
	else if (strstr($_SERVER['HTTP_REFERER'], '?'))
		Tools::redirect($_SERVER['HTTP_REFERER'].'&monerror=1&message='.$error_message, '');
	else
		Tools::redirect($_SERVER['HTTP_REFERER'].'?monerror=1&message='.$error_message, '');

	exit;
}
function getError($mpgResponse, $avs_default_error_msg = '') {
    $message = '';
    switch ($AvsResultCode) {
		case 'B':
		case 'C':
			$message .= '<br>REASON: AVS Code '.mpgResponse::ERROR_AVS_POSTAL_CODE;
			break;
		case 'G':
		case 'I':
		case 'P':
		case 'S':
		case 'U':
		case 'Z':
			$message .= '<br>REASON: AVS Code '.mpgResponse::ERROR_AVS_ADDRESS;
			break;
		case 'N':
			$message .= '<br>REASON: AVS Code '.mpgResponse::ERROR_AVS_NO_MATCH;
			break;
		case 'R':
			$message .= '<br>REASON: AVS Code '.mpgResponse::ERROR_AVS_TIMEOUT;
			break;
		default:
//			$message .= '<br>REASON: AVS Code '.mpgResponse::ERROR_AVS;
            $message .= $avs_default_error_msg;
	}
    return $message;
}
?>

