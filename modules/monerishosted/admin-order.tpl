<div class="clearfix"></div>
{foreach from=$moneris_transaction_details item=moneris_transaction}
<div class="row">
    <fieldset>
        <legend><img src="{$module_dir}logo.gif" alt="" /> {l s='Moneris transaction details' mod='monerishosted'}</legend>
    	<table cellpadding="0" cellspacing="0" class="table">
    		<tr>
    			<td>{l s='Transaction ID' mod='monerishosted'}</td>
    			<td>{$moneris_transaction.id_transaction|escape:'htmlall':'UTF-8'}</td>
    		</tr>
    		<tr>
    			<td>{l s='Order ID' mod='monerishosted'}</td>
    			<td>{$moneris_transaction.moneris_order|escape:'htmlall':'UTF-8'}</td>
    		</tr>
    		<tr>
    			<td>{l s='Amount charged' mod='monerishosted'}</td>
    			<td>{$moneris_transaction.amount|escape:'htmlall':'UTF-8'} {$moneris_transaction.currency|escape:'htmlall':'UTF-8'}</td>
    		</tr>
    		<tr>
    			<td>{l s='Mode' mod='monerishosted'}</td>
    			<td>{if $moneris_transaction.mode == 'Production'}<span style="color: #CC0000;">{l s='Live' mod='monerishosted'}</span>{else}{l s='Test' mod='monerishosted'}{/if}</td>
    		</tr>
    		<tr>
    			<td>{l s='Date' mod='monerishosted'}</td>
    			<td>{$moneris_transaction.date_add|escape:'htmlall':'UTF-8'}</td>
    		</tr>
    		{if isset($moneris_transaction.cc_type) && $moneris_transaction.cc_type != ''}
    		<tr>
    			<td>{l s='Credit card type' mod='monerishosted'}</td>
    			<td>{$moneris_transaction.cc_type|escape:'htmlall':'UTF-8'}</td>
    		</tr>
    		{/if}
    		{if isset($moneris_transaction.cc_exp) && $moneris_transaction.cc_exp != ''}
    		<tr>
    			<td>{l s='Credit expiration date' mod='monerishosted'}</td>
    			<td>{$moneris_transaction.cc_exp|escape:'htmlall':'UTF-8'}</td>
    		</tr>
    		{/if}
    		{if isset($moneris_transaction.cc_last_digits) && $moneris_transaction.cc_last_digits != ''}
    		<tr>
    			<td>{l s='Credit card last 4 digits' mod='monerishosted'}</td>
    			<td>{$moneris_transaction.cc_last_digits|escape:'htmlall':'UTF-8'}</td>
    		</tr>
    		{/if}		
    	</table>
    </fieldset>
    
    <br />
    <fieldset>
    	<h3><img src="{$module_dir}logo.gif" alt="" /> {l s='Proceed to a full or partial refund via Moneris' mod='monerishosted'}</h3>
        {if $moneris_transaction.id_transaction == $moneris_refund_id_transaction}
        	{if isset($moneris_refund) && $moneris_refund}
        		<div class="conf alert alert-success">{l s='Refund successfully performed' mod='monerishosted'}</div><br />
        	{else}
        		{if isset($moneris_refund) && !$moneris_refund}
        		<div class="error alert alert-danger">{l s='An error occured during this refund' mod='monerishosted'}{if isset($moneris_refund_error) && $moneris_refund_error} - {$moneris_refund_error|escape:'htmlall':'UTF-8'}{/if}</div><br />
        		{/if}
        	{/if}
         {/if}
    	{if $moneris_more60d}
    		<div class="info">{l s='This order has been placed more than 60 days ago or no transaction details are available. Therefore, it cannot be refunded anymore.' mod='monerishosted'}</div>
    	{/if}
    	<table class="table" cellpadding="0" cellspacing="0">
    		<tr>
    			<th>{l s='Date' mod='monerishosted'}</th>
    			<th>{l s='Order ID/RAN' mod='monerishosted'}</th>
    			<th>{l s='Amount refunded' mod='monerishosted'}</th>
    		</tr>
    		{assign var=total_refund value=0}
    		{foreach from=$moneris_refund_details item=refund_transaction}
                {if $refund_transaction.moneris_order == $moneris_transaction.moneris_order}
            		<tr>
            			<td>{$refund_transaction.date_add|escape:'htmlall':'UTF-8'} </td>
            			<td>{$refund_transaction.moneris_order|escape:'htmlall':'UTF-8'} </td>
            			<td>{$refund_transaction.amount|escape:'htmlall':'UTF-8'} {$refund_transaction.currency|escape:'htmlall':'UTF-8'} </td>
            		</tr>
            		{assign var=total_refund value = $total_refund + $refund_transaction.amount}
                {/if}
    		{/foreach}
    		<tr>
    			<td>{l s='Total refunded:' mod='monerishosted'}</td>
    			<td colspan="2">{$total_refund|escape:'htmlall':'UTF-8'} {$refund_transaction.currency|escape:'htmlall':'UTF-8'} </td>
    		</tr>
    	</table>
    	<br />
    	{if $moneris_transaction.amount == $total_refund && $total_refund}
    		{l s='This order has been fully refunded.' mod='monerishosted'}
    	{else}
    		<form method="post" action="" name="refund">
    			{l s='Refund:' mod='monerishosted'} <input type="text" name="refund_amount" value="{($moneris_transaction.amount-$total_refund)|floatval}" />
    			<input type="hidden" name="id_transaction" value="{$moneris_transaction.id_transaction|escape:'htmlall':'UTF-8'}" />
     			<input type="hidden" name="moneris_order" value="{$moneris_transaction.moneris_order|escape:'htmlall':'UTF-8'}" />
   			<input type="submit" name="process_refund" value ="{l s='Process Refund' mod='monerishosted'}" class="button" />
    		</form>
    	{/if}
    </fieldset>
</div>
{/foreach}
 <br /> <br />