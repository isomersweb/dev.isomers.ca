<?php
define('PRESTASHOP_INTEGRATION_VERSION', true);
define ('PS_DIR', __DIR__ . '/../');
//define('_PS_MODE_DEV_', true);
require_once PS_DIR .'/config/config.inc.php';
require_once PS_DIR .'/modules/orderduplicate/orderduplicate.php';

if (!defined('_PS_VERSION_'))
	exit;
    
$orders = Db::getInstance()->ExecuteS('SELECT o.*, c.firstname, c.lastname, c.email, od.recurring, od.recurring_date
    FROM `'._DB_PREFIX_.'orders` o
    LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.`id_order` = od.`id_order`
    LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.`id_customer` = o.`id_customer`
    WHERE od.`recurring` > 0
    AND od.recurring_date + INTERVAL od.recurring DAY < CURRENT_DATE
    AND DATE_ADD(o.date_add, INTERVAL 1 YEAR) > CURRENT_DATE
    Order by id_order DESC');
//d($orders);
$context = Context::getContext();
$context->currency = new Currency((int)$context->cookie->id_currency);
$context->language = new Language((int)$context->cookie->id_lang);
//d($context);   

/*
$neworder = new Order(5826);
$order = new Order(5826);
if ($id_payment_method != 0) {
    $payment_module = Module::getInstanceById((int)$id_payment_method);    
} else {
    $payment_module = Module::getInstanceByName($order->module);
}
addPayment($payment_module, $neworder, $order, $context);
d();
*/

foreach ($orders AS $order) {
    p($order['id_order']);
file_put_contents("./recurring.log", date('Y-m-d H:i:s') . "\nORDER ID: {$order['id_order']} \n", FILE_APPEND);
    $customer = new Customer($order['id_customer']);
    $context->customer = $customer;
    $context->cookie->logged = 1;
    $context->customer->logged = 1;
    $neworder = cloneOrder($context, $order);
    if (Validate::isLoadedObject($neworder)) {
        $oldorder = new Order((int)$order['id_order']);
        $newodlist = $neworder->getOrderDetailList();
        $odlist = $oldorder->getOrderDetailList();
        foreach ($odlist As $val) {            
            foreach ($newodlist as $no_list) {
                if($no_list['product_id'] == $val['product_id']) {
                    $odl = new OrderDetail($val['id_order_detail']);
                    $odl->recurring_date = date('Y-m-d H:i:s');
file_put_contents("./recurring.log", "NEW ORDER ID: {$neworder->id} \n", FILE_APPEND);
file_put_contents("./recurring.log", "recurring_date updated \n\n", FILE_APPEND);
                    $odl->update();                    
                }
            }
        }
/*        $id_lang = (int)$order['id_lang'];
        $id_lang = $context->language->id;
        $subject = 'Recurring Order created';
        $mail = $order['email'];
        $name = $order['firstname'].' '.$order['lastname'];
        $template = 'recurring_create_order';        

        $invoice = new Address((int)$neworder->id_address_invoice);
        $delivery = new Address((int)$neworder->id_address_delivery);
        $delivery_state = $delivery->id_state ? new State((int)$delivery->id_state) : false;
        $invoice_state = $invoice->id_state ? new State((int)$invoice->id_state) : false;

        $product_var_tpl_list = array();
        $product_list_txt = '';
        $product_list_html = '';
        foreach ($order->product_list as $product) {
            $product_var_tpl_list = array(
                'reference' => $product['reference'],
                'name' => $product['name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : ''),
                'unit_price' => Tools::displayPrice($product_price, $context->currency, false),
                'price' => Tools::displayPrice($product_price * $product['quantity'], $context->currency, false),
                'quantity' => $product['quantity'],
                'customization' => array()
            );    
        }
        if (count($product_var_tpl_list) > 0) {
            $product_list_txt = getEmailTemplateContent('order_conf_product_list.txt', Mail::TYPE_TEXT, $product_var_tpl_list, $context);
            $product_list_html = getEmailTemplateContent('order_conf_product_list.tpl', Mail::TYPE_HTML, $product_var_tpl_list, $context);
        }
        
        $data = array(
            '{firstname}' => $context->customer->firstname,
            '{lastname}' => $context->customer->lastname,
            '{email}' => $context->customer->email,        
            '{delivery_block_txt}' => getFormatedAddress($delivery, "\n"),
            '{invoice_block_txt}' => getFormatedAddress($invoice, "\n"),
            '{delivery_block_html}' => getFormatedAddress($delivery, '<br />', array(
                'firstname'    => '<span style="font-weight:bold;">%s</span>',
                'lastname'    => '<span style="font-weight:bold;">%s</span>'
            )),
            '{invoice_block_html}' => getFormatedAddress($invoice, '<br />', array(
                    'firstname'    => '<span style="font-weight:bold;">%s</span>',
                    'lastname'    => '<span style="font-weight:bold;">%s</span>'
            )),
            '{delivery_company}' => $delivery->company,
            '{delivery_firstname}' => $delivery->firstname,
            '{delivery_lastname}' => $delivery->lastname,
            '{delivery_address1}' => $delivery->address1,
            '{delivery_address2}' => $delivery->address2,
            '{delivery_city}' => $delivery->city,
            '{delivery_postal_code}' => $delivery->postcode,
            '{delivery_country}' => $delivery->country,
            '{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
            '{delivery_phone}' => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
            '{delivery_other}' => $delivery->other,
            '{invoice_company}' => $invoice->company,
            '{invoice_vat_number}' => $invoice->vat_number,
            '{invoice_firstname}' => $invoice->firstname,
            '{invoice_lastname}' => $invoice->lastname,
            '{invoice_address2}' => $invoice->address2,
            '{invoice_address1}' => $invoice->address1,
            '{invoice_city}' => $invoice->city,
            '{invoice_postal_code}' => $invoice->postcode,
            '{invoice_country}' => $invoice->country,
            '{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
            '{invoice_phone}' => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
            '{invoice_other}' => $invoice->other,
            '{order_name}' => $neworder->getUniqReference(),
            '{date}' => Tools::displayDate(date('Y-m-d H:i:s'), null, 1),
            '{carrier}' => ($virtual_product || !isset($carrier->name)) ? Tools::displayError('No carrier') : $carrier->name,
            '{payment}' => Tools::substr($neworder->payment, 0, 32),
            '{products}' => $product_list_html,
            '{products_txt}' => $product_list_txt,
            '{discounts}' => $cart_rules_list_html,
            '{discounts_txt}' => $cart_rules_list_txt,
            '{total_paid}' => Tools::displayPrice($neworder->total_paid, $context->currency, false),
            '{total_products}' => Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $neworder->total_products : $neworder->total_products_wt, $context->currency, false),
            '{total_discounts}' => Tools::displayPrice($neworder->total_discounts, $context->currency, false),
            '{total_discounts_tax_excl}' => Tools::displayPrice($neworder->total_discounts_tax_excl, $context->currency, false),
            '{total_shipping}' => Tools::displayPrice($neworder->total_shipping, $context->currency, false),
            '{total_shipping_tax_excl}' => Tools::displayPrice($neworder->total_shipping_tax_excl, $context->currency, false),
            '{total_wrapping}' => Tools::displayPrice($neworder->total_wrapping, $context->currency, false),
            '{total_tax_paid}' => Tools::displayPrice(
            ($neworder->total_paid_tax_incl - $neworder->total_paid_tax_excl),
            $context->currency, false));
        sendMail($id_lang, $template, $subject, $data, $mail, $name);
*/        
    }
}

d('OK');

function cloneOrder($context, $order) {

    $id_customer = $order['id_customer'];
    $id_address_delivery = $order['id_address_delivery'];
    $id_address_invoice = $order['id_address_invoice'];
    $id_order = $order['id_order'];
    $id_order_state = 22;
    $id_payment_method = 0;
//    $id_order_state = $order['id_order_state'];

/*    
    if ($order['module'] == 'monerishosted') {
        session_start();
        $_SESSION['payment']['payment_method'] = 'cc_payment';
        $_SESSION['payment']['cc_num'] = "";
        $_SESSION['payment']['cc_owner'] = "";
        $_SESSION['payment']['cc_exp'] = "";
        $_SESSION['payment']['cc_cvd'] = "";
    //    include PS_DIR .'/modules/monerishosted/validation.php';        
        return;
    }
*/    
    $order = new Order((int)$id_order);
    if ($id_payment_method != 0) {
        $payment_module = Module::getInstanceById((int)$id_payment_method);    
    } else {
        $payment_module = Module::getInstanceByName($order->module);
    }

    if (Validate::isLoadedObject($order)) {
        if (Validate::isLoadedObject($payment_module)
            && $cart = cloneCart(
                $order->id_cart,
                $id_customer,
                $id_address_delivery,
                $id_address_invoice,
                $id_order                
            )
        ) {
file_put_contents("./recurring.log", "Cart cloned \n", FILE_APPEND);
            $context->cart = $cart;
            $total_cart = Cart::getTotalCart($cart->id, true, Cart::BOTH);
            try {
                $extra_vars = array();
                if ($payment_module->validateOrder(
                    $cart->id,
                    (int)$id_order_state,
                    $total_cart,
                    ((int)$id_payment_method != 0 ? $payment_module->displayName : $order->payment),
                    NULL, $extra_vars, NULL, false,	$cart->secure_key
                )) {
                    $neworder = new Order((int)$payment_module->currentOrder);
                    $neworder->reference = $neworder->id . '/' . $id_order;
                    $neworder->update();
                    
file_put_contents("./recurring.log", "New Order Payment module: {$neworder->module} \n", FILE_APPEND);
                    if ($neworder->module == 'monerishosted') {
file_put_contents("./recurring.log", "addPayment enter to the function\n", FILE_APPEND);                        
                        addPayment($payment_module, $neworder, $order, $context);
                    }
                    
file_put_contents("./recurring.log", "Hook actionOrderCloned started\n", FILE_APPEND);                        
                    Hook::exec('actionOrderCloned', array(
                        'order_old' => $order,
                        'order_new' => $neworder
                    ));
                    
                    

                    return $neworder;
                }
            } catch (PrestaShopException $e) { 
file_put_contents("./recurring.log", "Unable to clone this order: ".$e->getMessage()." \n", FILE_APPEND);
                $name = $order['firstname'].' '.$order['lastname'];
                $mail = Configuration::get('PS_SHOP_EMAIL', null, null, $order['id_shop']);
                $subject = 'Unable to clone this order #'. $order['id_order'];
                $template = 'recurring_error';
                $data = array(
                    'message' => 'Unable to clone this order'.': '.$e->getMessage()
                );
                sendMail($context->language->id, $template, $subject, $data, $mail, $name);
            }
        }
    }
}

function cloneCart($id_cart, $id_customer, $id_address_delivery, $id_address_invoice, $id_order)
{
    if (Validate::isLoadedObject($cart = new Cart((int)$id_cart))) {
        $cloned = $cart;

        $cloned->id = null;
        $cloned->id_customer = (int)$id_customer;
        $cloned->id_address_delivery = (int)$id_address_delivery;
        $cloned->id_address_invoice = (int)$id_address_invoice;

        $delivery_option = array();
        $delivery_option[$cloned->id_address_delivery] = $cloned->id_carrier.',';
        $cloned->delivery_option = serialize($delivery_option);

        if ($cloned->add()) {
            $new_cart_id = $cloned->id;

            $cart_products = Db::getInstance()->ExecuteS(
                'SELECT product_id AS id_product, product_quantity AS quantity, product_attribute_id AS id_product_attribute
                    FROM `'._DB_PREFIX_.'order_detail` 
                    WHERE id_order = \''.(int)$id_order .'\'
                    AND recurring > 0 
                    AND recurring_date + INTERVAL recurring DAY < CURRENT_DATE'
                    
            );
            if (count($cart_products)) {
                foreach ($cart_products as $cart_product) {
                    $cart_product['id_cart'] = $new_cart_id;
                    $cart_product['id_address_delivery'] = (int)$id_address_delivery;
                    $cart_product['date_add'] = date('Y-m-d H:i:s');

                    Db::getInstance()->insert('cart_product', $cart_product);
                }
            }

            return $cloned;
        }
    }

    return false;
}

function addPayment($payment_module, $neworder, $oldorder, $context) {

file_put_contents("./recurring.log", "Add Payment function started \n", FILE_APPEND);
    $resp = include_once dirname(__FILE__)."/monerishosted/mpgClassesVault.php";
file_put_contents("./recurring.log", "Add Payment require is done . $resp \n", FILE_APPEND);

    $store_id = Configuration::get('MH_STORE_ID');
    $api_token = Configuration::get('MH_HPP_KEY');
    
    $dynamic_descriptor = '';
    $amount=number_format((float)$neworder->total_paid, 2, '.', '');

    $store_abbr = strtolower(substr(Configuration::get('PS_SHOP_NAME'), 0, 3)) . "-";
    $store_abbr = str_replace(" ", "", $store_abbr);
    $order_id = $store_abbr . $neworder->id_cart . "-" .substr(number_format(time() * rand(),0,'',''),0,10);

    $original_transaction = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'moneris_transaction WHERE id_order = \''.$oldorder->id.'\' AND type = \'payment\' AND id_shop = '.(int)$context->shop->id);
file_put_contents("./recurring.log", "Add Payment select is done \n", FILE_APPEND);

    $txnArray = array(
            'type' => 'res_purchase_cc',
            'data_key' => $oldorder->ms_data_key,
            'order_id' => $order_id,
            'amount' => $amount,
            'crypt_type' => '7',
            'cust_id'=> $original_transaction['id_customer'],
            'dynamic_descriptor'=>$dynamic_descriptor
    );
    $mpgTxn = new mpgTransaction($txnArray);
    $mpgRequest = new mpgRequest($mpgTxn);
    $mpgHttpPost  =new mpgHttpsPost($store_id, $api_token, $mpgRequest);
      
    $mpgResponse=$mpgHttpPost->getMpgResponse();
    $responseCode = $mpgResponse->getResponseCode();
/*
p($txnArray);  
echo '<pre>';       
print("\nCardType = " . $mpgResponse->getCardType());
print("\nTransAmount = " . $mpgResponse->getTransAmount());
print("\nTxnNumber = " . $mpgResponse->getTxnNumber());
print("\nReceiptId = " . $mpgResponse->getReceiptId());
print("\nTransType = " . $mpgResponse->getTransType());
print("\nReferenceNum = " . $mpgResponse->getReferenceNum());
print("\nResponseCode = " . $mpgResponse->getResponseCode());
print("\nISO = " . $mpgResponse->getISO());
print("\nMessage = " . $mpgResponse->getMessage());
echo '</pre>';
//d();
*/
file_put_contents("./recurring.log", "Order Payment Transaction Info: ". print_r($mpgResponse, TRUE) ." \n", FILE_APPEND);

		if ($responseCode < 50 && (int) $responseCode != 0)
		{
			$new_transaction = $original_transaction;
			$new_transaction['amount'] = $mpgResponse->getTransAmount();
			$new_transaction['id_transaction'] = $mpgResponse->getReferenceNum();
			$new_transaction['tnx_number'] = $mpgResponse->getTxnNumber();
			$new_transaction['moneris_order'] = $order_id;
			$new_transaction['date_add'] = date('Y-m-d H:i:s');
			$new_transaction['currency'] = $original_transaction['currency'];
			$new_transaction['id_shop'] = (int)$context->shop->id;
			$new_transaction['id_order'] = $neworder->id;
			$new_transaction['id_cart'] = $neworder->id_cart;            
            $currency = new Currency((int)$neworder->id_currency);

			$payment_module->addTransaction('payment', $new_transaction);
                       
            $neworder->addOrderPayment($amount, $payment_module->name, $new_transaction['id_transaction'], $currency, $new_transaction['date_add']);

file_put_contents("./recurring.log", "OrderPayment completed \n", FILE_APPEND);
            
            return true;
		}
		else
		{
			$neworder->current_state = 8;
            $neworder->update();
file_put_contents("./recurring.log", "OrderPayment ERROR: { $mpgResponse->getResponseCode() } \n", FILE_APPEND);
		}
}

function sendMail($id_lang, $template, $subject, $data, $mail, $name) {
    Mail::Send((int)$id_lang, $template, $subject, $data, $mail, $name, Configuration::get('PS_SHOP_EMAIL'), Configuration::get('PS_SHOP_NAME'), NULL, NULL, _PS_ROOT_DIR_.'/mails/');
}

function getFormatedAddress(Address $the_address, $line_sep, $fields_style = array())
{
    return AddressFormat::generateAddress($the_address, array('avoid' => array()), $line_sep, ' ', $fields_style);
}

function getEmailTemplateContent($template_name, $mail_type, $var, $context)
{
    $email_configuration = Configuration::get('PS_MAIL_TYPE');
    if ($email_configuration != $mail_type && $email_configuration != Mail::TYPE_BOTH) {
        return '';
    }

    $theme_template_path = _PS_THEME_DIR_.'mails'.DIRECTORY_SEPARATOR.$context->language->iso_code.DIRECTORY_SEPARATOR.$template_name;
    $default_mail_template_path = _PS_MAIL_DIR_.$context->language->iso_code.DIRECTORY_SEPARATOR.$template_name;

    if (Tools::file_exists_cache($theme_template_path)) {
        $default_mail_template_path = $theme_template_path;
    }

    if (Tools::file_exists_cache($default_mail_template_path)) {
        $context->smarty->assign('list', $var);
        return $context->smarty->fetch($default_mail_template_path);
    }
    return '';
}