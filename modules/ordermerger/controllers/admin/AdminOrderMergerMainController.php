<?php
/**
 * wFirmapanel2 Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

class AdminOrderMergerMainController extends ModuleAdminController
{
    private $link;

    public function __construct()
    {
        $this->table = 'configuration';
        $this->display = 'view';
        $this->bootstrap = true;
        $this->meta_title = 'Order merger controller';
        parent::__construct();
        $this->path = $this->path ? $this->path : _MODULE_DIR_ . $this->module->name;
    }

    // @Override
    public function init()
    {
        parent::init();
        if (!Tools::getValue('ajax', null)) {
            $url = $this->context->link->getAdminLink('AdminModules');
            return Tools::redirectAdmin($url . '&configure=' . Tools::safeOutput($this->module->name));
        }
    }

    // @Override
    public function renderView()
    {
        return "";
    }

    public function displayAjaxCompareOrders()
    {
        $srcOrderId = (int) Tools::getValue('srcOrderId', null);
        $dstOrderId = (int) Tools::getValue('dstOrderId', null);

        $srcOrder = new Order($srcOrderId);
        $dstOrder = new Order($dstOrderId);

        if (!Validate::isLoadedObject($srcOrder) || !Validate::isLoadedObject($dstOrder)) {
            return $this->jsonResponse(array(
                'error' => 'There is no order with given ID!'
            ), 400);
        }

        $comparator = new \Ordermerger\Comparator\OrderComparator();
        $differences = $comparator->compare($srcOrder, $dstOrder);

        return $this->jsonResponse(array(
            'differences' => \Ordermerger\Common\Serializer::serializeOrderDifferences($differences)
        ));
    }

    public function displayAjaxMergeOrders()
    {
        $reqBody = file_get_contents('php://input');
        $reqData = Tools::jsonDecode($reqBody, true);

        if (!$reqData || !isset($reqData['options']) ||
            !isset($reqData['srcOrderId']) || !isset($reqData['dstOrderId'])) {
            return $this->jsonResponse(array(
                'error' => 'srcOrderId, dstOrderId or options parameter missing'
            ), 400);
        }

        $srcOrder = new Order($reqData['srcOrderId']);
        $dstOrder = new Order($reqData['dstOrderId']);

        if (!Validate::isLoadedObject($srcOrder) || !Validate::isLoadedObject($dstOrder)) {
            return $this->jsonResponse(array(
                'error' => 'There is no order with given ID!'
            ), 400);
        }

        $config = new \Ordermerger\Common\Config();
        $mergeOptions = \Ordermerger\Common\Serializer::deserializeMergeOptions($reqData['options']);
        $merger = new \Ordermerger\Merger\OrderMerger($config);
        $orderLogManager = new \Ordermerger\Orderlog\OrderLogManager(Db::getInstance());

        if ($orderLogManager->wasOrderASource($srcOrder->id)) {
            return $this->jsonResponse(array(
                'error' => 'Given order was a source before!'
            ), 400);
        }
        
        try {
            $merger->merge($srcOrder, $dstOrder, $mergeOptions);
            $log = new \Ordermerger\Orderlog\MergeSplitLogModel();
            $log->setDstOrder($dstOrder)
                ->setSrcOrder($srcOrder);
            $orderLogManager->saveLog($log);
        } catch (Exception $e) {
            return $this->jsonResponse(array(
                'success' => false,
                'error' => $e->getMessage()
            ), 500);
        }

        return $this->jsonResponse(array(
            'success' => true,
        ));
    }

    private function jsonResponse($data, $httpCode = 200)
    {
        header('X-PHP-Response-Code: '.$httpCode, true, $httpCode);
        header('Content-Type: application/json');
        echo Tools::jsonEncode($data);
        return true;
    }
}
