<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    pkrava
 *  @copyright 2017-2017 
 *  @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once 'src/ordermerger.loader.php';

class ordermerger extends Module
{
    protected $config_form = false;

    /**
     * Contain all orders actually loaded on orders list.
     * It will have items only on AdminOrdersController page
     * @var array
     */
    private $ordersList = array();

    public function __construct()
    {
        $this->name = 'ordermerger';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'pkrava';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = '08e08ce332a5d3b443cb6fa09855a0fa';

        parent::__construct();

        $this->displayName = $this->l('Merge two orders into one');
        $this->description = $this->l('This module helps you to easyily merge few orders from one client');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        if (!parent::install() ||
            !$this->registerHook('displayAdminOrder') ||
            !$this->registerHook('displayBackOfficeFooter') ||
            !$this->registerHook('actionAdminOrdersListingResultsModifier')) {
            return false;
        }
        $stateId = \Ordermerger\Common\ModuleOrderState::install();
        \Ordermerger\Common\ModuleTabs::install();

        $config = new \Ordermerger\Common\Config();
        $config->mergedOrderStateId = $stateId;
        $config->update(false);

        return true;
    }

    public function uninstall()
    {
        \Ordermerger\Common\ModuleTabs::uninstall();
        \Ordermerger\Common\ModuleOrderState::uninstall();
        include(dirname(__FILE__).'/sql/uninstall.php');

        $config = new \Ordermerger\Common\Config();
        $config->purge();

        return parent::uninstall();
    }

    public function getContent()
    {
        $config = new Ordermerger\Common\Config();

        if (Tools::getValue('action') == 'updateConfig' && $this->validateConfigFields()) {
            $config->update();
            $this->context->controller->informations[] = $this->l('Configuration was saved');
        }

        $order_states = OrderState::getOrderStates($this->context->language->id);
        
        usort($order_states, function ($item1, $item2) {
            if ($item1['id_order_state'] == $item2['id_order_state']) {
                return 0;
            }
            return $item1['id_order_state'] < $item2['id_order_state'] ? -1 : 1;
        });
        
        $this->context->smarty->assign(array(
            'config' => $config,
            'order_states' => $order_states,
        ));
        
        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            return $this->display(__FILE__, 'views/templates/admin/config_page_v16.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/admin/config_page_v15.tpl');
        }
    }

    private function validateConfigFields()
    {
        $valid = true;
        
        if (Tools::getValue('config_mergeableStatuses') && !Validate::isArrayWithIds(Tools::getValue('config_mergeableStatuses'))) {
            $this->context->controller->errors[] = $this->l('Mergable statuses should be array with ids');
            $valid = false;
        }

        if (Tools::getValue('config_mergedOrderStateId') && !Validate::isInt(Tools::getValue('config_mergedOrderStateId'))) {
            $this->context->controller->errors[] = $this->l('Merged order state id should be a number');
            $valid = false;
        }

        return $valid;
    }

    /**
     * Called after all orders for list are loaded
     * @param  array $params contain key 'list' with orders on list
     */
    public function hookActionAdminOrdersListingResultsModifier($params)
    {
        // file_put_contents(__DIR__.'/test.log', var_export($params['list'], true));
        // $params['list'][0]['customer'] = 'Testing feature';
        $this->ordersList = $params['list'];
    }
    
    public function hookDisplayBackOfficeFooter($params)
    {
        if (!count($this->ordersList)) {
            return;
        }
    }

    /**
     * Display content in order details page
     * @param  array $params
     * @return string
     */
    public function hookDisplayAdminOrder($params)
    {
        $config = new Ordermerger\Common\Config();
        $orderLogManager = new \Ordermerger\Orderlog\OrderLogManager(Db::getInstance());
        $orderFinder = new \Ordermerger\Common\OrderFinder($config, $orderLogManager);

        $this->context->controller->addJS($this->_path.'views/js/ordermerger.min.v101.js');
        $this->context->controller->addCSS($this->_path.'views/css/simillar.order.css');

        $order = new Order((int)$params['id_order']);
        $similarOrders = array();
        foreach ($orderFinder->findMergeableOrders($order) as $simOrder) {
            $tmpState = new OrderState($simOrder->current_state);

            $similarOrders[] = array(
                'order' => $simOrder,
                'state' => $tmpState,
                'products' => $simOrder->getProducts(),
            );
        }

        $logs = $orderLogManager->getLogsByOrderId($params['id_order']);
        $isMerge = $orderLogManager->wasOrderASource($params['id_order']);
        
        $idWithWhatMerge = 0;
        foreach ($logs as $log) {
            $srcOrder = $log->getSrcOrder();
            if ($params['id_order'] == $srcOrder->id) {
                $idWithWhatMerge = $log->getDstOrder()->id;
                break;
            }
        }

        $this->context->smarty->assign(array(
            'modulePath' => $this->_path,
            'similarOrders' => $similarOrders,
            'idOrder' => $params['id_order'],
            'config' => $config,
            'id_lang' => (int) $this->context->language->id,
            'logs' => $logs,
            'isMerge' => $isMerge,
            'idWithWhatMerge' => $idWithWhatMerge,
            'restUrl' => $this->context->link->getAdminLink('AdminOrderMergerMain'),
            'orderDetailsUrl' => $this->context->link->getAdminLink('AdminOrders').'&vieworder'
        ));

        if (version_compare(_PS_VERSION_, '1.6.0', '>=') === true) {
            return $this->display(__FILE__, 'views/templates/admin/hook_admin_order_v16.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/admin/hook_admin_order_v15.tpl');
        }
    }
}
