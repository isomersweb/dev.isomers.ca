<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

require_once 'common/Config.php';
require_once 'common/OrderFinder.php';
require_once 'common/ModuleTabs.php';
require_once 'common/Serializer.php';
require_once 'common/ModuleMailer.php';
require_once 'common/ModuleOrderState.php';

require_once 'comparator/OrderComparator.php';
require_once 'comparator/OrderDifferences.php';

require_once 'merger/MergeOptions.php';
require_once 'merger/OrderMerger.php';

require_once 'orderlog/MergeSplitLogModel.php';
require_once 'orderlog/OrderLogManager.php';
