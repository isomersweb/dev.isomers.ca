<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Common;

class ModuleOrderState
{
    public static $moduleName = 'ordermerger';

    /**
     * Installes new order state
     * @return integer id of the new order state
     */
    public static function install()
    {
        $state = new \OrderState();
        $state->name = array();
        foreach(\Language::getLanguages() as $lang) {
            $state->name[$lang['id_lang']] = 'Order merged';
        }
        // $state->name = array(
        //     \Context::getContext()->language->id => 'Order merged',
        // );
        $state->module_name = self::$moduleName;
        $state->color = "#FF8C00";
        $state->unremovable = 1;
        $state->save();
        return (int) $state->id;
    }

    public static function uninstall()
    {
        $idLang = \Context::getContext()->language->id;
        $states = \OrderState::getOrderStates($idLang);
        foreach ($states as $state) {
            if ($state['module_name'] == self::$moduleName) {
                $sObj = new \OrderState($state['id_order_state']);
                $sObj->delete();
            }
        }
        return true;
    }
}
