<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Common;

use Ordermerger\Comparator\OrderDifferences;
use Ordermerger\Merger\MergeOptions;
use Ordermerger\Orderlog\MergeSplitLogModel;

/**
 * Class provides interface for serialization objects used in this module
 * @todo all methods to serialize/deserialize :)
 */
class Serializer
{
    /**
     * Serializes order differences into array
     * @todo   whole method
     * @param  OrderDifferences $diffs
     * @return array
     */
    public static function serializeOrderDifferences(OrderDifferences $diffs)
    {
        $r = array(
            'differentAddress' => $diffs->isDifferentAddress(),
            'differentInvoiceAddress' => $diffs->isDifferentInvoiceAddress(),
            'differentPayment' => $diffs->isDifferentPayment(),
            'differentCarrier' => $diffs->isDifferentCarrier(),
            'differentStatus' => $diffs->isDifferentStatus(),

            'srcAddress' => $diffs->getSrcAddress(),
            'dstAddress' => $diffs->getDstAddress(),
            'srcInvoiceAddress' => $diffs->getSrcInvoiceAddress(),
            'dstInvoiceAddress' => $diffs->getDstInvoiceAddress(),

            'srcPayment' => $diffs->getSrcPayment(),
            'dstPayment' => $diffs->getDstPayment(),

            'srcCarrier' => $diffs->getSrcCarrier(),
            'dstCarrier' => $diffs->getDstCarrier(),

            'srcStatus' => $diffs->getSrcStatus(),
            'dstStatus' => $diffs->getDstStatus()
        );
        return $r;
    }

    /**
     * Deserializes merger options
     * @param  array $data raw data
     * @return MergeOptions
     */
    public static function deserializeMergeOptions($data)
    {
        $fields = array('addressFrom', 'carrierFrom', 'paymentFrom', 'statusFrom', 'sendEmail');
        $mergeOptions = new MergeOptions();

        foreach ($fields as $f) {
            $method = 'set' . \Tools::ucfirst($f);
            if (isset($data[$f]) && method_exists($mergeOptions, $method)) {
                $mergeOptions->$method($data[$f]);
            }
        }

        return $mergeOptions;
    }

    public static function serializeMergeLog(MergeSplitLogModel $log)
    {
        $r = array(
            'id' => (int) $log->getId(),
            'srcOrder' => $log->getSrcOrder(),
            'dstOrder' => $log->getDstOrder(),
            'logDate' => $log->getLogDate()->getTimestamp(),
            'emailSent' => $log->getEmailSent(),
            'split' => $log->getSplit()
        );

        return $r;
    }
}
