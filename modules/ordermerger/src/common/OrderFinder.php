<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Common;

use Ordermerger\Orderlog\OrderLogManager;
use Ordermerger\Common\Config;

class OrderFinder
{
    /** @var Config configuration */
    private $config;

    /** @var OrderLogManager order logs manager (merge split logs) */
    private $orderLogManager;

    public function __construct(Config $config, OrderLogManager $olm)
    {
        $this->config = $config;
        $this->orderLogManager = $olm;
    }

    /**
     * Finds orders which can be merged with order given as parameter
     * @param  \Order $dstOrder
     * @return Order[]
     */
    public function findMergeableOrders(\Order $dstOrder)
    {
        $r = array();
        $dstCustomer = $dstOrder->getCustomer();
        if (!$dstCustomer) {
            return $r;
        }

        // 1. find orders with same email address
        $orders = $this->getOrdersByEmail($dstCustomer->email);

        // 2. filter these orders with mergeable statuses from config
        // 3. check if any of these orders was merged before
        foreach ($orders as $order) {
            if ($order->id == $dstOrder->id) {
                continue;
            }

            if (!in_array($order->current_state, $this->config->mergeableStatuses)) {
                continue;
            }

            if ($this->orderLogManager->wasOrderASource($order->id)) {
                continue;
            }

            $r[] = $order;
        }

        return $r;
    }

    
    /**
     * Gets all orders by given email
     * @param  string $email
     * @return \Order[]        [description]
     */
    private function getOrdersByEmail($email)
    {
        $r = array();
        $customers = \Customer::getCustomersByEmail($email);
        if (!$customers || count($customers) < 1) {
            return $r;
        }

        foreach ($customers as $customer) {
            $orders = \Order::getCustomerOrders($customer['id_customer']);
            foreach ($orders as $ord) {
                $r[] = new \Order($ord['id_order']);
            }
        }

        return $r;
    }
}
