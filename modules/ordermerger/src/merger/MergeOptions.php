<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Merger;

/**
 * Contains options used by OrderMerger
 */
class MergeOptions
{
    const SRC_ORDER = 'src';
    const DST_ORDER = 'dst';

    /**
     * From which order should take address
     * @var string
     */
    private $addressFrom;

    /**
     * From which order should take carrier
     * @var string
     */
    private $carrierFrom;

    /**
     * From which order should take payment
     * @var string
     */
    private $paymentFrom;

    /**
     * From which order should take status
     * @var string
     */
    private $statusFrom;

    /**
     * Should send email after operation?
     * @var boolean
     */
    private $sendEmail = false;

    /**
     * Gets the From which order should take address.
     * @return string
     */
    public function getAddressFrom()
    {
        return $this->addressFrom;
    }

    /**
     * Sets the From which order should take address.
     *
     * @param string $addressFrom the address from (src|dst)
     * @return self
     */
    public function setAddressFrom($addressFrom)
    {
        $this->addressFrom = $addressFrom;
        return $this;
    }

    /**
     * Gets the From which order should take carrier.
     * @return string
     */
    public function getCarrierFrom()
    {
        return $this->carrierFrom;
    }

    /**
     * Sets the From which order should take carrier.
     *
     * @param string $carrierFrom the carrier from (src|dst)
     * @return self
     */
    public function setCarrierFrom($carrierFrom)
    {
        $this->carrierFrom = $carrierFrom;
        return $this;
    }

    /**
     * Gets the From which order should take payment.
     * @return string
     */
    public function getPaymentFrom()
    {
        return $this->paymentFrom;
    }

    /**
     * Sets the From which order should take payment.
     *
     * @param string $paymentFrom the payment from (src|dst)
     * @return self
     */
    public function setPaymentFrom($paymentFrom)
    {
        $this->paymentFrom = $paymentFrom;
        return $this;
    }

    /**
     * Gets the From which order should take status.
     * @return string
     */
    public function getStatusFrom()
    {
        return $this->statusFrom;
    }

    /**
     * Sets the From which order should take status.
     *
     * @param string $statusFrom the status from (src|dst)
     * @return self
     */
    public function setStatusFrom($statusFrom)
    {
        $this->statusFrom = $statusFrom;
        return $this;
    }

    /**
     * Gets the Should send email after operation?.
     * @return boolean
     */
    public function getSendEmail()
    {
        return $this->sendEmail;
    }

    /**
     * Sets the Should send email after operation?
     *
     * @param boolean $sendEmail the send email
     * @return self
     */
    public function setSendEmail($sendEmail)
    {
        $this->sendEmail = $sendEmail;
        return $this;
    }
}
