<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Comparator;

/**
 * Model class with orders differences
 */
class OrderDifferences
{
    private $differentAddress = false;
    private $differentInvoiceAddress = false;
    private $differentPayment = false;
    private $differentCarrier = false;
    private $differentStatus = false;

    /**
     * Source order address
     * @var \Address
     */
    private $srcAddress;

    /**
     * Destination order address
     * @var \Address
     */
    private $dstAddress;

    /**
     * Source order InvoiceAddress
     * @var \Address
     */
    private $srcInvoiceAddress;

    /**
     * Destination order InvoiceAddress
     * @var \Address
     */
    private $dstInvoiceAddress;

    /**
     * Source order payment
     * @var string
     */
    private $srcPayment;

    /**
     * Source order payment
     * @var string
     */
    private $dstPayment;

    /**
     * Source order Carrier
     * @var \Carrier
     */
    private $srcCarrier;

    /**
     * Source order Carrier
     * @var \Carrier
     */
    private $dstCarrier;

    /**
     * Source order Status
     * @var \OrderState
     */
    private $srcStatus;

    /**
     * Source order Status
     * @var \OrderState
     */
    private $dstStatus;

    /**
     * Gets the value of differentAddress.
     * @return boolean
     */
    public function isDifferentAddress()
    {
        return $this->differentAddress;
    }

    /**
     * Sets the value of differentAddress.
     *
     * @param boolean $differentAddress the different address
     * @return self
     */
    public function setDifferentAddress($differentAddress)
    {
        $this->differentAddress = $differentAddress;
        return $this;
    }

    /**
     * Gets the value of differentInvoiceAddress.
     * @return boolean
     */
    public function isDifferentInvoiceAddress()
    {
        return $this->differentInvoiceAddress;
    }

    /**
     * Sets the value of differentInvoiceAddress.
     *
     * @param boolean $differentInvoiceAddress the different address
     * @return self
     */
    public function setDifferentInvoiceAddress($differentInvoiceAddress)
    {
        $this->differentInvoiceAddress = $differentInvoiceAddress;
        return $this;
    }

    /**
     * Gets the value of differentPayment.
     * @return boolean
     */
    public function isDifferentPayment()
    {
        return $this->differentPayment;
    }

    /**
     * Sets the value of differentPayment.
     *
     * @param boolean $differentPayment the different payment
     * @return self
     */
    public function setDifferentPayment($differentPayment)
    {
        $this->differentPayment = $differentPayment;
        return $this;
    }

    /**
     * Gets the value of differentCarrier.
     * @return boolean
     */
    public function isDifferentCarrier()
    {
        return $this->differentCarrier;
    }

    /**
     * Sets the value of differentCarrier.
     *
     * @param boolean $differentCarrier the different carrier
     * @return self
     */
    public function setDifferentCarrier($differentCarrier)
    {
        $this->differentCarrier = $differentCarrier;
        return $this;
    }

    /**
     * Gets the value of differentStatus.
     * @return boolean
     */
    public function isDifferentStatus()
    {
        return $this->differentStatus;
    }

    /**
     * Sets the value of differentStatus.
     *
     * @param boolean $differentStatus the different status
     * @return self
     */
    public function setDifferentStatus($differentStatus)
    {
        $this->differentStatus = $differentStatus;
        return $this;
    }

    /**
     * Gets the Source order address.
     *
     * @return \Address
     */
    public function getSrcAddress()
    {
        return $this->srcAddress;
    }

    /**
     * Sets the Source order address.
     *
     * @param \Address $srcAddress the src address
     * @return self
     */
    public function setSrcAddress(\Address $srcAddress)
    {
        $this->srcAddress = $srcAddress;
        return $this;
    }

    /**
     * Gets the Destination order address.
     *
     * @return \Address
     */
    public function getDstAddress()
    {
        return $this->dstAddress;
    }

    /**
     * Sets the Destination order address.
     *
     * @param \Address $dstAddress the dst address
     * @return self
     */
    public function setDstAddress(\Address $dstAddress)
    {
        $this->dstAddress = $dstAddress;
        return $this;
    }

        /**
     * Gets the Source order InvoiceAddress.
     * @return \Address
     */
    public function getSrcInvoiceAddress()
    {
        return $this->srcInvoiceAddress;
    }

    /**
     * Sets the Source order InvoiceAddress.
     * @param \Address $srcInvoiceAddress the src invoice address
     * @return self
     */
    public function setSrcInvoiceAddress(\Address $srcInvoiceAddress)
    {
        $this->srcInvoiceAddress = $srcInvoiceAddress;
        return $this;
    }

    /**
     * Gets the Destination order InvoiceAddress.
     * @return \Address
     */
    public function getDstInvoiceAddress()
    {
        return $this->dstInvoiceAddress;
    }

    /**
     * Sets the Destination order InvoiceAddress.
     * @param \Address $dstInvoiceAddress the dst invoice address
     * @return self
     */
    public function setDstInvoiceAddress(\Address $dstInvoiceAddress)
    {
        $this->dstInvoiceAddress = $dstInvoiceAddress;
        return $this;
    }

    /**
     * Gets the Source order payment.
     *
     * @return string
     */
    public function getSrcPayment()
    {
        return $this->srcPayment;
    }

    /**
     * Sets the Source order payment.
     *
     * @param string $srcPayment the src payment
     * @return self
     */
    public function setSrcPayment($srcPayment)
    {
        $this->srcPayment = $srcPayment;
        return $this;
    }

    /**
     * Gets the Source order payment.
     *
     * @return string
     */
    public function getDstPayment()
    {
        return $this->dstPayment;
    }

    /**
     * Sets the Source order payment.
     *
     * @param string $dstPayment the dst payment
     * @return self
     */
    public function setDstPayment($dstPayment)
    {
        $this->dstPayment = $dstPayment;
        return $this;
    }

    /**
     * Gets the Source order Carrier.
     *
     * @return \Carrier
     */
    public function getSrcCarrier()
    {
        return $this->srcCarrier;
    }

    /**
     * Sets the Source order Carrier.
     *
     * @param \Carrier $srcCarrier the src carrier
     * @return self
     */
    public function setSrcCarrier(\Carrier $srcCarrier)
    {
        $this->srcCarrier = $srcCarrier;
        return $this;
    }

    /**
     * Gets the Source order Carrier.
     *
     * @return \Carrier
     */
    public function getDstCarrier()
    {
        return $this->dstCarrier;
    }

    /**
     * Sets the Source order Carrier.
     *
     * @param \Carrier $dstCarrier the dst carrier
     * @return self
     */
    public function setDstCarrier(\Carrier $dstCarrier)
    {
        $this->dstCarrier = $dstCarrier;
        return $this;
    }

    /**
     * Gets the Source order Status.
     *
     * @return \OrderState
     */
    public function getSrcStatus()
    {
        return $this->srcStatus;
    }

    /**
     * Sets the Source order Status.
     *
     * @param \OrderState $srcStatus the src status
     * @return self
     */
    public function setSrcStatus(\OrderState $srcStatus)
    {
        $this->srcStatus = $srcStatus;
        return $this;
    }

    /**
     * Gets the Source order Status.
     *
     * @return \OrderState
     */
    public function getDstStatus()
    {
        return $this->dstStatus;
    }

    /**
     * Sets the Source order Status.
     *
     * @param \OrderState $dstStatus the dst status
     * @return self
     */
    public function setDstStatus(\OrderState $dstStatus)
    {
        $this->dstStatus = $dstStatus;
        return $this;
    }
}
