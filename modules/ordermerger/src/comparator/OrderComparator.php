<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Comparator;

use Ordermerger\Comparator\OrderDifferences;

/**
 * Allows to compare two orders
 */
class OrderComparator
{

    /**
     * Compares two orders
     * @param  \Order $srcOrder
     * @param  \Order $dstOrder
     * @return OrderDifferences
     */
    public function compare(\Order $srcOrder, \Order $dstOrder)
    {
        $diff = new OrderDifferences();
        $this->compareAddresses($srcOrder, $dstOrder, $diff);
        $this->compareCarriers($srcOrder, $dstOrder, $diff);
        $this->compareStatuses($srcOrder, $dstOrder, $diff);

        $diff->setDstPayment($dstOrder->payment)
             ->setSrcPayment($srcOrder->payment)
             ->setDifferentPayment($dstOrder->payment != $srcOrder->payment);


        return $diff;
    }

    private function compareStatuses(\Order $srcOrder, \Order $dstOrder, OrderDifferences $diffsOut)
    {
        $diffsOut->setDstStatus(new \OrderState($dstOrder->current_state))
                 ->setSrcStatus(new \OrderState($srcOrder->current_state));

        $diffsOut->setDifferentStatus($dstOrder->current_state != $srcOrder->current_state);
    }

    private function compareCarriers(\Order $srcOrder, \Order $dstOrder, OrderDifferences $diffsOut)
    {
        $diffsOut->setDstCarrier(new \Carrier($dstOrder->id_carrier))
                 ->setSrcCarrier(new \Carrier($srcOrder->id_carrier));

        $diffsOut->setDifferentCarrier($dstOrder->id_carrier != $srcOrder->id_carrier);
    }

    private function compareAddresses(\Order $srcOrder, \Order $dstOrder, OrderDifferences $diffsOut)
    {
        $diffsOut->setDstAddress(new \Address($dstOrder->id_address_delivery))
                 ->setDstInvoiceAddress(new \Address($dstOrder->id_address_invoice))
                 ->setSrcAddress(new \Address($srcOrder->id_address_delivery))
                 ->setSrcInvoiceAddress(new \Address($srcOrder->id_address_invoice));

        $diffsOut->setDifferentAddress($this->areAddressesDifferent($diffsOut->getDstAddress(), $diffsOut->getSrcAddress()));
        $diffsOut->setDifferentInvoiceAddress($this->areAddressesDifferent($diffsOut->getDstInvoiceAddress(), $diffsOut->getSrcInvoiceAddress()));

        return $diffsOut;
    }

    /**
     * Metod compares real address values (not its id)
     * @param  \Address $addr1
     * @param  \Address $addr2
     * @return boolean         true if addresses are different
     */
    private function areAddressesDifferent(\Address $addr1, \Address $addr2)
    {
        $stringsFieldsToCompare = array('company', 'lastname', 'firstname', 'address1', 'address2', 'postcode', 'city', 'phone', 'phone_mobile', 'vat_number', 'dni');

        if ($addr1->id_country != $addr2->id_country) {
            return true;
        }

        foreach ($stringsFieldsToCompare as $field) {
            if (!$addr1->$field && !$addr2->$field) {
                continue;
            }
            if (strcasecmp(trim($addr1->$field), trim($addr2->$field)) != 0) {
                return true;
            }
        }

        // if method wasnt return earlier that means that addresses are not different
        return false;
    }
}
