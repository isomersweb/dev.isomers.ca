<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Orderlog;

use Ordermerger\Orderlog\MergeSplitLogModel;

class OrderLogManager
{
    /** @var \Db database manager instance */
    private $db;

    public function __construct(\Db $db)
    {
        $this->db = $db;
    }

    public function getLogsByOrderId($orderId)
    {
        $r = array();
        $sql = "SELECT * FROM "._DB_PREFIX_."ordermerger_log ".
               "WHERE src_order_id = ".(int) $orderId." ".
               "OR dst_order_id = ".(int) $orderId;
        $results = $this->db->executeS($sql);

        if (!$results) {
            return $r;
        }

        foreach ($results as $row) {
            $r[] = $this->parseDbData($row);
        }

        return $r;
    }

    /**
     * Checks if given order id was a source order in any merge operation
     * @param  integer $orderId
     * @return boolean
     */
    public function wasOrderASource($orderId)
    {
        $sql = "SELECT * FROM "._DB_PREFIX_."ordermerger_log ".
               "WHERE src_order_id = ".(int)$orderId;
        $results = $this->db->executeS($sql);
        
        if (!$results) {
            return false;
        }

        return true;
    }

    public function saveLog(MergeSplitLogModel $log)
    {
        if (!$log->getId()) {
            $this->db->insert('ordermerger_log', $this->getDbData($log));
            $log->setId($this->db->Insert_ID());
        } else {
            $this->db->update('ordermerger_log', $this->getDbData($log), 'id = '.(int)$log->getId());
        }

        return $log;
    }

    private function parseDbData($data)
    {
        $log = new MergeSplitLogModel();
        $log->setId((int)$data['id'])
            ->setSrcOrder(new \Order($data['src_order_id']))
            ->setDstOrder(new \Order($data['dst_order_id']))
            ->setLogDate(new \DateTime('@'.$data['timestamp']))
            ->setEmailSent((int)$data['email_sent'] ? true : false)
            ->setSplit((int)$data['is_split'] ? true : false);

        return $log;
    }

    private function getDbData(MergeSplitLogModel $log)
    {
        $r = array(
            'src_order_id' => $log->getSrcOrder() ? (int)$log->getSrcOrder()->id : null,
            'dst_order_id' => $log->getDstOrder() ? (int)$log->getDstOrder()->id : null,
            'email_sent' => $log->getEmailSent() ? 1 : 0,
            'is_split' => $log->getSplit() ? 1 : 0,
            'timestamp' => (int) $log->getLogDate()->getTimestamp()
        );

        if ($log->getId()) {
            $r['id'] = (int) $log->getId();
        }

        return $r;
    }

    public static function getInstallSQLQuery()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."ordermerger_log` (
           `id` INT NOT NULL AUTO_INCREMENT,
           `src_order_id` INT NOT NULL,
           `dst_order_id` INT NOT NULL,
           `email_sent` TINYINT(1) NOT NULL DEFAULT 0,
           `is_split` TINYINT(1) NOT NULL DEFAULT 0,
           `timestamp` INT,
           PRIMARY KEY  (`id`)
           ) ENGINE="._MYSQL_ENGINE_."  DEFAULT CHARSET=utf8";
        return $sql;
    }

    public static function getUninstallSQLQuery()
    {
        $sql = "DROP TABLE IF EXISTS `"._DB_PREFIX_."ordermerger_log`";
        return $sql;
    }
}
