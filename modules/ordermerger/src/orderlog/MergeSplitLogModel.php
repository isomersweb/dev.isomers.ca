<?php
/**
 * OrderMerger Prestashop module
 *
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Wiktor Koźmiński
 *  @copyright 2017-2017 Silver Rose Wiktor Koźmiński
 *  @license   LICENSE.txt
 */

namespace Ordermerger\Orderlog;

/**
 * Model of log which contains information about splitted/merged orders
 */
class MergeSplitLogModel
{
    /**
     * Log id
     * @var integer
     */
    private $id;

    /**
     * Source order
     * @var \Order
     */
    private $srcOrder;

    /**
     * Dst order
     * @var \Order
     */
    private $dstOrder;

    /**
     * Log date and time
     * @var \DateTime
     */
    private $logDate;

    /**
     * Was email send during action
     * @var boolean
     */
    private $emailSent = false;

    /**
     * Was it split or merge operation
     * @var boolean
     */
    private $split = false;

    public function __construct()
    {
        $this->logDate = new \DateTime();
    }

    /**
     * Gets the Log id.
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Log id.
     * @param integer $id the id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Gets the Source order.
     *
     * @return \Order
     */
    public function getSrcOrder()
    {
        return $this->srcOrder;
    }

    /**
     * Sets the Source order.
     *
     * @param \Order $srcOrder the src order
     * @return self
     */
    public function setSrcOrder(\Order $srcOrder)
    {
        $this->srcOrder = $srcOrder;
        return $this;
    }

    /**
     * Gets the Dst order.
     *
     * @return \Order
     */
    public function getDstOrder()
    {
        return $this->dstOrder;
    }

    /**
     * Sets the Dst order.
     *
     * @param \Order $dstOrder the dst order
     * @return self
     */
    public function setDstOrder(\Order $dstOrder)
    {
        $this->dstOrder = $dstOrder;
        return $this;
    }

    /**
     * Gets the Log date and time.
     *
     * @return \DateTime
     */
    public function getLogDate()
    {
        return $this->logDate;
    }

    /**
     * Sets the Log date and time.
     *
     * @param \DateTime $logDate the log date
     * @return self
     */
    public function setLogDate(\DateTime $logDate)
    {
        $this->logDate = $logDate;
        return $this;
    }

    /**
     * Gets the Was email send during action.
     *
     * @return boolean
     */
    public function getEmailSent()
    {
        return $this->emailSent;
    }

    /**
     * Sets the Was email send during action.
     *
     * @param boolean $emailSent the email sent
     * @return self
     */
    public function setEmailSent($emailSent)
    {
        $this->emailSent = $emailSent;
        return $this;
    }

    /**
     * Gets the Was it split or merge operation.
     *
     * @return boolean
     */
    public function getSplit()
    {
        return $this->split;
    }

    /**
     * Sets the Was it split or merge operation.
     *
     * @param boolean $split the split
     * @return self
     */
    public function setSplit($split)
    {
        $this->split = $split;
        return $this;
    }
}
