

$(document).ready(function() {

    $("#hbp_newhook_button").toggle(function(){
       $("#hbp_newhook_form").show("fast");
    }, function(){
       $("#hbp_newhook_form").hide("fast"); 
    });

    $(".editbutton").hover(
		function(){
			$(this).fadeTo("fast",1.0);
		},
		function(){
			$(this).fadeTo("fast",0.3);
		}
	);
    
    $(".remove, .edit").hover(
		function(){
			$(this).fadeTo("fast",1.0);
		},
		function(){
			$(this).fadeTo("fast",0.3);
		}
	);
	
	
    $(".accordion").toggle(
		function(){
			$(".hook_blocks").css("display","none");
			var alt=$(this).attr("alt");
            //$(".hook_"+alt).css("display","table-row");
            $(".hook_"+alt).show("fast");
		},
        function(){
			$(".hook_blocks").css("display","none");
			var alt=$(this).attr("alt");
            //$(".hook_"+alt).css("display","none");
            $(".hook_"+alt).hide("fast");
		}
	);
    
	
});