<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{htmlboxpro}prestashop>htmlboxpro_bd22070ef92ab8e2eb2b60e0736ba537'] = 'HTML Box Pro';
$_MODULE['<{htmlboxpro}prestashop>htmlboxpro_2a5b55f8f0ea547b659506ee561a1f1d'] = 'Avec ce module, vous pouvez mettre le code HTML / JavaScript / CSS où vous voulez';
$_MODULE['<{htmlboxpro}prestashop>htmlboxpro_50794fe456ddaeea90fab8fc12af5c12'] = 'Configuration';
$_MODULE['<{htmlboxpro}prestashop>htmlboxpro_54108d6986ee41288fa9d5b4fdec420c'] = 'Où afficher HTML Box Pro?';
$_MODULE['<{htmlboxpro}prestashop>htmlboxpro_01090c774c1734d594584ec91fb7ab47'] = 'Entrez le code html / js / css ici';
$_MODULE['<{htmlboxpro}prestashop>htmlboxpro_9daf1fb753b42c3cdc8f1d01669cd6d8'] = 'Enregistrer les paramètres';
