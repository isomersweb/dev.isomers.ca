$(document).ready(function() {
	if ($("#product-tab-content-ModuleExtraproducttab").hasClass('not-loaded'))
	{
		//Hide the save buttons until the extra product tab is loaded to avoid sending empty product tabs post and deleting it 
		$("#desc-product-save").hide().attr("id","saveMitsos");
		$("#desc-product-save-and-stay").hide().attr("id","saveMitsos2");
	}
	
	$("#product-tab-content-ModuleExtraproducttab").on('loaded', function(){
			//display translation flags
			displayFlags(languages, id_language, allowEmployeeFormLang);
			//display the hidden save buttons now that the extra product tab has finished loading
			$("#saveMitsos").show().attr("id","desc-product-save");
			$("#saveMitsos2").show().attr("id","desc-product-save-and-stay");
	});
});

