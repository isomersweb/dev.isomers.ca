{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{*{capture name=path}{l s='Price drop'}{/capture}

<h1 class="page-heading product-listing">{l s='Price drop'}</h1>*}

{if $vouchers}
    <div class="products_list_wrapper">
        <ul class="row">
            {foreach from=$vouchers item=voucher name=vouchers}

                <li class="col-xs-6 col-sm-4 col-md-3" itemscope="" itemtype="https://schema.org/Product">
                    <div class="products_list_item voucher_item">
                        <div class="voucher-descr header-bg-orange">
                            <h3 class="uppercase text-center">{$voucher.name}</h3>
                            <p itemprop="description">{$voucher.description}</p>
                        </div>
                    </div>
                </li>
            {/foreach}
        </ul>
    </div>
{/if}

{if $products}
{*	<div class="content_sortPagiBar">
    	<div class="sortPagiBar clearfix">
			{include file="./product-sort.tpl"}
			{include file="./nbr-product-page.tpl"}
		</div>
    	<div class="top-pagination-content clearfix">
        	{include file="./product-compare.tpl"}
            {include file="$tpl_dir./pagination.tpl" no_follow=1}
        </div>
	</div>*}

    {foreach from=$products item=product name=products} {$product.main_offer}
        {if $product.main_offer == 1}
        <div class="row offer-wrapper">aaa
            <div class="col-sm-6 img-wrapper">
                <a href="{$product.link|escape:'html':'UTF-8'}"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" /></a>
            </div>
            <div class="col-sm-6 block-content clearfix">
                <h2> <a href="{$product.link|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></h2>
                {hook h='displayProductListReviews' product=$product}
                <p itemprop="description">{$product.description_short|strip_tags:'UTF-8'|truncate:100:'...'}</p>
                <div class="price-wrapper">
                    <span itemprop="price" class="price product-price">
                        {hook h="displayProductPriceBlock" product=$product type="before_price"}
						{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
					</span>
					<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
					{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
						{hook h="displayProductPriceBlock" product=$product type="old_price"}
						<span class="old-price product-price">
							{displayWtPrice p=$product.price_without_reduction}
						</span>
						{if $product.specific_prices.reduction_type == 'percentage'}
							<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
						{/if}
					{/if}                                    
                </div>
            </div>
        </div>
        {/if}
    {/foreach}

    <div class="divider"></div>
    <div class="products_list_wrapper">
		{include file="./product-list.tpl" products=$products}
    </div>

	<div class="content_sortPagiBar">
        <div class="bottom-pagination-content clearfix">
			{include file="./pagination.tpl" no_follow=1 paginationId='bottom'}
        </div>
	</div>
{*	{else}
	<p class="alert alert-warning">{l s='No price drop'}</p>*}
{/if}


