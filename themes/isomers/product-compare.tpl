{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $comparator_max_item}
	<div class="block_content" id="compare_block">
		<div>
			<a rel="nofollow" title="Clear Compare list" href="#" class="btn btn-clearall btn-delete">{l s="Clear All"}</a>
			<form method="post" action="{$link->getPageLink('products-comparison')|escape:'html':'UTF-8'}" class="compare-form">
				<button type="submit" class="btn purple btn-compare">
					<span>{l s="Compare All"}</span>
				</button>
				<input type="hidden" name="compare_product_count" class="compare_product_count" value="{count($compared_products)}" />
				<input type="hidden" name="compare_product_list" class="compare_product_list" value="" />
			</form>
		</div>
		<a class="btn-close" href="#">Close Block</a>
	</div>
	{if !isset($paginationId) || $paginationId == ''}
		{addJsDefL name=min_item}{l s='Please select at least one product' js=1}{/addJsDefL}
		{addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}
		{addJsDef comparator_max_item=$comparator_max_item}
		{addJsDef comparedProductsIds=$compared_products}
	{/if}
{/if}