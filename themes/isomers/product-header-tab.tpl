<div role="tabpanel" class="tab-pane fade active in box-cart-bottom row no-print ">
    <div class="wishlist col-sm-6">
        <a class="btn btn-wishlist-white black wishlistProd_{$product->id|intval}" href="#" rel="{$product->id|intval}" onclick="WishlistCart('wishlist_block_list', 'add', '{$product->id|intval}', false, 1); return false;">
            {l s="Add to Wishlist" mod='blockwishlist'}
        </a>
    </div>
    <div id="add_to_cart"  class="col-sm-6 {if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE}unvisible{/if}">
        <button class="btn purple btn-cart" type="submit" name="Submit">
            <span>{l s='Add to cart'}</span>
        </button>
    </div>
</div>
<div role="tabpanel" class="tab-pane fade" id="sendfriend">
    {include file="$tpl_dir./product-btn-extra.tpl"}
    {include file="$tpl_dir./modules/sendtoafriend/sendtoafriend-extra.tpl"}
</div>
<div role="tabpanel" class="tab-pane fade" id="share">
    {include file="$tpl_dir./product-btn-extra.tpl"}
    {Hook::exec('displaySocialSharing')}
</div>