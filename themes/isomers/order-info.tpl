<div class="order-summary">
    {include file="$tpl_dir./shopping-cart.tpl"}
    <div class="row cart_total_address">
        <div class="text-label col-xs-6">
            <h5 class="uppercase">{l s='Address' mod="order"}</h5>
        </div>
        <div class="value col-xs-6 address_value text-right">
            {$delivery->address1}<br />
            {$delivery->city}, {if $delivery_state}{$delivery_state}, {/if}{$delivery->country}<br />
            {$delivery->postcode}
        </div>
    </div>        
</div>