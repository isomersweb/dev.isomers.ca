var formSelector = '.ajax_form';
var closeMessage = 'Your message has been successfully sent to our team.';
$(document).ready(function() {
      
	$.jGrowl.defaults.closerTemplate = '<div><strong>[ '+closeMessage+' ]</strong></div>';

                 
});

$(document).on('submit', formSelector, function(e) {
	$(this).ajaxSubmit({
		dataType: 'json'
		,url: window.location.href
		,beforeSerialize: function(form, options) {
			form.find(':submit').each(function() {
				if (!form.find('input[type="hidden"][name="' + $(this).attr('name') + '"]').length) {
					$(form).append(
						$('<input type="hidden">').attr({
							name: $(this).attr('name'),
							value: $(this).attr('value')
						})
					);
				}
			})
		}
		,beforeSubmit: function(fields, form) {
			if (typeof(afValidated) != 'undefined' && afValidated == false) {
				return false;
			}
			form.find('.error').html('');
			form.find('.error').removeClass('error');
			form.find('input,textarea,select,button').attr('disabled', true);
			return true;
		}
        ,error: function(response, status, xhr, form) {
            return response;            
        }
		,success: function(response, status, xhr, form) {
			form.find('input,textarea,select,button')
				.attr('disabled', false)
                .removeClass('error')
                .attr('title', '')
                .tooltip('destroy');
			response.form=form;
			$(document).trigger('af_complete', response);
            if (!response.success) {
				$.jGrowl(response.message, {life:5000, sticky: false});
				if (response.data) {
					var key, value;
					for (key in response.data) {
						if (response.data.hasOwnProperty(key)) {
							value = response.data[key];
							form.find('.error_' + key).html(value).addClass('error');
							form.find('[name="' + key + '"]').addClass('error').attr('title', value).tooltip('fixTitle').tooltip('hide');
						}
					}
				}
			}
			else {
    	        response.message = closeMessage;
				if (response.alreadySent == 1) {
                     response.message = 'Your message has already been sent.';
                 } 
                 $.jGrowl(response.message, {life:5000, sticky: false});
				form.find('.error').removeClass('error');
				form[0].reset();
			}
		}
	});
	e.preventDefault();
	return false;
});

$(document).on('reset', formSelector, function(e) {
	$(this).find('.error').html('');
});











