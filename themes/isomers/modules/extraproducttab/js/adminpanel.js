$(function(){
	$('body').on('click','.deleteExtraTab',function(){

		var tabID = parseInt($(this).attr('id').replace('deleteExtraTab_',''));
		var answer = confirm(confirmDialog);
		if (answer == true)
		{
			var query = $.ajax({
			  type: 'POST',
			  async: true,
			  url: baseDir + 'modules/extraproducttab/ajax.php',
			  data: 'action=delete&tabID='+tabID,
			  dataType: 'html',
			  success: function(response) {
			  
				if(response == 'ok')
				{
					$('fieldset#'+tabID).replaceWith('<div class="extraTabNotification">'+successfulDelete+'</div>');
					$('.extraTabNotification').fadeOut(3000);
				}
			  },
			  error: function() {
				alert('Something went wrong');
				}
			});
		}
		
		});
		
	$("#addExtraTab").click(function(){
		//find the next id for insertion
		var maxID = 0;
		$('fieldset').each(function(){
			var examinedID = parseInt($(this).attr('id'));
			if (examinedID > maxID)
			{	
				maxID = examinedID;
			}
			
		});
		var newID=maxID+1;
		//alert(maxID);
		//find current lang id
		var currentLangID=id_language;
		$("[class^=lang_]").each(function(){
			if ($(this).css('display') != 'none')
				{	
					var className = $(this).attr('class');
					currentLangID = parseInt(className[className.length-1]);
					return false;
				}
			
			});
				

		var query = $.ajax({
			  type: 'POST',
			  async: true,
			  url: baseDir + 'modules/extraproducttab/ajax.php',
			  data: 'action=createNewTab&newID='+newID,
			  dataType: 'html',
			  success: function(response) {
				//ready. Append it to existing fieldset
				$('form#extraProductTabsForm').append(response);
				
				displayFlags(languages, currentLangID, allowEmployeeFormLang);
				hideProperFlags(currentLangID);
			  },
			  error: function() {
				alert('Something went wrong');
				}
			});
		
		
	
	});


});

function hideProperFlags(id_lang)
{
	$("[class^=lang_]").each(function(){
			if ($(this).attr('class')!= 'lang_'+id_lang)
				$(this).hide();
		});	
}

function submitForm()
{	
	//build a string will all Tab IDs separated with semicolon
	var tabIDs = '';
	$("input[id^='tabID_']").each(function(){
		if (tabIDs.length > 0)
			tabIDs += ';';
		tabIDs += $(this).val();
	});
	//set the value the hidden input in order to submit it
	$("input#tabIDs").val(tabIDs);
	$("input#submitExtraTabs").click();

}


$(document).ready(function(){
	displayFlags(languages, id_language, allowEmployeeFormLang);
	hideProperFlags(id_language);
	});