<div id="myaccount_block" class="block_content">
		{if $is_logged}
            <div>
                <a class="btn btn-signin" href="{$link->getPageLink('my-account')}" title="{l s='My Account' mod='blockmyaccount'}" rel="nofollow">{l s='My Account' mod='blockmyaccount'}</a>
                <a class="btn btn-continue btn-blockwishlist" href="{$link->getPageLink('index')}?logout" title="{l s='Sign out' mod='blockmyaccount'}" rel="nofollow">{l s='Sign out' mod='blockmyaccount'}</a>
            </div>

		{else}
			<div>
            <form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="login_form" class="">
				<h3>{l s='Sign In'}</h3>
				<div class="form_content clearfix">
					<div class="form-group">
						<input class="is_required validate account_input form-control" data-validate="isEmail" type="email" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}" placeholder="{l s='Email address'}" />
					</div>
					<div class="form-group">
						<input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="passwd" name="passwd" value="" placeholder="{l s='Password'}" />
					</div>
					<p class="submit">
						{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'html':'UTF-8'}" />{/if}
						<button type="submit" id="SubmitLogin" name="SubmitLogin" class="btn black btn-signin">
							<span>
								<i class="icon-lock left"></i>
								{l s='Sign in'}
							</span>
						</button>
					</p>
					<p class="lost_password form-group"><a href="{$link->getPageLink('password')|escape:'html':'UTF-8'}" title="{l s='Recover your forgotten password'}" rel="nofollow">{l s='Forgot your password?'}</a></p>
				</div>
			</form>    
            <form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="create-account_form" class="">
				<h3>{l s='CREATE ACCOUNT'}</h3>
				<div class="form_content clearfix">					
					<div class="alert alert-danger" id="create_account_error" style="display:none"></div>
					<div class="form-group">
						<input type="email" class="is_required validate account_input form-control" data-validate="isEmail" id="email_create" name="email_create" value="{if isset($smarty.post.email_create)}{$smarty.post.email_create|stripslashes}{/if}" placeholder="{l s='Email address'}" />
					</div>
					<div class="submit">
						{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'html':'UTF-8'}" />{/if}
						<button class="btn black btn-createaccount" type="submit" id="SubmitCreate" name="SubmitCreate">
							<span>
								<i class="icon-user left"></i>
								{l s='Register'}
							</span>
						</button>
						<input type="hidden" class="hidden" name="SubmitCreate" value="{l s='Create an account'}" />
					</div>
				</div>
			</form>       
			</div>
		{/if}

	<a href="#" class="btn-close">Close Block</a>
</div> <!-- .block_content -->