{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

{if isset($blocks_products) && $blocks_products}
    <div id="related_products_free" class="products_list_wrapper jcarousel-wrapper clearfix no-print">
        <h4 class="text-uppercase">{l s='Also consider' mod='relatedfree'}</h4>
       <div class="jcarousel">
    	   {include file="$tpl_dir./product-list.tpl" class="related_products tab-pane" id="related_products" products=$blocks_products}
        </div>
        <div class="jcarousel-nav-wrapper">
            <a href="#" class="jcarousel-control-prev">&nbsp;</a>
            <a href="#" class="jcarousel-control-next">&nbsp;</a>
        </div>
    </div>
{/if}
