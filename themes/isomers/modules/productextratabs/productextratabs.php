<?php

if (!defined('_PS_VERSION_'))
    exit;

class newProductExtraTabs extends Module
{
    /* @var boolean error */
    protected $_errors = false;

    public function __construct()
    {
        $this->name = 'productextratabs';
        $this->tab = 'Extra Content';
        $this->version = '1.0';
        $this->author = 'Krava';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Product Extra Tabs');
        $this->description = $this->l('');
    }

    public function install()
    {
        if (!parent::install() OR
            !$this->alterTable('add') OR
            !$this->registerHook('actionAdminControllerSetMedia') OR
            !$this->registerHook('actionProductUpdate') OR
            !$this->registerHook('displayAdminProductsExtra'))
            return false;
        return true;
    }

    public function uninstall()
    {
        /*if (!parent::uninstall() OR !$this->alterTable('remove'))
            return false;*/
        return true;
    }


    public function alterTable($method)
    {
        switch ($method) {
            case 'add':
                $sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'product_lang ADD `formulation` TEXT NOT NULL; ';
                $sql .= 'ALTER TABLE ' . _DB_PREFIX_ . 'product_lang ADD `directions` TEXT NOT NULL; ' ;
                $sql .= 'ALTER TABLE ' . _DB_PREFIX_ . 'product_lang ADD `beforeafter` TEXT NOT NULL; ' ;
                break;

            case 'remove':
                $sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'product_lang DROP COLUMN `formulation`; ';
                $sql .= 'ALTER TABLE ' . _DB_PREFIX_ . 'product_lang DROP COLUMN `directions`; ';
                $sql .= 'ALTER TABLE ' . _DB_PREFIX_ . 'product_lang DROP COLUMN `beforeafter`; ';
                break;
        }

        if(!Db::getInstance()->Execute($sql))
            return false;
        return true;
    }
    public function prepareNewTab()
    {

        $this->context->smarty->assign(array(
            'custom_field' => $this->getCustomField((int)Tools::getValue('id_product')),
            'languages' => $this->context->controller->_languages,
            'default_language' => (int)Configuration::get('PS_LANG_DEFAULT')
        ));

    }

    public function hookDisplayAdminProductsExtra($params)
    {
        if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product'))))
        {
            $this->prepareNewTab();
            return $this->display(__FILE__, 'productextratabs.tpl');
        }
    }

    public function hookActionAdminControllerSetMedia($params)
    {

        // add necessary javascript to products back office
        if($this->context->controller->controller_name == 'AdminProducts' && Tools::getValue('id_product'))
        {
            $this->context->controller->addJS($this->_path.'/js/productextratabs.js');
        }

    }

    public function hookActionProductUpdate($params)
    {
        // get all languages
        // for each of them, store the custom field!

        $id_product = (int)Tools::getValue('id_product');
        $languages = Language::getLanguages(true);
        foreach ($languages as $lang) {
            if(!Db::getInstance()->update('product_lang', array('formulation'=> pSQL(Tools::getValue('formulation_'.$lang['id_lang']))) ,'id_lang = ' . $lang['id_lang'] .' AND id_product = ' .$id_product ))
                $this->context->controller->_errors[] = Tools::displayError('Error: ').mysql_error();

            if(!Db::getInstance()->update('product_lang', array('directions'=> pSQL(Tools::getValue('directions_'.$lang['id_lang']))) ,'id_lang = ' . $lang['id_lang'] .' AND id_product = ' .$id_product ))
                $this->context->controller->_errors[] = Tools::displayError('Error: ').mysql_error();

            if(!Db::getInstance()->update('product_lang', array('beforeafter'=> pSQL(Tools::getValue('beforeafter_'.$lang['id_lang']))) ,'id_lang = ' . $lang['id_lang'] .' AND id_product = ' .$id_product ))
                $this->context->controller->_errors[] = Tools::displayError('Error: ').mysql_error();
        }

    }

    public function getCustomField($id_product)
    {
        $result = Db::getInstance()->ExecuteS('SELECT formulation, directions, beforeafter, id_lang FROM '._DB_PREFIX_.'product_lang WHERE id_product = ' . (int)$id_product);
        if(!$result)
            return array();

        foreach ($result as $field) {
            $fields[$field['id_lang']] = $field['formulation'];
            $fields[$field['id_lang']] = $field['directions'];
            $fields[$field['id_lang']] = $field['beforeafter'];
        }

        return $fields;
    }

}