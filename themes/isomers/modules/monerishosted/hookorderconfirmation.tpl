{*
*  2013 Zack Hussain
*
*  @author 		Zack Hussain <me@zackhussain.ca>
*  @copyright  	2013 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*}
{if $status == 'ok'}
	<p>{l s='Your order on' mod='monerishosted'} <span class="bold">{$shop_name}</span> {l s='is complete.' mod='monerishosted'}
		<br /><br /><span class="bold">{l s='Your order will be sent as soon as possible.' mod='monerishosted'}</span>
		<br /><br />{l s='For any questions or for further information, please contact our' mod='monerishosted'} <a href="{$link->getPageLink('contact', true)}">{l s='customer support' mod='monerishosted'}</a>.
	</p>
{else}
	<p class="warning">
		{l s='We noticed a problem with your order. If you think this is an error, you can contact our' mod='monerishosted'}
		<a href="{$link->getPageLink('contact', true)}">{l s='customer support' mod='monerishosted'}</a>.
	</p>
{/if}
