{*
*  2013 Zack Hussain
*
*  @author 		Zack Hussain <me@zackhussain.ca>
*  @copyright  	2013 Zack Hussain
*
*  DISCLAIMER
*
*  Do not redistribute without my permission. Feel free to modify the code as needed.
*  Modifying the code may break future PrestaShop updates.
*  Do not remove this comment containing author information and copyright.
*
*}
{$smarty.cookies.firstname}
<div id="moneris_cc_item" class="payment_option item row">
    <div class="col-xs-8 col-sm-10 carrier_name">
        <form name="monerishosted_form" id="monerishosted_form" action="{$action_url}" method="POST">
			<input type="radio" id="cc_payment" name="cc_payment" class="payment_method" checked="checked">
			<span class="lbl">{l s='Pay with Credit Card' mod='monerishosted'}</span>
		</form>
    </div>
    <div class="col-xs-4 col-sm-2 payment-logo">
        <img src="{$modules_dir}monerishosted/logo_cc.png" title="{l s='Secure Credit Card Payment by Moneris' mod='monerishosted'}">
    </div>
</div>

{*<div class="payment_module">
<h3 class="moneris_title" style="padding-bottom:0;"><img alt="" src="{$module_dir}img/lock.png" />{l s='Secure Credit Card Payment' mod='monerishosted'}</h3>
        {if !empty($smarty.get.monerror)}
        <p style="color: red;padding-bottom: 0;">
        		{l s='Error: ' mod='monerishosted'}{$smarty.get.message|htmlentities}
		</p>
        {/if}
	<br>
	<form name="monerishosted_form" id="monerishosted_form" action="{$action_url}" method="POST">
		<span>
			<div style="display:block">
                <input type="hidden" name="order_id" value="{$order_id}" />
                <input type="hidden" name="ps_store_id" value="{$storeid}" />
                <input type="hidden" name="hpp_key" value="{$hppkey}" />
				<input type="hidden" name="charge_total" value="{$charge_total}" />
				<input type="hidden" name="moneris_form" value="1" />
				<input type="submit" name="submit " value="{l s='Click to proceed to Secure Checkout Page' mod='monerishosted'}" class="moneris-submit-button button" />
				<br class="clear" />
			</div>
		</span>
	</form>
</div>*}
