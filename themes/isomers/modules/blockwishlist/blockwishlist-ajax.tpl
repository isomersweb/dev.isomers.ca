{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $products}
    <div class="products">
        <div id="ajax_wishlist_quantity" class="hidden">{$total_wishlist}</div>
		{foreach from=$products item=product name=i}
			<div class="{if $smarty.foreach.i.first}first_item{elseif $smarty.foreach.i.last}last_item{else}item{/if} product_item row">
				<div class="col-xs-1">
					<a class="ajax_cart_block_remove_link" href="javascript:;" rel="nofollow" title="{l s='remove this product from my wishlist' mod='blockwishlist'}" onclick="javascript:WishlistCart('wishlist_block_list', 'delete', '{$product.id_product}', {$product.id_product_attribute}, '0', '{if isset($token)}{$token}{/if}');">
						<i class="icon-remove-sign "></i>Delete
					</a>
                </div>
                <div class="col-xs-8">
					<a class="cart_block_product_name" href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category_rewrite)|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}">
						{$product.name|escape:'html':'UTF-8'}
					</a>
                </div>
                <div class="col-xs-3">
					<div class="price">{$currency->sign}{Product::getPriceStatic($product.id_product)|number_format:2}</div>
                    <div class="quantity">
                        <input type="text" class="form-control product_quantity" name="quantity" value="{$product.quantity|intval}" />
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                    <a class="btn btn-addcart ajax_add_to_cart_button add-to-cart-in-wl" href="{$link->getPageLink('cart', true, NULL, "qty={$product.quantity|intval}&amp;id_product={$product.id_product|intval}&amp;add")|escape:'html':'UTF-8'}" data-id-attribute="{$product.id_product_attribute}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{$product.quantity|intval}" title="{l s='Add to cart' mod='blockwishlist'}"><span>{l s='Add to cart' mod='blockwishlist'}</span></a>
                
			</div>						
		{/foreach}
	</div>
	{*<dl class="products">
		{foreach from=$products item=product name=i}
			<dt class="{if $smarty.foreach.i.first}first_item{elseif $smarty.foreach.i.last}last_item{else}item{/if}">
				<span class="quantity-formated">
					<span class="quantity">{$product.quantity|intval}</span>x
				</span>
				<a class="cart_block_product_name" href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category_rewrite)|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}">
					{$product.name|truncate:13:'...'|escape:'html':'UTF-8'}
				</a>
				<a class="ajax_cart_block_remove_link" href="javascript:;" onclick="javascript:WishlistCart('wishlist_block_list', 'delete', '{$product.id_product}', {$product.id_product_attribute}, '0');" rel="nofollow" title="{l s='remove this product from my wishlist' mod='blockwishlist'}">
					<i class="icon-remove-sign"></i>
				</a>
			</dt>
			{if isset($product.attributes_small)}
			<dd class="{if $smarty.foreach.i.first}first_item{elseif $smarty.foreach.i.last}last_item{else}item{/if}">
				<a href="{$link->getProductLink($product.id_product, $product.link_rewrite)|escape:'html':'UTF-8'}" title="{l s='Product detail' mod='blockwishlist'}">
					{$product.attributes_small|escape:'html':'UTF-8'}
				</a>
			</dd>
			{/if}
		{/foreach}
	</dl>*}
{else}
	<div class="products no-products">
		{if isset($error) && $error}
			<div>{l s='You must create a wishlist before adding products' mod='blockwishlist'}</div>
		{else}
			<div>{l s='No products' mod='blockwishlist'}</div>
		{/if}
	</div>
{/if}

<a class="btn btn-continue btn-blockwishlist" href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'html':'UTF-8'}" title="{l s='My wishlists' mod='blockwishlist'}">
    		<span>
    			{l s='My wishlists' mod='blockwishlist'}<i class="icon-chevron-right right"></i>
    		</span>
</a>
<a href="#" class="btn-close">Close Block</a>
