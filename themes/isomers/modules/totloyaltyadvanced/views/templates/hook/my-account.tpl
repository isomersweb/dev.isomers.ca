{*
* @version 1.0
* @author 202-ecommerce
* @copyright 2014-2015 202-ecommerce
* @license ?
*}
<!-- MODULE totLoyaltyAdvanced -->
	<li class="loyalty">
		<a href="{$link->getModuleLink('totloyaltyadvanced', 'default', ['process' => 'summary'])|escape:'html':'UTF-8'}" title="{l s='My totLoyaltyAdvanced points' mod='totloyaltyadvanced'}">
			{if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
				<img src="{$module_template_dir|escape:'html':'UTF-8'}views/img/loyalty.gif" alt="{l s='My totLoyaltyAdvanced points' mod='totloyaltyadvanced'}" class="icon" /> {l s='My totLoyaltyAdvanced points' mod='totloyaltyadvanced'}
			{else}
				<i><img src="{$module_template_dir|escape:'html':'UTF-8'}views/img/loyalty.gif" alt="{l s='My totLoyaltyAdvanced points' mod='totloyaltyadvanced'}" class="icon" /></i><span>{l s='My totLoyaltyAdvanced points' mod='totloyaltyadvanced'}</span>
			{/if}
		</a>
	</li>
<!-- END : MODULE totLoyaltyAdvanced -->
